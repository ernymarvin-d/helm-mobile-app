/**
 * @format
 */
import React from 'react';
import {AppRegistry} from 'react-native';
import App from './src/App';
import {name as appName} from './app.json';
import {YellowBox} from 'react-native';
import configureStore from './src/store';
import {Provider} from 'react-redux';

YellowBox.ignoreWarnings([
  'Remote debugger',
  'Warning: Each',
  'Warning: Failed',
  'Warning: ...',
  'Failed prop type',
  'FlatList:',
  'Each child in',
  'current pos is',
]);

const store = configureStore();
const MainApp = () => (
  <Provider store={store}>
    <App />
  </Provider>
);

AppRegistry.registerComponent(appName, () => MainApp);
