import {SET_ACTIVE_TUTORIAL, SET_TUTORIAL} from './Types';

export const setActiveTutorial = (tutorial) => {
  return {
    type: SET_ACTIVE_TUTORIAL,
    tutorial,
  };
};

export const setTutorials = (tutorials) => {
  return {
    type: SET_TUTORIAL,
    tutorials,
  };
};
