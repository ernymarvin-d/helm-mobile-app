import {UPDATE_PLAN_TYPE} from './Types';

export const setPlanType = (planType) => {
  return {
    type: UPDATE_PLAN_TYPE,
    planType,
  };
};
