import {
  USER_LOGOUT,
  USER_LOGIN,
  REGISTER_AUTH_LISTENER,
  UPDATE_CALCULATOR_OPTION,
  ADD_TO_MY_DEALS,
  REMOVE_FROM_MY_DEALS,
  UPDATE_DEALS_CALCULATOR_SETTING,
  UPDATE_USER_INFO,
  UPDATE_USER_PROFILE_IMAGE,
} from './Types';

export const loginUser = (user) => ({
  type: USER_LOGIN,
  user: user,
});

export const logoutUser = () => ({
  type: USER_LOGOUT,
  user: null,
});

export const registerAuthListener = (authListener) => ({
  type: REGISTER_AUTH_LISTENER,
  authListener: authListener,
});

export const updateCalculatorSettingsOption = ({
  settingsOptionKey,
  settings,
}) => {
  return {
    type: UPDATE_CALCULATOR_OPTION,
    settingsOptionKey: settingsOptionKey,
    settings: settings,
  };
};

export const addToMyDeals = (deal) => ({
  type: ADD_TO_MY_DEALS,
  deal,
});

export const removeFromMyDeals = (deal) => ({
  type: REMOVE_FROM_MY_DEALS,
  deal,
});

export const updateCalupdateDealsSettingsOption = ({
  settingsOptionKey,
  settings,
  myDealKey,
}) => ({
  type: UPDATE_DEALS_CALCULATOR_SETTING,
  settingsOptionKey,
  settings,
  myDealKey,
});

export const updateUserBasicInfo = ({data}) => ({
  type: UPDATE_USER_INFO,
  data,
});

export const updateUserPhotoURL = (photoURL) => {
  console.log({photoURL});
  return {
    type: UPDATE_USER_PROFILE_IMAGE,
    photoURL,
  };
};
