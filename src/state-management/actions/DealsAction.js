import {
  DEAL_OPTIONS_UPDATE,
  INDIVIDUAL_DEAL_OPTION_UPDATE,
  TOGGLE_SELECTED_DEALS_OPTION,
  DEAL_OPTIONS_RESET,
  RESET_SELECTED_DEALS_OPTION,
} from './Types';

export const updateDealOptions = (dealOptions) => ({
  type: DEAL_OPTIONS_UPDATE,
  dealOptions: dealOptions,
});

export const updateIndividualDealOptions = ({
  dealsOptionKey,
  dealOptionValue,
}) => ({
  type: INDIVIDUAL_DEAL_OPTION_UPDATE,
  dealsOptionKey,
  dealOptionValue,
});

export const toggleSelectedDealOptions = ({dealOption}) => ({
  type: TOGGLE_SELECTED_DEALS_OPTION,
  dealOption: dealOption,
});

export const resetDealsOptionState = () => ({
  type: DEAL_OPTIONS_RESET,
});

export const resetSelectedDealsOption = () => ({
  type: RESET_SELECTED_DEALS_OPTION,
});
