import {
  USER_LOGOUT,
  USER_LOGIN,
  REGISTER_AUTH_LISTENER,
  FLAG_NAVIGATED_TO_HOME,
  UPDATE_CALCULATOR_OPTION,
  ADD_TO_MY_DEALS,
  REMOVE_FROM_MY_DEALS,
  UPDATE_DEALS_CALCULATOR_SETTING,
  UPDATE_USER_PROFILE_IMAGE,
} from '../actions/Types';

const initialState = {
  user: null,
  authListener: null,
  flagAlreadyNavigatedToHome: false,
  myDeals: {},
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case USER_LOGOUT:
      return {
        ...state,
        user: action.user,
      };
    case USER_LOGIN:
      return {
        ...state,
        user: action.user,
      };
    case REGISTER_AUTH_LISTENER:
      return {
        ...state,
        authListener: action.authListener,
      };
    case UPDATE_USER_PROFILE_IMAGE:
      console.log('auth reducer', 'UPDATE_USER_PROFILE_IMAGE');
      return {
        ...state,
        user: {
          ...state.user,
          info: {
            ...state.user.info,
            photoURL: action.photoURL,
          },
        },
      };
    case UPDATE_CALCULATOR_OPTION:
      const wowow = {
        ...state,
        user: {
          ...state.user,
          calculatorGeneralSettings: {
            ...state.user.calculatorGeneralSettings,
            [action.settingsOptionKey]: action.settings,
          },
        },
      };
      console.log({wowow});
      return {
        ...state,
        user: {
          ...state.user,
          calculatorGeneralSettings: {
            ...state.user.calculatorGeneralSettings,
            [action.settingsOptionKey]: action.settings,
          },
        },
      };
    case ADD_TO_MY_DEALS:
      console.log('ADD_TO_MY_DEALS');
      return {
        ...state,
        myDeals: {
          ...state.myDeals,
          [action.deal.id]: action.deal,
        },
      };
    case UPDATE_DEALS_CALCULATOR_SETTING: {
      console.log('UPDATE_DEALS_CALCULATOR_SETTING');
      return {
        ...state,
        myDeals: {
          ...state.myDeals,
          [action.myDealKey]: {
            ...state.myDeals[action.myDealKey],
            dealsGeneralSettings: {
              ...state.myDeals[action.myDealKey].dealsGeneralSettings,
              [action.settingsOptionKey]: action.settings,
            },
          },
        },
      };
    }
    case REMOVE_FROM_MY_DEALS: {
      const myDeals = {...state.myDeals};
      delete myDeals[action.deal.id];
      console.log(
        'REMOVE_FROM_MY_DEALS',
        state.myDeals,
        action.deal.id,
        myDeals,
      );
      return {
        ...state,
        myDeals,
      };
    }
    default:
      return state;
  }
};

export default authReducer;
