import {UPDATE_PLAN_TYPE} from '../actions/Types';
import {PLAN_TYPE} from '../../assets/Constants';

const planReducer = (state = PLAN_TYPE.free, action) => {
  switch (action.type) {
    case UPDATE_PLAN_TYPE:
      return action.planType;
    default:
      return state;
  }
};

export default planReducer;
