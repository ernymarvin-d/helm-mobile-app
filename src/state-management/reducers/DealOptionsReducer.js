import {
  DEAL_OPTIONS_UPDATE,
  INDIVIDUAL_DEAL_OPTION_UPDATE,
  TOGGLE_SELECTED_DEALS_OPTION,
  DEAL_OPTIONS_RESET,
  UPDATE_DEALS_CALCULATOR_SETTING,
  RESET_SELECTED_DEALS_OPTION,
} from '../actions/Types';

const initialState = {
  dealOptions: null,
  selectedDealOptions: [],
};

const dealOptionsReducer = (state = initialState, action) => {
  switch (action.type) {
    case DEAL_OPTIONS_UPDATE:
      return {
        ...state,
        dealOptions: action.dealOptions,
      };
    case INDIVIDUAL_DEAL_OPTION_UPDATE:
      return {
        ...state,
        dealOptions: {
          ...state.dealOptions,
          [action.dealsOptionKey]: action.dealOptionValue,
        },
      };
    case TOGGLE_SELECTED_DEALS_OPTION: {
      return {
        ...state,
        selectedDealOptions: toggleSelectionOptions(
          state.selectedDealOptions,
          action.dealOption,
        ),
      };
    }
    case DEAL_OPTIONS_RESET: {
      console.log('deal option state has been reset.');
      return {
        dealOptions: null,
        selectedDealOptions: [],
      };
    }
    case RESET_SELECTED_DEALS_OPTION: {
      return {
        ...state,
        selectedDealOptions: [],
      };
    }
    default:
      return state;
  }
};

export default dealOptionsReducer;

const toggleSelectionOptions = (currentSelectedOption, newSelectedOption) => {
  const dealOptions = currentSelectedOption;
  const hasDuplicate = dealOptions.filter(
    (option) => option.title2 === newSelectedOption.title2,
  );
  if (hasDuplicate.length > 0) {
    const index = dealOptions.indexOf(hasDuplicate[0]);
    dealOptions.splice(index, 1);
  } else {
    dealOptions.push(newSelectedOption);
  }
  console.log('Selected My Deals Updated', dealOptions);
  return dealOptions;
};
