import {SET_ACTIVE_TUTORIAL, SET_TUTORIAL} from '../actions/Types';

const initialState = {
  activeTutorial: null,
  tutorials: [],
};

const tutorialReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_ACTIVE_TUTORIAL: {
      return {
        ...state,
        activeTutorial: action.tutorial,
      };
    }
    case SET_TUTORIAL: {
      return {
        ...state,
        tutorials: action.tutorials,
      };
    }
    default: {
      return state;
    }
  }
};

export default tutorialReducer;
