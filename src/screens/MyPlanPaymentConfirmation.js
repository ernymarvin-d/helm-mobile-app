import React, {useLayoutEffect} from 'react';
import {View, Text, Image, ScrollView} from 'react-native';
import {getCustomHeader} from '../utils/NavigationUtils';
import {globalStyles} from '../assets/styles/globalStyles';
import EStyleSheet from 'react-native-extended-stylesheet';
import successIcon from '../assets/images/success.png';
import {BLUE} from '../assets/colors';

const headerLeftButtonComponent = () => (
  <Text style={globalStyles.p2}>Cancel</Text>
);

const styles = EStyleSheet.create({
  root: {justifyContent: 'center'},
  container: {
    justifyContent: 'center',
    width: '100%',
    alignItems: 'center',
  },
  headline: {
    color: BLUE,
  },
  icon: {
    width: '80rem',
    height: '80rem',
    tintColor: BLUE,
  },
  messageWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: '30rem',
  },
  message: {
    marginVertical: '20rem',
    textAlign: 'center',
    lineHeight: '25rem',
  },
});

const MyPlanPaymentConfirmation = ({navigation}) => {
  useLayoutEffect(() => {
    navigation.setOptions(
      getCustomHeader({navigation, headerLeftButtonComponent}),
    );
  }, [navigation]);

  return (
    // <View style={[globalStyles.root]}>
    <View style={[globalStyles.root, styles.root]}>
      <View style={styles.container}>
        <Image source={successIcon} style={styles.icon} resizeMode="contain" />
        <View style={styles.messageWrapper}>
          <Text style={[globalStyles.h2, styles.headline]}>
            Payment Successful
          </Text>
          <Text style={[globalStyles.p1, styles.message]}>
            Your Pro plan is activated.Your renew date 12/12/2019.
          </Text>
        </View>
      </View>
    </View>
    // </View>
  );
};

export default MyPlanPaymentConfirmation;
