import React, {useLayoutEffect, useState} from 'react';
import {
  View,
  Text,
  PermissionsAndroid,
  Alert,
  Platform,
  Dimensions,
} from 'react-native';
import {getDefaultHeader} from '../utils/NavigationUtils';
import {globalStyles} from '../assets/styles/globalStyles';
import {ScrollView} from 'react-native-gesture-handler';
import EStyleSheet from 'react-native-extended-stylesheet';
import TextButton from '../components/atoms/TextButton';
import {MARGIN_DEFAULT, PADDING_DEFAULT} from '../assets/dimens';
import downloadIcon from '../assets/images/ic-download.png';
import {connect} from 'react-redux';
import PleaseWaitDialog from '../components/atoms/PleaseWaitDialog';
import {saveDealsCreatedToDb} from '../utils/DbUtils';
import {createPDF, openFile} from '../utils/TextUtil';
import {resetDealsOptionState} from './../state-management/actions/DealsAction';
import moment from 'moment';
import {generateLoi} from '../assets/LOITemplate';
import HTML from 'react-native-render-html';
import {PLAN_TYPE} from '../assets/Constants';

const styles = EStyleSheet.create({
  contentWrapper: {
    height: '100%',
    padding: PADDING_DEFAULT,
  },
  contentText: {
    marginBottom: MARGIN_DEFAULT,
  },
  headerText: {
    marginBottom: MARGIN_DEFAULT,
    fontFamily: 'Montserrat-Bold',
  },
  bottomButtonWrapper: {
    width: '100%',
    padding: PADDING_DEFAULT,
    flex: 0,
  },
  scrollView: {flex: 1},
});

const tagsStyles = {
  p: globalStyles.p1,
  h4: {textAlign: 'center'},
  h2: [globalStyles.h2, {textAlign: 'center'}],
};

const CreateDealsLoiTemplate = ({
  route,
  navigation,
  calculatorGeneralSettings,
  selectedDealOptions,
  user,
  resetDealsOptionState,
  dealOptions,
  planType,
}) => {
  const {sellerInformation, propertyInformation} = route.params;
  const [showPleaseWaitDialog, setShowPleaseWaitDialog] = useState(false);
  const [filePath, setFilePath] = useState(null);
  const [LoiTemplate, setLoiTemplate] = useState(
    generateLoi({
      sellerInformation,
      selectedDealOptions,
      propertyInformation,
      user,
      calculatorGeneralSettings,
    }),
  );
  const [savedToDb, setSavedToDb] = useState(false);

  useLayoutEffect(() => {
    navigation.setOptions(getDefaultHeader({navigation}));
  }, [navigation]);

  const handleGenerateProposalButtonClick = async () => {
    if (planType === PLAN_TYPE.free) {
      if (planType !== PLAN_TYPE.proPlus) {
        Alert.alert(
          'Upgrade',
          'Please upgrade your account to access this feature.',
          [
            {
              text: 'Not now',
            },
            {
              text: 'Upgrade',
              onPress: () => navigation.navigate('MyPlanStack'),
            },
          ],
        );
      }
      return;
    }

    setShowPleaseWaitDialog(true);
    await createDealsAndSaveToDb();
    setShowPleaseWaitDialog(false);
    navigation.reset({
      index: 0,
      routes: [{name: 'CreateDealsSuccess', params: {sellerInformation}}],
    });
    resetDealsOptionState();
  };

  const createDealsAndSaveToDb = async () => {
    const userId = user.userId;
    const deal = {
      date: moment().toDate(),
      //   selectedDealOptions,   <- this is used to create a proposal only, not to be saved in the database
      propertyInformation,
      sellerInformation,
      userId,
      dealOptions,
      loiTemplate: LoiTemplate,
      dealsGeneralSettings: calculatorGeneralSettings,
    };
    if (!savedToDb) {
      await saveDealsCreatedToDb({user, deal, planType});
      setSavedToDb(true);
    }
  };

  const askPermission = () => {
    async function requestExternalWritePermission() {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Generate PDF',
            message: 'Helm App needs access to Storage data in your SD Card ',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          //If WRITE_EXTERNAL_STORAGE Permission is granted
          //changing the state to show Create PDF option
          handleCreatePdf();
        } else {
          alert('WRITE_EXTERNAL_STORAGE permission denied');
        }
      } catch (err) {
        alert('Write permission err', err);
        console.warn(err);
      }
    }
    //Calling the External Write permission function
    if (Platform.OS === 'android') {
      requestExternalWritePermission();
    } else {
      handleCreatePdf();
    }
  };

  const handleCreatePdf = async () => {
    const filePathResult = await createPDF(LoiTemplate);
    await createDealsAndSaveToDb();
    alertUserWithThePdf(filePathResult);
  };

  const alertUserWithThePdf = (filePath) => {
    setFilePath(filePath);
    if (filePath) {
      Alert.alert(
        'Open File',
        `The  pdf file has been saved at: \n\n${filePath}.\n\n Would you like to view the file?`,
        [
          {
            text: 'Not Now',
            onPress: () => {},
          },
          {text: 'Okay', onPress: () => openFile(filePath)},
        ],
      );
    } else {
      Alert.alert(
        'PDF Generator',
        `Can not create your PDF at this time, please try again later.`,
      );
    }
  };

  const handleOnPressDownloadPDF = () => {
    if (filePath) {
      alertUserWithThePdf(filePath);
    } else {
      askPermission();
    }
  };

  return (
    <View style={globalStyles.rootNoPadding}>
      <ScrollView style={styles.scrollView}>
        <View style={styles.contentWrapper}>
          <Text style={[globalStyles.h3, styles.headerText]}>LOI Template</Text>
          <HTML
            html={LoiTemplate}
            imagesMaxWidth={Dimensions.get('window').width}
            tagsStyles={tagsStyles}
          />
        </View>
      </ScrollView>
      <View style={styles.bottomButtonWrapper}>
        <TextButton
          text="SEND TO SELLER"
          type="primary"
          onPress={handleGenerateProposalButtonClick}
        />
        <TextButton
          text="Download PDF (287kb)"
          type="link"
          leftIcon={downloadIcon}
          onPress={handleOnPressDownloadPDF}
        />
      </View>
      <PleaseWaitDialog showDialog={showPleaseWaitDialog} />
    </View>
  );
};

const mapStateToProps = (state) => {
  return {
    user: state.auth.user,
    calculatorGeneralSettings: state.auth.user.calculatorGeneralSettings,
    selectedDealOptions: state.dealsCalculator.selectedDealOptions,
    dealOptions: state.dealsCalculator.dealOptions,
    planType: state.planType,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    resetDealsOptionState: () => dispatch(resetDealsOptionState()),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CreateDealsLoiTemplate);
