import React, {useLayoutEffect, useState, useCallback} from 'react';
import {View, Text, ScrollView, Alert} from 'react-native';
import {getWhiteHeader} from '../utils/NavigationUtils';
import {globalStyles} from '../assets/styles/globalStyles';
import EStyleSheet from 'react-native-extended-stylesheet';
import RbOptions from '../components/molecules/RbOptions';
import LabeledInputText from '../components/atoms/LabeledInputText';
import TextButton from '../components/atoms/TextButton';
import PleaseWaitDialog from '../components/atoms/PleaseWaitDialog';
import {MARGIN_LARGE, MARGIN_DEFAULT} from '../assets/dimens';
import {addNewFeedback} from '../utils/DbUtils';
import auth from '@react-native-firebase/auth';
import {connect} from 'react-redux';
import moment from 'moment';
import _ from 'lodash';

const questionOption = [
  {label: 'Very Satisfied', vlaue: 0},
  {label: 'Neither satisfied or dessatisfied', vlaue: 1},
  {label: 'Dissatisfied', vlaue: 2},
  {label: 'Very Dissatisfied', vlaue: 3},
];
const sendFeedbackSuccessMessage =
  'Feedback recieved. \n\n Thank you for helping us improve our service.';
const sendFeedbackFailMessage =
  'Submitting your feedback failed. Please try again later.';

const styles = EStyleSheet.create({
  header: {
    fontWeight: 'bold',
    marginBottom: MARGIN_LARGE,
  },
  rbWrapper: {
    marginVertical: MARGIN_DEFAULT,
  },
  note: {
    marginVertical: MARGIN_DEFAULT,
  },
});

const ProfileGiveFeedback = ({navigation, user}) => {
  useLayoutEffect(() => {
    navigation.setOptions(getWhiteHeader({navigation}));
  }, [navigation]);

  const [overallService, setOverallService] = useState(questionOption[0].label);
  const [content, setContent] = useState('');
  const [showPleaseWaitDialog, setShowPleaseWaitDialog] = useState(false);
  const [resetRbOption, setResetRbOption] = useState(false);

  const handleOnPressGiveFeedback = () => {
    if (_.isEmpty(content)) {
      Alert.alert(
        'Oops!!',
        'We would like to hear from you. \n\nPlease enter your feedback and let us know how to serve you better.',
      );
      return;
    }
    const feedback = {
      userInfo: user.info,
      userId: user.userId,
      date: moment().toDate(),
      overallService,
      content,
      businessInfo: user.businessInfo,
    };
    setShowPleaseWaitDialog(true);
    addNewFeedback(feedback).then((success) => {
      setShowPleaseWaitDialog(false);
      resetForm();
      setTimeout(() => showAlertMessage(success), 500);
    });
  };
  const handleOnSelectRbOption = (itemSelected) =>
    setOverallService(itemSelected.label);
  const resetForm = () => {
    setOverallService(questionOption[0].label);
    setContent('');
    setResetRbOption(!resetRbOption);
  };
  const showAlertMessage = (success) => {
    const msg = success ? sendFeedbackSuccessMessage : sendFeedbackFailMessage;
    Alert.alert(
      'Feedback',
      msg,
      [
        {
          text: 'OK',
        },
      ],
      {cancelable: false},
    );
  };

  return (
    <ScrollView>
      <View style={globalStyles.root}>
        <Text style={[globalStyles.h3, styles.header]}>Give Feedback</Text>
        <View style={styles.questionWrapper}>
          <Text style={[globalStyles.inputTextLabel, styles.question]}>
            {`Overall, how did you feel about \nthe service?`}
          </Text>
          <View style={styles.rbWrapper}>
            <RbOptions
              options={questionOption}
              onSelect={handleOnSelectRbOption}
              reset={resetRbOption}
            />
          </View>
        </View>
        <View style={styles.questionWrapper}>
          <LabeledInputText
            label={`How could we improve our \nservice?`}
            value={content}
            inputLabelStyle={{}}
            textInputProps={{
              multiline: true,
              maxLength: 1200,
              onChangeText: (value) => setContent(value),
              textAlignVertical: 'top',
            }}
            textInputHeight={100}
            textInputStyle={globalStyles.addressTextInput}
            footerText="Limit is 1200 characters"
          />
        </View>
        <Text style={[globalStyles.p1, styles.note]}>
          {`Please dont include any financial \ninformation. Example, your \ncredit card.`}
        </Text>
        <TextButton
          text="GIVE FEEDBACK"
          type="primary"
          onPress={handleOnPressGiveFeedback}
        />
        <PleaseWaitDialog showDialog={showPleaseWaitDialog} />
      </View>
    </ScrollView>
  );
};

const mapStateToProps = (state) => {
  return {
    user: state.auth.user,
  };
};

export default connect(mapStateToProps)(ProfileGiveFeedback);
