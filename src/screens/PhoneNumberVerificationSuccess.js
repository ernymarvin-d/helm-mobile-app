import React, {useState} from 'react';
import {Image, Text, View} from 'react-native';
import successIcon from './../assets/images/success.png';
import {globalStyles} from '../assets/styles/globalStyles';
import TextButton from '../components/atoms/TextButton';
import EStyleSheet from 'react-native-extended-stylesheet';
import {BLUE} from '../assets/colors';

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    width: '100%',
    alignItems: 'center',
  },
  phoneNumber: {
    fontFamily: 'Montserrat-Bold',
    marginBottom: '5rem',
  },
  icon: {
    width: '25%',
    height: '13%',
    tintColor: BLUE,
  },
  messageWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: '50rem',
  },
});

const PhoneNumberVerificationSuccess = ({route, navigation}) => {
  const {phoneNumber, selectedCountry} = route.params;
  const handleStayLoginButtonPress = () => {
    navigation.reset({
      index: 0,
      routes: [{name: 'HomeTab'}],
    });
  };
  return (
    <View style={[globalStyles.root, globalStyles.backgroundGrey]}>
      <View style={styles.container}>
        <Image source={successIcon} style={styles.icon} resizeMode="contain" />
        <View style={styles.messageWrapper}>
          <Text style={[globalStyles.h3, styles.phoneNumber]}>
            {`+${selectedCountry.callingCode[0]}${phoneNumber}`}
          </Text>
          <Text style={[globalStyles.p1, styles.message]}>
            Phone Number Verification Successful
          </Text>
        </View>
        <TextButton
          width="100%"
          text="STAY LOG IN"
          type="secondary"
          onPress={handleStayLoginButtonPress}
        />
      </View>
    </View>
  );
};

export default PhoneNumberVerificationSuccess;
