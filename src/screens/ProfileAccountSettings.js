import React, {useLayoutEffect, useState} from 'react';
import {View, Text, ScrollView, Alert} from 'react-native';
import {getDefaultHeader} from '../utils/NavigationUtils';
import {globalStyles} from '../assets/styles/globalStyles';
import EStyleSheet from 'react-native-extended-stylesheet';
import ProfileBasicInformationDisplay from '../components/atoms/ProfileBasicInformationDisplay';
import {MARGIN_DEFAULT} from '../assets/dimens';
import {connect} from 'react-redux';
import _ from 'lodash';
import {updateUserInfo} from '../utils/DbUtils';
import {useEffect} from 'react';
import auth from '@react-native-firebase/auth';
import InputDialog from '../components/atoms/InputDialog';
import {updateEmail, changePassword} from '../utils/AuthUtils';
import {useCallback} from 'react';
import PleaseWaitDialog from '../components/atoms/PleaseWaitDialog';

const styles = EStyleSheet.create({
  header: {
    marginTop: MARGIN_DEFAULT,
    fontWeight: 'bold',
  },
  infoWrapper: {},
});

const ALERT_TITLE = 'Helm App';

const ProfileAccountSettings = ({navigation, user}) => {
  console.log(auth().currentUser._user.providerId);
  const {businessInfo, info} = user;
  const [isEmailEditable, setIsEmailEditable] = useState(true);
  const [showPleaseWaitDialog, setShowPleaseWaitDialog] = useState(false);

  useEffect(() => {
    const providerData = auth().currentUser._user.providerData;
    console.log(providerData.length);
    console.log('PROVIDER DATA', providerData);

    const checkIfEditable = (providers) => {
      let editable = true;
      if (providers) {
        _.forEach(providers, (provider) => {
          const {providerId} = provider;
          console.log(provider);
          if (
            _.isEqual(providerId, 'google.com') ||
            _.isEqual(providerId, 'apple.com') ||
            _.isEqual(providerId, 'facebook.com')
          ) {
            editable = false;
            return false;
          }
        });
      }
      console.log('return editable', editable);
      return editable;
    };

    if (!checkIfEditable(providerData)) {
      // if only google or faceboo
      console.log('emailEditable false');
      setIsEmailEditable(false);
    }
  }, []);
  useLayoutEffect(() => {
    navigation.setOptions(getDefaultHeader({navigation}));
  }, [navigation]);

  const saveData = (key, value) => {
    if (_.isEmpty(value)) {
      setTimeout(() => Alert.alert(ALERT_TITLE, 'Invalid input'), 500);
    } else {
      sumbitUpdate({key, value});
    }
  };

  const sumbitUpdate = (data) => {
    const payload = {
      uid: user.userId,
      data,
    };
    updateUserInfo(payload).then((success) => {
      if (!success) {
        setTimeout(
          () =>
            Alert.alert(
              ALERT_TITLE,
              'Unable to change your information, please try again later.',
            ),
          500,
        );
      }
    });
  };

  const showAlertUnableToChangeEmail = () => {
    const providerId = user.providerData.providerId;
    const provider = _.toUpper(providerId.replace('.com', ''));
    Alert.alert(
      'Settings',
      `Can not change your email address. Signin using Google, Facebook or Apple accounts can not be changed.`,
    );
  };

  const handleOnSaveEmail = (value) => {
    setShowPleaseWaitDialog(true);
    updateEmail(value).then((result) =>
      handleUpdateEmailPasswordResult({
        result,
        value: 'emailAddress',
      }),
    );
  };
  const handleOnSavePassword = (value) => {
    setShowPleaseWaitDialog(true);
    changePassword(value).then((result) =>
      handleUpdateEmailPasswordResult({
        result,
        value: 'pw',
      }),
    );
  };

  const handleUpdateEmailPasswordResult = ({result, value}) => {
    setShowPleaseWaitDialog(false);
    var {error} = result;
    var el = value === 'pw' ? 'password' : 'email address';
    const msg = _.isUndefined(error)
      ? `Your ${el} has been updated.`
      : result.error.nativeErrorMessage;
    showAlertMessage(`\n${msg}`);
  };

  const showAlertMessage = (message) => Alert.alert('Update Settings', message);

  return (
    <View style={[globalStyles.root]}>
      <PleaseWaitDialog showDialog={showPleaseWaitDialog} />
      <ScrollView>
        <Text style={[globalStyles.h3, styles.header]}>Basic Infomation</Text>
        <View style={styles.infoWrapper}>
          <ProfileBasicInformationDisplay
            value={info.fullName}
            label="Full Name"
            onSave={(val) => saveData('info.fullName', val)}
          />
          <ProfileBasicInformationDisplay
            value={info.emailAddress}
            label="Email Address"
            readOnly={!isEmailEditable}
            onPress={!isEmailEditable ? showAlertUnableToChangeEmail : false}
            onSave={handleOnSaveEmail}
          />
          {isEmailEditable ? (
            <ProfileBasicInformationDisplay
              value="********"
              label="Password"
              onSave={handleOnSavePassword}
            />
          ) : null}
          <ProfileBasicInformationDisplay
            value={businessInfo.businessName}
            label="Business Name"
            onSave={(val) => saveData('businessInfo.businessName', val)}
          />
          <ProfileBasicInformationDisplay
            value={businessInfo.phoneNumber}
            label="Phone Number"
            onSave={(val) => saveData('businessInfo.phoneNumber', val)}
          />
          <ProfileBasicInformationDisplay
            value={businessInfo.address}
            label="Address"
            onSave={(val) => saveData('businessInfo.address', val)}
          />
          <ProfileBasicInformationDisplay
            value={businessInfo.websiteAddress}
            label="Website"
            onSave={(val) => saveData('businessInfo.websiteAddress', val)}
          />
        </View>
      </ScrollView>
    </View>
  );
};

const mapStateToProps = (state) => {
  return {
    user: state.auth.user,
  };
};

export default connect(mapStateToProps)(ProfileAccountSettings);
