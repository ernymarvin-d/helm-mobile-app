import React, {useState, useLayoutEffect, useEffect} from 'react';
import {View, Text, Button, Dimensions, Alert} from 'react-native';
import {TabView, SceneMap} from 'react-native-tab-view';
import TutorialInvesments from '../components/organisms/TutorialInvesments';
import TutorialWebinar from '../components/organisms/TutorialWebinar';
import {globalStyles} from '../assets/styles/globalStyles';
import {BLUE, ORANGE} from '../assets/colors';
import {getDefaultHeader} from '../utils/NavigationUtils';
import DefaultTabBar from '../components/atoms/DefaultTabBar';
import {setActiveTutorial} from '../state-management/actions/TutorialActions';
import {getAvailableTutorials} from '../utils/DbUtils';
import {setTutorials} from '../state-management/actions/TutorialActions';
import {connect} from 'react-redux';
import PleaseWaitDialog from '../components/atoms/PleaseWaitDialog';
import {PLAN_TYPE} from '../assets/Constants';

const initialLayout = {
  height: 0,
  width: Dimensions.get('window').width,
};

const Tutorials = ({navigation, jumpTo, setTutorials, planType}) => {
  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      console.log('tutorial tab press', planType);
      if (planType !== PLAN_TYPE.proPlus) {
        Alert.alert(
          'Upgrade',
          'Please subscribe PRO PLUS to access this feature.',
          [
            {
              text: 'Not now',
              onPress: () => navigation.goBack(),
            },
            {
              text: 'Upgrade',
              onPress: () => navigation.navigate('MyPlanStack'),
            },
          ],
        );
      }
    });
    return unsubscribe;
  }, [navigation, planType]);
  useEffect(() => {
    populateTutorials();
  }, []);

  useLayoutEffect(() => {
    navigation.setOptions(getDefaultHeader({onPressLeftButton}));
  }, [navigation]);

  const [showPleaseWaitDialog, setShowPleaseWaitDialog] = useState(false);

  const populateTutorials = async () => {
    setShowPleaseWaitDialog(true);
    const availableTutorials = await getAvailableTutorials();
    setShowPleaseWaitDialog(false);
    setTutorials(availableTutorials);
  };

  const renderTutorialInvesment = ({route, jumpTo}) => (
    <TutorialInvesments jumpTo={jumpTo} onRefresh={populateTutorials} />
  );
  const renderTutorialWebinar = ({route, jumpTo}) => (
    <TutorialWebinar jumpTo={jumpTo} />
  );

  const onPressLeftButton = () => navigation.navigate('HomeTab');

  const [activeIndex, setActiveIndex] = useState(0);
  const [routes] = useState([
    {key: 'tutorial', title: 'INVESMENT COURSE'},
    {key: 'webinar', title: 'WEBINAR'},
  ]);

  const renderScene = SceneMap({
    tutorial: renderTutorialInvesment,
    webinar: renderTutorialWebinar,
  });

  const renderTabBar = (props) => {
    const onTabPress = ({route, preventDefault}) => {
      console.log({route});
      if (route.key === routes[0].key) {
        onTabPressTutorial();
      } else if (route.key === routes[1].key) {
        onTabPressWebinar({preventDefault, route});
      }
    };
    const onTabPressTutorial = () => {
      console.log('tutorial tab pressed');
      setActiveTutorial(null);
    };
    const onTabPressWebinar = ({preventDefault, route}) => {
      console.log('webinar tab pressed');
      preventDefault();
      if (activeIndex === 0) {
        Alert.alert(
          'Courses Catalog',
          "Please select a course from the list to view the tutorial's webinar details.",
        );
      }
    };
    return <DefaultTabBar {...props} onTabPress={onTabPress} />;
  };

  const handleOnIndexChange = (i) => setActiveIndex(i);

  return (
    <View style={globalStyles.rootNoPadding}>
      <TabView
        activeColor={ORANGE}
        navigationState={{index: activeIndex, routes}}
        renderScene={renderScene}
        renderTabBar={renderTabBar}
        onIndexChange={handleOnIndexChange}
        initialLayout={initialLayout}
      />
      <PleaseWaitDialog showDialog={showPleaseWaitDialog} />
    </View>
  );
};

const mapStateToProps = (state) => {
  return {
    planType: state.planType,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    setTutorials: (tutorials) => dispatch(setTutorials(tutorials)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Tutorials);
