import React, {useLayoutEffect} from 'react';
import {View, Text, SafeAreaView} from 'react-native';
import HomePageToolbar from '../components/molecules/HomePageToolbar';
import {globalStyles} from './../assets/styles/globalStyles';
import SliderImage from './../components/atoms/SliderImage';
import EStyleSheet from 'react-native-extended-stylesheet';
import {PADDING_DEFAULT, MARGIN_DEFAULT} from '../assets/dimens';
import {ScrollView} from 'react-native-gesture-handler';
import HowItWorksItem from '../components/molecules/HowItWorksItem';
import creatDealsIcon from './../assets/images/create-deal.png';
import writeDealsIcon from './../assets/images/write-deals.png';
import sendEmailIcon from './../assets/images/send-email.png';
import {useIsFocused} from '@react-navigation/native';
import {connect} from 'react-redux';
import SubscriptionManager from '../components/atoms/SubscriptionManager';

const styles = EStyleSheet.create({
  root: {backgroundColor: '#ffffff'},
  contentWrapper: {
    width: '100%',
    height: '100%',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  bodyWrapper: {
    width: '100%',
    alignItems: 'flex-start',
    padding: PADDING_DEFAULT,
  },
  title: {
    textAlign: 'left',
    fontFamily: 'Montserrat-Medium',
    fontWeight: '700',
    marginVertical: MARGIN_DEFAULT,
    marginBottom: '40rem',
  },
  scroller: {
    width: '100%',
    flexGrow: 1,
  },
  workItemWrapper: {
    width: '100%',
  },
});

const Home = ({navigation, user}) => {
  const isFocused = useIsFocused();

  useLayoutEffect(() => {
    const parent = navigation.dangerouslyGetParent();
    parent.setOptions({
      tabBarVisible: isFocused,
    });
  }, [navigation, isFocused]);

  const handleCreatDealsButtonClick = () =>
    navigation.navigate('CreateDealsStack');

  const openUserProfileStack = () => navigation.navigate('UserProfileStack');

  return (
    <SafeAreaView style={styles.root}>
      <View style={styles.contentWrapper}>
        <HomePageToolbar
          onLeftMenuClick={openUserProfileStack}
          onPressAvatar={openUserProfileStack}
          onCreateDealsClicked={handleCreatDealsButtonClick}
          user={user}
        />
        <SliderImage />
        <ScrollView style={styles.scroller}>
          <View style={styles.bodyWrapper}>
            <Text style={[globalStyles.h3, styles.title]}>How It Works</Text>
            <View style={styles.workItemWrapper}>
              <HowItWorksItem
                icon={creatDealsIcon}
                title={'Create Deals'}
                showDivider
                content={
                  'Simply input the address and rehab estimate required, then click analyze deal. The app will evaluate the deal 8 different ways and recommend the top 3 offers to make'
                }
              />
              <HowItWorksItem
                icon={writeDealsIcon}
                title={'Write Offers'}
                showDivider
                content={
                  "After you confirm the 3 offers that you'd like to make, the app will create a 3 option letter of intent individualized with the sellers information and offers."
                }
              />
              <HowItWorksItem
                icon={sendEmailIcon}
                title={'Email Offers'}
                content={
                  'After the offer is generated you can send the seller the 3 option letter of intent for review and signature from the app.'
                }
              />
            </View>
          </View>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};

const mapStateToProps = (state) => {
  return {
    user: state.auth.user,
  };
};

export default connect(mapStateToProps)(Home);
