import React, {useLayoutEffect, useState} from 'react';
import {
  View,
  Text,
  Image,
  TouchableWithoutFeedback,
  ScrollView,
} from 'react-native';
import auth from '@react-native-firebase/auth';
import {getDefaultHeader} from '../utils/NavigationUtils';
import {globalStyles} from '../assets/styles/globalStyles';
import Avatar, {Sizes, IconTypes} from 'rn-avatar';
import elon from '../assets/images/elon.jpg';
import dollarIcon from '../assets/images/ic-dollar.png';
import redoIcon from '../assets/images/ic-redo.png';
import rocketIcon from '../assets/images/ic-rocket.png';
import starIcon from '../assets/images/ic-star.png';
import profileIcon from '../assets/images/ic-profile.png';
import EStyleSheet from 'react-native-extended-stylesheet';
import {
  BLUE,
  RED,
  GREEN,
  BLUE_FADED,
  BLUE_SECONDARY,
  ORANGE,
  BOX_BORDER_COLOR,
  INPUT_TEXT_BORDER_COLOR,
  GREY,
  BOX_BORDER_COLOR_2,
} from '../assets/colors';
import {
  PADDING_LARGE,
  PADDING_DEFAULT,
  PADDING_SMALL,
  MARGIN_LARGE,
  MARGIN_SMALL,
} from '../assets/dimens';
import {connect} from 'react-redux';
import ImagePicker from 'react-native-image-picker';
import storage from '@react-native-firebase/storage';
import {updateUserPhotoURL} from '../state-management/actions/AuthActions';
import _ from 'lodash';
import {logoutUser} from '../state-management/actions/AuthActions';
import {resetDealsOptionState} from '../state-management/actions/DealsAction';

const options = {
  title: 'Select Avatar',
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

const styles = EStyleSheet.create({
  imageWrapper: {
    backgroundColor: BLUE,
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: PADDING_LARGE,
  },
  profileMenuWrapper: {
    marginTop: '30rem',
    width: '90%',
    alignSelf: 'center',
  },
  avatarContainerStyle: {
    backgroundColor: GREEN,
    borderWidth: 2,
    borderColor: BLUE_FADED,
  },
  iconStyle: {
    width: '26rem',
    height: '25rem',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    fontSize: '26rem',
    alignSelf: 'center',
    marginHorizontal: 'auto',
  },
  menuIcon: {
    tintColor: ORANGE,
    width: '30rem',
    height: '30rem',
    marginRight: '16rem',
  },
  menuContent: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: '100%',
  },
  menuWrapper: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: '100%',
  },
  menuName: {
    fontWeight: '600',
    width: '100%',
    padding: 0,
    margin: 0,
  },
  menuTextWrapper: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    borderColor: GREY[4],
    borderBottomWidth: 1,
    paddingVertical: '20rem',
    width: '85%',
  },
  profileNameWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: MARGIN_LARGE,
  },
  profileName: {
    color: 'white',
  },
  txtCompany: {
    color: BLUE_SECONDARY,
    marginVertical: '4rem',
  },
  borderNone: {
    borderBottomWidth: 0,
  },
  scrollView: {height: '50%'},
});

const ProfileSettingsMenuItem = ({noBorder, name, icon, onPress}) => {
  return (
    <View style={[styles.menuWrapper]}>
      <TouchableWithoutFeedback onPress={onPress}>
        <View style={styles.menuContent}>
          <Image
            source={icon}
            style={styles.menuIcon}
            resizeMethod="resize"
            resizeMode="contain"
          />
          <View
            style={[styles.menuTextWrapper, noBorder ? styles.borderNone : {}]}>
            <Text style={[globalStyles.p1, styles.menuName]}>{name}</Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
    </View>
  );
};

const avatarEditButton = {
  name: 'camera-outline',
  type: IconTypes.MaterialCommunity,
  color: BLUE_SECONDARY,
  underlayColor: BLUE_SECONDARY,
  style: {
    backgroundColor: BLUE,
    borderColor: BLUE_FADED,
    borderWidth: 2,
    right: 5,
  },
  size: 50,
  iconStyle: styles.iconStyle,
};

const ProfileMyProfile = ({
  navigation,
  user,
  updateUserPhotoURL,
  logoutUser,
  resetDealsOptionState,
}) => {
  useState(() => {
    const unsubscribe = auth().onAuthStateChanged(async (user) => {
      console.log('on Splash', {user});
      if (!user) {
        console.log('splash - unauth');
        navigateUnAuthUser();
        resetDealsOptionState();
        logoutUser();
      }
    });

    return () => unsubscribe();
  }, []);

  const navigateUnAuthUser = async () => {
    console.log('Profile Screen', 'User is logged out');
    navigation.reset({
      index: 0,
      routes: [{name: 'AuthStack'}],
    });
  };

  console.log('User', {user});
  const {info, businessInfo} = _.isNull(user)
    ? {info: {}, businessInfo: {}}
    : user;
  console.log({info, businessInfo});
  const [avatarSource, setAvatarSource] = useState(
    info.photoURL ? info.photoURL : '',
  );

  useLayoutEffect(() => {
    navigation.setOptions(getDefaultHeader({navigation}));
  }, [navigation]);

  const handleAccountSettingMenuClick = () =>
    navigation.navigate('AccountSettings');

  const handleMyPlanMenuClick = () => navigation.navigate('MyPlanStack');
  const handleAddPaymentMethodMenuClick = () =>
    navigation.navigate('AddPaymentMethod');
  const handleGiveFeedbackMethodMenuClick = () =>
    navigation.navigate('GiveFeedback');
  const handleOnEditPressAvatar = () => {
    console.log('open camera or photo select');
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = {uri: response.uri};
        console.log({source, response});
        updateUserPhotoURL(source.uri);
        setAvatarSource(source.uri);
        const task = storage()
          .ref(`profile-images/${info.userId}.png`)
          .putFile(source.uri);
        task.on('state_changed', (taskSnapshot) => {
          console.log(
            `${taskSnapshot.bytesTransferred} transferred out of ${response.fileSize}`,
          );
        });
        task.then((aaaaa) => {
          console.log('Image uploaded to the bucket!', {aaaaa});
        });
      }
    });
  };

  return (
    <View style={[globalStyles.rootNoPadding]}>
      <View style={styles.imageWrapper}>
        <Avatar
          editButton={avatarEditButton}
          containerStyle={styles.avatarContainerStyle}
          source={{uri: avatarSource}}
          size={170}
          rounded
          showEditButton
          title={info.fullName}
          onEditPress={handleOnEditPressAvatar}
        />
        <View style={styles.profileNameWrapper}>
          <Text style={[globalStyles.h2, styles.profileName]}>
            {info.fullName}
          </Text>
          <Text style={[globalStyles.p1, styles.txtCompany]}>
            {businessInfo.businessName}
          </Text>
        </View>
      </View>
      <ScrollView style={styles.scrollView}>
        <View style={styles.profileMenuWrapper}>
          <ProfileSettingsMenuItem
            icon={profileIcon}
            name="Account Settings"
            onPress={handleAccountSettingMenuClick}
          />
          <ProfileSettingsMenuItem
            icon={rocketIcon}
            name="My Plan"
            onPress={handleMyPlanMenuClick}
          />
          {/* <ProfileSettingsMenuItem
            icon={dollarIcon}
            name="Add Payment Method"
            onPress={handleAddPaymentMethodMenuClick}
          /> */}
          <ProfileSettingsMenuItem
            icon={starIcon}
            name="Give Feedback"
            onPress={handleGiveFeedbackMethodMenuClick}
          />
          <ProfileSettingsMenuItem
            noBorder
            icon={redoIcon}
            name="Logout"
            onPress={() => auth().signOut()}
          />
        </View>
      </ScrollView>
    </View>
  );
};

const mapStateToProps = (state) => {
  return {
    user: state.auth.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateUserPhotoURL: (photoURL) => dispatch(updateUserPhotoURL(photoURL)),
    logoutUser: () => dispatch(logoutUser(null)),
    resetDealsOptionState: () => dispatch(resetDealsOptionState()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileMyProfile);
