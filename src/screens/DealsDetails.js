import React, {useLayoutEffect} from 'react';
import {View, Text, Alert} from 'react-native';
import {getDefaultHeader} from '../utils/NavigationUtils';
import DealsOption1 from '../components/organisms/DealsOption1';
import DealsOption2 from '../components/organisms/DealsOption2';
import DealsOption3 from '../components/organisms/DealsOption3';
import DealsOption4 from '../components/organisms/DealsOption4';
import DealsOption5 from '../components/organisms/DealsOption5';
import DealsOption6 from '../components/organisms/DealsOption6';
import DealsOption7 from '../components/organisms/DealsOption7';
import DealsOption8 from '../components/organisms/DealsOption8';
import {globalStyles} from '../assets/styles/globalStyles';
import {ScrollView} from 'react-native-gesture-handler';
import EStyleSheet from 'react-native-extended-stylesheet';
import TextButton from '../components/atoms/TextButton';
import {MARGIN_DEFAULT} from '../assets/dimens';
import {connect} from 'react-redux';
import {resetSelectedDealsOption} from '../state-management/actions/DealsAction';
import {PLAN_TYPE} from '../assets/Constants';

const styles = EStyleSheet.create({
  buttonWrapper: {flexShrink: 1, margin: MARGIN_DEFAULT},
});

const DealsDetails = ({
  route,
  navigation,
  myDeals,
  selectedDealOptions,
  resetSelectedDealsOption,
  planType,
}) => {
  console.log('Route', route.params);
  const {id} = route.params.dealItem;
  const {
    dealsGeneralSettings,
    propertyInformation,
    sellerInformation,
  } = myDeals[id];
  useLayoutEffect(() => {
    navigation.setOptions(
      getDefaultHeader({
        navigation,
        onPressLeftButton: handleOnPressHeaderBackButton,
      }),
    );
    console.log({selectedDealOptions});
  }, [navigation]);

  const handleOnPressHeaderBackButton = () => {
    resetSelectedDealsOption();
    navigation.pop();
  };

  const handleGenerateProposal = () => {
    if (selectedDealOptions.length === 0) {
      Alert.alert(
        'Deals Calculator',
        'Please select a maximum of 3 deal options.',
      );
      return;
    }
    navigation.navigate('DealsSellerInformation', {
      sellerInformation,
      propertyInformation,
    });
  };

  return (
    <View style={globalStyles.rootNoPadding}>
      <View style={{flex: 1, flexGrow: 1}}>
        <ScrollView>
          <View style={globalStyles.root}>
            <DealsOption1
              dealKey={id}
              settings={dealsGeneralSettings.option1}
              propertyInformation={propertyInformation}
            />
            <View style={globalStyles.divider} />
            <DealsOption2
              dealKey={id}
              settings={dealsGeneralSettings.option2}
              propertyInformation={propertyInformation}
            />
            <View style={globalStyles.divider} />
            <DealsOption3
              dealKey={id}
              propertyInformation={propertyInformation}
              settings={dealsGeneralSettings.option3}
            />
            {planType !== PLAN_TYPE.free ? (
              <>
                <View style={globalStyles.divider} />
                <DealsOption4
                  dealKey={id}
                  propertyInformation={propertyInformation}
                  settings={dealsGeneralSettings.option4}
                />
                <View style={globalStyles.divider} />
                <DealsOption5
                  dealKey={id}
                  propertyInformation={propertyInformation}
                  settings={dealsGeneralSettings.option5}
                />
                <View style={globalStyles.divider} />
                <DealsOption6
                  dealKey={id}
                  propertyInformation={propertyInformation}
                  settings={dealsGeneralSettings.option6}
                />
                <View style={globalStyles.divider} />
                <DealsOption7
                  dealKey={id}
                  propertyInformation={propertyInformation}
                  settings={dealsGeneralSettings.option7}
                />
                <View style={globalStyles.divider} />
                <DealsOption8
                  dealKey={id}
                  settings={dealsGeneralSettings.option8}
                  propertyInformation={propertyInformation}
                />
              </>
            ) : (
              <></>
            )}
          </View>
        </ScrollView>
      </View>
      <View style={styles.buttonWrapper}>
        <TextButton
          text="GENERATE PROPOSAL"
          type="primary"
          onPress={handleGenerateProposal}
        />
      </View>
    </View>
  );
};

const mapStateToProps = (state) => {
  return {
    myDeals: state.auth.myDeals,
    selectedDealOptions: state.dealsCalculator.selectedDealOptions,
    planType: state.planType,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    resetSelectedDealsOption: () => dispatch(resetSelectedDealsOption()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DealsDetails);
