import React, {useLayoutEffect} from 'react';
import {View, ScrollView} from 'react-native';
import {globalStyles} from '../assets/styles/globalStyles';
import LetterOfIntentTemplate from '../components/organisms/LetterOfIntentTemplate';
import {getDefaultHeader} from '../utils/NavigationUtils';

const DealsLoi = ({route, navigation}) => {
  useLayoutEffect(() => {
    navigation.setOptions(getDefaultHeader({navigation}));
  }, [navigation]);

  const {sellerInformation} = route.params;
  const handleOnPressBtnSendToSeller = () => {
    // navigation.navigate('CreateDealsSuccess', {sellerInformation});
    navigation.reset({
      index: 0,
      routes: [{name: 'CreateDealsSuccess', params: {sellerInformation}}],
    });
  };
  return (
    <View style={globalStyles.rootNoPadding}>
      <ScrollView>
        <LetterOfIntentTemplate
          sendToSellerBtnClick={handleOnPressBtnSendToSeller}
        />
      </ScrollView>
    </View>
  );
};

export default DealsLoi;
