import React, {useState} from 'react';
import {Image, Text, View} from 'react-native';
import successIcon from './../assets/images/success.png';
import {globalStyles} from '../assets/styles/globalStyles';
import TextButton from '../components/atoms/TextButton';
import EStyleSheet from 'react-native-extended-stylesheet';
import {BLUE} from '../assets/colors';

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    width: '100%',
    alignItems: 'center',
  },
  header: {
    textAlign: 'center',
    marginBottom: '40rem',
  },
  icon: {
    width: '25%',
    height: '13%',
    tintColor: BLUE,
  },
  messageWrapper: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: '50rem',
  },
  message: {
    textAlign: 'center',
    width: '85%',
    lineHeight: '24rem',
  },
});

const ResetPasswordSuccess = ({navigation}) => {
  const handleStayLoginButtonPress = () => navigation.navigate('HomeTab');
  return (
    <View style={[globalStyles.root, globalStyles.backgroundGrey]}>
      <View style={styles.container}>
        <Image source={successIcon} style={styles.icon} resizeMode="contain" />
        <View style={styles.messageWrapper}>
          <Text style={[globalStyles.h2, styles.header]}>
            {'Password Reset \n Successfully'}
          </Text>
          <Text style={[globalStyles.p1, styles.message]}>
            You have successfully reset your password. Please use the new
            password to login.
          </Text>
        </View>
        <TextButton
          width="100%"
          text="STAY LOG IN"
          type="secondary"
          onPress={handleStayLoginButtonPress}
        />
      </View>
    </View>
  );
};

export default ResetPasswordSuccess;
