import React, {useState, useLayoutEffect} from 'react';
import {View, Dimensions} from 'react-native';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import FaqQuestions from '../components/organisms/FaqQuestions';
import FaqVideos from '../components/organisms/FaqVideos';
import {globalStyles} from '../assets/styles/globalStyles';
import {BLUE, ORANGE} from '../assets/colors';
import {getDefaultHeader} from '../utils/NavigationUtils';
import DefaultTabBar from '../components/atoms/DefaultTabBar';
import {useEffect} from 'react';
import {getAllFaqs} from '../utils/DbUtils';
const initialLayout = {
  height: 0,
  width: Dimensions.get('window').width,
};

const renderTutorialInvesment = () => <FaqQuestions />;
const renderTutorialWebinar = () => <FaqVideos />;

const SettingsFaq = ({navigation}) => {
  useLayoutEffect(() => {
    navigation.setOptions(getDefaultHeader({navigation}));
  }, [navigation]);

  const [index, setIndex] = useState(0);

  const [routes] = useState([
    {key: 'first', title: 'QUESTIONS'},
    {key: 'second', title: 'VIDEOS'},
  ]);

  const renderScene = SceneMap({
    first: renderTutorialInvesment,
    second: renderTutorialWebinar,
  });

  const renderTabBar = (props) => <DefaultTabBar {...props} />;

  return (
    <View style={globalStyles.rootNoPadding}>
      <TabView
        style={{backgroundColor: BLUE}}
        activeColor={ORANGE}
        renderTabBar={renderTabBar}
        navigationState={{index, routes}}
        renderScene={renderScene}
        onIndexChange={(index) => setIndex(index)}
        initialLayout={initialLayout}
      />
    </View>
  );
};

export default SettingsFaq;
