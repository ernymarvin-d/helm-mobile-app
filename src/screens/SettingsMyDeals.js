import React, {useLayoutEffect} from 'react';
import {
  View,
  Text,
  FlatList,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from 'react-native';
import {getDefaultHeader} from '../utils/NavigationUtils';
import EStyleSheet from 'react-native-extended-stylesheet';
import {globalStyles} from '../assets/styles/globalStyles';
import {myDealsData} from '../utils/Mocks';
import {SafeAreaView} from 'react-native-safe-area-context';
import MyDealsListItem from '../components/molecules/MyDealsListItem';
import {PADDING_DEFAULT} from '../assets/dimens';

const styles = EStyleSheet.create({
  flatListWrapper: {
    padding: PADDING_DEFAULT,
  },
});

const SettingsMyDeals = ({route, navigation}) => {
  console.log(route.params);
  useLayoutEffect(() => {
    navigation.setOptions(getDefaultHeader({onPressLeftButton}));
  }, [navigation]);

  const onPressLeftButton = () => navigation.navigate('HomeTab');
  const handleItemClick = (item) => {
    navigation.push('DealsDetails', {dealItem: item.item});
  };

  const renderMyDeals = (item) => {
    return (
      <TouchableWithoutFeedback
        key={item.name}
        onPress={() => handleItemClick(item)}>
        <View>
          <MyDealsListItem item={item.item} />
        </View>
      </TouchableWithoutFeedback>
    );
  };

  return (
    <View style={globalStyles.rootNoPadding}>
      <FlatList
        data={myDealsData}
        renderItem={renderMyDeals}
        keyExtractor={(item) => item.name}
      />
    </View>
  );
};

export default SettingsMyDeals;
