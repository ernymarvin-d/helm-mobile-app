import React, {useLayoutEffect, useState} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {globalStyles} from './../assets/styles/globalStyles';
import LabeledInputText from './../components/atoms/LabeledInputText';
import TextButton from '../components/atoms/TextButton';
import EStyleSheet from 'react-native-extended-stylesheet';
import ProgressBar from '../components/atoms/ProgressBar';
import {getDefaultHeader} from '../utils/NavigationUtils';
import _ from 'lodash';
import {validateEmailAddress} from '../utils/TextUtil';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import Autocomplete from 'react-native-autocomplete-input';
import {useEffect} from 'react';

const styles = EStyleSheet.create({
  root: {
    flex: 1,
    flexGrow: 1,
    justifyContent: 'space-between',
    backgroundColor: 'white',
  },
  contentWrapper: {
    padding: '16rem',
    flexGrow: 1,
    backgroundColor: '#fff',
  },
  buttonWrapper: {
    marginVertical: '18rem',
    width: '100%',
    height: '100rem',
    justifyContent: 'space-between',
    zIndex: -10,
  },
  formWrapper: {
    marginTop: '20rem',
  },
});

const CreateDealsSellerInformation = ({navigation}) => {
  useLayoutEffect(() => {
    navigation.setOptions(getDefaultHeader({navigation}));
    const parent = navigation.dangerouslyGetParent();
    parent.setOptions({
      tabBarVisible: false,
    });
  }, [navigation]);

  const [sellerName, setSellerName] = useState('');
  const [emailAddress, setEmailAddress] = useState('');
  const [propertyAddress, setPropertyAddress] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [addressAutoComplete, setAddressAutoComplete] = useState([]);
  const [selectedAddressFromTheApi, setSelectedAddressFromTheApi] = useState(
    null,
  );

  const [isValidSellerName, setIsValidSellerName] = useState(true);
  const [isValidEmailAddress, setIsValidEmailAddress] = useState(true);
  const [isValidPropertyAddress, setIsValidPropertyAddress] = useState(true);
  const [isValidPhoneNumber, setIsValidPhoneNumber] = useState(true);

  const [stopQueryApi, setStopQueryApi] = useState(false);

  const autoCompleteAddress = () => {
    if (_.isEmpty(propertyAddress)) {
      setAddressAutoComplete([]);
      return;
    }
    const endPoint = `https://realtor.p.rapidapi.com/locations/auto-complete?input=${propertyAddress}`;
    const payload = {
      method: 'GET',
      headers: {
        'x-rapidapi-host': 'realtor.p.rapidapi.com',
        'x-rapidapi-key': 'b95d7ea138msh9a87884cca31a37p1f9ad4jsn54895dff4be2',
        useQueryString: true,
      },
    };
    fetch(endPoint, payload)
      .then((response) => response.json())
      .then((data) => {
        setAddressAutoComplete(
          data.autocomplete.filter(
            (address) => !_.isEmpty(address.full_address),
          ),
        );
        console.log({propertyAddress}, {addressAutoComplete});
      })
      .catch((error) => console.log(error));
  };

  useEffect(() => {
    if (stopQueryApi) return;
    const queryAddress = setTimeout(() => autoCompleteAddress(), 1000);
    return () => clearTimeout(queryAddress);
  }, [propertyAddress]);

  const _handleOnButtonNextPressed = () => {
    console.log([sellerName]);
    console.log(selectedAddressFromTheApi, propertyAddress);

    if (!isFormValueValid()) return;

    const sellerInformation = {
      sellerName,
      emailAddress,
      propertyAddress,
      phoneNumber,
      selectedAddressFromTheApi,
    };
    navigation.navigate('CreateDealsPropertiesInformation', {
      sellerInformation,
    });
  };

  const isFormValueValid = () => {
    let isValid = true;
    if (_.isEmpty(sellerName)) {
      setIsValidSellerName(false);
      isValid = false;
    } else {
      setIsValidSellerName(true);
    }

    if (!validateEmailAddress(emailAddress)) {
      setIsValidEmailAddress(false);
      isValid = false;
    } else {
      setIsValidEmailAddress(true);
    }

    if (_.isEmpty(propertyAddress)) {
      setIsValidPropertyAddress(false);
      isValid = false;
    } else {
      setIsValidPropertyAddress(true);
    }

    return isValid;
  };

  return (
    <SafeAreaView style={styles.root}>
      <KeyboardAwareScrollView>
        <ProgressBar progress="50%" />
        <View style={[styles.contentWrapper]}>
          <View style={styles.formWrapper}>
            <LabeledInputText
              label="Seller Name"
              type="name"
              placeholder="John Doe"
              textInputProps={{
                onChangeText: (value) => setSellerName(_.trim(value)),
                onFocus: () => setIsValidSellerName(true),
              }}
              isValid={isValidSellerName}
            />
            <LabeledInputText
              label="Email Address"
              type="emailAddress"
              placeholder="ex:inform@gmail.com"
              textInputProps={{
                onChangeText: (value) => setEmailAddress(_.trim(value)),
                keyboardType: 'email-address',
                onFocus: () => setIsValidEmailAddress(true),
              }}
              isValid={isValidEmailAddress}
            />
            <LabeledInputText
              label="Properties Address"
              textInputProps={{
                onChangeText: (value) => setPropertyAddress(_.trim(value)),
                onFocus: () => setIsValidPropertyAddress(true),
              }}
              isValid={isValidPropertyAddress}
            />
            {/* <View style={{marginVertical: EStyleSheet.value('10rem')}}>
              <Text style={[globalStyles.inputTextLabel]}>
                Properties Address
              </Text>
              <Autocomplete
                inputContainerStyle={[globalStyles.inputText]}
                data={addressAutoComplete}
                defaultValue={propertyAddress}
                onChangeText={(value) => {
                  console.log('on text change');
                  setPropertyAddress(value);
                }}
                onKeyPress={() => {
                  setStopQueryApi(false);
                  setSelectedAddressFromTheApi(null);
                }}
                onFocus={() => setIsValidPropertyAddress(true)}
                renderItem={({item, i}) => {
                  const DropDownText = ({text, apiAddress}) => (
                    <TouchableOpacity
                      onPress={() => {
                        setStopQueryApi(true);
                        setPropertyAddress(text);
                        setSelectedAddressFromTheApi(apiAddress);
                        setAddressAutoComplete([]);
                      }}>
                      <Text>{text}</Text>
                    </TouchableOpacity>
                  );
                  if (!_.isEmpty(item.full_address)) {
                    const view = (
                      <DropDownText
                        apiAddress={item}
                        text={item.full_address[0]}
                      />
                    );
                    return <View>{view}</View>;
                  }
                }}
              />
            </View> */}
            <LabeledInputText
              label="Phone Number"
              optional
              textInputProps={{
                onChangeText: (value) => setPhoneNumber(_.trim(value)),
                keyboardType: 'numeric',
              }}
              isValid={isValidPhoneNumber}
              containerStyle={{zIndex: -10}}
            />
          </View>
          <View style={styles.buttonWrapper}>
            <TextButton
              type="primary"
              text="Next"
              onPress={_handleOnButtonNextPressed}
            />
          </View>
        </View>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};

export default CreateDealsSellerInformation;
