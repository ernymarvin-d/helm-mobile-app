import React, {useState} from 'react';
import {View, Text, SafeAreaView} from 'react-native';
import {globalStyles} from './../assets/styles/globalStyles';
import LabeledInputText from './../components/atoms/LabeledInputText';
import TextButton from '../components/atoms/TextButton';
import EStyleSheet from 'react-native-extended-stylesheet';
import ProgressBar from '../components/atoms/ProgressBar';
import PasswordTextInput from './../components/atoms/PasswordTextInput';
import {validateEmailAddress, validatePassword} from './../utils/TextUtil';
import _ from 'lodash';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

const styles = EStyleSheet.create({
  root: {
    flex: 1,
    flexGrow: 1,
    justifyContent: 'space-between',
    backgroundColor: 'white',
  },
  contentWrapper: {
    padding: '16rem',
    flexGrow: 1,
    backgroundColor: '#fff',
  },
  headerWrapper: {
    justifyContent: 'center',
    flex: 1,
    alignItems: 'center',
    marginVertical: '40rem',
  },
  headerText: {},
  subHeaderText: {
    textAlign: 'center',
    paddingVertical: '8rem',
  },
  buttonWrapper: {
    marginVertical: '18rem',
    width: '100%',
    height: '100rem',
    justifyContent: 'space-between',
  },
  formWrapper: {},
});

const SignupAccountInfo = ({navigation}) => {
  const [fullName, setFullName] = useState('');
  const [emailAddress, setEmailAddress] = useState('');
  const [password, setPassword] = useState('');
  const [passwordConfirm, setPasswordConfirm] = useState('');

  const [isFullNameValid, setIsFullNameValid] = useState(true);
  const [isEmailAddressValid, setIsEmailAddressValid] = useState(true);
  const [isPasswordMatch, setIsPasswordMatch] = useState(true);
  const [isPasswordValid, setIsPasswordValid] = useState(true);

  const _handleOnButtonNextPressed = () => {
    if (!areFormsHasValidValues()) return;
    const userInfo = {
      fullName: _.trim(fullName),
      emailAddress: _.trim(emailAddress),
      password: _.trim(password),
    };
    navigation.navigate('SignupBusinessInfo', {userInfo});
  };

  const handleOnTextChange = ({text, setStateText}) => setStateText(text);
  const areFormsHasValidValues = () => {
    if (_.isEmpty(fullName)) {
      setIsFullNameValid(false);
      return false;
    } else {
      setIsFullNameValid(true);
    }

    const validEmail = validateEmailAddress(_.trim(emailAddress));
    if (!validEmail) {
      setIsEmailAddressValid(false);
      return false;
    } else {
      setIsEmailAddressValid(true);
    }

    const validPassword = validatePassword(password);
    console.log({validPassword});
    if (!validPassword) {
      setIsPasswordValid(false);
      return false;
    } else {
      setIsPasswordValid(true);
    }

    if (!_.isEqual(password, passwordConfirm)) {
      setIsPasswordMatch(false);
      return false;
    } else {
      setIsPasswordMatch(true);
    }

    return true;
  };

  return (
    <SafeAreaView style={styles.root}>
      <ProgressBar progress="50%" />
      <KeyboardAwareScrollView>
        <View style={[styles.contentWrapper]}>
          <View style={styles.headerWrapper}>
            <Text style={[globalStyles.h2]}>Account Info</Text>
            <Text style={[globalStyles.p2, styles.subHeaderText]}>
              Enter your personal information
            </Text>
          </View>
          <View style={styles.formWrapper}>
            <LabeledInputText
              label="Full Name"
              type="name"
              textInputProps={{
                onChangeText: (text) =>
                  handleOnTextChange({text, setStateText: setFullName}),
              }}
              isValid={isFullNameValid}
              value={fullName}
            />
            <LabeledInputText
              label="Email Address"
              type="emailAddress"
              textInputProps={{
                onChangeText: (text) => {
                  setIsEmailAddressValid(true);
                  handleOnTextChange({text, setStateText: setEmailAddress});
                },
              }}
              errorText="Email Adress is invalid."
              isValid={isEmailAddressValid}
              value={emailAddress}
            />
            <PasswordTextInput
              label={'Password'}
              textInputProps={{
                onChangeText: (text) => {
                  setIsPasswordValid(true);
                  handleOnTextChange({text, setStateText: setPassword});
                },
              }}
              errorText="Invalid password. Password should be at least 8 characters long."
              isValid={isPasswordValid}
              value={password}
            />
            <PasswordTextInput
              label={'Re-enter Password'}
              textInputProps={{
                onChangeText: (text) => {
                  setIsPasswordMatch(true);
                  handleOnTextChange({text, setStateText: setPasswordConfirm});
                },
              }}
              value={passwordConfirm}
              errorText="Password did not match."
              isValid={isPasswordMatch}
            />
          </View>
          <View style={styles.buttonWrapper}>
            <TextButton
              type="primary"
              text="Next"
              onPress={_handleOnButtonNextPressed}
            />
            {/* <TextButton
              type="link"
              textStyle={globalStyles.p1}
              text="Skip"
              onPress={() =>
                navigation.navigate('PhoneNumberVerificationStack')
              }
            /> */}
          </View>
        </View>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};

export default SignupAccountInfo;
