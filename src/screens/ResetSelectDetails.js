import React from 'react';
import {View, Text, TouchableOpacity, Alert} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {globalStyles} from '../assets/styles/globalStyles';
import EmailContactDetails from './../components/molecules/EmailContactDetails';
import EStyleSheet from 'react-native-extended-stylesheet';
import {PADDING_DEFAULT} from '../assets/dimens';
import TextButton from '../components/atoms/TextButton';
import LabeledInputText from '../components/atoms/LabeledInputText';
import {useState} from 'react';
import ResetInputPassword from './ResetInputPassword';
import {firebase} from '@react-native-firebase/auth';

const styles = EStyleSheet.create({
  contentWrapper: {
    width: '100%',
    height: '65%',
    padding: PADDING_DEFAULT,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  headerWrapper: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: '70rem',
  },
  header: {
    marginVertical: '10rem',
  },
  subHeader: {textAlign: 'center', lineHeight: 25, width: '75%'},
  boxWrapper: {
    width: '100%',
  },
  buttonWrapper: {
    marginTop: '10rem',
  },
});

const ResetSelectDetails = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [isValidEmailAddress, setIsValidEmailAddress] = useState(true);

  const _handleOnButtonUpdatePasswordPressed = () => {
    console.log('Send reset password to email', {email});
    firebase
      .auth()
      .sendPasswordResetEmail(email)
      .then((result) => {
        console.log('reset email send', {result});
        Alert.alert(
          'Reset Email',
          'Reset email has been sent to your email address.',
        );
        setEmail('');
      })
      .catch((error) => {
        console.log({error});
        Alert.alert('Reset Email', error.nativeErrorMessage);
      });
  };
  // const _handleOnBoxSelected = (event) =>
  //   navigation.navigate('ResetRecoveryPin');
  return (
    <SafeAreaView style={globalStyles.root}>
      <View style={styles.contentWrapper}>
        <View style={styles.headerWrapper}>
          <Text style={[globalStyles.h2, styles.header]}>Reset Password</Text>
          <Text style={[globalStyles.p2, styles.subHeader]}>
            Select which contact details should we use to reset your password.
          </Text>
        </View>
        <View style={styles.boxWrapper}>
          {/* <TouchableOpacity onPress={_handleOnBoxSelected}>
            <EmailContactDetails />
          </TouchableOpacity> */}
          <LabeledInputText
            label="Email Address"
            textInputProps={{
              onChangeText: (value) => setEmail(value),
              keyboardType: 'email-address',
            }}
            isValid={isValidEmailAddress}
          />
          <View style={styles.buttonWrapper}>
            <TextButton
              type="primary"
              text="Change Password"
              onPress={_handleOnButtonUpdatePasswordPressed}
            />
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default ResetSelectDetails;
