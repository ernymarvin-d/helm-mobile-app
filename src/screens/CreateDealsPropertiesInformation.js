import React, {useLayoutEffect, useState} from 'react';
import {View, Text, SafeAreaView} from 'react-native';
import {globalStyles} from './../assets/styles/globalStyles';
import LabeledInputText from './../components/atoms/LabeledInputText';
import TextButton from '../components/atoms/TextButton';
import EStyleSheet from 'react-native-extended-stylesheet';
import ProgressBar from '../components/atoms/ProgressBar';
import PasswordTextInput from './../components/atoms/PasswordTextInput';
import {getDefaultHeader} from '../utils/NavigationUtils';
import {ScrollView} from 'react-native-gesture-handler';
import GeneralSettingsModal from '../components/organisms/GeneralSettingsModal';
import _ from 'lodash';
import {
  calculateOption1,
  calculateOption2,
  calculateOption3,
  calculateOption4,
  calculateOption5,
  calculateOption6,
  calculateOption7,
  calculateOption8,
  isValidNumber,
} from '../utils/OptionsCalculatorUtil';
import {connect} from 'react-redux';
import {updateDealOptions} from './../state-management/actions/DealsAction';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useEffect} from 'react';

const styles = EStyleSheet.create({
  root: {
    flex: 1,
    flexGrow: 1,
    justifyContent: 'space-between',
    backgroundColor: 'white',
  },
  contentWrapper: {
    padding: '16rem',
    flexGrow: 1,
    backgroundColor: '#fff',
  },
  buttonWrapper: {
    marginVertical: '18rem',
    width: '100%',
    height: '50rem',
    justifyContent: 'center',
  },
  formWrapper: {
    marginTop: '20rem',
  },
});

const CreateDealsPropertiesInformation = ({
  route,
  navigation,
  user,
  updateDealOptions,
}) => {
  useLayoutEffect(() => {
    navigation.setOptions(getDefaultHeader({navigation}));
  }, [navigation]);

  const {sellerInformation} = route.params;
  const [showGeneralSettingsModal, setShowGeneralSettingsModal] = useState(
    false,
  );
  const [afterRepairMarketValue, setAfterRepairMarketValue] = useState('');
  const [annualPropertyTax, setAnnualPropertyTax] = useState('');
  const [currentMortgateBalance, setCurrentMortgateBalance] = useState('');
  const [piti, setPiti] = useState('');
  const [bathroom, setBathroom] = useState('');
  const [bedRoom, setBedRoom] = useState('');
  const [rentRateMonth, setRentRateMonth] = useState('');
  const [squareFootRehabFixFlip, setSquareFootRehabFixFlip] = useState('');
  const [squareFootRehabRental, setSquareFootRehabRental] = useState('');
  const [squareFootage, setSquareFootage] = useState('i');
  const [propertyInformation, setPropertyInformation] = useState(null);

  const [
    isValidAfterRepairMarketValue,
    setIsValidAfterRepairMarketValue,
  ] = useState(true);
  const [isValidAnnualPropertyTax, setIsValidAnnualPropertyTax] = useState(
    true,
  );
  const [
    isValidCurrentMortgageBalance,
    setIsValidCurrentMortgageBalance,
  ] = useState(true);
  const [isValidPiti, setIsValidPiti] = useState(true);
  const [isValidBathroom, setIsValidBathroom] = useState(true);
  const [isValidBedRoom, setIsValidBedRoom] = useState(true);
  const [isValidRentRateMonth, setIsValidRentRateMonth] = useState(true);
  const [
    isValidSquareFootRehabFixFlip,
    setIsValidSquareFootRehabFixFlip,
  ] = useState(true);
  const [
    isValidSquareFootRehabRental,
    setIsValidSquareFootRehabRental,
  ] = useState(true);
  const [isValidSquareFootage, setIsValidSquareFootage] = useState(true);

  const handleDismissGeneralSettingsModal = () => {
    setShowGeneralSettingsModal(false);
  };

  const handleOnSavedGeneralSettingsModal = () => {
    const {
      option1,
      option2,
      option3,
      option4,
      option5,
      option6,
      option7,
      option8,
    } = user.calculatorGeneralSettings;
    const dealOption1 = calculateOption1({
      settings: option1,
      propertyInformation,
    });
    const dealOption2 = calculateOption2({
      settings: option2,
      propertyInformation,
    });
    const dealOption3 = calculateOption3({
      settings: option3,
      propertyInformation,
    });
    const dealOption4 = calculateOption4({
      settings: option4,
      propertyInformation,
    });
    const dealOption5 = calculateOption5({
      settings: option5,
      propertyInformation,
    });
    const dealOption6 = calculateOption6({
      settings: option6,
      propertyInformation,
    });
    const dealOption7 = calculateOption7({
      settings: option7,
      propertyInformation,
    });
    const dealOption8 = calculateOption8({
      settings: option8,
      propertyInformation,
    });
    const dealOptions = {
      dealOption1,
      dealOption2,
      dealOption3,
      dealOption4,
      dealOption5,
      dealOption6,
      dealOption7,
      dealOption8,
    };

    updateDealOptions(dealOptions);
    navigation.navigate('CreateDealsDealsCalculator', {
      sellerInformation,
      propertyInformation,
    });
  };

  const inputFormsValidated = () => {
    let isValid = true;
    if (!isValidNumber(afterRepairMarketValue)) {
      isValid = false;
      setIsValidAfterRepairMarketValue(false);
    } else {
      setIsValidAfterRepairMarketValue(true);
    }
    if (!isValidNumber(annualPropertyTax)) {
      isValid = false;
      setIsValidAnnualPropertyTax(false);
    } else {
      setIsValidAnnualPropertyTax(true);
    }
    if (!isValidNumber(currentMortgateBalance)) {
      isValid = false;
      setIsValidCurrentMortgageBalance(false);
    } else {
      setIsValidCurrentMortgageBalance(true);
    }
    if (!isValidNumber(piti)) {
      isValid = false;
      setIsValidPiti(false);
    } else {
      setIsValidPiti(true);
    }
    if (!isValidNumber(bathroom)) {
      isValid = false;
      setIsValidBathroom(false);
    } else {
      setIsValidBathroom(true);
    }
    if (!isValidNumber(bedRoom)) {
      isValid = false;
      setIsValidBedRoom(false);
    } else {
      setIsValidBedRoom(true);
    }
    if (!isValidNumber(rentRateMonth)) {
      isValid = false;
      setIsValidRentRateMonth(false);
    } else {
      setIsValidRentRateMonth(true);
    }
    if (!isValidNumber(squareFootRehabFixFlip)) {
      isValid = false;
      setIsValidSquareFootRehabFixFlip(false);
    } else {
      setIsValidSquareFootRehabFixFlip(true);
    }
    if (!isValidNumber(squareFootRehabRental)) {
      isValid = false;
      setIsValidSquareFootRehabRental(false);
    } else {
      setIsValidSquareFootRehabRental(true);
    }
    if (!isValidNumber(squareFootage)) {
      isValid = false;
      setIsValidSquareFootage(false);
    } else {
      setIsValidSquareFootage(true);
    }
    return isValid;
  };

  const _handleOnButtonNextPressed = () => {
    if (!inputFormsValidated()) return;
    generatePropertyInformationData();
    setShowGeneralSettingsModal(true);
  };

  const generatePropertyInformationData = () => {
    setPropertyInformation({
      afterRepairMarketValue: _.toNumber(afterRepairMarketValue),
      annualPropertyTax: _.toNumber(annualPropertyTax),
      bathroom: _.toNumber(bathroom),
      bedRoom: _.toNumber(bedRoom),
      rentRateMonth: _.toNumber(rentRateMonth),
      squareFootRehabFixFlip: _.toNumber(squareFootRehabFixFlip),
      squareFootRehabRental: _.toNumber(squareFootRehabRental),
      squareFootage: _.toNumber(squareFootage),
      piti: _.toNumber(piti),
      currentMortgateBalance: _.toNumber(currentMortgateBalance),
    });
  };

  const updateStateWithValue = (value, setState) => setState(value);

  return (
    <SafeAreaView style={styles.root}>
      <KeyboardAwareScrollView keyboardShouldPersistTaps="handled">
        <ProgressBar progress="100%" />

        <View style={[styles.contentWrapper]}>
          <View style={styles.formWrapper}>
            <LabeledInputText
              label="After repair value / Market Value"
              type="number"
              placeholder="$200,000.00"
              textInputProps={{
                onChangeText: (value) =>
                  updateStateWithValue(value, setAfterRepairMarketValue),
                keyboardType: 'numeric',
                onFocus: () => setIsValidAfterRepairMarketValue(true),
              }}
              isValid={isValidAfterRepairMarketValue}
            />
            <LabeledInputText
              label="Square Footage"
              type="number"
              placeholder="1500"
              textInputProps={{
                onChangeText: (value) =>
                  updateStateWithValue(value, setSquareFootage),
                keyboardType: 'numeric',
                onFocus: () => setIsValidSquareFootage(true),
              }}
              isValid={isValidSquareFootage}
            />
            <LabeledInputText
              label="Rent Rate Month"
              type="number"
              placeholder="$1,000"
              textInputProps={{
                onChangeText: (value) =>
                  updateStateWithValue(value, setRentRateMonth),
                keyboardType: 'numeric',
                onFocus: () => setIsValidRentRateMonth(true),
              }}
              isValid={isValidRentRateMonth}
            />
            <LabeledInputText
              label="Annual Property Taxes"
              type="number"
              placeholder="$741"
              textInputProps={{
                value: annualPropertyTax,
                onChangeText: (value) =>
                  updateStateWithValue(value, setAnnualPropertyTax),
                keyboardType: 'numeric',
                onFocus: () => setIsValidAnnualPropertyTax(true),
              }}
              isValid={isValidAnnualPropertyTax}
            />
            <LabeledInputText
              label="Current Mortgage Balance"
              type="number"
              placeholder="$70,000"
              textInputProps={{
                value: currentMortgateBalance,
                onChangeText: (value) =>
                  updateStateWithValue(value, setCurrentMortgateBalance),
                keyboardType: 'numeric',
                onFocus: () => setIsValidCurrentMortgageBalance(true),
              }}
              isValid={isValidCurrentMortgageBalance}
            />
            <LabeledInputText
              label="Current Mortgage Payment (PITI)"
              type="number"
              placeholder="$0"
              textInputProps={{
                onChangeText: (value) => updateStateWithValue(value, setPiti),
                keyboardType: 'numeric',
                onFocus: () => setIsValidPiti(true),
              }}
              isValid={isValidPiti}
            />
            <LabeledInputText
              label="Bedroom"
              type="number"
              placeholder="3"
              textInputProps={{
                onChangeText: (value) =>
                  updateStateWithValue(value, setBedRoom),
                keyboardType: 'numeric',
                onFocus: () => setIsValidBedRoom(true),
              }}
              isValid={isValidBedRoom}
            />
            <LabeledInputText
              label="Bathroom"
              type="number"
              placeholder="1"
              textInputProps={{
                onChangeText: (value) =>
                  updateStateWithValue(value, setBathroom),
                keyboardType: 'numeric',
                onFocus: () => setIsValidBathroom(true),
              }}
              isValid={isValidBathroom}
            />
            <LabeledInputText
              label="$/Square foot Rehab (Fix/Flip)"
              type="number"
              placeholder="$10.00"
              textInputProps={{
                onChangeText: (value) =>
                  updateStateWithValue(value, setSquareFootRehabFixFlip),
                keyboardType: 'numeric',
                onFocus: () => setIsValidSquareFootRehabFixFlip(true),
              }}
              isValid={isValidSquareFootRehabFixFlip}
            />
            <LabeledInputText
              label="$/Square foot Rehab (Rental)"
              type="number"
              placeholder="$10.00"
              textInputProps={{
                onChangeText: (value) =>
                  updateStateWithValue(value, setSquareFootRehabRental),
                keyboardType: 'numeric',
                onFocus: () => setIsValidSquareFootRehabRental(true),
              }}
              isValid={isValidSquareFootRehabRental}
            />
          </View>
          <View style={styles.buttonWrapper}>
            <TextButton
              type="primary"
              text="Next"
              onPress={_handleOnButtonNextPressed}
            />
          </View>
        </View>
        <GeneralSettingsModal
          show={showGeneralSettingsModal}
          dismissModal={handleDismissGeneralSettingsModal}
          onSaved={handleOnSavedGeneralSettingsModal}
        />
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};

const mapStateToProps = (state) => {
  return {
    user: state.auth.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateDealOptions: (dealOptions) =>
      dispatch(updateDealOptions(dealOptions)),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CreateDealsPropertiesInformation);
