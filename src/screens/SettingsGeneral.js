import React, {useLayoutEffect} from 'react';
import {View, Text, ScrollView} from 'react-native';
import {getDefaultHeader} from '../utils/NavigationUtils';
import {globalStyles} from '../assets/styles/globalStyles';
import EStyleSheet from 'react-native-extended-stylesheet';
import CashCalculator1 from '../components/organisms/CashCalculator1';
import CashCalculator2 from '../components/organisms/CashCalculator2';
import CashCalculator3 from '../components/organisms/CashCalculator3';
import CashCalculator4 from '../components/organisms/CashCalculator4';
import CashCalculator5 from '../components/organisms/CashCalculator5';
import CashCalculator6 from '../components/organisms/CashCalculator6';
import CashCalculator7 from '../components/organisms/CashCalculator7';
import CashCalculator8 from '../components/organisms/CashCalculator8';
import {MARGIN_LARGE} from '../assets/dimens';
import {BOX_BACKGROUND_COLOR} from '../assets/colors';
import TextButton from '../components/atoms/TextButton';
import {connect} from 'react-redux';
import {PLAN_TYPE} from '../assets/Constants';

const styles = EStyleSheet.create({
  root: {
    backgroundColor: 'white',
    width: '100%',
  },
  marginTop: {
    marginTop: '15rem',
  },
  headerWrapper: {
    paddingVertical: MARGIN_LARGE,
    alignItems: 'center',
    backgroundColor: BOX_BACKGROUND_COLOR,
  },
  header: {
    fontWeight: 'bold',
  },
});

// option2: {downPayment: 20000, sellingCost: 0.08, priceDiscount: 0.7, lendingPts: 0.01, lendingApr: 0.11, …}
// option3: {downPayment: 0.2, interestRateOnMortgate: 0.06, wholeSaleFee: 10000, priceDiscount: 0.8, numYearsOnMortgage: 30}
// option4: {downPayment: 0.1, yearsTillBalloonPayment: 5, priceDiscount: 0.85, interestRateOnLandContract: 0.05, wholeSaleFee: 5000}
// option5: {downPayment: 0.1, paymentTermLength: 30, wholeSaleFee: 5000, priceDiscount: 1, numerOfYearsTillBalloon: 5}
// option6: {minOptionDepositFromTenantBuyer: 5000, optionDepositToSeller: 500, numYears: 5, priceDiscount: 0.85}
// option7: {minOptionDepositFromTenantBuyer: 5000, optionDepositToSeller: 500, numYears: 5, priceDiscount: 1}
// option8: {cashForKeysPurchaseAmountAboveBalance: 5000, balanceOnMortgage: 70000}

const SettingsGeneral = ({
  asModal,
  navigation,
  calculatorGeneralSettings,
  planType,
}) => {
  //   console.log({calculatorGeneralSettings});
  const {
    option1,
    option2,
    option3,
    option4,
    option5,
    option6,
    option7,
    option8,
  } = calculatorGeneralSettings;
  useLayoutEffect(() => {
    if (!asModal) {
      //   navigation.setOptions(getDefaultHeader({navigation, renderHeaderRight}));
      navigation.setOptions(getDefaultHeader({navigation}));
    }
  }, [navigation]);

  const renderHeaderRight = () => {
    return (
      <TextButton
        text="SAVE"
        buttonCustomStyle={globalStyles.rightButtonText}
      />
    );
  };

  const renderHeader = () => {
    return (
      <View style={styles.headerWrapper}>
        <Text style={[globalStyles.h3, styles.header]}>General Settings</Text>
      </View>
    );
  };

  return (
    <View style={[globalStyles.rootNoPadding, styles.root]}>
      <ScrollView>
        <View>
          {asModal ? renderHeader() : <View />}
          <CashCalculator1 />
          <CashCalculator2 style={styles.marginTop} />
          <CashCalculator3 style={styles.marginTop} />
          {planType !== PLAN_TYPE.free ? (
            <>
              <CashCalculator4 style={styles.marginTop} />
              <CashCalculator5 style={styles.marginTop} />
              <CashCalculator6 style={styles.marginTop} />
              <CashCalculator7 style={styles.marginTop} />
              <CashCalculator8 style={styles.marginTop} />
            </>
          ) : (
            <></>
          )}
        </View>
      </ScrollView>
    </View>
  );
};

const mapStateToProps = (state) => {
  return {
    calculatorGeneralSettings: state.auth.user.calculatorGeneralSettings,
    planType: state.planType,
  };
};
export default connect(mapStateToProps)(SettingsGeneral);
