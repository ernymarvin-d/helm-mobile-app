import React, {useState} from 'react';
import AppIntroSlider from 'react-native-app-intro-slider';
import {slides} from './slideData';
import _ from 'lodash';
import IntroSliderDefaultControls from '../../components/molecules/IntroSliderDefaultControls/IntroSliderDefaultControls';
import IntroSliderLastSlideControls from '../../components/molecules/IntroSliderLastSlideControls';
import AppIntroContent from '../../components/organisms/AppIntroContent';
import {persistIntroScreenAlreadyShown} from '../../utils/AsyncStorageUtil';

const AppIntro = ({navigation}) => {
  const [sliderRef, setSliderRef] = useState(null);

  const _keyExtractor = (item) => item.key;
  const _renderItem = ({item}) => <AppIntroContent content={item} />;
  const navigateToAuthStack = () => {
    console.log('navigate to auth stack');
    // navigation.navigate('AuthStack');
    persistIntroScreenAlreadyShown(true);
    navigation.reset({
      index: 0,
      routes: [{name: 'AuthStack'}],
    });
  };

  const _renderLastSlideControl = (activeIndex) => {
    const onPrevButtonClick = () => sliderRef.goToSlide(activeIndex - 1, true);
    const onNextButtonClick = () => navigateToAuthStack();
    const onDoneButtonClick = () => navigateToAuthStack();
    return (
      <IntroSliderLastSlideControls
        onPrevButtonClick={onPrevButtonClick}
        onNextButtonClick={onNextButtonClick}
        onDoneButtonClick={onDoneButtonClick}
      />
    );
  };

  const _renderDefaultControl = (activeIndex) => {
    const previousSlide = activeIndex - 1;
    const nextSlide = activeIndex + 1;

    const onPrevButtonClick = () => sliderRef.goToSlide(previousSlide, true);
    const onNextButtonClick = () => sliderRef.goToSlide(nextSlide, true);
    const onDotPress = (index) => sliderRef.goToSlide(index, true);

    return (
      <IntroSliderDefaultControls
        activeIndex={activeIndex}
        onPrevButtonClick={onPrevButtonClick}
        onNextButtonClick={onNextButtonClick}
        onDotPress={onDotPress}
        data={slides}
      />
    );
  };

  const _renderButtonControls = (activeIndex) => {
    const isLastSlide = _.isEqual(activeIndex, slides.length - 1);
    return !isLastSlide
      ? _renderDefaultControl(activeIndex)
      : _renderLastSlideControl(activeIndex);
  };

  return (
    <AppIntroSlider
      ref={(ref) => setSliderRef(ref)}
      key={_keyExtractor}
      data={slides}
      renderItem={_renderItem}
      renderPagination={_renderButtonControls}
    />
  );
};

export default AppIntro;
