import house1 from '../../assets/images/house-1.jpg';
import walkthrough1 from '../../assets/images/Walkthrough_1.png';
import walkthrough2 from '../../assets/images/Walkthrough_2.png';
import walkthrough3 from '../../assets/images/Walkthrough_3.png';
import walkthrough4 from '../../assets/images/Walkthrough_4.png';

export const slides = [
  {
    key: 'onboarding-1',
    title: 'Create Deals',
    text: 'Create custom deals in the app.',
    backgroundImage: walkthrough1,
    backgroundColor: '#59b2ab',
  },
  {
    key: 'onboarding-2',
    title: 'Custom Criteria',
    text: 'Choose and edit your criteria\nfor each new deal.',
    backgroundImage: walkthrough2,
    backgroundColor: '#59b2ab',
  },
  {
    key: 'onboarding-3',
    title: 'Create and Send Proposals',
    text: 'You can auto-generate\nproposals to send to sellers!',
    backgroundImage: walkthrough3,
    backgroundColor: '#59b2ab',
  },
  {
    key: 'onboarding-4',
    title: 'Go Premium!',
    text:
      "When you upgrade your account you will be able to create unlimited deals, send proposals to sellers directly from the app, and will have unlimited access to Jeff Helm's courses and tutorials.",
    backgroundImage: walkthrough4,
    backgroundColor: '#59b2ab',
  },
];
