import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  Image,
  AppState,
  Platform,
} from 'react-native';
import {BLUE} from '../assets/colors';
import {globalStyles} from '../assets/styles/globalStyles';
import {PADDING_DEFAULT} from '../assets/dimens';
import logoImage from './../assets/images/helm-logo-name-white.png';
import EStyleSheet from 'react-native-extended-stylesheet';
import auth from '@react-native-firebase/auth';
import {isIntroScreenAlreadyShown} from '../utils/AsyncStorageUtil';
import {GoogleSignin} from '@react-native-community/google-signin';
import {observeCreatedUserDeals, observeUserData} from './../utils/DbUtils';
import {
  UserDbCollection,
  getUserData,
  createNewUserDataFromProvider,
} from '../utils/DbUtils';
import {
  loginUser,
  logoutUser,
  registerAuthListener,
  setFlagAlreadyNavigatedToHome,
  addToMyDeals,
  removeFromMyDeals,
} from '../state-management/actions/AuthActions';
import {resetDealsOptionState} from '../state-management/actions/DealsAction';
import {connect} from 'react-redux';
import _ from 'lodash';
import SubscriptionManager from '../components/atoms/SubscriptionManager';
import {setPlanType} from '../state-management/actions/PlanTypeAction';
import {PLAN_TYPE} from '../assets/Constants';
import KeyboardManager from 'react-native-keyboard-manager';

const styles = EStyleSheet.create({
  root: {
    backgroundColor: BLUE,
    padding: PADDING_DEFAULT,
  },
  contentWrapper: {
    width: '100%',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  box: {
    flexGrow: 1,
    flex: 1,
    width: '100%',
  },
  logo: {
    width: '300rem',
    height: '250rem',
    alignSelf: 'center',
    marginTop: 'auto',
    marginBottom: '10%',
  },
  headerText: {
    fontSize: '20rem',
    fontFamily: 'Montserrat-Light',
    alignSelf: 'center',
    letterSpacing: 0.2,
    color: 'white',
  },
  footerText: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    textAlign: 'center',
    width: '100%',
    color: 'white',
  },
});

const Splash = ({
  navigation,
  loginUser,
  logoutUser,
  registerAuthListener,
  addToMyDeals,
  removeFromMyDeals,
  authListener,
  resetDealsOptionState,
  setPlanType,
}) => {
  GoogleSignin.configure({
    webClientId:
      // '462027125855-r8opld23i5lkadqdagcd3iismc2dfcdu.apps.googleusercontent.com', <-dev
      '751279957389-7ad7p98a2hl6q6qatbr6obqsk13u484b.apps.googleusercontent.com',
    iosClientId:
      '751279957389-h8fm6f6q2sbndkfvfo94rp08itk20ba6.apps.googleusercontent.com',
  });

  useEffect(() => {
    if (Platform.OS === 'ios') {
      KeyboardManager.setEnable(true);
      KeyboardManager.setToolbarDoneBarButtonItemText('Dismiss');
      KeyboardManager.setToolbarPreviousNextButtonEnable(true);
    }
  }, []);

  useEffect(() => {
    if (_.isNull(authListener)) {
      registerAuthListener(
        auth().onAuthStateChanged(async (user) => {
          console.log('on Splash', {user});
          if (user) {
            if (user.email.toLowerCase() === 'demo@gmail.com') {
              setPlanType(PLAN_TYPE.proPlus);
            }
            saveUserInfoToState(user).then(() => {
              navigation.reset({
                index: 0,
                routes: [{name: 'HomeTab'}],
              });
            });
            let observer = observeCreatedUserDeals({
              userId: user.uid,
              stateDispatcher: {addToMyDeals, removeFromMyDeals},
            });
          } else {
            console.log('splash - unauth');
            navigateUnAuthUser();
            resetDealsOptionState();
            logoutUser();
          }
        }),
      );
    } else {
      console.log(
        'auth listener is setup already. Do not add auth listener to redux',
      );
    }
  }, [authListener, navigation]);

  const saveUserInfoToState = async (user) => {
    console.log('save user info to state', {user});
    const userDbData = await getUserData(user.uid);
    console.log({userDbData});
    if (userDbData.exists) {
      return observeUserData({user, stateDispatcher: {loginUser}});
    } else {
      const {providerId} = user._user.providerData[0];
      console.log({providerId});
      switch (providerId) {
        case 'facebook.com':
        case 'google.com':
        case 'apple.com':
          return createNewUserDataFromProvider(user).then((newUserData) => {
            if (!_.isNull(newUserData)) {
              observeUserData({user, stateDispatcher: {loginUser}});
            }
            return;
          });
        default:
          return;
      }
    }
  };

  const navigateUnAuthUser = async () => {
    console.log('User is logged out');
    let routes = (await isIntroScreenAlreadyShown())
      ? [{name: 'AuthStack'}]
      : [{name: 'AppIntro'}];
    navigation.reset({
      index: 0,
      routes,
    });
  };

  return (
    <SafeAreaView style={[globalStyles.root, styles.root]}>
      <View style={styles.contentWrapper}>
        <View style={styles.box}>
          <Image
            source={logoImage}
            style={styles.logo}
            resizeMethod="resize"
            resizeMode="contain"
          />
        </View>
        <View style={styles.box}>
          <Text style={[globalStyles.p1, styles.headerText]}>
            WELCOME TO HELM
          </Text>
        </View>
        <Text style={[globalStyles.p2, styles.footerText]}>
          Powered by The App Guys
        </Text>
      </View>
      <SubscriptionManager />
    </SafeAreaView>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    loginUser: (user) => dispatch(loginUser(user)),
    logoutUser: () => dispatch(logoutUser(null)),
    registerAuthListener: (authListener) =>
      dispatch(registerAuthListener(authListener)),
    addToMyDeals: (deal) => dispatch(addToMyDeals(deal)),
    removeFromMyDeals: (deal) => dispatch(removeFromMyDeals(deal)),
    resetDealsOptionState: () => dispatch(resetDealsOptionState()),
    setPlanType: (planType) => dispatch(setPlanType(planType)),
  };
};

const mapStateToProps = (state) => {
  return {
    user: state.auth.user,
    authListener: state.auth.authListener,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Splash);
