import React, {useState, useLayoutEffect} from 'react';
import {View, Text} from 'react-native';
import {getDefaultHeader} from '../utils/NavigationUtils';
import {ScrollView} from 'react-native-gesture-handler';
import SettingItem from '../components/molecules/SettingsItem';
import {globalStyles} from '../assets/styles/globalStyles';
import EStyleSheet from 'react-native-extended-stylesheet';
import {BOX_BACKGROUND_COLOR} from '../assets/colors';
import auth from '@react-native-firebase/auth';
import {logoutUser} from '../state-management/actions/AuthActions';
import {resetDealsOptionState} from '../state-management/actions/DealsAction';
import {connect} from 'react-redux';

const styles = EStyleSheet.create({
  root: {
    backgroundColor: BOX_BACKGROUND_COLOR,
  },
});

const Settings = ({navigation, logoutUser, resetDealsOptionState}) => {
  const navigateUnAuthUser = async () => {
    console.log('Settings Screen', 'User is logged out');
    navigation.reset({
      index: 0,
      routes: [{name: 'AuthStack'}],
    });
  };

  useState(() => {
    const unsubscribe = auth().onAuthStateChanged(async (user) => {
      console.log('on Splash', {user});
      if (!user) {
        console.log('splash - unauth');
        navigateUnAuthUser();
        resetDealsOptionState();
        logoutUser();
      }
    });

    return () => unsubscribe();
  }, []);

  useLayoutEffect(() => {
    navigation.setOptions(getDefaultHeader({onPressLeftButton}));
  }, [navigation]);

  const onPressLeftButton = () => navigation.navigate('HomeTab');

  const handlerOnPressGeneral = () => navigation.navigate('General');
  const handlerOnPressMyDeals = () =>
    navigation.navigate('MyDealsStack', {
      screen: 'MyDeals',
      params: {
        headerBackButtonPop: true,
        fromSettings: true,
      },
    });
  const handlerOnPressContact = () => navigation.navigate('Contact');
  const handlerOnPressFAQ = () => navigation.navigate('Faq');
  const handlerOnPressTOS = () => navigation.navigate('Tos');
  return (
    <View style={[globalStyles.rootNoPadding, styles.root]}>
      <ScrollView>
        <SettingItem title="General Settings" onPress={handlerOnPressGeneral} />
        <SettingItem title="My Deals" onPress={handlerOnPressMyDeals} />
        <SettingItem title="Contact Helm" onPress={handlerOnPressContact} />
        <SettingItem title="FAQ" onPress={handlerOnPressFAQ} />
        <SettingItem title="Terms of Uses" onPress={handlerOnPressTOS} />
        <SettingItem title="Logout" onPress={() => auth().signOut()} />
      </ScrollView>
    </View>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    logoutUser: () => dispatch(logoutUser(null)),
    resetDealsOptionState: () => dispatch(resetDealsOptionState()),
  };
};

export default connect(null, mapDispatchToProps)(Settings);
