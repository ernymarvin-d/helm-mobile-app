import React, {useLayoutEffect} from 'react';
import {View, Text, ScrollView, Image} from 'react-native';
import {getDefaultHeader} from '../utils/NavigationUtils';
import {globalStyles} from '../assets/styles/globalStyles';
import EStyleSheet from 'react-native-extended-stylesheet';
import LabeledInputText from '../components/atoms/LabeledInputText';
import TextButton from '../components/atoms/TextButton';
import GrayRoundedBox from '../components/atoms/GrayRoundedBox';
import paypalIcon from '../assets/images/paypal.png';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
import {MARGIN_SMALL, MARGIN_DEFAULT, MARGIN_LARGE} from '../assets/dimens';
import RbPaymentOptions from '../components/molecules/RbPaymentOptions';
import DateSelectorInputBox from '../components/atoms/DateSelectorInputBox';

const styles = EStyleSheet.create({
  header: {
    fontWeight: 'bold',
  },
  cardSelectionWrapper: {
    marginTop: '40rem',
  },
  selectionWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: MARGIN_SMALL,
  },
  inputBoxWrapper: {
    marginTop: '50rem',
  },
  buttonUpdate: {
    marginVertical: MARGIN_DEFAULT,
  },
});

const paymentOptions = [
  {
    label: 'Paypal',
    value: 0,
    description: 'Protected online secure payment method.',
  },
  {
    label: 'Visa',
    value: 1,
    description: 'Protected online secure payment method.',
  },
];

const ProfileAddPaymentMethod = ({navigation}) => {
  useLayoutEffect(() => {
    navigation.setOptions(getDefaultHeader({navigation}));
  }, [navigation]);

  return (
    <ScrollView>
      <View style={[globalStyles.root]}>
        <Text style={[globalStyles.h3, styles.header]}>My Payment Details</Text>
        <View style={styles.cardSelectionWrapper}>
          <Text style={[globalStyles.inputTextLabel]}>Select Card</Text>
          <RbPaymentOptions options={paymentOptions} />
        </View>
        <View style={styles.inputBoxWrapper}>
          <LabeledInputText label="Card Number" placeholder="XXXXXXXX" />
          <LabeledInputText label="CVV Code" placeholder="XXXX" />
          <DateSelectorInputBox label="Expire Date" placeholder="12/12/2020" />
          <LabeledInputText label="Card Holder Name" placeholder="Pronab" />
          <DateSelectorInputBox label="Birth Date" placeholder="12/12/2020" />
          <TextButton
            style={styles.buttonUpdate}
            text="UPDATE MY PAYMENT DETAILS"
            type="primary"
          />
        </View>
      </View>
    </ScrollView>
  );
};

export default ProfileAddPaymentMethod;

const radioButton = ({label}) => {
  return (
    <View style={{marginVertical: 16}}>
      <RadioButton labelHorizontal={true} key={i}>
        <RadioButtonInput
          obj={obj}
          index={i}
          isSelected={selected === i}
          onPress={() => setSelected(i)}
          borderWidth={1}
          buttonInnerColor={selected === i ? BLUE : GREY[3]}
          buttonOuterColor={GREY[5]}
          buttonSize={15}
          buttonOuterSize={20}
          buttonStyle={{borderWidth: 2, backgroundColor: GREY[3]}}
          buttonWrapStyle={{marginLeft: 10}}
        />
        <RadioButtonLabel
          obj={obj}
          index={i}
          labelHorizontal={true}
          onPress={() => {}}
          labelStyle={[globalStyles.p1]}
          labelWrapStyle={{}}
        />
      </RadioButton>
    </View>
  );
};
