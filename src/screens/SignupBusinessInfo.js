import React, {useState} from 'react';
import {View, Text, SafeAreaView, Alert} from 'react-native';
import {globalStyles} from './../assets/styles/globalStyles';
import LabeledInputText from './../components/atoms/LabeledInputText';
import TextButton from '../components/atoms/TextButton';
import EStyleSheet from 'react-native-extended-stylesheet';
import ProgressBar from '../components/atoms/ProgressBar';
import {registerUserWithPassword} from '../utils/AuthUtils';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import {UserDbCollection} from '../utils/DbUtils';
import PleaseWaitDialog from '../components/atoms/PleaseWaitDialog';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

const styles = EStyleSheet.create({
  root: {
    flex: 1,
    flexGrow: 1,
    backgroundColor: '#fff',
  },
  contentWrapper: {
    padding: '16rem',
    flexGrow: 1,
    backgroundColor: '#fff',
    justifyContent: 'space-between',
  },
  headerWrapper: {
    justifyContent: 'center',
    flex: 1,
    alignItems: 'center',
  },
  headerText: {},
  subHeaderText: {
    textAlign: 'center',
    paddingVertical: '8rem',
  },
  buttonWrapper: {
    marginVertical: '12rem',
    width: '100%',
    justifyContent: 'flex-end',
  },
  formWrapper: {},
  addressTextInput: {height: '100rem', paddingTop: '10rem'},
});

const SignupBusinessInfo = ({route, navigation}) => {
  const {userInfo} = route.params;

  const [businessName, setBusinessName] = useState('');
  const [address, setAddress] = useState('');
  const [websiteAddress, setWebsiteAddress] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [showPleaseWaitDailog, setshowPleaseWaitDailogDailog] = useState(false);

  const _handleOnButtonSignupPressed = () => {
    setshowPleaseWaitDailogDailog(true);
    const emailAddress = userInfo.emailAddress;
    const fullName = userInfo.fullName;

    const password = userInfo.password;
    const userDbData = {
      info: {
        emailAddress,
        fullName,
      },
      businessInfo: {
        businessName,
        address,
        websiteAddress,
        phoneNumber,
      },
    };
    setshowPleaseWaitDailogDailog(false);
    navigation.navigate('PhoneNumberVerificationStack', {
      screen: 'PhoneNumberVerification',
      params: {userDbData, password},
    });
  };

  const handleSignupError = (error) => {
    let alertMessage = '';
    if (error.code === 'auth/email-already-in-use') {
      console.log('That email address is already in use!');
      alertMessage = 'That email address is already in use!';
    }
    if (error.code === 'auth/invalid-email') {
      console.log('That email address is invalid!');
      alertMessage = 'That email address is invalid!';
    }
    console.error(error);
    Alert.alert('', alertMessage);
  };

  const handleOnTextChange = ({text, setStateText}) => setStateText(text);

  return (
    <SafeAreaView style={styles.root}>
      <ProgressBar progress="100%" />
      <KeyboardAwareScrollView>
        <View style={[styles.contentWrapper]}>
          <View style={styles.headerWrapper}>
            <Text style={[globalStyles.h2]}>Business Details</Text>
            <Text style={[globalStyles.p2, styles.subHeaderText]}>
              Enter your business information
            </Text>
          </View>
          <View style={styles.formWrapper}>
            <LabeledInputText
              label="Business Name"
              type="name"
              textInputProps={{
                onChangeText: (text) => {
                  handleOnTextChange({text, setStateText: setBusinessName});
                },
              }}
              value={businessName}
            />
            <LabeledInputText
              label="Address"
              type="fullStreetAddress"
              placeholder="Address..."
              textInputHeight={100}
              textInputStyle={styles.addressTextInput}
              textInputProps={{
                onChangeText: (text) => {
                  handleOnTextChange({text, setStateText: setAddress});
                },
                multiline: true,
                textAlignVertical: 'top',
              }}
              value={address}
            />
            <LabeledInputText
              label="Website Address"
              placeholder="www."
              textInputProps={{
                onChangeText: (text) => {
                  handleOnTextChange({text, setStateText: setWebsiteAddress});
                },
              }}
              value={websiteAddress}
            />
            <LabeledInputText
              label="Phone Number"
              type="number"
              textInputProps={{
                onChangeText: (text) => {
                  handleOnTextChange({text, setStateText: setPhoneNumber});
                },
              }}
              value={phoneNumber}
            />
          </View>
          <View style={styles.buttonWrapper}>
            <TextButton
              type="primary"
              text="Sign Up"
              onPress={_handleOnButtonSignupPressed}
            />
          </View>
        </View>
        <PleaseWaitDialog showDialog={showPleaseWaitDailog} />
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};

export default SignupBusinessInfo;
