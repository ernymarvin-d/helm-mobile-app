import React, {useLayoutEffect, useState, useEffect} from 'react';
import {View, FlatList, TouchableWithoutFeedback} from 'react-native';
import {getDefaultHeader} from '../utils/NavigationUtils';
import EStyleSheet from 'react-native-extended-stylesheet';
import {globalStyles} from '../assets/styles/globalStyles';
import {myDealsData} from '../utils/Mocks';
import MyDealsListItem from '../components/molecules/MyDealsListItem';
import {PADDING_DEFAULT} from '../assets/dimens';
import {MenuProvider} from 'react-native-popup-menu';
import _ from 'lodash';
import {connect} from 'react-redux';
import {
  addToMyDeals,
  removeFromMyDeals,
} from '../state-management/actions/AuthActions';
import {updateDealOptions} from '../state-management/actions/DealsAction';
import {deleteMyDeal} from '../utils/DbUtils';
import MessageDialog from '../components/atoms/MessageDialog';
import {NavigationContainer} from '@react-navigation/native';

const styles = EStyleSheet.create({
  flatListWrapper: {
    padding: PADDING_DEFAULT,
  },
});

const MyDeals = ({
  route,
  navigation,
  myDeals,
  user,
  addToMyDeals,
  removeFromMyDeals,
  updateDealOptions,
}) => {
  console.log('AAAAAA', {myDeals});
  const [fullControl, setFullControl] = useState(false);
  const [observerMyDeals, setObserverMyDeals] = useState(null);
  const [showNoDealsMessageDialog, setShowNoDealsMessageDialog] = useState(
    false,
  );
  const headerBackButtonPop = route.params?.headerBackButtonPop;

  const hasFullControl = () => {
    const fromSettings = route.params?.fromSettings;
    setFullControl(_.isUndefined(fromSettings) ? false : true);
  };

  useEffect(() => {
    navigation.addListener('focus', () => {
      setShowNoDealsMessageDialog(_.isEmpty(myDeals));
    });
  }, [navigation]);

  useEffect(() => {
    hasFullControl();
  });

  useLayoutEffect(() => {
    const backToHome = _.isUndefined(headerBackButtonPop)
      ? true
      : !headerBackButtonPop;
    let defaultHeaderParams = backToHome ? {onPressLeftButton} : {navigation};
    navigation.setOptions(getDefaultHeader(defaultHeaderParams));
  }, [navigation]);

  const onPressLeftButton = () => navigation.navigate('HomeTab');

  const handleItemClick = (dealItem) => {
    console.log({dealItem});
    updateDealOptions(dealItem.dealOptions);
    navigation.push('DealsDetails', {dealItem});
  };

  const renderMyDeals = ({item, index}) => {
    const key = item;
    const myDealItem = myDeals[key];
    console.log('renderMyDeals', {key});
    return (
      <TouchableWithoutFeedback
        key={key}
        onPress={() => handleItemClick(myDealItem)}>
        <View>
          <MyDealsListItem
            dealItemKey={key}
            item={myDealItem}
            showMenu={fullControl}
            name={`Deal no. ${index + 1}`}
            navigation={navigation}
            onMenuSelectedDelete={() => deleteMyDeal({myDealKey: key})}
            onMenuSelectedView={() => handleItemClick(myDealItem)}
          />
        </View>
      </TouchableWithoutFeedback>
    );
  };

  return (
    <MenuProvider>
      <View style={globalStyles.rootNoPadding}>
        <FlatList
          data={Object.keys(myDeals)}
          renderItem={renderMyDeals}
          keyExtractor={(item) => item}
        />
        <MessageDialog
          visible={showNoDealsMessageDialog}
          title="My Deals"
          description="You do not have any deals yet. Create one one."
          onPressPositiveButton={() => {
            setShowNoDealsMessageDialog(false);
            navigation.goBack();
          }}
          positiveButtonText="Back to Main"
        />
      </View>
    </MenuProvider>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    addToMyDeals: (deal) => dispatch(addToMyDeals(deal)),
    removeFromMyDeals: (deal) => dispatch(removeFromMyDeals(deal)),
    updateDealOptions: (dealOptions) =>
      dispatch(updateDealOptions(dealOptions)),
  };
};

const mapStateToProps = (state) => {
  console.log('mapStateToProps', 'MyDeals', {state});
  return {
    myDeals: state.auth.myDeals,
    user: state.auth.user,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MyDeals);
