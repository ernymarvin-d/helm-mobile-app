import React, {useLayoutEffect, useState} from 'react';
import {View, Text, SafeAreaView, Alert} from 'react-native';
import {globalStyles} from './../assets/styles/globalStyles';
import EStyleSheet from 'react-native-extended-stylesheet';
import {getDefaultHeader} from '../utils/NavigationUtils';
import {ScrollView} from 'react-native-gesture-handler';
import {connect} from 'react-redux';
import DealsOption1 from '../components/organisms/DealsOption1';
import DealsOption2 from '../components/organisms/DealsOption2';
import DealsOption3 from '../components/organisms/DealsOption3';
import DealsOption4 from '../components/organisms/DealsOption4';
import DealsOption5 from '../components/organisms/DealsOption5';
import DealsOption6 from '../components/organisms/DealsOption6';
import DealsOption7 from '../components/organisms/DealsOption7';
import DealsOption8 from '../components/organisms/DealsOption8';
import TextButton from '../components/atoms/TextButton';
import {MARGIN_DEFAULT, MARGIN_SMALL} from '../assets/dimens';
import IconButton from '../components/atoms/IconButton';
import questionIcon from '../assets/images/ic-question.png';
import ColorInfoModal from '../components/organisms/ColorInfoModal';
import _ from 'lodash';
import {PLAN_TYPE} from '../assets/Constants';
import {resetSelectedDealsOption} from '../state-management/actions/DealsAction';

const styles = EStyleSheet.create({
  root: {
    flex: 1,
    flexGrow: 1,
    justifyContent: 'space-between',
    backgroundColor: 'white',
  },
  headerWrapper: {
    marginVertical: '20rem',
  },
  buttonWrapper: {flexShrink: 1, margin: MARGIN_DEFAULT},
  questionButton: {
    backgroundColor: 'transparent',
    margin: 0,
    padding: 0,
    marginRight: MARGIN_SMALL,
  },
});

const CreateDealsDealsCalculator = ({
  route,
  navigation,
  dealOptions,
  calculatorGeneralSettings,
  selectedDealOptions,
  planType,
  resetSelectedDealsOption,
}) => {
  console.log({planType});
  useLayoutEffect(() => {
    navigation.setOptions(
      getDefaultHeader({
        navigation,
        renderHeaderRight,
        onPressLeftButton: handleOnPressHeaderBackButton,
      }),
    );
  }, [navigation]);

  const handleOnPressHeaderBackButton = () => {
    resetSelectedDealsOption();
    navigation.pop();
  };

  const {sellerInformation, propertyInformation} = route.params;
  const [showColorInfoModal, setShowColorInfoModal] = useState(false);
  const renderHeaderRight = () => {
    return (
      <IconButton
        icon={questionIcon}
        resizeMethod="resize"
        resizeMode="center"
        containerStyle={styles.questionButton}
        onPress={_handleOnQuestionIconButtonPress}
      />
    );
  };

  const _handleOnButtonGenerateProposalPressed = () => {
    if (selectedDealOptions.length === 0) {
      Alert.alert(
        'Deals Calculator',
        'Please select a maximum of 3 deal options.',
      );
      return;
    }
    navigation.navigate('CreateDealsLoiTemplate', {
      sellerInformation,
      propertyInformation,
    });
  };

  const _handleOnQuestionIconButtonPress = () => setShowColorInfoModal(true);
  const _handleOnDismissColorInfoModal = () => setShowColorInfoModal(false);

  return (
    <SafeAreaView style={styles.root}>
      <ScrollView>
        <View style={[globalStyles.root]}>
          <View style={styles.headerWrapper}>
            <Text style={globalStyles.h3}>8 Deals are generated below</Text>
            <Text style={globalStyles.p2}>
              You can select multiple deals to generate proposals (max selection
              3 deals)
            </Text>
          </View>
          <DealsOption1
            settings={calculatorGeneralSettings.option1}
            propertyInformation={propertyInformation}
          />
          <View style={globalStyles.divider} />
          <DealsOption2
            // dealKey={}
            settings={calculatorGeneralSettings.option2}
            propertyInformation={propertyInformation}
          />
          <View style={globalStyles.divider} />
          <DealsOption3
            propertyInformation={propertyInformation}
            settings={calculatorGeneralSettings.option3}
          />
          {planType !== PLAN_TYPE.free ? (
            <>
              <View style={globalStyles.divider} />
              <DealsOption4
                propertyInformation={propertyInformation}
                settings={calculatorGeneralSettings.option4}
              />
              <View style={globalStyles.divider} />
              <DealsOption5
                propertyInformation={propertyInformation}
                settings={calculatorGeneralSettings.option5}
              />
              <View style={globalStyles.divider} />
              <DealsOption6
                propertyInformation={propertyInformation}
                settings={calculatorGeneralSettings.option6}
              />
              <View style={globalStyles.divider} />
              <DealsOption7
                propertyInformation={propertyInformation}
                settings={calculatorGeneralSettings.option7}
              />
              <View style={globalStyles.divider} />
              <DealsOption8
                settings={calculatorGeneralSettings.option8}
                propertyInformation={propertyInformation}
              />
            </>
          ) : (
            <></>
          )}
        </View>
      </ScrollView>
      <View style={styles.buttonWrapper}>
        <TextButton
          text="GENERATE PROPOSAL"
          type="primary"
          onPress={_handleOnButtonGenerateProposalPressed}
        />
      </View>
      <ColorInfoModal
        show={showColorInfoModal}
        dismissModal={_handleOnDismissColorInfoModal}
      />
    </SafeAreaView>
  );
};

const mapStateToProps = (state) => {
  return {
    calculatorGeneralSettings: state.auth.user.calculatorGeneralSettings,
    dealOptions: state.dealsCalculator.dealOptions,
    selectedDealOptions: state.dealsCalculator.selectedDealOptions,
    planType: state.planType,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    resetSelectedDealsOption: () => dispatch(resetSelectedDealsOption()),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CreateDealsDealsCalculator);
