import React from 'react';
import {View, Text, SafeAreaView, TouchableOpacity} from 'react-native';
import {globalStyles} from '../assets/styles/globalStyles';
import EStyleSheet from 'react-native-extended-stylesheet';
import {PADDING_DEFAULT} from '../assets/dimens';
import NumberCodeVerificationInput from './../components/molecules/NumberCodeVerificationInput';
import TextButton from '../components/atoms/TextButton';

const styles = EStyleSheet.create({
  contentWrapper: {
    width: '100%',
    height: '80%',
    padding: PADDING_DEFAULT,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  headerWrapper: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: '70rem',
  },
  header: {
    marginVertical: '20rem',
  },
  subHeader: {textAlign: 'center', lineHeight: 25},
  boxWrapper: {
    width: '280rem',
  },
  resendLink: {
    fontFamily: 'Montserrat-Medium',
    fontWeight: '700',
  },
  buttonWrapper: {
    width: '100%',
    marginTop: '40rem',
  },
});

export default ResetRecoveryPin = ({navigation}) => {
  const _handleResetPasswordButtonClick = () =>
    navigation.navigate('ResetInputPassword');
  return (
    <SafeAreaView style={globalStyles.root}>
      <View style={styles.contentWrapper}>
        <View style={styles.headerWrapper}>
          <Text style={[globalStyles.h2, styles.header]}>
            {`Enter 4-Digit \nRecovery Pin`}
          </Text>
          <Text style={[globalStyles.p2, styles.subHeader]}>
            Recovery code was sent to you via SMS no ***808302. Please enter
            code here.
          </Text>
        </View>
        <View style={styles.boxWrapper}>
          <NumberCodeVerificationInput numDigit={4} />
        </View>
        <Text style={[globalStyles.p1, styles.footerText]}>
          <Text>Did not get the code yet! </Text>
          <Text style={[globalStyles.link, styles.resendLink]} onPress={{}}>
            Resend
          </Text>
        </Text>
        <View style={styles.buttonWrapper}>
          <TextButton
            width="100%"
            text="RESET PASSWORD"
            type="primary"
            onPress={_handleResetPasswordButtonClick}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};
