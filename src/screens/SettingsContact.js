import React, {useLayoutEffect, useState} from 'react';
import {View, Text} from 'react-native';
import {getDefaultHeader} from '../utils/NavigationUtils';
import {globalStyles} from '../assets/styles/globalStyles';
import LabeledInputText from '../components/atoms/LabeledInputText';
import TextButton from '../components/atoms/TextButton';
import EStyleSheet from 'react-native-extended-stylesheet';
import {MARGIN_LARGE} from '../assets/dimens';
import {sendMessageToHelm} from '../utils/DbUtils';
import {connect} from 'react-redux';
import _ from 'lodash';
import {firebase} from '@react-native-firebase/firestore';

const styles = EStyleSheet.create({
  root: {
    backgroundColor: 'white',
  },
  buttonWrapper: {
    marginTop: '50rem',
  },
});

const SettingsContact = ({navigation, user}) => {
  const [subject, setSubject] = useState('');
  const [message, setMessage] = useState('');

  const [isValidaSubject, setIsValidSubject] = useState(true);
  const [isValidMessage, setIsValidMessage] = useState(true);

  useLayoutEffect(() => {
    navigation.setOptions(getDefaultHeader({navigation}));
  }, [navigation]);

  const isMessageValidated = () => {
    let isValid = true;
    if (_.isEmpty(subject)) {
      setIsValidSubject(false);
      isValid = false;
    } else {
      setIsValidSubject(true);
    }

    if (_.isEmpty(message) || _.size(message) === 200) {
      setIsValidMessage(false);
      isValid = false;
    } else {
      setIsValidMessage(true);
    }

    return isValid;
  };

  const handleSendToHelmPressEvent = () => {
    if (!isMessageValidated()) return;

    const messageData = {
      subject,
      message,
      senderUserInfo: user.info,
      businessInfo: user.businessInfo,
      senderUid: user.userId,
      // date: new Date(),
      date: firebase.firestore.FieldValue.serverTimestamp(),
    };

    sendMessageToHelm({message: messageData});
    navigation.navigate('ContactEmailConfirmationScreen');
  };

  const updateStateWithValue = (value, set) => {
    set(value);
  };

  return (
    <View style={[globalStyles.root, styles.root]}>
      <LabeledInputText
        label="Subject"
        value={subject}
        textInputProps={{
          onChangeText: (value) => updateStateWithValue(value, setSubject),
          onFocus: () => setIsValidSubject(true),
        }}
        isValid={isValidaSubject}
      />
      <LabeledInputText
        label="Your Message"
        textInputHeight={100}
        textInputStyle={globalStyles.addressTextInput}
        textInputProps={{
          onChangeText: (value) => updateStateWithValue(value, setMessage),
          onFocus: () => setIsValidMessage(true),
          multiline: true,
          textAlignVertical: 'top',
        }}
        value={message}
        isValid={isValidMessage}
      />
      <Text style={globalStyles.p3}>Character limit: 200</Text>
      <View style={styles.buttonWrapper}>
        <TextButton
          text="SEND TO HELM"
          type="primary"
          onPress={handleSendToHelmPressEvent}
        />
      </View>
    </View>
  );
};

const mapStateToProps = (state) => {
  return {
    user: state.auth.user,
  };
};

export default connect(mapStateToProps)(SettingsContact);
