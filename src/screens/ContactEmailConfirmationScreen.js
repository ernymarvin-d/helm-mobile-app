import React, {useLayoutEffect} from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {PADDING_DEFAULT} from '../assets/dimens';
import {globalStyles} from '../assets/styles/globalStyles';
import helmIcon from '../assets/images/helm_icon_blue.png';
import {GREY, TEXT_SECONDARY} from '../assets/colors';
import EStyleSheet from 'react-native-extended-stylesheet';
import {getWhiteHeader} from '../utils/NavigationUtils';

const styles = EStyleSheet.create({
  root: {
    flexGrow: 1,
  },
  contentWrapper: {
    width: '100%',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: PADDING_DEFAULT,
  },
  box: {
    flex: 1,
    flexDirection: 'column',
    width: '100%',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  logo: {
    width: '60%',
    height: '40%',
  },
  headerText: {
    width: '80%',
    textAlign: 'left',
    marginTop: 20,
    marginBottom: 20,
    color: TEXT_SECONDARY,
  },
});

const message =
  'Hi there, Jeff here. \nGot your message. Thank you for contacting me. I will be back with a reply as soon as I can.\nGood day!';

const ContactEmailConfirmationScreen = ({navigation}) => {
  useLayoutEffect(() => {
    navigation.setOptions(getWhiteHeader({onPressLeftButton}));
  }, [navigation]);

  const onPressLeftButton = () => {
    navigation.navigate('Settings');
  };
  return (
    <SafeAreaView style={globalStyles.root}>
      <View style={styles.contentWrapper}>
        <View style={[styles.box]}>
          <Image
            source={helmIcon}
            style={styles.logo}
            resizeMethod="resize"
            resizeMode="contain"
          />
          <Text style={[globalStyles.h3, styles.headerText]}>{message}</Text>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default ContactEmailConfirmationScreen;
