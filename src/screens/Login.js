import React, {useState, useEffect} from 'react';
import {View, Text, Alert, Platform} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {PADDING_DEFAULT} from '../assets/dimens';
import {globalStyles} from '../assets/styles/globalStyles';
import {TEXT_SECONDARY, BLUE} from '../assets/colors';
import TextButton from '../components/atoms/TextButton';
import ContinueWithText from './../components/atoms/ContinueWithText';
import LabeledInputText from './../components/atoms/LabeledInputText';
import GoogleAndFacebookSigninButton from './../components/molecules/GoogleAndFacebookSigninIButton';
import CheckBox from 'react-native-check-box';
import EStyleSheet from 'react-native-extended-stylesheet';
import auth from '@react-native-firebase/auth';
import _ from 'lodash';
import PleaseWaitDialog from '../components/atoms/PleaseWaitDialog';
import AppleSigninButton from '../components/atoms/AppleSigninButton';
import {ScrollView} from 'react-native-gesture-handler';

const styles = EStyleSheet.create({
  contentWrapper: {
    width: '100%',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: PADDING_DEFAULT,
  },
  box: {
    flex: 1,
    flexDirection: 'column',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  boxBottom: {
    flex: 0.8,
  },
  boxTop: {
    flex: 1,
    justifyContent: 'space-evenly',
  },
  loginComponentWrapper: {
    width: '100%',
    alignItems: 'center',
  },
  resetLinkWrapper: {},
  footer: {
    width: '100%',
    height: 'auto',
    alignSelf: 'flex-end',
  },
  resetPasswordText: {
    fontFamily: 'Montserrat-Medium',
    fontWeight: '700',
    fontSize: '14rem',
  },
  footerText: {
    textAlign: 'center',
    color: TEXT_SECONDARY,
  },
  inputBoxWrapper: {
    width: '100%',
    padding: 0,
    margin: 0,
  },
  checkboxText: {
    color: TEXT_SECONDARY,
    paddingLeft: '5rem',
  },
  formWrapper: {
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  headerText: {
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: '20rem',
    textAlign: 'center',
  },
});

const Login = ({navigation}) => {
  useEffect(() => {
    return () => setShowPleaseWaitDialog(false);
  });

  const [emailAddress, setEmailAddress] = useState('');
  const [password, setPassword] = useState('');
  const [showPleaseWaitDialog, setShowPleaseWaitDialog] = useState(false);

  const _handleResetButtonClick = () =>
    navigation.navigate('ResetPasswordStack');
  const _handleSignupNowButtonClick = () => navigation.navigate('SignupStack');
  const _handleLoginButtonClick = () => {
    if (_.isEmpty(emailAddress) || _.isEmpty(password)) {
      Alert.alert('Helm Signin', 'Email address and password is required.');
      return;
    }
    setShowPleaseWaitDialog(true);
    auth()
      .signInWithEmailAndPassword(emailAddress, password)
      .then(() => {})
      .catch((error) => {
        setShowPleaseWaitDialog(false);
        Alert.alert('Helm Signin', error.userInfo.message);
      });
  };

  return (
    <SafeAreaView style={globalStyles.root}>
      <ScrollView contentContainerStyle={{flexGrow: 1}}>
        <View style={styles.contentWrapper}>
          <View style={[styles.box]}>
            <View style={[globalStyles.fullWidth, styles.boxTop]}>
              <Text style={[globalStyles.h2, styles.headerText]}>Sign In</Text>
              <View style={styles.formWrapper}>
                <LabeledInputText
                  type="emailAddress"
                  label="Email"
                  name="email"
                  placeholder="Your Email Address"
                  value={emailAddress}
                  textInputProps={{
                    onChangeText: (value) => setEmailAddress(value),
                  }}
                />
                <LabeledInputText
                  type="password"
                  label="Password"
                  name="password"
                  placeholder="Password"
                  value={password}
                  textInputProps={{
                    onChangeText: (value) => setPassword(value),
                  }}
                />
                <View style={globalStyles.checkboxWrapper}>
                  <CheckBox
                    onClick={() => {}}
                    checkBoxColor={TEXT_SECONDARY}
                    isChecked
                  />
                  <Text style={[globalStyles.p2, styles.checkboxText]}>
                    Remember login info
                  </Text>
                </View>
              </View>
            </View>
          </View>
          <View style={[styles.box, styles.boxBottom]}>
            <View style={styles.loginComponentWrapper}>
              <TextButton
                width="100%"
                text="LOGIN"
                type="primary"
                onPress={_handleLoginButtonClick}
              />
              {Platform.OS === 'ios' ? (
                <View
                  style={{width: '100%', marginTop: EStyleSheet.value('8rem')}}>
                  <AppleSigninButton />
                </View>
              ) : null}
              <ContinueWithText />
              <GoogleAndFacebookSigninButton />
            </View>
            <View style={styles.footer}>
              <View style={styles.resetLinkWrapper}>
                <TextButton
                  type="link"
                  text="Reset Password"
                  width="100%"
                  textStyle={styles.resetPasswordText}
                  onPress={_handleResetButtonClick}
                />
              </View>
              <Text style={[globalStyles.h3, styles.footerText]}>
                <Text>I dont have an account. </Text>
                <Text
                  style={globalStyles.link}
                  onPress={_handleSignupNowButtonClick}>
                  Sign up Now!
                </Text>
              </Text>
            </View>
          </View>
        </View>
        <PleaseWaitDialog showDialog={showPleaseWaitDialog} />
      </ScrollView>
    </SafeAreaView>
  );
};

export default Login;
