import React, {useLayoutEffect, useState, useEffect} from 'react';
import {View, Text, ScrollView, Platform, Linking} from 'react-native';
import {getDefaultHeader} from '../utils/NavigationUtils';
import {globalStyles} from '../assets/styles/globalStyles';
import GrayRoundedBox from '../components/atoms/GrayRoundedBox';
import EStyleSheet from 'react-native-extended-stylesheet';
import {BLUE, GREY} from '../assets/colors';
import {MARGIN_DEFAULT, MARGIN_SMALL} from '../assets/dimens';
import Note from '../components/atoms/Note';
import UpgradeBox from '../components/organisms/UpgradeBox';
import * as RNIap from 'react-native-iap';
import {getAllPlans} from '../utils/DbUtils';
import _ from 'lodash';
import {connect} from 'react-redux';
import {PLAN_TYPE} from '../assets/Constants';
import {TouchableOpacity} from 'react-native-gesture-handler';

const styles = EStyleSheet.create({
  headerWrapper: {
    marginVertical: '40rem',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  headerLine: {
    width: '35rem',
    height: '2rem',
    backgroundColor: GREY[5],
  },
  headerText: {
    marginHorizontal: MARGIN_SMALL,
  },
  hasMarginBottom: {
    marginBottom: '20rem',
  },
});

// const MOCK_SKU = ['helm_pro_2', 'helm_pro_plus_2'];

const ProfileMyPlan = ({navigation, planType}) => {
  useLayoutEffect(() => {
    navigation.setOptions(getDefaultHeader({navigation}));
  }, [navigation]);

  const [subscriptions, setSubscriptions] = useState([]);
  const [plans, setPlans] = useState([]);
  const [itemSku, setItemSku] = useState([]);
  const [useDbSkuField, setUseDbSkuField] = useState([]);

  useEffect(() => {
    const promise = [];
    getAllPlans().then((plans) => {
      console.log('plans', plans);
      setPlans(plans);
      const skus = getItemSku(plans);
      getSubscriptions(skus);
    });
  }, []);

  const handleOnPressUpgradeButton = () =>
    navigation.navigate('MyPlanPaymentFlow');

  const getItemSku = (plans) => {
    console.log(plans);
    const skuFields = Platform.select({
      ios: 'iosSku',
      android: 'androidSku',
    });
    setUseDbSkuField(skuFields);
    const itemSkus = [];
    _.forEach(plans, (plan) => {
      itemSkus.push(plan[skuFields]);
    });
    console.log(itemSkus);
    setItemSku(itemSkus);
    return itemSkus;
  };

  const getSubscriptions = async (skus) => {
    console.log({skus});
    try {
      const allSubs = await RNIap.getSubscriptions(skus);
      console.log({allSubs});
      setSubscriptions(allSubs);
    } catch (err) {
      console.log(err); // standardized err.code and err.message available
    }
  };

  const requestSubscription = async (sku) => {
    console.log('request subscription', {sku});
    try {
      await RNIap.requestSubscription(sku);
    } catch (err) {
      console.log(err.code, err.message);
    }
  };

  const renderPlanNoteMessage = () => {
    const unSubLink = Platform.select({
      ios: 'https://apps.apple.com/account/subscriptions',
      android:
        'https://play.google.com/store/account/subscriptions?package=com.helmapp',
    });
    let message =
      'Currently your are using our free plan. \nYou can upgrade below.';
    let footerNote = (
      <TouchableOpacity onPress={() => Linking.openURL(unSubLink)}>
        <Text style={{textDecorationLine: 'underline'}}>
          Manage Your Subscription
        </Text>
      </TouchableOpacity>
    );
    if (planType === PLAN_TYPE.pro) {
      message =
        'Congratulations your are using our PRO plan. \n\nUpgrade to HELM PRO PLUS now to view tutorials.';
    } else if (planType === PLAN_TYPE.proPlus) {
      message =
        'Congratulations for using our PRO PLUS plan. \n\nYou can now use the app without restriction.';
    } else {
      footerNote = null;
    }

    return (
      <Note
        message={message}
        success={planType !== PLAN_TYPE.free}
        footerNote={footerNote}
      />
    );
  };

  const freePlanBox = () => {
    return (
      <UpgradeBox
        title={''}
        price={'FREE'}
        paymentTerms={'You are currently on free version'}
        checklist={[
          'Limited to 3 Deal options',
          'Download LOI as PDF',
          'Limited access to tutorial',
        ]}
        primary={true}
        containerStyle={styles.hasMarginBottom}
      />
    );
  };

  const renderPlanBox = () => {
    if (planType === PLAN_TYPE.free) {
      const planBox = [];
      planBox.push(
        <View style={styles.headerWrapper}>
          <View style={styles.headerLine} />
          <Text style={[globalStyles.h3, styles.headerText]}>OUR PLAN</Text>
          <View style={styles.headerLine} />
        </View>,
      );
      const upgradeBox = plans.map((plan) => (
        <UpgradeBox
          title={plan.title}
          price={`$${plan.price}`}
          paymentTerms={plan.paymentTerms}
          checklist={plan.checklist}
          // primary={plan.primary || false}
          containerStyle={styles.hasMarginBottom}
          onPressUpgradeButton={() => requestSubscription(plan[useDbSkuField])}
        />
      ));
      return [...planBox, ...[freePlanBox()], ...upgradeBox];
    } else if (planType === PLAN_TYPE.pro) {
      const planBox = [];
      planBox.push(
        <View style={styles.headerWrapper}>
          <View style={styles.headerLine} />
          <Text style={[globalStyles.h3, styles.headerText]}>OUR PLAN</Text>
          <View style={styles.headerLine} />
        </View>,
      );
      const upgradeBox = [];
      _.forEach(plans, (plan) => {
        if (_.toLower(plan.title) === PLAN_TYPE.pro) return;
        upgradeBox.push(
          <UpgradeBox
            title={plan.title}
            price={`$${plan.price}`}
            paymentTerms={plan.paymentTerms}
            checklist={plan.checklist}
            // primary
            containerStyle={styles.hasMarginBottom}
            onPressUpgradeButton={() =>
              requestSubscription(plan[useDbSkuField])
            }
          />,
        );
      });
      return [...planBox, ...upgradeBox];
    }
  };

  return (
    <ScrollView>
      <View style={globalStyles.root}>
        <View>
          {renderPlanNoteMessage()}
          {renderPlanBox()}
        </View>
      </View>
    </ScrollView>
  );
};

const mapStateToProps = (state) => {
  console.log({state});
  return {
    planType: state.planType,
  };
};

export default connect(mapStateToProps)(ProfileMyPlan);
