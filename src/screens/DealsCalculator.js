import React, {useLayoutEffect} from 'react';
import {View, Text} from 'react-native';
import {getDefaultHeader} from '../utils/NavigationUtils';

const DealsCalculator = ({navigation}) => {
  useLayoutEffect(() => {
    navigation.setOptions(getDefaultHeader({navigation}));
  }, [navigation]);

  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text>DealsCalculator</Text>
    </View>
  );
};

export default DealsCalculator;
