import React, {useLayoutEffect} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import SellerInformationForm from './../components/organisms/SellerInformationForm';
import {getDefaultHeader} from '../utils/NavigationUtils';
import EStyleSheet from 'react-native-extended-stylesheet';
import {connect} from 'react-redux';

const styles = EStyleSheet.create({
  root: {
    flex: 1,
    flexGrow: 1,
    justifyContent: 'space-between',
    backgroundColor: 'white',
  },
});

const DealsSellerInformation = ({route, navigation, myDeals}) => {
  // console.log('AAAAA', {myDeals});
  const {
    sellerName,
    emailAddress,
    phoneNumber,
    propertyAddress,
  } = route.params.sellerInformation;

  console.log('aaa', route.params.sellerInformation);

  useLayoutEffect(() => {
    navigation.setOptions(getDefaultHeader({navigation}));
  }, [navigation]);

  const handleOnPressSubmitButton = (formData) => {
    navigation.navigate('DealsLoi', {
      sellerInformation: formData,
      propertyInformation: route.params.propertyInformation,
    });
  };

  return (
    <SafeAreaView style={styles.root}>
      <KeyboardAwareScrollView>
        <SellerInformationForm
          emailAddressDefaultValue={emailAddress}
          phoneNumberDefaultValue={phoneNumber}
          sellerNameDefaultValue={sellerName}
          propertyAddressDefaultValue={propertyAddress}
          onPressSubmitButton={handleOnPressSubmitButton}
          submitButtonText={'Generate Proposal'}
        />
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};

const mapStateToProps = (state) => {
  return {
    myDeals: state.auth.myDeals,
  };
};

export default connect(mapStateToProps)(DealsSellerInformation);
