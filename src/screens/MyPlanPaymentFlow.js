import React, {useLayoutEffect} from 'react';
import {View, Text, ScrollView} from 'react-native';
import {getWhiteHeader} from '../utils/NavigationUtils';
import {globalStyles} from '../assets/styles/globalStyles';
import PlanInvoiceBox from '../components/organisms/PlanInvoiceBox';
import EStyleSheet from 'react-native-extended-stylesheet';
import TextButton from '../components/atoms/TextButton';
import RbOptions from '../components/molecules/RbOptions';
import {BLUE} from '../assets/colors';

const paymentOptions = [
  {label: 'Paypal', vlaue: 0},
  {label: 'Visa', vlaue: 1},
  {label: 'American Express', vlaue: 2},
];

const styles = EStyleSheet.create({
  paymentOptionWrapper: {
    marginVertical: '50rem',
  },
  subHeader: {
    color: BLUE,
    fontWeight: '600',
  },
  header: {
    fontWeight: 'bold',
  },
  optionsWrapper: {
    marginVertical: '30rem',
  },
});

const MyPlanPaymentFlow = ({navigation}) => {
  useLayoutEffect(() => {
    navigation.setOptions(getWhiteHeader({navigation}));
  }, [navigation]);

  const handleOnPressPayButton = () =>
    navigation.navigate('MyPlanPaymentConfirmation');

  return (
    <ScrollView style={[globalStyles.root]}>
      <View>
        <PlanInvoiceBox />
        <View style={styles.paymentOptionWrapper}>
          <Text style={[globalStyles.h3, styles.header]}>Payment Options:</Text>
          <Text style={[globalStyles.p2, styles.subHeader]}>
            Add a new credit/debit card
          </Text>
          <View style={styles.optionsWrapper}>
            <RbOptions options={paymentOptions} />
          </View>
          <TextButton
            text="Pay $29.00"
            type="primary"
            onPress={handleOnPressPayButton}
          />
        </View>
      </View>
    </ScrollView>
  );
};

export default MyPlanPaymentFlow;
