import * as React from 'react';
import {View, Text, StyleSheet, Image, Platform} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import GoogleIconButton from '../components/atoms/GoogleIconButton';
import FacebookIconButton from '../components/atoms/FacebookIconButton';
import {PADDING_DEFAULT} from '../assets/dimens';
import {globalStyles} from '../assets/styles/globalStyles';
import helmIcon from '../assets/images/helm_icon_blue.png';
import {GREY, TEXT_SECONDARY} from '../assets/colors';
import TextButton from '../components/atoms/TextButton';
import ContinueWithText from '../components/atoms/ContinueWithText';
import GoogleAndFacebookSigninButton from '../components/molecules/GoogleAndFacebookSigninIButton';
import EStyleSheet from 'react-native-extended-stylesheet';
import AppleSigninButton from '../components/atoms/AppleSigninButton';
import {ScrollView} from 'react-native-gesture-handler';

const styles = EStyleSheet.create({
  root: {
    flexGrow: 1,
  },
  contentWrapper: {
    width: '100%',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: PADDING_DEFAULT,
  },
  box: {
    flex: 1,
    flexDirection: 'column',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  boxBottom: {justifyContent: 'flex-start'},
  boxTop: {
    flex: 1.5,
  },
  logo: {
    width: '60%',
    height: '40%',
  },
  headerText: {
    width: '100%',
    textAlign: 'center',
    marginTop: 40,
    marginBottom: 20,
    color: TEXT_SECONDARY,
  },
  loginComponentWrapper: {
    height: '75%',
    width: '100%',
    alignItems: 'center',
    marginBottom: '1rem',
  },
  divider: {
    width: 140,
    height: 1,
    borderTopWidth: 2,
    borderTopColor: GREY[4],
  },
  signupButtonWrapper: {
    width: '100%',
    height: Platform.OS === 'ios' ? '150rem' : '105rem',
    justifyContent: 'space-between',
  },
});

const AuthSelection = ({navigation}) => {
  const _handleLoginButtonClick = () => navigation.navigate('Login');
  const _handleSignupNowButton = () => navigation.navigate('SignupStack');

  return (
    <SafeAreaView style={globalStyles.root}>
      <ScrollView contentContainerStyle={{flexGrow: 1}}>
        <View style={styles.contentWrapper}>
          <View style={[styles.box, styles.boxTop]}>
            <Image
              source={helmIcon}
              style={styles.logo}
              resizeMethod="resize"
              resizeMode="contain"
            />
            <Text style={[globalStyles.h3, styles.headerText]}>
              Increase sales in business and engage with customers
            </Text>
          </View>
          <View style={[styles.box, styles.boxBottom]}>
            <View style={styles.loginComponentWrapper}>
              <View style={styles.signupButtonWrapper}>
                <TextButton
                  width="100%"
                  text="SIGN UP"
                  type="primary"
                  onPress={_handleSignupNowButton}
                />
                <TextButton
                  width="100%"
                  text="LOGIN"
                  type="secondary"
                  onPress={_handleLoginButtonClick}
                />
                {Platform.OS === 'ios' ? <AppleSigninButton /> : null}
              </View>
              <ContinueWithText />
              <GoogleAndFacebookSigninButton />
            </View>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default AuthSelection;
