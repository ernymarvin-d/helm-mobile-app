import React from 'react';
import {Image, Text, View} from 'react-native';
import successIcon from './../assets/images/success.png';
import {globalStyles} from '../assets/styles/globalStyles';
import EStyleSheet from 'react-native-extended-stylesheet';
import {BLUE, TEXT_SECONDARY} from '../assets/colors';
import TextButton from '../components/atoms/TextButton';
import {connect} from 'react-redux';
import {resetDealsOptionState} from '../state-management/actions/DealsAction';
import {useEffect} from 'react';

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    width: '100%',
    alignItems: 'center',
  },
  icon: {
    width: '25%',
    height: '13%',
    tintColor: BLUE,
  },
  messageWrapper: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: '50rem',
  },
  footerText: {
    textAlign: 'center',
    color: TEXT_SECONDARY,
  },
  btnBackToMain: {
    marginTop: '16rem',
  },
});

const CreateDealsSuccess = ({route, navigation, resetDealsOptionState}) => {
  useEffect(() => {
    resetDealsOptionState();
  }, []);

  const _handleOnBackToMainButtonPressed = () => {
    navigation.reset({
      index: 0,
      routes: [{name: 'HomeTab'}],
    });
  };
  const {sellerInformation} = route.params;
  //   console.log({sellerInformation});
  const handleSellerEmailOnPress = () => navigation.navigate('HomePage');
  return (
    <View style={[globalStyles.root, globalStyles.backgroundGrey]}>
      <View style={styles.container}>
        <Image source={successIcon} style={styles.icon} resizeMode="contain" />
        <View style={styles.messageWrapper}>
          <Text style={[globalStyles.p1, styles.message]}></Text>
          <Text style={[globalStyles.h3, styles.footerText]}>
            <Text>Your proposal was successfully sent to </Text>
            <Text style={globalStyles.link} onPress={handleSellerEmailOnPress}>
              {sellerInformation.emailAddress}
            </Text>
          </Text>
          <TextButton
            style={styles.btnBackToMain}
            text="Back to Main"
            type="link"
            width="100%"
            onPress={_handleOnBackToMainButtonPressed}
          />
        </View>
      </View>
    </View>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    resetDealsOptionState: () => dispatch(resetDealsOptionState()),
  };
};

const mapStateToProps = (state) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateDealsSuccess);
