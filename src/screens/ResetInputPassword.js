import React from 'react';
import {View, Text, SafeAreaView} from 'react-native';
import {globalStyles} from '../assets/styles/globalStyles';
import EStyleSheet from 'react-native-extended-stylesheet';
import {PADDING_DEFAULT} from '../assets/dimens';
import PasswordTextInput from './../components/atoms/PasswordTextInput';
import TextButton from './../components/atoms/TextButton';

const styles = EStyleSheet.create({
  contentWrapper: {
    width: '100%',
    height: '75%',
    padding: PADDING_DEFAULT,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  headerWrapper: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: '70rem',
  },
  header: {
    marginVertical: '10rem',
  },
  subHeader: {textAlign: 'center', lineHeight: 25, width: '75%'},
  boxWrapper: {
    width: '100%',
  },
  buttonWrapper: {
    marginTop: '20rem',
  },
});

export default ResetInputPassword = ({navigation}) => {
  const _handleSetPasswordButtonClick = () =>
    navigation.navigate('ResetPasswordSuccess');
  return (
    <SafeAreaView style={globalStyles.root}>
      <View style={styles.contentWrapper}>
        <View style={styles.headerWrapper}>
          <Text style={[globalStyles.h2, styles.header]}>Reset Password</Text>
        </View>
        <View style={styles.boxWrapper}>
          <PasswordTextInput label="Password" placeholder="******" />
          <PasswordTextInput label="Confirm Password" placeholder="******" />
          <View style={styles.buttonWrapper}>
            <TextButton
              width="100%"
              text="SET AS NEW PASSWORD"
              type="primary"
              onPress={_handleSetPasswordButtonClick}
            />
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};
