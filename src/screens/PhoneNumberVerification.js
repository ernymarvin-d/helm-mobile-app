import React, {useState, useEffect} from 'react';
import {View, Text, Image, Alert, KeyboardAvoidingView} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import EStyleSheet from 'react-native-extended-stylesheet';
import CountryPicker from 'react-native-country-picker-modal';
import auth from '@react-native-firebase/auth';
import _ from 'lodash';

import {globalStyles} from '../assets/styles/globalStyles';
import {GREY, BLUE} from '../assets/colors';
import smsPhoneIcon from './../assets/images/phone-verification-logo.png';
import {PADDING_DEFAULT} from '../assets/dimens';
import NumberCountryHolder from '../components/atoms/NumberCountryHolder';
import TextButton from './../components/atoms/TextButton';
import VerifyPhoneNumberModal from './../components/organisms/VerifyPhoneNumberModal';
import LabeledInputText from '../components/atoms/LabeledInputText';
import PleaseWaitDialog from '../components/atoms/PleaseWaitDialog';
import {
  UserDbCollection,
  addUserData,
  observeUserData,
  getUserData,
} from '../utils/DbUtils';
import {useCallback} from 'react';
import {useLayoutEffect} from 'react';
import {DEFAULT_USER_DEALS_OPTION_SETTINGS} from '../assets/Constants';
import {loginUser} from '../state-management/actions/AuthActions';
import {connect} from 'react-redux';

// {
//     "cca2":"AS",
//     "currency":["USD"],
//     "callingCode":["1684"],
//     "region":"Oceania",
//     "subregion":"Polynesia",
//     "flag":"flag-as",
//     "name":"American Samoa"
// }

const styles = EStyleSheet.create({
  root: {
    flexGrow: 1,
    padding: 0,
  },
  contentWrapper: {
    width: '100%',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  box: {
    flex: 1,
    flexDirection: 'column',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  boxBottom: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: PADDING_DEFAULT,
  },
  boxTop: {
    flex: 1,
    backgroundColor: GREY[3],
  },
  smsLogoWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '200rem',
    height: '200rem',
    borderRadius: '200rem',
    backgroundColor: 'white',
  },
  formWrapper: {
    flex: 1,
    width: '100%',
    justifyContent: 'space-between',
  },
  header: {
    // marginVertical: '4rem',
  },
  inputWrapper: {
    height: '110rem',
    justifyContent: 'space-between',
  },
  footerText: {
    fontSize: '20rem',
  },
  containerButtonStyle: {height: 1, width: 1},
});

const INVALID_PHONE_NUMBER = 'Invalid phone number.';
const REQUIRED_PHONE_NUMBER = 'Phone number is required.';

const PhoneNumberVerification = ({route, navigation, loginUser}) => {
  const {userDbData, password} = route.params;
  const [showVerifyModal, setShowVerifyModal] = useState(false);
  const [showCountryPicker, setShowCountryPicker] = useState(false);
  const [selectedCountry, setSelectedCountry] = useState({
    cca2: 'US',
    currency: ['USD'],
    callingCode: ['1'],
    region: 'Americas',
    subregion: 'North America',
    flag: 'flag-us',
    name: 'United States',
  });
  const [selectedCountryValid, setSelectedCountryValid] = useState(true);
  const [phoneNumber, setPhoneNumber] = useState('');
  const [phoneNumberIsValid, setPhoneNumberIsValid] = useState(true);
  const [phoneNumberErrorText, setPhoneNumberErrorText] = useState(null);
  const [phoneConfirmation, setPhoneConfirmation] = useState(null);
  const [showPleaseWaitDialog, setShowPleaseWaitDialog] = useState(false);
  const [unsubscribeToAuthState, setUnsubscribeToAuthState] = useState(null);

  console.log({userDbData, password});

  //   userDbData:
  //   businessInfo:
  //   address: "G"
  //   businessName: "G"
  //   phoneNumber: "234"
  //   websiteAddress: "Tg"
  //   __proto__: Object
  //   info:
  //   emailAddress: "Hr@hd.com"
  //   fullName: "Hd"

  useEffect(() => {
    const unsubscribe = auth().onAuthStateChanged(onAuthStateChanged);
    return () => unsubscribe();
  }, [selectedCountry, phoneNumber]);

  const onAuthStateChanged = (user) => {
    console.log('PHN auth state change', user);
    if (user) {
      const afterLoginPromise = [];
      afterLoginPromise.push(linkPhoneNumberToUser());
      afterLoginPromise.push(addUserDataToDb(user));
      Promise.all(afterLoginPromise).then((results) => {
        setShowVerifyModal(false);
        showVerificationSuccessScreen();
      });
    }
  };

  const linkPhoneNumberToUser = () => {
    const emailCredentials = auth.EmailAuthProvider.credential(
      userDbData.info.emailAddress,
      password,
    );
    return auth()
      .currentUser.linkWithCredential(emailCredentials)
      .then((userCred) => {
        return;
      })
      .catch((error) => {
        console.log(error.message);
        return;
      });
  };

  const addUserDataToDb = async (user) => {
    const currentUserData = await getUserData(user.uid);
    if (currentUserData.exists) {
      return;
    }
    const {address, businessName, websiteAddress} = userDbData.businessInfo;
    const {emailAddress, fullName} = userDbData.info;
    const userPhoneNumber = `+${selectedCountry.callingCode[0]}${phoneNumber}`;

    const newUser = {
      info: {
        emailAddress,
        fullName,
        userId: auth().currentUser.uid,
        phoneNumber: userPhoneNumber,
      },
      businessInfo: {
        businessName,
        address,
        websiteAddress,
        phoneNumber: userDbData.businessInfo.phoneNumber,
      },
      calculatorGeneralSettings: DEFAULT_USER_DEALS_OPTION_SETTINGS,
    };

    return addUserData(newUser).then(() => {
      return observeUserData({
        user: auth().currentUser,
        stateDispatcher: {loginUser},
      });
    });
  };

  const showVerificationSuccessScreen = () => {
    console.log('shoe verification screen');
    setShowPleaseWaitDialog(false);
    navigation.navigate('PhoneNumberVerificationSuccess', {
      phoneNumber,
      selectedCountry,
    });
  };

  const showCountryPickerModal = () => {
    setShowCountryPicker(true);
  };

  const _handleOnSendCodeButtonPressed = () => {
    if (countryValidated() && phoneNumberValidated()) {
      sendConfirmationCode();
      setShowVerifyModal(!showVerifyModal);
    }
  };

  const _handleModalVerifyButtonPress = (confirmationCode) => {
    console.log({confirmationCode});
    setShowPleaseWaitDialog(true);
    const callback = (error) => {
      console.log('phone verification callback EEE', error);
      if (error !== null) {
        console.log('Phone Verification Failed', error.message);
        setTimeout(() => {
          Alert.alert(
            'Phone Verification',
            'Phone number verification failed.',
          );
        }, 800);
        setShowPleaseWaitDialog(false);
      } else {
        console.log('Phone Verification Successful');
      }
    };
    confirmCode({confirmationCode, callback});
  };

  const _handleModalOnResendButtonPressed = () => sendConfirmationCode();

  const sendConfirmationCode = async () => {
    var callingCode = '';
    if (!_.isUndefined(selectedCountry.callingCode[0])) {
      callingCode = selectedCountry.callingCode[0];
    }
    const trimmedPhoneNumber = phoneNumber.replace(/^0+/, '');
    const phoneNumberToConfirm = `+${callingCode}${trimmedPhoneNumber}}`;

    try {
      const confirmation = await auth().signInWithPhoneNumber(
        phoneNumberToConfirm,
      );
      setPhoneConfirmation(confirmation);
    } catch (err) {
      Alert.alert('Phone Verification', err.message);
    }
  };

  const confirmCode = async ({confirmationCode, callback}) => {
    let error = null;
    try {
      await phoneConfirmation.confirm(confirmationCode);
    } catch (err) {
      error = {};
      error['message'] = 'Invalid code.';
      console.log('invalid code EEEE', {err});
    }
    if (callback) callback(error);
  };

  const phoneNumberValidated = () => {
    if (_.isEmpty(phoneNumber)) {
      setPhoneInputError({message: REQUIRED_PHONE_NUMBER});
      return false;
    }
    if (_.isNumber(phoneNumber)) {
      setPhoneInputError({message: INVALID_PHONE_NUMBER});
      return false;
    }
    return true;
  };

  const setPhoneInputError = ({message}) => {
    setPhoneNumberIsValid(false);
  };

  const countryValidated = () => {
    if (_.isEmpty(selectedCountry)) {
      setSelectedCountryValid(false);
      return false;
    }
    return true;
  };

  const CountryPickerDialog = () => (
    <CountryPicker
      onSelect={(country) => {
        console.log('CountryPicker selected', {country});
        setSelectedCountry(country);
        setSelectedCountryValid(true);
      }}
      translation="eng"
      visible={showCountryPicker}
      countryCode={!_.isEmpty(selectedCountry) ? selectedCountry.cca2 : null}
      country={selectedCountry}
      onClose={() => setShowCountryPicker(false)}
      withFlag={true}
      withCallingCodeButton={true}
      withCountryNameButton={true}
      withCallingCode={true}
      containerButtonStyle={styles.containerButtonStyle}
    />
  );

  return (
    <KeyboardAvoidingView
      style={{flex: 1}}
      behavior={Platform.OS === 'ios' ? 'padding' : null}>
      <SafeAreaView style={[globalStyles.root, styles.root]}>
        <View style={styles.contentWrapper}>
          <View style={[styles.box, styles.boxTop]}>
            <View style={styles.smsLogoWrapper}>
              <Image
                source={smsPhoneIcon}
                style={{tintColor: BLUE}}
                width={'80%'}
                height={'80%'}
              />
            </View>
          </View>
          <View style={[styles.box, styles.boxBottom]}>
            <View style={styles.formWrapper}>
              <Text style={[globalStyles.h2, styles.header]}>
                Phone Verifications
              </Text>
              <View style={styles.inputWrapper}>
                <NumberCountryHolder
                  country={selectedCountry}
                  onPress={showCountryPickerModal}
                  isValid={selectedCountryValid}
                />
                <LabeledInputText
                  placeholder="1234567890"
                  name="phoneNumber"
                  value={phoneNumber}
                  inpu
                  textInputProps={{
                    keyboardType: 'numeric',
                    onChangeText: (text) => {
                      setPhoneNumberIsValid(true);
                      setPhoneNumber(text);
                    },
                  }}
                  isValid={phoneNumberIsValid}
                  errorText={phoneNumberErrorText}
                />
              </View>
              <Text style={[globalStyles.h4]}>
                We will send you a 6-digit code number
              </Text>
              <TextButton
                type="primary"
                text="SEND CODE"
                onPress={_handleOnSendCodeButtonPressed}
              />
            </View>
          </View>
          <VerifyPhoneNumberModal
            dismissModal={() => setShowVerifyModal(false)}
            show={showVerifyModal}
            onVerifyButtonPressed={_handleModalVerifyButtonPress}
            onResendCodePressed={_handleModalOnResendButtonPressed}
          />
          <CountryPickerDialog />
          <PleaseWaitDialog showDialog={showPleaseWaitDialog} />
        </View>
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    loginUser: (user) => dispatch(loginUser(user)),
  };
};

const mapStateToProps = () => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PhoneNumberVerification);
