import {createStore, combineReducers} from 'redux';
import authReducer from './state-management/reducers/AuthReducer';
import dealOptionsReducer from './state-management/reducers/DealOptionsReducer';
import tutorialReducer from './state-management/reducers/TutorialReducer';
import planReducer from './state-management/reducers/PlanTypeReducer';

const rootReducer = combineReducers({
  auth: authReducer,
  dealsCalculator: dealOptionsReducer,
  tutorials: tutorialReducer,
  planType: planReducer,
});

const configureStore = () => createStore(rootReducer);

export default configureStore;
