export const MARGIN_DEFAULT = '16rem';
export const MARGIN_LARGE = '26rem';
export const MARGIN_SMALL = '8rem';

export const PADDING_DEFAULT = '16rem';
export const PADDING_LARGE = '26rem';
export const PADDING_SMALL = '8rem';

export const BUTTON_HEIGHT_DEFAULT = '44rem';
export const INPUT_TEXT_HEIGHT_DEFAULT = '44rem';
export const BUTTON_WIDTH_DEFAULT = '280rem';
export const BUTTON_WIDTH_MEDIUM = '224rem';
export const BUTTON_WIDTH_SMALL = '44rem';
export const BUTTON_HEIGHT_SMALL = '44rem';
export const DEALS_BOX_BORDER_RADIUS = '10rem';
