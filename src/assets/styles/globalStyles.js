import {StyleSheet, Dimensions} from 'react-native';
import {
  GREY,
  BLUE,
  INPUT_TEXT_BORDER_COLOR,
  BOX_BACKGROUND_COLOR_2,
  BOX_BACKGROUND_COLOR,
  BLUE_FADED,
  RED,
  GREEN,
} from '../colors';
import {
  BUTTON_HEIGHT_DEFAULT,
  BUTTON_WIDTH_SMALL,
  INPUT_TEXT_HEIGHT_DEFAULT,
  PADDING_SMALL,
  MARGIN_SMALL,
  PADDING_DEFAULT,
} from '../dimens';
import EStyleSheet from 'react-native-extended-stylesheet';

const entireScreenWidth = Dimensions.get('window').width;
EStyleSheet.build({$rem: entireScreenWidth / 380});

export const globalStyles = EStyleSheet.create({
  rootNoPadding: {
    flexGrow: 1,
    backgroundColor: '#ffffff',
  },
  root: {
    flexGrow: 1,
    backgroundColor: '#ffffff',
    padding: '16rem',
  },
  fullWidth: {
    width: '100%',
  },
  h1: {
    fontFamily: 'Montserrat-Medium',
    fontSize: '30rem',
    color: 'white',
  },
  h2: {
    fontFamily: 'Montserrat-Bold',
    fontSize: '20rem',
    color: GREY[0],
  },
  h3: {
    fontFamily: 'Montserrat-Medium',
    fontSize: '16rem',
    color: GREY[0],
    lineHeight: '30rem',
  },
  h4: {
    fontFamily: 'Montserrat-Medium',
    fontSize: '14rem',
    color: GREY[0],
    lineHeight: 30,
  },
  p1: {
    fontFamily: 'Montserrat-Regular',
    fontSize: '14rem',
    color: GREY[1],
    lineHeight: '22rem',
  },
  p2: {
    fontFamily: 'Montserrat-Regular',
    fontSize: '13rem',
    color: GREY[1],
    lineHeight: '20rem',
  },
  p3: {
    fontFamily: 'Montserrat-Regular',
    fontSize: '10rem',
    color: GREY[1],
  },
  btnText: {
    color: 'white',
    fontFamily: 'Montserrat-Medium',
    fontSize: '13rem',
    textAlign: 'center',
  },
  btnSmall: {
    borderRadius: 10,
    height: BUTTON_HEIGHT_DEFAULT,
    width: BUTTON_WIDTH_SMALL,
    fontFamily: 'Montserrat-Medium',
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnMedium: {
    borderRadius: 3,
    fontFamily: 'Montserrat-Medium',
    fontSize: '13rem',
    flex: 1,
    height: BUTTON_HEIGHT_DEFAULT,
  },
  btnLarge: {
    borderRadius: 3,
    fontFamily: 'Montserrat-Medium',
    fontSize: '13rem',
    height: BUTTON_HEIGHT_DEFAULT,
  },
  link: {color: BLUE},
  checkboxWrapper: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  inputText: {
    width: '100%',
    backgroundColor: BOX_BACKGROUND_COLOR_2,
    borderRadius: 3,
    borderColor: INPUT_TEXT_BORDER_COLOR,
    borderWidth: 1,
    padding: 0,
    margin: 0,
    paddingHorizontal: '20rem',
    height: INPUT_TEXT_HEIGHT_DEFAULT,
    fontFamily: 'Montserrat-Regular',
  },
  textInputDisabled: {
    borderColor: '#ded4d7',
    backgroundColor: '#EDEEF0',
    color: '#545557',
  },
  textInputGray: {
    borderColor: '#ded4d7',
    backgroundColor: '#EDEEF0',
    color: '#545557',
  },
  textInputOrange: {
    borderColor: '#F7CCB3',
    backgroundColor: '#FFF7F4',
    color: '#545557',
  },
  textInputRed: {
    borderColor: RED,
    backgroundColor: '#ff7961',
    color: '#545557',
  },
  textInputGreen: {
    borderColor: GREEN,
    backgroundColor: '#d7ffd9',
    color: '#545557',
  },
  inputTextLabel: {
    width: '100%',
    textAlign: 'left',
    alignSelf: 'flex-start',
    marginBottom: '10rem',
    fontFamily: 'Montserrat-Bold',
    fontSize: '14rem',
    color: GREY[0],
  },
  backgroundGrey: {
    backgroundColor: GREY[3],
  },
  addressTextInput: {
    height: '100rem',
    paddingTop: '10rem',
    fontFamily: 'Montserrat-Regular',
  },
  divider: {marginBottom: 30},
  rightButtonText: {
    backgroundColor: BLUE_FADED,
    height: '28rem',
    marginRight: PADDING_DEFAULT,
    paddingHorizontal: PADDING_DEFAULT,
  },
});

export const calculatorLayoutStyles = EStyleSheet.create({
  calculatorHeaderWrapper: {
    borderBottomWidth: 1,
    borderBottomColor: '#E5E6E8',
    paddingBottom: PADDING_SMALL,
    marginBottom: MARGIN_SMALL,
    width: '100%',
  },
  calculatorBox: {
    backgroundColor: BOX_BACKGROUND_COLOR,
    padding: PADDING_DEFAULT,
  },
  calculatorTitle: {
    fontWeight: 'bold',
  },
  calculatorEntryBox: {
    borderRadius: '5rem',
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: GREY[4],
    padding: PADDING_DEFAULT,
  },
});

export const modalStyles = EStyleSheet.create({
  root: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    alignSelf: 'center',
    backgroundColor: GREY[3],
    marginTop: '-10rem',
  },
  modalView: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: '90%',
    backgroundColor: 'white',
    borderRadius: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    height: '75%',
  },
  asModal: {
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
});
