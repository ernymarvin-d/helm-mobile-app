import moment from 'moment';
import {
  toStringMoneyValue,
  decimalValuetoPercentage,
} from '../utils/OptionsCalculatorUtil';
import {ValueType} from './Constants';

const generateLoi = ({
  sellerInformation,
  selectedDealOptions,
  propertyInformation,
  user,
  calculatorGeneralSettings,
}) => {
  //   console.log({sellerInformation, user, selectedDealOptions});
  console.log({selectedDealOptions, calculatorGeneralSettings});
  const {
    emailAddress,
    phoneNumber,
    propertyAddress,
    sellerName,
  } = sellerInformation;

  const {info, businessInfo} = user;

  const generateOptionText = (option) => {
    const showToSeller = option.showToSeller;

    if (showToSeller) {
      let toValue = '';
      switch (option.type) {
        case ValueType.price: {
          toValue = toStringMoneyValue(option.value);
          break;
        }
        case ValueType.percentage: {
          toValue = `${decimalValuetoPercentage(option.value)}%`;
          break;
        }
        case ValueType.year: {
          toValue = `${option.value} Years`;
        }
      }
      return `<p>● ${option.label}: ${toValue}</p>`;
    }

    return '';
  };

  let optionText = '';
  let counter = 1;
  selectedDealOptions.forEach((option) => {
    console.log('HRHRHRHRHR', {option});
    optionText += `<p></p><p><b><u>OPTION ${counter} - ${option.title}</u></b></p><p></p>`;
    Object.keys(option.values).forEach((key) => {
      const optionResult = option.values[key];
      optionText += generateOptionText(optionResult);
    });
    Object.keys(option.settings).forEach((key) => {
      const settingsResult = option.settings[key];
      optionText += generateOptionText(settingsResult);
    });
    counter++;
  });

  console.log({optionText});

  return `
<p>${info.fullName}</p>
<p>${businessInfo.address}</p>
<p>${businessInfo.phoneNumber}</p>
<p></p>
<p>Date: ${moment().format('MM/DD/YYYY')}</p>
<p></p>
<p>Mr/Mrs. ${sellerName}</p>
<p></p>
<p>RE: ${propertyAddress}</p>
<p></p>
<p>Dear Mr/Mrs. ${sellerName},</p>
<p></p>
<p>It was nice talking with you about your house at ${propertyAddress}.</p>
<p></p>
<p>After discussing your needs and researching the property and market we have developed a
strategic financing solution with a short term, that will allow you to dispose of your property for
above retail price. Please review our best purchase option we can offer to buy your house.</p>
<p></p>
<p>Call me if you have any questions please give me a call.</p>
<p></p>
<p>If this offer doesn’t work for you right now, that’s okay. Please keep this letter for future reference.</p>
<p></p>
<p>Sincerely,</p>
<p></p>
<p>${info.fullName}</p>
<p>${businessInfo.businessName}</p>
<br /><br />
<h4 style="text-align: center"><em><b>“We can buy your house ‘as-is’, for a fair price, on any date you choose.”</b></em></h4>
<br /><br />
<h2 style="text-align: center"><u>LETTER OF INTENT -MULTIPLE OPTIONS</u></h2>
<br /><br />
<p><b>Date: ${moment().format('MM/DD/YYYY')}</b></p>
<p></p>
<p><b>Property Address: ${propertyAddress}</b></p>
<p></p>
<p>We can buy your home any of the following ways, and close through a real estate attorney or
title company.</p>
${optionText}
<p></p>
<hr />
<p></p>
<p>This letter of intent is contingent upon JR Capital LLC performing due diligence which may
include property inspection, city inspection, rental inspection, pest inspection and any other
necessary evaluations.</p>
<p></p>
<p>Option That Most Interests You: _________</p>
<p>Seller(s) Acceptance: _________________________________ Date: __________</p>
<p></p>
<p>Thank you for taking the time to explore our Lease Option and Land Contract program. We
hope you see the benefits of PRICE, CONVENIENCE, and SECURITY that we have worked to 
create in our program. We look forward to working with you soon and making the sale of your
home a stress-free and easy one. The following will explain the benefits of selling on lease
option or land contract.</p>
<p></p>
<p><b>Call me if you have any questions and want to discuss your options @ ${
    businessInfo.phoneNumber
  }</b></p>
<br /><br />
<h2 style="text-align: center">Why is a LEASE OPTION is a Great Alternative to Selling a Home?</h2>
<br /><br />
<p>Our fast, easy approach to home selling has helped hundreds of people in the Detroit Area.<p>
<p></p>
<p><b>How can it help you?</b></p>
<p></p>
<p>Our objective is to make this as SIMPLE AS POSSIBLE for you so that you can move on with
your life, and quit worrying about your house.</p>
<p></p>
<p>It is our goal to ensure the process of selling your home is a smooth and easy one for you. We
strive to achieve a win-win-win situation in all our lease option purchases.<p>
<p></p>
<p>You, as the seller of the house, win by receiving the assurance of knowing that while the house
is being sold, all of the expenses are taken care of, the house is maintained, and you are
receiving all of the tax benefits associated with being a landlord, with none of the management
responsibilities.</p>
<p></p>
<p>Our tenant/buyer wins by getting to own a home in a shorter amount of time than it would take
them if they were trying to buy conventionally, as well as being able to immediately occupy their
new home, while qualifying for a loan. We have created a system that creates the paperwork
that mortgage lenders look for when qualifying people for loans to assist our tenant/buyers. In
addition, we give our tenant/buyers immediate credit tips to assist in the loan qualification
process.</p>
<p></p>
<p>We win by profiting from the sale of the home, as well as gaining two more satisfied customers.
The reason that we are in business, of course, is to make a profit, as well as to use our
expertise in real estate transactions to assist homebuyers and sellers in making the sale or
purchase of their home an easy, stress-free one.<p>
<br /><br />
<h2 style="text-align: center">Summary of Benefits For You:</h2>
<br /><br />
<p>● No more vacancies</p>
<p>● We take the house in an as-is condition</p>
<p>● Save or repair your credit</p>
<p>● Immediate debt relief</p>
<p>● We make your payments, guaranteed.</p>
<p>● No more missed or late mortgage payments</p>
<p>● Longer than a normal lease period</p>
<p>● Automatic principal reduction in your mortgage</p>
<p>● No management / rental headaches.</p>
<p>● No more minor repairs</p>
<p>● Opportunity for significant increase in total income</p>
<p>● You retain all property tax benefits</p>
<p>● You may qualify for additional tax deductions (including depreciation, improvements, and repairs)</p>
<p>● Possibly decrease tax liability by turning short term capital gain into long term capital gain (lower tax rate)</p>
<br /><br />
<h2 style="text-align: center">Frequently Asked Questions… Lease Option</h2>
<br /><br />
<p><b>What is a Lease Option?</b></p>
<p></p>
<p>A lease option is essentially a purchase contract combined with a rental agreement. The buyer
leases the property for a specified period of time and then has the option of purchasing the
property before the end of the lease agreement. Sales price, length of rental, closing costs, and
maintenance are all negotiated much the same as a conventional real estate transaction. A
lease option, if properly utilized, is both a seller and a buyer’s dream come true because it can
eliminate many of the negatives normally attributed to the selling and buying of a home.</p>
<p></p>
<p><b>How does it work?</b></p>
<p></p>
<p>We present a monthly lease amount as well as a pre-determined sales price that is agreed to by
all parties. A lease option / purchase is basically a monthly lease set up over a pre-determined
period of time. At the end of that time, we purchase the home from you for whatever the predetermined price is.</p>
<p></p>
<p>The basics are simple: </p>
<p></p>
<p>● You rent your property to JR Capital LLC</p>
<p>● You give JR Capital LLC the right to buy your property for a set price.</p>
<p>● JR Capital LLC places a tenant/buyer in your property.</p>
<p>● The tenant/buyer receives the right to buy your property at or above your agreed price.</p>
<p></p>
<p><b>How much is this going to cost me?</b></p>
<p></p>
<p>The answer is simple. It doesn’t cost you anything. We make our money from our
tenant/buyers, not from you. If you decide to do a lease option with us, you do not have to pay
us a commission.</p>
<p></p>
<p><b>What are the advantages of selling my home by lease option over listing it with a
Realtor?</b></p>
<p></p>
<p>We make your monthly payments while a tenant/buyer is qualifying for a loan. All your expenses
related to the house are taken care of.<p>
<p></p>
<p>Another advantage is that our objective is to put only high quality tenant/buyers in the house
since we make our profit by selling for higher than we buy. Since we guarantee all minor
maintenance on the house, it is in our best interest to ensure that our tenant/buyer will take care
of the home and eventually secure financing to cash you, the seller, and us out. This means that
we put our tenant/buyers through an intensive pre-screening process before they are even
permitted to look at the house.</p>
<p></p>
<p>We don't work for commissions. And, because our profits are made by selling for slightly higher
than what we buy for, we have a vested interest in the house and in making sure it gets sold.</p>
<p></p>
<p><b>How long does it take before your tenant/buyer cashes me out?</b></p>
<p></p>
<p>That can depend on a number of different factors. We work with many mortgage brokers that
are usually able to get most people financed after they have paid for 12 months on the lease
option. Since everyone's credit history and circumstances vary, that time period can be shorter
or longer for the tenant/buyer that we eventually put into the home. Because of this, we cannot
guarantee the exact time our tenant/buyer will secure financing. However, until our tenant/buyer
qualifies for financing, we continue to pay all the expenses related to the home. It is also in our
best interest to get our tenant/buyer a new loan as soon as possible, since that cashes us out
as well. We aggressively work to get our tenant/buyer financed as soon as possible.</p>
<p></p>
<p><b>Why don't I just sell my house myself or rent it in the meantime?</b></p>
<p></p>
<p>These are always options available to you. The difference would be that you are responsible for
your monthly payments, maintenance and repairs during the selling period. You need to try to
find someone who can actually qualify for financing, wait for them to get approved, and hope
that they don't pull out of the deal, making you start the whole process over again. Renting
during this period opens entirely new difficulties that would-be landlords often overlook,
especially when you are trying to show the home and sell while renters are living there.</p>
<p></p>
<p><b>Why don't I just find my own tenant/buyer?</b></p>
<p></p>
<p>If you have the expertise to screen potential tenant/buyers, check references, know what sort of 
credit scores will allow someone to be able to be financed, deal with maintaining your home
while someone else lives in it, work with mortgage brokers to get them financed, and finally,
process all the paperwork and set up a closing, then finding a tenant/buyer on your own would
be a viable option for you. If you lack the expertise in any of these areas, we are able to help.</p>
<p></p>
<p><b>What if your tenant/buyer doesn't buy the house?</b></p>
<p></p>
<p>Our tenant/buyers are carefully pre-screened to ensure that they want to buy the house and are
able to do so at some point in the future. However, circumstances can change in someone's life,
such as an unexpected job transfer that can make it necessary to move. In situations like that,
we continue to pay all the expenses for the house while we find another qualified tenant/buyer
to put into the home. Remember, we make our money when your house sells.</p>
<p></p>
<p><b>How can you say I won’t have any vacancies?</b></p>
<p></p>
<p>You won’t because your tenant will be JR Capital LLC. We’ll agree on a lease term—3 years, 5
years, whatever you want. And that’s the end of your vacancy problem. If our tenant/buyer
moves out, then that’s our problem, not yours. We’ll keep paying like clockwork.</p>
<p></p>
<p><b>How do you eliminate my maintenance and repair problems?</b></p>
<p></p>
<p>You’ll have 4 levels of protection. First, JR Capital LLC will purchase a homeowner’s warranty at
no cost to you and will keep it in force for the life of the lease. Second, the tenant/buyer will be
required, by the terms of his or her lease, to perform all regular maintenance and repairs. Third,
if any problem comes up that the tenant/buyer is unable or unwilling to perform, JR Capital LLC
will do it. And, fourth, you’ll maintain your homeowner’s policy to protect against incidents
covered by insurance.</p>
<p></p>
<p><b>What do I have to do?</b></p>
<p></p>
<p>For the most part, as the homeowner you don’t have to do anything. We take care of all the
paperwork as well as the placement of tenants and monthly payments. And because you don’t
have to worry about the day to day repairs, the only thing you need to decide is where you want
your monthly checks sent. It is really quite simple.</p>
<p></p>
<p><b>How do I get my payment each month?</b></p>
<p></p>
<p>We can set it up however you like. Some of our homeowners like for us to electronically send
the payment each month straight to their bank account. Others prefer us to set up an escrow
account where we mail the rent to the escrow account and the account pays the mortgage.
(You can call the escrow account anytime and request a statement of activity so you can see
when the rent was paid and when the mortgage was paid.) We normally set it up to
automatically pay your lender each month and send you the difference. That way you won’t
have to worry about a thing.</p>
<p></p>
<p><b>What if a tenant tears up my house?</b></p>
<p></p>
<p>There's no way that I can guarantee you that a tenant won't damage your house since you or I
won't be living with them. But under the agreement that I will have with you, as the seller, if that
happens, I will repair it at my expense. My objective is not to find a "tenant" - my objective is to
find a Buyer who will eventually own your house. I investigate them thoroughly before entering
into an agreement with them. Damages are rarely a problem. In fact, in most instances we
have found that our buyers many times have improved a house with new carpet or some other
upgrades, such as fencing. Again, if it does happen, we'll fix it, and I put that in writing.</p>
<p></p>
<p><b>Who handles the paperwork? Do I have to pay for that?</b></p>
<p></p>
<p>Our attorney handles all of the necessary documents to make the transaction happen. And the
best part is that you don’t need your PhD to be able to read them. Of course, you are always
welcome to have these items reviewed by your real estate agent or lawyer at any time during
the course of the transaction. We want you to be 100% comfortable with everything before
moving forward.<p>
<br /><br />
<h2 style="text-align: center">Benefits of Selling on Land Contract</h2>
<br /><br />
<p><b>What is a Land Contract?</b></p>
<p></p>
<p>A land contract is a written legal contract, or agreement, used to purchase real estate, such as
vacant land, a house, an apartment building, a commercial building, or other real property. A
land contract is a form of seller financing. It is similar to a mortgage, but rather than borrowing
money from a lender or bank to buy real estate, the buyer makes payments to the real estate
owner, or seller, until the purchase price is paid in full.</p>
<p></p>
<p>A buyer and a seller both sign the land contract covering agreed upon terms and conditions of
the sale. Upon satisfaction of all contract terms and conditions, including payment of the
purchase price over a specified time period, the legal title of the property transfers from the
seller to the buyer by way of a warranty deed, or other deed used to convey title.</p>
<p></p>
<p><b>Faster and Easier Closing</b></p>
<p>When you use a land contract to sell your property, you do not have to go through a bank's
underwriting and closing process. This means that the buyer avoids applying for a loan, your
property avoids being appraised, and you can skip the use of certain closing services. A land
contract not only speeds up the transaction but also saves both parties money.</p>
<p></p>
<p><b>Ongoing Cash Flow</b></p>
<p></p>
<p>A land contract turns your property into a cash-flow stream. Until the buyer pays off the deal,
you'll collect monthly payments of principal and interest. In the case of an interest-only contract,
you'll receive monthly interest payments only with a large principal balloon payment in the
future. Or you can continue to get monthly cash-flow by offering principal only payments for a
duration of time, say 15 years.<p>
<p></p>
<p><b>Capital Gains Tax Deferral</b></p>
<p></p>
<p>Land contracts save you from getting a large lump sum of cash at closing. Typically, any gains in
your sale proceeds are going to be subject to capital gains taxes. With a land contract, you take
a little bit of profit every month and, as such, spread your gain out over time. </p>
<br /><br />
<h2 style="text-align: center">FAQs… Land Contracts</h2>
<br /><br />
<p><b>When Does the Buyer Become the New Owner of the Land Contract Property? </b><p>
<p></p>
<p>While the buyer is making payments to the seller, the buyer is considered to have an “equitable
title” to the property. As an equitable title holder, the buyer has an interest in the land contract
property and the seller is precluded from selling the property to a third party or subjecting the
property to a lien or encumbrance that would interfere with the buyer’s interest in the property.
The “legal title” to the property remains with the seller until the buyer makes the final payment.
When the final payment is made, and all conditions of the land contract are met, the deed to the
property will be filed with the appropriate government office, such as the county register of
deeds, naming the buyer as the new owner of the property.</p>
<p></p>
<p><b>What Happens if the Buyer Fails to Make the Land Contract Payments Due?</b></p>
<p></p>
<p>If the buyer defaults on the land contract, or fails to make the monthly payments to the seller as
required, the seller can file a court action called land contract forfeiture. Forfeiture will result in
the buyer “forfeiting,” or giving up, all money paid to the seller for the property pursuant to the
land contract and the equitable title of the buyer will be extinguished. In other words, if the buyer
fails to pay, the seller keeps all money received, plus the seller keeps the real estate.</p>
<p></p>
<p><b>Who pay for taxes and insurance?</b></p>
<p></p>
<p>On a land contract, the buyer is responsible for property taxes, insurance and mortgage interest.
If the seller requires a yearly escrow account can be set up whereby insurance and tax
payments are paid to the seller for him/her to make the payments.</p>
<p></p>
<p><b>Does an Attorney Need to Review a Land Contract?</b></p>
<p></p>
<p>Land contracts may be a good, or sometimes the only, option available to buyers and sellers of
real estate. Real estate rules vary by state, so it is important to consult with an experienced real
estate attorney to draft a land contract in order to allow for appropriate terms and to be able to
enforce a forfeiture action, if needed by the seller.</p>
<p></p>
<p><b>Any Other Questions?</b></p>
<p></p>
<p>If you have any other questions or concerns, please feel free to call us at ${
    businessInfo.phoneNumber
  }.</p>
`;
};

export {generateLoi};

{
  /* <p><b><u>OPTION 1 - Cash</u></b></p>
<p></p>
<p>● Purchase Price: $104,000</p>
<p>● Close in 7-30 Days</p>
<p>● Buyer to pay all closing costs</p>
<p></p>
<p><b><u>OPTION 2 – Sandwich Lease Option - “Perfect Tenant Program”</u></b></p>
<p></p>
<p>We will be your tenant-buyer, and we will sub-lease the house to one of our pre-qualified
tenants. We will stay in the middle for the entire term, pay the rent every month even if the
house is vacant, and be responsible for all the regular maintenance and repairs under $500.</p>
<p></p>
<p>● Option Price: $145,000</p>
<p>● Option Consideration: $500</p>
<p>● Monthly Rent: $800</p>
<p>● Term: 3 Years</p>
<p></p>
<p><b><u>OPTION 3 – Land Contract</u></b></p>
<p></p>
<p>If Option 2 doesn’t work for you, we will buy the property on land contract terms</p>
<p></p>
<p>● Land Contract Price: $XXXXXX</p>
<p>● Down Payment: $XXXXXX</p>
<p>● Monthly Payment: $XXXX</p>
<p>● Term: XXXX Years</p> */
}
