import {GREY, ORANGE, GREEN, RED} from '../assets/colors';

export const CURRENCY_SYMBOL = '$';

export const ValueType = {
  price: 'price',
  percentage: 'percentage',
  year: 'year',
};

export const colorInformation = [
  {
    color: ORANGE,
    description:
      'This form field input is always editable by click. You can change the value.',
  },
  {
    color: RED,
    description:
      'This deal does not meet our minimum deals standard. edit value to meet the standard.',
  },
  {
    color: GREEN,
    description: 'This form field input can not change the value.',
  },
  {
    color: GREY[5],
    description:
      'The price on the form field is gonna be shown to the Letter of intent.',
  },
];

export const PLAN_TYPE = {
  free: 'free',
  pro: 'pro',
  proPlus: 'pro plus',
};

export const DEFAULT_USER_DEALS_OPTION_SETTINGS = {
  option1: {
    priceDiscount: {
      value: 0.7,
    },
    wholeSaleFee: {
      value: 10000,
    },
  },
  option2: {
    downPayment: {
      value: 20000,
    },
    holdingPeriod: {
      value: 6,
    },
    lendingApr: {
      value: 0.11,
    },
    lendingPts: {
      value: 0.01,
    },
    priceDiscount: {
      value: 0.7,
    },
    sellingCost: {
      value: 0.08,
    },
    formatProfitMargin: {
      value: 0.2,
    },
    formatReturnOnCachInvestment: {
      value: 1,
    },
  },
  option3: {
    downPayment: {
      value: 0.2,
    },
    interestRateOnMortgate: {
      value: 0.06,
    },
    numYearsOnMortgage: {
      value: 30,
    },
    priceDiscount: {
      value: 0.8,
    },
    wholeSaleFee: {
      value: 10000,
    },
    formatCapRateForEndBuyer: {
      value: 0.08,
    },
    formatCashOnCashRateForEndBuyer: {
      value: 0.15,
    },
  },
  option4: {
    downPayment: {
      value: 0.1,
    },
    interestRateOnLandContract: {
      value: 0.05,
    },
    priceDiscount: {
      value: 0.85,
    },
    wholeSaleFee: {
      value: 5000,
    },
    yearsTillBalloonPayment: {
      value: 5,
      label: '# of Year Till Balloon',
      type: ValueType.year,
      showToSeller: true,
    },
    formatCapRateForEndBuyer: {
      value: 0.08,
    },
    formatCashOnCashRateForEndBuyer: {
      value: 0.15,
    },
  },
  option5: {
    downPayment: {
      value: 0.1,
    },
    paymentTermLength: {
      value: 30,
    },
    priceDiscount: {
      value: 1,
    },
    wholeSaleFee: {
      value: 5000,
    },
    numerOfYearsTillBalloon: {
      value: 5,
      label: '# of Year Till Balloon',
      type: ValueType.year,
      showToSeller: true,
    },
    formatCapRateForEndBuyer: {
      value: 0.08,
    },
    formatCashOnCashRateForEndBuyer: {
      value: 0.15,
    },
  },
  option6: {
    numYears: {
      value: 5,
      label: 'Number of Years',
      type: ValueType.year,
      showToSeller: true,
    },
    optionDepositToSeller: {
      value: 500,
      label: 'Option Deposit',
      type: ValueType.price,
      showToSeller: true,
    },
    priceDiscount: {
      value: 0.85,
    },
    minOptionDepositFromTenantBuyer: {
      value: 5000,
    },
    formatMinOptionDepositFromTenantBuyer: {
      value: 5000,
    },
    formatTotalProfit: {
      value: 20000,
    },
  },
  option7: {
    numYears: {
      value: 5,
      label: 'Number of Years',
      type: ValueType.year,
      showToSeller: true,
    },
    optionDepositToSeller: {
      value: 500,
      label: 'Option Deposit',
      type: ValueType.price,
      showToSeller: true,
    },
    priceDiscount: {
      value: 1,
    },
    minOptionDepositFromTenantBuyer: {
      value: 5000,
    },
    formatMinOptionDepositFromTenantBuyer: {
      value: 5000,
    },
  },
  option8: {
    // balanceOnMortgage: {
    //   value: 70000,
    // },
    cashForKeysPurchaseAmountAboveBalance: {
      value: 5000,
      label: 'Cash for Keys / Purchase amount above Balance',
      type: ValueType.price,
      showToSeller: true,
    },
    formatCashOnCashReturn: {
      value: 0.2,
    },
  },
};
