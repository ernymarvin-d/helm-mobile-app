export const BLUE = '#193954';
export const BLUE_SECONDARY = '#75889A';
export const BLUE_FADED = '#2F4F68';
export const ORANGE = '#D9570B';
export const GREEN = '#38C663';
export const RED = '#D90C08';
export const GREY = [
  '#101111',
  '#505152',
  '#87898B',
  '#F4F6F8',
  '#E3E9ED',
  '#D2D3D5',
];
export const SPLASH_BG_GRADIENT_COLORS = ['transparent', BLUE];

export const BUTTON_PRIMARY = ORANGE;
export const BUTTON_SECONDARY = GREY[4];
export const GOOGLE_BUTTON_COLOR = '#3F91D5';
export const FB_BUTTON_COLOR = '#487CCC';

export const TEXT_PRIMARY = BLUE;
export const TEXT_SECONDARY = GREY[2];

export const INPUT_TEXT_BORDER_COLOR = '#ecf0f2';

export const BOX_BORDER_COLOR = '#ecf0f3';
export const BOX_BACKGROUND_COLOR = GREY[3];

export const BOX_BACKGROUND_COLOR_2 = '#FAFBFC';
export const BOX_BORDER_COLOR_2 = '#ecf0f3';

export const DIVIDER_COLOR = '#E4E6E9';
