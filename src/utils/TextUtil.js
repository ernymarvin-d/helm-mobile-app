import RNHTMLtoPDF from 'react-native-html-to-pdf';
import FileViewer from 'react-native-file-viewer';
import moment from 'moment';
import {generateLoi} from '../assets/LOITemplate';

const validateEmailAddress = (email) => {
  const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return reg.test(email);
};

const validatePassword = (password) => password.length >= 8;

// TODO LATER
// https://github.com/Hopding/react-native-pdf-lib/issues/57
// use this library instead if have time and define the page margin.
const createPDF = async (lioTemplate) => {
  console.log({lioTemplate});
  let options = {
    html: `<div style='margin: 52'>${lioTemplate}</div>`,
    fileName: `${moment().format('dddd-MMMM-Do-YYYY-h-mm-ssa')}-Helm-App-LOI`,
    directory: 'docs',
  };
  try {
    let file = await RNHTMLtoPDF.convert(options);
    console.log('path to the new PDF', file.filePath);
    return file.filePath;
  } catch (e) {
    return null;
  }
};

const openFile = (filePath) => {
  FileViewer.open(filePath)
    .then(() => console.log('file opened'))
    .catch((e) => console.log(e.message));
};

export {validateEmailAddress, validatePassword, createPDF, openFile};
