import React from 'react';
import IconButton from '../components/atoms/IconButton';
import backbuttonIconBold from '../assets/images/ic-header-back-button-bold.png';
import {GREY, BLUE} from '../assets/colors';
import {TouchableOpacity} from 'react-native-gesture-handler';

const DEFAULT_HEADER_STYLE = {
  backgroundColor: BLUE,
  elevation: 0,
  shadowOpacity: 0,
};

const CLEAR_HEADER_STYLE = {
  backgroundColor: 'white',
  elevation: 0,
  shadowOpacity: 0,
};

const getDefaultHeader = ({
  onPressLeftButton,
  navigation,
  renderHeaderRight,
}) => {
  const popStack = () => navigation.pop();
  const header = {
    headerTitleAlign: 'left',
    headerLeft: () => {
      return (
        <IconButton
          bgColor="transparent"
          tintColor={GREY[2]}
          icon={backbuttonIconBold}
          onPress={onPressLeftButton ? onPressLeftButton : popStack}
        />
      );
    },
    // headerRight: renderHeaderRight,
    headerStyle: DEFAULT_HEADER_STYLE,
    headerTintColor: 'white',
  };
  if (renderHeaderRight) {
    header.headerRight = renderHeaderRight;
  }
  return header;
};

const getWhiteHeader = ({onPressLeftButton, navigation}) => {
  const popStack = () => navigation.pop();
  const header = {
    headerTitleAlign: 'left',
    headerLeft: () => {
      return (
        <IconButton
          bgColor="transparent"
          tintColor={GREY[2]}
          icon={backbuttonIconBold}
          onPress={onPressLeftButton ? onPressLeftButton : popStack}
        />
      );
    },
    headerStyle: CLEAR_HEADER_STYLE,
    headerTintColor: 'white',
  };
  return header;
};

const getCustomHeader = ({
  onPressLeftButton,
  navigation,
  headerLeftButtonComponent,
}) => {
  const popStack = () => navigation.pop();
  const header = {
    headerTitleAlign: 'left',
    headerLeft: () => {
      return (
        <TouchableOpacity
          style={{marginLeft: 20}}
          onPress={onPressLeftButton ? onPressLeftButton : popStack}>
          {headerLeftButtonComponent()}
        </TouchableOpacity>
      );
    },
    headerStyle: CLEAR_HEADER_STYLE,
    headerTintColor: 'white',
  };
  return header;
};

export {getDefaultHeader, getWhiteHeader, getCustomHeader};
