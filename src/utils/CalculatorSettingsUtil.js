import _ from 'lodash';
import {updateUserGeneralCalculatorSettings} from '../utils/DbUtils';

export const updateUserDefaultCalculatorSetting = ({
  dealKey,
  settingsKey,
  value,
  settingsOptionKey,
  settings,
  stateAction,
  userId,
}) => {
  console.log('adf', {userId});
  const newSettings = {
    settingsOptionKey,
    settings: {
      ...settings,
      [settingsKey]: {
        ...settings[settingsKey],
        ['value']: _.toNumber(value),
      },
    },
  };
  console.log({dealKey, newSettings});
  if (!_.isNull(dealKey) && !_.isUndefined(dealKey)) {
    stateAction.updateCalupdateDealsSettingsOption({
      ...newSettings,
      myDealKey: dealKey,
    });
  }
  stateAction.updateCalculatorSettingsOption(newSettings);
  updateUserGeneralCalculatorSettings({userId, settings: newSettings});
};
