export const sortDealsOption = (dealOptions) => {
  return Object.values(dealOptions).sort(function (a, b) {
    return a.dealOptionNumber - b.dealOptionNumber;
  });
};
