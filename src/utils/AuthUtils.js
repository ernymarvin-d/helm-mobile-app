import React from 'react';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import {GoogleSignin} from '@react-native-community/google-signin';
import {updateUserInfo} from './DbUtils';

export const registerUserWithPassword = (data) => {
  const {email, password, callback} = data;

  auth()
    .createUserWithEmailAndPassword(email, password)
    .then(() => {
      console.log('User account created & signed in!');
      if (callback) callback(true);
    })
    .catch((error) => {
      let alertMessage = '';
      if (error.code === 'auth/email-already-in-use') {
        console.log('That email address is already in use!');
        alertMessage = 'That email address is already in use!';
      }

      if (error.code === 'auth/invalid-email') {
        console.log('That email address is invalid!');
        alertMessage = 'That email address is invalid!';
      }

      console.error(error);
      if (callback) callback(false);
    });
};

export const reAuthUser = (currentPasswrod) => {
  const user = auth().currentUser;
  const cred = auth.EmailAuthProvider.credential(user.email, currentPasswrod);
  return user.reauthenticateWithCredential(cred);
};

export const changePassword = (newPassword) => {
  const user = auth().currentUser;
  return user
    .updatePassword(newPassword)
    .then(() => {
      console.log('Password updated');
      return {};
    })
    .catch((error) => {
      console.log('Password update error:', error.message);
      return {error};
    });
};

export const updateEmail = (newEmail) => {
  const user = auth().currentUser;
  return user
    .updateEmail(newEmail)
    .then(() => {
      updateUserInfo({
        data: {
          key: 'info.emailAddress',
          value: newEmail,
        },
      });
      console.log('Email Updated');
      return {};
    })
    .catch((error) => {
      console.log('Update email error:', {error});
      return {error};
    });
};
