import firestore from '@react-native-firebase/firestore';
import {
  DEFAULT_USER_DEALS_OPTION_SETTINGS,
  PLAN_TYPE,
} from '../assets/Constants';
import auth from '@react-native-firebase/auth';
import _ from 'lodash';

const USERS = 'users';
const DEALS = 'deals';
const MESSAGES = 'messages';
const FAQS = 'faqs';
const FEEDBACKS = 'feedbacks';
const TUTORIALS = 'tutorials';
const PLANS = 'plans';
const TRANSACTIONS = 'transactions';

export const UserDbCollection = firestore().collection(USERS);
export const DealsDbCollection = firestore().collection(DEALS);
export const MessagesDbCollection = firestore().collection(MESSAGES);
export const FaqsDbCollection = firestore().collection(FAQS);
export const FeedbackDbCollection = firestore().collection(FEEDBACKS);
export const TutorialsDbCollection = firestore().collection(TUTORIALS);
export const PlansDbCollection = firestore().collection(PLANS);
export const TransactionDbCollection = firestore().collection(TRANSACTIONS);

// user data
export const getUserData = async (userId) => {
  try {
    return await UserDbCollection.doc(userId).get();
  } catch (error) {
    console.log(error.message);
  }
};

export const createNewUserDataFromProvider = (user) => {
  console.log(user.uid, '11111');
  const {displayName, email, photoURL} = user._user;
  const userDbData = {
    info: {
      emailAddress: email || '',
      fullName: displayName || '',
      photoURL: photoURL || '',
      userId: user.uid,
    },
    businessInfo: {
      businessName: '',
      address: '',
      websiteAddress: '',
      phoneNumber: '',
    },
    calculatorGeneralSettings: DEFAULT_USER_DEALS_OPTION_SETTINGS,
  };

  return addUserData(userDbData);
};

export const addUserData = (user) => {
  console.log('add to the database', {user});
  return UserDbCollection.doc(user.info.userId)
    .set(user)
    .then(() => {
      console.log('new user has been added to the database');
      return user;
    })
    .catch((error) => {
      console.log(error);
      return null;
    });
};

export const updateUserInfo = ({uid, data}) => {
  const newUserInfo = {
    [`${data.key}`]: data.value,
  };
  return UserDbCollection.doc(auth().currentUser.uid)
    .update(newUserInfo)
    .then(() => true)
    .catch((error) => {
      console.log(error.message);
      return false;
    });
};

export const observeUserData = ({user, stateDispatcher}) => {
  // console.log(user.uid, user._user.providerData[0], '111');
  const {uid} = user;
  return UserDbCollection.doc(uid).onSnapshot(
    (querySnapshot) => {
      if (_.isNull(querySnapshot)) {
        return null;
      }
      const userData = querySnapshot.data();
      userData.userId = uid;
      userData.providerData = user._user.providerData[0];
      stateDispatcher.loginUser(userData);
      return;
    },
    (error) => {
      console.log(error.message);
      return;
    },
  );
};

// end user data

// deals
export const saveDealsCreatedToDb = ({user, deal, planType}) => {
  const userId = user.userId;
  deal.sendEmail = planType !== PLAN_TYPE.free;
  return DealsDbCollection.add(deal)
    .then((ref) => {
      console.log('added to database:', ref.id);
      return saveDealIdToTheUser({userId, dealRefId: ref.id})
        .then(() => console.log('added to user deals field'))
        .catch((error) => {
          console.log(error);
          return true;
        });
    })
    .catch((error) => {
      console.log(error);
      return true;
    });
};

const saveDealIdToTheUser = ({userId, dealRefId}) => {
  return UserDbCollection.doc(userId).update({
    deals: firestore.FieldValue.arrayUnion(dealRefId),
  });
};

export const observeCreatedUserDeals = ({userId, stateDispatcher}) => {
  // console.log('observeCreatedUserDeals', {userId, stateDispatcher});
  return DealsDbCollection.orderBy('date', 'desc')
    .where('userId', '==', userId)
    .onSnapshot(
      (querySnapshot) => {
        // console.log('querySnapshot', {querySnapshot});
        if (_.isNull(querySnapshot)) return;
        querySnapshot.docChanges().forEach((change) => {
          console.log('change.doc', change.doc.id);
          const deals = change.doc.data();
          const id = change.doc.id;
          deals.id = id;
          if (change.type === 'added') {
            stateDispatcher.addToMyDeals(deals);
            console.log('added new deal', {deals});
          }
          if (change.type === 'removed') {
            stateDispatcher.removeFromMyDeals(deals);
            console.log('removed new deal: ', {deals});
          }
        });
        return;
      },
      (error) => console.log(error.message),
    );
};

export const deleteMyDeal = ({myDealKey}) => {
  console.log({myDealKey});
  return DealsDbCollection.doc(myDealKey).delete();
};
// end deals

// settings
export const updateUserGeneralCalculatorSettings = ({userId, settings}) => {
  const generalSettingsKey = `calculatorGeneralSettings.${settings.settingsOptionKey}`;
  return UserDbCollection.doc(userId)
    .update({
      [generalSettingsKey]: settings.settings,
    })
    .then(() => {
      console.log('Calculator Settings updated:', {settings});
    })
    .catch((error) => {
      console.log(error.message);
    });
};
// end settings

// message
export const sendMessageToHelm = ({message}) => {
  return MessagesDbCollection.add(message)
    .then(() => true)
    .catch((error) => console.log(error.message));
};
//end message

// faqs
export const getAllFaqs = ({type}) => {
  return FaqsDbCollection.where('type', '==', type)
    .get()
    .then((querySnapshot) => {
      if (querySnapshot.docs.length !== 0) {
        return querySnapshot.docs.map((doc) => doc.data());
      }
      return [];
    })
    .catch((error) => {
      console.log(error.message);
      return [];
    });
};
// end faqs

// feedback //
export const addNewFeedback = (feedback) => {
  return FeedbackDbCollection.add(feedback)
    .then(() => true)
    .catch((error) => {
      console.log(error.message);
      return false;
    });
};
// end feedback //

// tutorials //
export const getAvailableTutorials = () => {
  return TutorialsDbCollection.orderBy('webinar.date', 'desc')
    .get()
    .then((snapShot) => {
      return snapShot.docs.map((tutorial) => tutorial.data());
    })
    .catch((error) => {
      console.log(error.message);
      return [];
    });
};
// end tutorials //

// Plans //
export const getAllPlans = () => {
  return PlansDbCollection.orderBy('price', 'desc')
    .get()
    .then((snapShot) => {
      return snapShot.docs.map((plan) => plan.data());
    })
    .catch((error) => {
      console.log(error.messge);
      return [];
    });
};
// Plans //

// transactions //
export const addTransacction = ({transaction, uid}) => {
  const data = transaction;
  data.uid = uid;
  return TransactionDbCollection.add(data)
    .then(() => true)
    .catch((error) => {
      console.log(error.message);
      return false;
    });
};
// transactions //
