import React from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import _ from 'lodash';

const KEY_INTRO_DONE = '@persist_intro_done';

export async function persistIntroScreenAlreadyShown(alreadyShown) {
  try {
    await AsyncStorage.setItem(KEY_INTRO_DONE, JSON.stringify(alreadyShown));
  } catch (error) {
    console.log(error.message);
  }
}

export async function isIntroScreenAlreadyShown() {
  try {
    const value = await AsyncStorage.getItem(KEY_INTRO_DONE);
    return value == 'true';
  } catch (error) {
    console.log(`isIntroScreenAlreadyShown`, error.message);
    return false;
  }
}
