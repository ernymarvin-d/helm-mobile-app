const tutorialData = [
  {
    header: 'Learn Invesment',
    tableOfContent: [
      {
        icon: 'video',
        title: 'Why use investment?',
      },
      {
        title: 'Neighborhood Shopping Center',
      },
      {
        title: 'Power Center',
      },
      {
        title: 'Regional Mall',
      },
    ],
  },
  {
    header: 'Learn Invesment',
    tableOfContent: [
      {
        icon: 'video',
        title: 'Why use investment?',
      },
      {
        title: 'Neighborhood Shopping Center',
      },
      {
        title: 'Power Center',
      },
      {
        title: 'Regional Mall',
      },
    ],
  },
  {
    header: 'Learn Invesment',
    tableOfContent: [
      {
        icon: 'video',
        title: 'Why use investment?',
      },
      {
        title: 'Neighborhood Shopping Center',
      },
      {
        title: 'Power Center',
      },
      {
        title: 'Regional Mall',
      },
    ],
  },
  {
    header: 'Learn Invesment',
    tableOfContent: [
      {
        icon: 'video',
        title: 'Why use investment?',
      },
      {
        title: 'Neighborhood Shopping Center',
      },
      {
        title: 'Power Center',
      },
      {
        title: 'Regional Mall',
      },
    ],
  },
  {
    header: 'Learn Invesment',
    tableOfContent: [
      {
        icon: 'video',
        title: 'Why use investment?',
      },
      {
        title: 'Neighborhood Shopping Center',
      },
      {
        title: 'Power Center',
      },
      {
        title: 'Regional Mall',
      },
    ],
  },
  {
    header: 'Learn Invesment',
    tableOfContent: [
      {
        icon: 'video',
        title: 'Why use investment?',
      },
      {
        title: 'Neighborhood Shopping Center',
      },
      {
        title: 'Power Center',
      },
      {
        title: 'Regional Mall',
      },
    ],
  },
  {
    header: 'Learn Invesment',
    tableOfContent: [
      {
        icon: 'video',
        title: 'Why use investment?',
      },
      {
        title: 'Neighborhood Shopping Center',
      },
      {
        title: 'Power Center',
      },
      {
        title: 'Regional Mall',
      },
    ],
  },
  {
    header: 'Learn Invesment',
    tableOfContent: [
      {
        icon: 'video',
        title: 'Why use investment?',
      },
      {
        title: 'Neighborhood Shopping Center',
      },
      {
        title: 'Power Center',
      },
      {
        title: 'Regional Mall',
      },
    ],
  },
  {
    header: 'Learn Invesment',
    tableOfContent: [
      {
        icon: 'video',
        title: 'Why use investment?',
      },
      {
        title: 'Neighborhood Shopping Center',
      },
      {
        title: 'Power Center',
      },
      {
        title: 'Regional Mall',
      },
    ],
  },
  {
    header: 'Learn Invesment',
    tableOfContent: [
      {
        icon: 'video',
        title: 'Why use investment?',
      },
      {
        title: 'Neighborhood Shopping Center',
      },
      {
        title: 'Power Center',
      },
      {
        title: 'Regional Mall',
      },
    ],
  },
  {
    header: 'Learn Invesment',
    tableOfContent: [
      {
        icon: 'video',
        title: 'Why use investment?',
      },
      {
        title: 'Neighborhood Shopping Center',
      },
      {
        title: 'Power Center',
      },
      {
        title: 'Regional Mall',
      },
    ],
  },
  {
    header: 'Learn Invesment',
    tableOfContent: [
      {
        icon: 'video',
        title: 'Why use investment?',
      },
      {
        title: 'Neighborhood Shopping Center',
      },
      {
        title: 'Power Center',
      },
      {
        title: 'Regional Mall',
      },
    ],
  },
  {
    header: 'Learn Invesment',
    tableOfContent: [
      {
        icon: 'video',
        title: 'Why use investment?',
      },
      {
        title: 'Neighborhood Shopping Center',
      },
      {
        title: 'Power Center',
      },
      {
        title: 'Regional Mall',
      },
    ],
  },
];

const myDealsData = [
  {
    name: 'Deal no. 1',
    location: 'Property Address Lorem Ipsum dolor sit amet',
    date: '04/19/2020',
    options: [
      {
        name: 'Option 1',
        header: 'Cash offer for Wholesales to Fix/Flip',
        content: 'Initial price offering $1,300,000.00',
      },
      {
        name: 'Option 2',
        header: 'Land Contracts Interest Only',
        content: 'Initial price offering $1,300,000.00',
      },
      {
        name: 'Option 3',
        header: 'Cash offer for your Fix/Flip',
        content: 'Initial price offering $1,300,000.00',
      },
    ],
  },
  {
    name: 'Deal no. 2',
    date: '04/19/2020',
    location: 'Property Address Lorem Ipsum dolor sit amet',
    options: [
      {
        name: 'Option 1',
        header: 'Cash offer for Wholesales to Fix/Flip',
        content: 'Initial price offering $1,300,000.00',
      },
      {
        name: 'Option 2',
        header: 'Land Contracts Interest Only',
        content: 'Initial price offering $1,300,000.00',
      },
      {
        name: 'Option 3',
        header: 'Cash offer for your Fix/Flip',
        content: 'Initial price offering $1,300,000.00',
      },
    ],
  },
  {
    name: 'Deal no. 3',
    date: '04/19/2020',
    location: 'Property Address Lorem Ipsum dolor sit amet',
    options: [
      {
        name: 'Option 1',
        header: 'Cash offer for Wholesales to Fix/Flip',
        content: 'Initial price offering $1,300,000.00',
      },
      {
        name: 'Option 2',
        header: 'Land Contracts Interest Only',
        content: 'Initial price offering $1,300,000.00',
      },
      {
        name: 'Option 3',
        header: 'Cash offer for your Fix/Flip',
        content: 'Initial price offering $1,300,000.00',
      },
    ],
  },
  {
    name: 'Deal no. 4',
    date: '04/19/2020',
    location: 'Property Address Lorem Ipsum dolor sit amet',
    options: [
      {
        name: 'Option 1',
        header: 'Cash offer for Wholesales to Fix/Flip',
        content: 'Initial price offering $1,300,000.00',
      },
      {
        name: 'Option 2',
        header: 'Land Contracts Interest Only',
        content: 'Initial price offering $1,300,000.00',
      },
      {
        name: 'Option 3',
        header: 'Cash offer for your Fix/Flip',
        content: 'Initial price offering $1,300,000.00',
      },
    ],
  },
];

const faqData = [
  {
    question: 'What is General Settings?',
    answer:
      'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ratione, eos nemo cupiditate facilis similique ab quam officiis deserunt, aspernatur minima sunt incidunt optio quo! Quam odio doloribus eos obcaecati laudantium.',
  },
  {
    question: 'How can I upgrade plan?',
    answer:
      'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ratione, eos nemo cupiditate facilis similique ab quam officiis deserunt, aspernatur minima sunt incidunt optio quo! Quam odio doloribus eos obcaecati laudantium.',
  },
  {
    question: 'How reliable is this app?',
    answer:
      'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ratione, eos nemo cupiditate facilis similique ab quam officiis deserunt, aspernatur minima sunt incidunt optio quo! Quam odio doloribus eos obcaecati laudantium.',
  },
  {
    question: 'Where can I find a real estate mentor?',
    answer:
      'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ratione, eos nemo cupiditate facilis similique ab quam officiis deserunt, aspernatur minima sunt incidunt optio quo! Quam odio doloribus eos obcaecati laudantium.',
  },
  {
    question: 'Where can I find a real estate mentor?',
    answer:
      'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ratione, eos nemo cupiditate facilis similique ab quam officiis deserunt, aspernatur minima sunt incidunt optio quo! Quam odio doloribus eos obcaecati laudantium.',
  },
  {
    question: 'Where can I find a real estate mentor?',
    answer:
      'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ratione, eos nemo cupiditate facilis similique ab quam officiis deserunt, aspernatur minima sunt incidunt optio quo! Quam odio doloribus eos obcaecati laudantium.',
  },
];

const faqVideoData = [
  {
    video: {
      url: 'http://google.com',
      placeholder: 'image',
    },
    title: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit.',
  },
  {
    video: {
      url: 'http://google.com',
      placeholder: 'image',
    },
    title: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit.',
  },
  {
    video: {
      url: 'http://google.com',
      placeholder: 'image',
    },
    title: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit.',
  },
  {
    video: {
      url: 'http://google.com',
      placeholder: 'image',
    },
    title: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit.',
  },
  {
    video: {
      url: 'http://google.com',
      placeholder: 'image',
    },
    title: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit.',
  },
];

export {tutorialData, myDealsData, faqData, faqVideoData};
