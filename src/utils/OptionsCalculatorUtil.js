// propertyInformation = {
//     afterRepairMarketValue,
//     annualPropertyTax,
//     bathroom,
//     bedRoom,
//     rentRateMonth,
//     squareFootRehabFixFlip,
//     squareFootRehabRental,
//     squareFootage,
//   }

import {CURRENCY_SYMBOL} from '../assets/Constants';
import _ from 'lodash';
import {ValueType} from '../assets/Constants';

const assumptions = {
  realEstateAppriciationRate: 0.02,
  vacancyRate: 0.08,
  managementFee: 0.1,
  maintenancePercentage: 0.06,
};

function pmt(rate, nperiod, pv, fv, type) {
  if (!fv) fv = 0;
  if (!type) type = 0;

  if (rate == 0) return -(pv + fv) / nperiod;

  var pvif = Math.pow(1 + rate, nperiod);
  var pmt = (rate / (pvif - 1)) * -(pv * pvif + fv);

  if (type == 1) {
    pmt /= 1 + rate;
  }

  return pmt;
}

export const calculateOption1 = ({settings, propertyInformation}) => {
  const priceDiscount = settings.priceDiscount.value;
  const wholeSaleFee = settings.wholeSaleFee.value;

  const {
    afterRepairMarketValue,
    annualPropertyTax,
    bathroom,
    bedRoom,
    rentRateMonth,
    squareFootRehabFixFlip,
    squareFootRehabRental,
    squareFootage,
  } = propertyInformation;
  let initialPriceOffered = priceDiscount * afterRepairMarketValue;
  initialPriceOffered -= squareFootRehabFixFlip * squareFootage;
  initialPriceOffered -= wholeSaleFee;
  return {
    dealOptionNumber: 1,
    title: 'Cash Offer',
    options: {
      initialPriceOffered: {
        value: initialPriceOffered,
        label: 'Initial Price Offered',
        type: ValueType.price,
        showToSeller: true,
      },
    },
  };
};

export const calculateOption2 = ({settings, propertyInformation}) => {
  const downPayment = settings.downPayment.value;
  const holdingPeriod = settings.holdingPeriod.value;
  const lendingApr = settings.lendingApr.value;
  const lendingPts = settings.lendingPts.value;
  const priceDiscount = settings.priceDiscount.value;
  const sellingCost = settings.sellingCost.value;

  const {
    afterRepairMarketValue,
    annualPropertyTax,
    bathroom,
    bedRoom,
    rentRateMonth,
    squareFootRehabFixFlip,
    squareFootRehabRental,
    squareFootage,
  } = propertyInformation;

  const initialPriceOffered =
    priceDiscount * afterRepairMarketValue -
    squareFootRehabFixFlip * squareFootage;

  const repairCost = squareFootRehabFixFlip * squareFootage;
  const remainingBalance = initialPriceOffered - downPayment + repairCost;
  const costOfInterestPoint = lendingPts * remainingBalance;
  const costOfInterestRate = (remainingBalance * lendingApr) / 12;
  const interestByHoldingPeriod = costOfInterestRate * holdingPeriod;
  const maturityCost = 120 * holdingPeriod;

  //   console.log({
  //     repairCost,
  //     remainingBalance,
  //     costOfInterestPoint,
  //     costOfInterestRate,
  //     interestByHoldingPeriod,
  //     maturityCost,
  //   });

  const profit =
    afterRepairMarketValue -
    afterRepairMarketValue * sellingCost -
    (initialPriceOffered - downPayment) -
    repairCost -
    maturityCost -
    costOfInterestPoint -
    interestByHoldingPeriod -
    annualPropertyTax * (holdingPeriod / 12);
  const returnOnCashInvestment = profit / downPayment;

  const profitMargin =
    profit /
    (initialPriceOffered +
      repairCost +
      maturityCost +
      costOfInterestPoint +
      interestByHoldingPeriod +
      sellingCost * afterRepairMarketValue);

  return {
    dealOptionNumber: 2,
    title: 'Cash Offer',
    options: {
      initialPriceOffered: {
        value: initialPriceOffered,
        label: 'Initial Price Offered',
        type: ValueType.price,
        showToSeller: true,
      },
      profitMargin: {
        value: profitMargin,
        label: 'Profit Margin',
        type: ValueType.percentage,
      },
      returnOnCashInvestment: {
        value: returnOnCashInvestment,
        label: 'Return on Cash Investment',
        type: ValueType.percentage,
      },
      profit: {
        value: profit,
        label: 'Profit',
        type: ValueType.price,
      },
    },
  };
};

export const calculateOption3 = ({settings, propertyInformation}) => {
  const downPayment = settings.downPayment.value;
  const interestRateOnMortgate = settings.interestRateOnMortgate.value;
  const numYearsOnMortgage = settings.numYearsOnMortgage.value;
  const priceDiscount = settings.priceDiscount.value;
  const wholeSaleFee = settings.wholeSaleFee.value;

  const {
    afterRepairMarketValue,
    annualPropertyTax,
    bathroom,
    bedRoom,
    rentRateMonth,
    squareFootRehabFixFlip,
    squareFootRehabRental,
    squareFootage,
  } = propertyInformation;

  const purchasePrice = afterRepairMarketValue * priceDiscount;
  const initialCapitalImprovement = squareFootRehabRental * squareFootage;
  const initialCashInvested = initialCapitalImprovement + purchasePrice;
  const monthlyVacancyLoss = rentRateMonth * -1 * assumptions.vacancyRate;
  const operatingIncome = rentRateMonth + monthlyVacancyLoss;
  const annualOperatingIncome = operatingIncome * 12; // GOOD

  const valueDownPayment = purchasePrice * downPayment; // GOOD
  const priceToSeller = afterRepairMarketValue * priceDiscount - wholeSaleFee;
  const priceToBuyer = wholeSaleFee + priceToSeller;

  const monthlyOperatingImcome = rentRateMonth + monthlyVacancyLoss; // GOOD

  // const propertyTaxExpense = (annualPropertyTax * -1) / 12;
  const propertyTaxExpense = annualPropertyTax * -1;
  const insuranceExpense = -1200; // sould this issuance be assumption or an input
  const electricGasWaterExpense = 0;
  const propertyManagementExpense = 0;
  const associationFeesExpense = 0;
  const maintenanceExpense =
    rentRateMonth * -1 * assumptions.maintenancePercentage;
  const others = 0;

  const monthlyOeratingExpenses =
    propertyTaxExpense / 12 +
    insuranceExpense / 12 +
    electricGasWaterExpense +
    propertyManagementExpense +
    associationFeesExpense +
    maintenanceExpense +
    others;

  const annualOperatingExpenses =
    propertyTaxExpense +
    insuranceExpense +
    electricGasWaterExpense * 12 +
    propertyManagementExpense * 12 +
    associationFeesExpense * 12 +
    maintenanceExpense * 12 +
    others * 12; //BAD
  const monthlyNetOperatingIncome =
    monthlyOperatingImcome + monthlyOeratingExpenses; // GOOD

  const terms = numYearsOnMortgage * 12;
  const principlePrice = purchasePrice - valueDownPayment;
  const interestRate = interestRateOnMortgate / 12;
  const monthlyMortgagePayment = pmt(interestRate, terms, principlePrice);

  const monthlyCashFlowProfit =
    monthlyNetOperatingIncome + monthlyMortgagePayment; // Good
  const annualCashFlowProfit = monthlyCashFlowProfit * 12; // GOOD

  const annualNetOperatingIncome =
    annualOperatingIncome + annualOperatingExpenses;
  const capRateForEndBuyer = annualNetOperatingIncome / initialCashInvested;
  const cashOnCashReturnForEndBuyer =
    annualCashFlowProfit / (valueDownPayment + initialCapitalImprovement);

  return {
    dealOptionNumber: 3,
    title: 'Cash Offer',
    options: {
      priceToSeller: {
        value: priceToSeller,
        label: 'Initial price offered',
        type: ValueType.price,
        showToSeller: true,
      },
      priceToBuyer: {
        value: priceToBuyer,
        label: 'Price to Buyer',
        type: ValueType.price,
      },
      capRateForEndBuyer: {
        value: capRateForEndBuyer,
        label: 'Cap Rate for End Buyer',
        type: ValueType.percentage,
      },
      cashOnCashReturnForEndBuyer: {
        value: cashOnCashReturnForEndBuyer,
        label: 'Cash on Cash Return for End Buyer',
        type: ValueType.percentage,
      },
    },
  };
};

export const calculateOption4 = ({settings, propertyInformation}) => {
  const downPayment = settings.downPayment.value;
  const interestRateOnLandContract = settings.interestRateOnLandContract.value;
  const priceDiscount = settings.priceDiscount.value;
  const wholeSaleFee = settings.wholeSaleFee.value;
  const yearsTillBalloonPayment = settings.yearsTillBalloonPayment.value;

  const {
    afterRepairMarketValue,
    annualPropertyTax,
    bathroom,
    bedRoom,
    rentRateMonth,
    squareFootRehabFixFlip,
    squareFootRehabRental,
    squareFootage,
  } = propertyInformation;

  const landContractPriceToSeller = priceDiscount * afterRepairMarketValue;
  const valueDownPaymentSeller = landContractPriceToSeller * downPayment;
  const balloonPayment = landContractPriceToSeller - valueDownPaymentSeller;
  const purchasePrice = afterRepairMarketValue * priceDiscount;

  const valueDownPayment =
    landContractPriceToSeller * downPayment + wholeSaleFee;
  const monthlyMortgagePayment =
    (((purchasePrice - valueDownPayment) * interestRateOnLandContract) / 12) *
    -1;
  const monthlyPayment = monthlyMortgagePayment * -1;

  const landContractPriceToBuyer = landContractPriceToSeller + wholeSaleFee;
  const valueDownPaymentBuyer = valueDownPaymentSeller + wholeSaleFee;

  const monthlyVacancyLoss = rentRateMonth * -1 * assumptions.vacancyRate;
  const operatingIncome = rentRateMonth + monthlyVacancyLoss;
  const annualOperatingIncome = operatingIncome * 12;
  // const propertyTaxExpense = (annualPropertyTax * -1) / 12;
  const propertyTaxExpense = annualPropertyTax * -1;
  const insuranceExpense = -1200; // sould this issuance be assumption or an input
  const electricGasWaterExpense = 0;
  const propertyManagementExpense = 0;
  const associationFeesExpense = 0;
  const maintenanceExpense =
    rentRateMonth * -1 * assumptions.maintenancePercentage;
  const others = 0;
  const annualOperatingExpenses =
    propertyTaxExpense +
    insuranceExpense +
    electricGasWaterExpense * 12 +
    propertyManagementExpense * 12 +
    associationFeesExpense * 12 +
    maintenanceExpense * 12 +
    others * 12;
  const annualNetOperatingIncome =
    annualOperatingIncome + annualOperatingExpenses;
  const initialCapitalImprovement = squareFootRehabRental * squareFootage;
  const initialCashInvested = initialCapitalImprovement + purchasePrice;
  const capRateForEndBuyer = annualNetOperatingIncome / initialCashInvested;

  const principlePrice = purchasePrice - valueDownPayment;
  const monthlyOperatingImcome = rentRateMonth + monthlyVacancyLoss;
  const monthlyOeratingExpenses =
    propertyTaxExpense / 12 +
    insuranceExpense / 12 +
    electricGasWaterExpense +
    propertyManagementExpense +
    associationFeesExpense +
    maintenanceExpense +
    others;
  const monthlyNetOperatingIncome =
    monthlyOperatingImcome + monthlyOeratingExpenses;
  const monthlyCashFlowProfit =
    monthlyNetOperatingIncome + monthlyMortgagePayment;
  const annualCashFlowProfit = monthlyCashFlowProfit * 12;
  const cashOnCashReturnForEndBuyer =
    annualCashFlowProfit / (valueDownPayment + initialCapitalImprovement);

  return {
    dealOptionNumber: 4,
    title: 'Land Contract Interest Only',
    options: {
      landContractPriceToSeller: {
        value: landContractPriceToSeller,
        label: 'Land Contract Price',
        type: ValueType.price,
        showToSeller: true,
      },
      landContractPriceToBuyer: {
        value: landContractPriceToBuyer,
        label: 'Land Contract Price to Buyer',
        type: ValueType.price,
      },
      monthlyPayment: {
        value: monthlyPayment,
        label: 'Monthly Payments',
        type: ValueType.price,
        showToSeller: true,
      },
      balloonPayment: {
        value: balloonPayment,
        label: 'Balloon Payment',
        type: ValueType.price,
        showToSeller: true,
      },
      valueDownPaymentSeller: {
        value: valueDownPaymentSeller,
        label: 'Down Payment',
        type: ValueType.price,
        showToSeller: true,
      },
      valueDownPaymentBuyer: {
        value: valueDownPaymentBuyer,
        label: 'Down Payment (buyer)',
        type: ValueType.price,
      },
      capRateForEndBuyer: {
        value: capRateForEndBuyer,
        label: 'Cap Rate for End Buyer',
        type: ValueType.percentage,
      },
      cashOnCashReturnForEndBuyer: {
        value: cashOnCashReturnForEndBuyer,
        label: 'Cash on Cash Return for End Buyer',
        type: ValueType.percentage,
      },
    },
  };
};

export const calculateOption5 = ({settings, propertyInformation}) => {
  const downPayment = settings.downPayment.value;
  const paymentTermLength = settings.paymentTermLength.value;
  const priceDiscount = settings.priceDiscount.value;
  const numerOfYearsTillBalloon = settings.numerOfYearsTillBalloon.value;
  const wholeSaleFee = settings.wholeSaleFee.value;

  const {
    afterRepairMarketValue,
    annualPropertyTax,
    bathroom,
    bedRoom,
    rentRateMonth,
    squareFootRehabFixFlip,
    squareFootRehabRental,
    squareFootage,
  } = propertyInformation;
  console.log({afterRepairMarketValue, priceDiscount});
  const priceToSeller = afterRepairMarketValue * priceDiscount;
  const valueDownPayment = priceToSeller * downPayment + wholeSaleFee;
  const monthlyMortgagePayment =
    ((priceToSeller - valueDownPayment) / (paymentTermLength * 12)) * -1;

  const monthlyPayment = monthlyMortgagePayment * -1;

  const priceToBuyer = priceToSeller + wholeSaleFee;
  const balloonPayment =
    priceToSeller * (1 - downPayment) -
    numerOfYearsTillBalloon * 12 * monthlyPayment;
  const valueDownPaymentSeller = downPayment * priceToSeller;
  const valueDownPaymentBuyer = valueDownPaymentSeller + wholeSaleFee;

  const monthlyVacancyLoss = rentRateMonth * -1 * assumptions.vacancyRate;
  const operatingIncome = rentRateMonth + monthlyVacancyLoss;
  const annualOperatingIncome = operatingIncome * 12;
  const propertyTaxExpense = (annualPropertyTax * -1) / 12;
  const insuranceExpense = -1200 / 12; // sould this issuance be assumption or an input
  const electricGasWaterExpense = 0;
  const propertyManagementExpense = 0;
  const associationFeesExpense = 0;
  const maintenanceExpense =
    rentRateMonth * -1 * assumptions.maintenancePercentage;
  const others = 0;
  const annualOperatingExpenses =
    propertyTaxExpense * 12 +
    insuranceExpense * 12 +
    electricGasWaterExpense * 12 +
    propertyManagementExpense * 12 +
    associationFeesExpense * 12 +
    maintenanceExpense * 12 +
    others * 12;
  const annualNetOperatingIncome =
    annualOperatingIncome + annualOperatingExpenses;
  const initialCapitalImprovement = squareFootRehabRental * squareFootage;
  const initialCashInvested = initialCapitalImprovement + priceToSeller;
  const capRateForEndBuyer = annualNetOperatingIncome / initialCashInvested;

  const monthlyOperatingImcome = rentRateMonth + monthlyVacancyLoss;
  const monthlyOeratingExpenses =
    propertyTaxExpense +
    insuranceExpense +
    electricGasWaterExpense +
    propertyManagementExpense +
    associationFeesExpense +
    maintenanceExpense +
    others;
  const monthlyNetOperatingIncome =
    monthlyOperatingImcome + monthlyOeratingExpenses;
  const monthlyCashFlowProfit =
    monthlyNetOperatingIncome + monthlyMortgagePayment;
  const annualCashFlowProfit = monthlyCashFlowProfit * 12;
  const cashOnCashReturnForEndBuyer =
    annualCashFlowProfit / (valueDownPayment + initialCapitalImprovement);

  return {
    dealOptionNumber: 5,
    title: 'Land Contract Principle Only',
    options: {
      priceToSeller: {
        value: priceToSeller,
        label: 'Land Contract Price',
        type: ValueType.price,
        showToSeller: true,
      },
      priceToBuyer: {
        value: priceToBuyer,
        label: 'Land Contract Price to Buyer',
        type: ValueType.price,
      },
      monthlyPayment: {
        value: monthlyPayment,
        label: 'Monthly Payments',
        type: ValueType.price,
        showToSeller: true,
      },
      balloonPayment: {
        value: balloonPayment,
        label: 'Balloon Payment',
        type: ValueType.price,
        showToSeller: true,
      },
      valueDownPaymentSeller: {
        value: valueDownPaymentSeller,
        label: 'Down Payment',
        type: ValueType.price,
        showToSeller: true,
      },
      valueDownPaymentBuyer: {
        value: valueDownPaymentBuyer,
        label: 'Down Payment (buyer)',
        type: ValueType.price,
      },
      capRateForEndBuyer: {
        value: capRateForEndBuyer,
        label: 'Cap Rate for End Buyer',
        type: ValueType.percentage,
      },
      cashOnCashReturnForEndBuyer: {
        value: cashOnCashReturnForEndBuyer,
        label: 'Cash on Cash Return for End Buyer',
        type: ValueType.percentage,
      },
    },
  };
};

export const calculateOption6 = ({settings, propertyInformation}) => {
  const numYears = settings.numYears.value;
  const optionDepositToSeller = settings.optionDepositToSeller.value;
  const priceDiscount = settings.priceDiscount.value;
  const minOptionDepositFromTenantBuyer =
    settings.minOptionDepositFromTenantBuyer.value;

  const {
    afterRepairMarketValue,
    annualPropertyTax,
    bathroom,
    bedRoom,
    rentRateMonth,
    squareFootRehabFixFlip,
    squareFootRehabRental,
    squareFootage,
  } = propertyInformation;

  const priceToSeller = priceDiscount * afterRepairMarketValue;
  const priceToTenant = afterRepairMarketValue;
  const leaseOptionRentToSeller = rentRateMonth * 0.75;
  const monthlyCashFlow = rentRateMonth - leaseOptionRentToSeller;
  const totalProfit =
    priceToTenant -
    priceToSeller +
    monthlyCashFlow * numYears * 12 +
    minOptionDepositFromTenantBuyer;

  return {
    dealOptionNumber: 6,
    title: 'Sandwich Lease Option',
    options: {
      priceToSeller: {
        value: priceToSeller,
        label: 'Price to Seller',
        type: ValueType.price,
        showToSeller: true,
      },
      priceToTenant: {
        value: priceToTenant,
        label: 'Price to Tenant',
        type: ValueType.price,
      },
      leaseOptionRentToSeller: {
        value: leaseOptionRentToSeller,
        label: 'Lease Option Rent',
        type: ValueType.price,
        showToSeller: true,
      },
      monthlyCashFlow: {
        value: monthlyCashFlow,
        label: 'Monthly Cash Flow',
        type: ValueType.price,
      },
      totalProfit: {
        value: totalProfit,
        label: 'Total Profit',
        type: ValueType.price,
      },
    },
  };
};

export const calculateOption7 = ({settings, propertyInformation}) => {
  const minOptionDepositFromTenantBuyer =
    settings.minOptionDepositFromTenantBuyer.value;
  const numYears = settings.numYears.value;
  const optionDepositToSeller = settings.optionDepositToSeller.value;
  const priceDiscount = settings.priceDiscount.value;

  const {
    afterRepairMarketValue,
    annualPropertyTax,
    bathroom,
    bedRoom,
    rentRateMonth,
    squareFootRehabFixFlip,
    squareFootRehabRental,
    squareFootage,
  } = propertyInformation;

  const priceToSeller = priceDiscount * afterRepairMarketValue;
  const priceToTenant = priceToSeller + minOptionDepositFromTenantBuyer;
  const leaseOptionRentToSeller = rentRateMonth;
  const totalProfit = minOptionDepositFromTenantBuyer - optionDepositToSeller;

  return {
    dealOptionNumber: 7,
    title: 'Wholesale Lease Option',
    options: {
      priceToSeller: {
        value: priceToSeller,
        label: 'Price to Seller',
        type: ValueType.price,
        showToSeller: true,
      },
      priceToTenant: {
        value: priceToTenant,
        label: 'Price to Tenant',
        type: ValueType.price,
      },
      leaseOptionRentToSeller: {
        value: leaseOptionRentToSeller,
        label: 'Lease Option Rent',
        type: ValueType.price,
        showToSeller: true,
      },
      totalProfit: {
        value: totalProfit,
        label: 'Total Profit',
        type: ValueType.price,
      },
    },
  };
};

export const calculateOption8 = ({settings, propertyInformation}) => {
  // const balanceOnMortgage = settings.balanceOnMortgage.value;
  const cashForKeysPurchaseAmountAboveBalance =
    settings.cashForKeysPurchaseAmountAboveBalance.value;
  const {
    afterRepairMarketValue,
    annualPropertyTax,
    bathroom,
    bedRoom,
    rentRateMonth,
    squareFootRehabFixFlip,
    squareFootRehabRental,
    squareFootage,
    piti,
    currentMortgateBalance,
  } = propertyInformation;

  const purchansePrice =
    currentMortgateBalance + cashForKeysPurchaseAmountAboveBalance;
  const downPayment = cashForKeysPurchaseAmountAboveBalance;

  const equityPosition = 1 - purchansePrice / afterRepairMarketValue;
  const pitiPaymentPerMonth = piti;

  const monthlyVacancyLoss = rentRateMonth * -1 * assumptions.vacancyRate;
  const operatingIncome = rentRateMonth + monthlyVacancyLoss;
  const annualOperatingIncome = operatingIncome * 12;
  const propertyTaxExpense = 0;
  const insuranceExpense = 0; // sould this issuance be assumption or an input
  const electricGasWaterExpense = 0;
  const propertyManagementExpense = 0;
  const associationFeesExpense = 0;
  const maintenanceExpense =
    rentRateMonth * -1 * assumptions.maintenancePercentage;
  const others = 0;
  const annualOperatingExpenses =
    propertyTaxExpense * 12 +
    insuranceExpense * 12 +
    electricGasWaterExpense * 12 +
    propertyManagementExpense * 12 +
    associationFeesExpense * 12 +
    maintenanceExpense * 12 +
    others * 12;
  const annualNetOperatingIncome =
    annualOperatingIncome + annualOperatingExpenses;
  const initialCapitalImprovement = squareFootRehabRental * squareFootage;
  const initialCashInvested = initialCapitalImprovement + purchansePrice;
  const capRate = annualNetOperatingIncome / initialCashInvested;

  const monthlyMortgagePayment = piti * -1;
  const monthlyOperatingImcome = rentRateMonth + monthlyVacancyLoss;
  const monthlyOeratingExpenses =
    propertyTaxExpense +
    insuranceExpense +
    electricGasWaterExpense +
    propertyManagementExpense +
    associationFeesExpense +
    maintenanceExpense +
    others;

  const monthlyNetOperatingIncome =
    monthlyOperatingImcome + monthlyOeratingExpenses;
  const monthlyCashFlowProfit =
    monthlyNetOperatingIncome + monthlyMortgagePayment;
  const annualCashFlowProfit = monthlyCashFlowProfit * 12;
  const cashOnCashReturnForEndBuyer =
    annualCashFlowProfit / (initialCapitalImprovement + downPayment);

  return {
    dealOptionNumber: 8,
    title: 'Subject To Mortgage',
    options: {
      equityPosition: {
        value: equityPosition,
        label: 'Equity Position',
        type: ValueType.percentage,
      },
      pitiPaymentPerMonth: {
        value: pitiPaymentPerMonth,
        label: 'PITI Payment / Mth',
        type: ValueType.price,
      },
      capRate: {
        value: capRate,
        label: 'Cap Rate',
        type: ValueType.percentage,
      },
      cashOnCashReturnForEndBuyer: {
        value: cashOnCashReturnForEndBuyer,
        label: 'Cash on Cash Return',
        type: ValueType.percentage,
      },
    },
  };
};

export const decimalValuetoPercentage = (value) => (value * 100).toFixed(2);
export const percentageToDecimalValue = (value) => value / 100;
export const toStringMoneyValue = (value) =>
  `${CURRENCY_SYMBOL} ${parseFloat(value.toFixed(2)).toLocaleString()}`;
export const isValidNumber = (value) => {
  if (_.isEmpty(value)) return false;
  const number = _.toNumber(value);
  return !_.isNaN(number);
};
