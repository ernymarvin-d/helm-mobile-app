import React from 'react';
import {View, Image, Text, TouchableOpacity} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Home from '../screens/Home';
import MyDeals from '../screens/MyDeals';
import Tutorials from '../screens/Tutorials';
import Settings from '../screens/Settings';
import homeIcon from './../assets/images/tab-icon-home.png';
import tutorialsIcon from './../assets/images/tab-icon-tutorials.png';
import myDealsIcon from './../assets/images/tab-icon-my-deals.png';
import settingsIcon from './../assets/images/tab-icon-settings.png';
import {createStackNavigator} from '@react-navigation/stack';
import TutorialStack from './TutorialStack';
import MyDealsStack from './MyDealsStack';
import SettingsStack from './SettingsStack';
import HomeStack from './HomeStack';

const Tab = createBottomTabNavigator();

const HomeTab = () => {
  const screenOptions = ({route}) => ({
    tabBarIcon: ({focused, color, size}) => {
      let icon;
      let tintColor = focused ? 'tomato' : 'gray';
      let iconTint = {tintColor};

      switch (route.name) {
        case 'HomeTab':
          icon = homeIcon;
          tintColor;
          break;
        case 'TutorialsTab':
          icon = tutorialsIcon;
          break;
        case 'MyDealsTab':
          icon = myDealsIcon;
          break;
        case 'SettingsTab':
          icon = settingsIcon;
          break;
      }
      return (
        <Image
          style={[iconTint]}
          source={icon}
          resizeMethod="auto"
          resizeMode="contain"
        />
      );
    },
  });

  const tabBarOptions = {
    activeTintColor: 'tomato',
    inactiveTintColor: 'gray',
  };

  return (
    <Tab.Navigator screenOptions={screenOptions} tabBarOptions={tabBarOptions}>
      <Tab.Screen
        name="HomeTab"
        component={HomeStack}
        options={{title: 'Home'}}
      />
      <Tab.Screen
        name="TutorialsTab"
        component={TutorialStack}
        options={{title: 'Tutorials', tabBarVisible: false}}
      />
      <Tab.Screen
        name="MyDealsTab"
        component={MyDealsStack}
        options={{title: 'My Deals', tabBarVisible: false}}
      />
      <Tab.Screen
        name="SettingsTab"
        component={SettingsStack}
        options={{title: 'Settings', tabBarVisible: false}}
      />
    </Tab.Navigator>
  );
};

export default HomeTab;
