import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import PhoneNumberVerificationSuccess from '../screens/PhoneNumberVerificationSuccess';
import PhoneNumberVerification from '../screens/PhoneNumberVerification';

const Stack = createStackNavigator();

const PhoneNumberVerificationStack = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen
        name="PhoneNumberVerification"
        component={PhoneNumberVerification}
      />
      <Stack.Screen
        name="PhoneNumberVerificationSuccess"
        component={PhoneNumberVerificationSuccess}
      />
    </Stack.Navigator>
  );
};

export default PhoneNumberVerificationStack;
