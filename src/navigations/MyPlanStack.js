import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import MyPlanPaymentConfirmation from '../screens/MyPlanPaymentConfirmation';
import MyPlanPaymentFlow from '../screens/MyPlanPaymentFlow';
import ProfileMyPlan from '../screens/ProfileMyPlan';

const Stack = createStackNavigator();

const MyPlanStack = () => {
  return (
    <Stack.Navigator initialRoute="ProfileMyPlan">
      <Stack.Screen
        name="ProfileMyPlan"
        component={ProfileMyPlan}
        options={{title: 'My Plan'}}
      />
      <Stack.Screen name="MyPlanPaymentFlow" component={MyPlanPaymentFlow} />
      <Stack.Screen
        name="MyPlanPaymentConfirmation"
        component={MyPlanPaymentConfirmation}
      />
    </Stack.Navigator>
  );
};

export default MyPlanStack;
