import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import AuthSelection from '../screens/AuthSelection';
import ResetPasswordStack from './ResetPasswordStack';
import Login from '../screens/Login';
import SignupStack from './SignupStack';

const Stack = createStackNavigator();

const AuthStack = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="AuthSelection" component={AuthSelection} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="SignupStack" component={SignupStack} />
      <Stack.Screen name="ResetPasswordStack" component={ResetPasswordStack} />
    </Stack.Navigator>
  );
};

export default AuthStack;
