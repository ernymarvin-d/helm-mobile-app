import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Splash from './../screens/Splash';
import AppIntro from './../screens/AppIntro';

const Stack = createStackNavigator();

const IntroStack = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Splash" component={Splash} />
      <Stack.Screen name="AppIntro" component={AppIntro} />
    </Stack.Navigator>
  );
};

export default IntroStack;
