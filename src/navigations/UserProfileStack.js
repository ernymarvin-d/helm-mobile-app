import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import ProfileMyProfile from '../screens/ProfileMyProfile';
import ProfileAccountSettings from '../screens/ProfileAccountSettings';
import ProfileGiveFeedback from '../screens/ProfileGiveFeedback';
import ProfileAddPaymentMethod from '../screens/ProfileAddPaymentMethod';
import MyPlanStack from './MyPlanStack';

const Stack = createStackNavigator();

const UserProfileStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{headerShown: true}}
      initialRouteName="MyProfile">
      <Stack.Screen
        name="MyProfile"
        component={ProfileMyProfile}
        options={{title: ''}}
      />
      <Stack.Screen name="AccountSettings" component={ProfileAccountSettings} />
      <Stack.Screen name="GiveFeedback" component={ProfileGiveFeedback} />
      <Stack.Screen
        name="AddPaymentMethod"
        component={ProfileAddPaymentMethod}
      />
      <Stack.Screen
        name="MyPlanStack"
        component={MyPlanStack}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default UserProfileStack;
