import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Home from '../screens/Home';
import UserProfileStack from './UserProfileStack';
import CreateDealsStack from './CreateDealsStack';

const Stack = createStackNavigator();

const HomeStack = () => (
  <Stack.Navigator>
    <Stack.Screen
      name="HomePage"
      component={Home}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="UserProfileStack"
      component={UserProfileStack}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="CreateDealsStack"
      component={CreateDealsStack}
      options={{headerShown: false}}
    />
  </Stack.Navigator>
);

export default HomeStack;
