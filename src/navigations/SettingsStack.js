import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Settings from '../screens/Settings';
import SettingsGeneral from '../screens/SettingsGeneral';
import SettingsFaq from '../screens/SettingsFaq';
import SettingsMyDeals from '../screens/SettingsMyDeals';
import SettingsTermsOfService from '../screens/SettingsTermsOfService';
import SettingsContact from '../screens/SettingsContact';
import ContactEmailConfirmationScreen from '../screens/ContactEmailConfirmationScreen';
import MyDealsStack from '../navigations/MyDealsStack';

const Stack = createStackNavigator();

const SettingsStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{headerShown: true}}
      initialRouteName="Settings">
      <Stack.Screen
        screenOptions={{headerShown: true}}
        name={'Settings'}
        component={Settings}
      />
      <Stack.Screen
        screenOptions={{headerShown: true}}
        options={{title: 'General Settings'}}
        name={'General'}
        component={SettingsGeneral}
      />
      <Stack.Screen
        screenOptions={{headerShown: true}}
        options={{title: 'FAQ'}}
        name={'Faq'}
        component={SettingsFaq}
      />
      <Stack.Screen
        screenOptions={{headerShown: false}}
        options={{headerShown: false}}
        name={'MyDealsStack'}
        component={MyDealsStack}
      />
      <Stack.Screen
        screenOptions={{headerShown: true}}
        options={{title: 'Terms of Service'}}
        name={'Tos'}
        component={SettingsTermsOfService}
      />
      <Stack.Screen
        screenOptions={{headerShown: true}}
        options={{title: 'Contact Helm'}}
        name={'Contact'}
        component={SettingsContact}
      />
      <Stack.Screen
        screenOptions={{headerShown: false}}
        options={{title: 'Contact Helm'}}
        name={'ContactEmailConfirmationScreen'}
        component={ContactEmailConfirmationScreen}
      />
    </Stack.Navigator>
  );
};

export default SettingsStack;
