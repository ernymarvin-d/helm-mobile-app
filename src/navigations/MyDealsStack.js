import React from 'react';
import {Button} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import Tutorials from '../screens/Tutorials';
import MyDeals from '../screens/MyDeals';
import DealsCalculator from '../screens/DealsCalculator';
import DealsDetails from '../screens/DealsDetails';
import DealsSellerInformation from '../screens/DealsSellerInformation';
import DealsLoi from '../screens/DealsLoi';
import CreateDealsSuccess from '../screens/CreateDealsSuccess';
import CreateDealsLoiTemplate from '../screens/CreateDealsLoiTemplate';
const Stack = createStackNavigator();

const MyDealsStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{headerShown: true}}
      initialRouteName="MyDeals">
      <Stack.Screen
        name={'MyDeals'}
        component={MyDeals}
        options={{title: 'My Deals'}}
      />
      <Stack.Screen
        name={'DealsDetails'}
        component={DealsDetails}
        options={{title: 'Deals Details'}}
      />
      <Stack.Screen
        name={'DealsSellerInformation'}
        component={DealsSellerInformation}
        options={{title: 'Seller Information'}}
      />
      {/* <Stack.Screen
        name={'DealsLoi'}
        component={DealsLoi}
        options={{title: 'Letter of Intent'}}
      /> */}
      <Stack.Screen
        name={'DealsLoi'}
        component={CreateDealsLoiTemplate}
        options={{title: 'Letter of Intent'}}
      />
      <Stack.Screen
        name={'CreateDealsSuccess'}
        component={CreateDealsSuccess}
        options={{title: 'Create Deals Success'}}
      />
    </Stack.Navigator>
  );
};

export default MyDealsStack;
