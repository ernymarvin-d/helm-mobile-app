import React from 'react';
import {Button} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import Tutorials from '../screens/Tutorials';
import MyPlanStack from './MyPlanStack';
const Stack = createStackNavigator();

const TutorialStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{headerShown: true}}
      initialRouteName="Tutorials">
      <Stack.Screen name={'Tutorials'} component={Tutorials} />
      <Stack.Screen
        name="MyPlanStack"
        component={MyPlanStack}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default TutorialStack;
