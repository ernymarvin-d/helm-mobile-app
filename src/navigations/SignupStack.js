import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import SignupAccountInfo from '../screens/SignupAccountInfo';
import SignupBusinessInfo from '../screens/SignupBusinessInfo';
import PhoneNumberVerificationStack from './PhoneNumberVerificationStack';

const Stack = createStackNavigator();

const SignupStack = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="SignupAccountInfo" component={SignupAccountInfo} />
      <Stack.Screen name="SignupBusinessInfo" component={SignupBusinessInfo} />
      <Stack.Screen
        name="PhoneNumberVerificationStack"
        component={PhoneNumberVerificationStack}
      />
    </Stack.Navigator>
  );
};

export default SignupStack;
