import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import CreateDealsSellerInformation from './../screens/CreateDealsSellerInformation';
import CreateDealsPropertiesInformation from './../screens/CreateDealsPropertiesInformation';
import CreateDealsDealsCalculator from './../screens/CreateDealsDealsCalculator';
import CreateDealsLoiTemplate from './../screens/CreateDealsLoiTemplate';
import CreateDealsSuccess from './../screens/CreateDealsSuccess';
import MyPlanStack from '../navigations/MyPlanStack';

const Stack = createStackNavigator();

// Stack.navigationOptions = ({navigation}) => {
//   let tabBarVisible = true;
//   let routeName = navigation.state.routes[navigation.state.index].routeName;
//   if (routeName === 'CreateDealsSellerInformation') {
//     tabBarVisible = false;
//   }
//   return {
//     tabBarVisible,
//   };
// };

const CreateDealsStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{headerShown: false}}
      initialRouteName="CreateDealsSellerInformation">
      <Stack.Screen
        options={{
          headerShown: true,
          title: 'Seller Information',
          tabBar: {visible: false},
        }}
        name="CreateDealsSellerInformation"
        component={CreateDealsSellerInformation}
      />
      <Stack.Screen
        options={{
          headerShown: true,
          title: 'Properties Information',
        }}
        name="CreateDealsPropertiesInformation"
        component={CreateDealsPropertiesInformation}
      />
      <Stack.Screen
        options={{
          headerShown: true,
          title: 'Deals Calculator',
        }}
        name="CreateDealsDealsCalculator"
        component={CreateDealsDealsCalculator}
      />
      <Stack.Screen
        options={{
          headerShown: true,
          title: 'Letter of Intent',
        }}
        name="CreateDealsLoiTemplate"
        component={CreateDealsLoiTemplate}
      />
      <Stack.Screen
        options={{
          headerShown: false,
        }}
        name="CreateDealsSuccess"
        component={CreateDealsSuccess}
      />
      <Stack.Screen
        name="MyPlanStack"
        component={MyPlanStack}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default CreateDealsStack;
