import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import ResetSelectDetails from '../screens/ResetSelectDetails';
import ResetRecoveryPin from '../screens/ResetRecoveryPin';
import ResetInputPassword from '../screens/ResetInputPassword';
import ResetPasswordSuccess from '../screens/ResetPasswordSuccess';

const Stack = createStackNavigator();

export default ResetPasswordStack = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="ResetSelectDetails" component={ResetSelectDetails} />
      <Stack.Screen name="ResetRecoveryPin" component={ResetRecoveryPin} />
      <Stack.Screen name="ResetInputPassword" component={ResetInputPassword} />
      <Stack.Screen
        name="ResetPasswordSuccess"
        component={ResetPasswordSuccess}
      />
    </Stack.Navigator>
  );
};
