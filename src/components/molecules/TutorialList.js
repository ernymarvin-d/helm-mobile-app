import React, {useState, useEffect} from 'react';
import {
  Text,
  Image,
  View,
  ListView,
  FlatList,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import Accordion from 'react-native-collapsible/Accordion';
import lockIcon from '../../assets/images/ic-lock.png';
import playButtonIcon from '../../assets/images/ic-play-button.png';
import listBulletIcon from '../../assets/images/ic-bold-arrow-right.png';
// import {tutorialData} from '../../utils/Mocks';
import EStyleSheet from 'react-native-extended-stylesheet';
import {
  PADDING_DEFAULT,
  PADDING_SMALL,
  MARGIN_SMALL,
} from '../../assets/dimens';
import {globalStyles} from '../../assets/styles/globalStyles';
import {
  GREY,
  BOX_BORDER_COLOR,
  BOX_BACKGROUND_COLOR,
  BLUE,
} from '../../assets/colors';
import {
  setActiveTutorial,
  setTutorials,
} from '../../state-management/actions/TutorialActions';
import {connect} from 'react-redux';

const styles = EStyleSheet.create({
  activeBgColor: {
    backgroundColor: BOX_BACKGROUND_COLOR,
  },
  headerList: {
    flexDirection: 'row',
    padding: PADDING_DEFAULT,
    justifyContent: 'flex-start',
    alignItems: 'center',
    borderTopWidth: 1,
    borderTopColor: BOX_BORDER_COLOR,
  },
  listBulletIcon: {
    width: '8rem',
    height: '8rem',
    tintColor: GREY[1],
  },
  contentIcon: {
    width: '14rem',
    height: '14rem',
    tintColor: GREY[2],
  },
  contentIconActive: {
    tintColor: BLUE,
  },
  accortionHeaderText: {
    paddingLeft: '10rem',
    fontWeight: 'bold',
  },
  rotateActive: {transform: [{rotate: '90deg'}]},
  contentTitle: {
    paddingLeft: '10rem',
  },
  contentItem: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: MARGIN_SMALL,
  },
  contentItemWrapper: {
    width: '80%',
    alignSelf: 'center',
  },
  contentWrapper: {flex: 1, paddingBottom: PADDING_DEFAULT},
});

const Sylabus = ({icon, iconStyle, title}) => {
  return (
    <View style={styles.contentItemWrapper}>
      <View style={styles.contentItem}>
        <Image
          source={icon}
          style={[styles.contentIcon, iconStyle ? iconStyle : {}]}
        />
        <Text style={[globalStyles.p2, styles.contentTitle]}>{title}</Text>
      </View>
    </View>
  );
};

const TutorialList = ({setActiveTutorial, jumpTo, tutorials, setTutorials}) => {
  const [activeSections, setActiveSections] = useState([]);
  const renderHeader = (content, index, isActive, sections) => {
    const boxActiveStyle = isActive ? styles.activeBgColor : {};
    const iconActiveStyle = isActive ? styles.rotateActive : {};
    return (
      <View style={[styles.headerList, boxActiveStyle]}>
        <Image
          source={listBulletIcon}
          style={[styles.listBulletIcon, iconActiveStyle]}
        />
        <Text style={[globalStyles.p1, styles.accortionHeaderText]}>
          {content.courseTitle}
        </Text>
      </View>
    );
  };

  const renderContent = (content, index, isActive, sections) => {
    console.log({content});
    const boxActiveStyle = isActive ? styles.activeBgColor : {};
    return (
      <TouchableOpacity
        onPress={() => {
          setActiveTutorial(content);
          jumpTo('webinar');
        }}>
        <View style={[boxActiveStyle, styles.contentWrapper]}>
          <Sylabus
            icon={playButtonIcon}
            title={content.videoTitle}
            iconStyle={{tintColor: BLUE}}
          />
          {content.syllabus.map((item) => {
            return <Sylabus icon={lockIcon} title={item} />;
          })}
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <View>
      <Accordion
        expandMultiple
        activeSections={activeSections}
        sections={tutorials}
        renderHeader={renderHeader}
        renderContent={renderContent}
        onChange={(value) => {
          console.log(value);
          setActiveSections(value);
        }}
      />
    </View>
  );
};

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    setActiveTutorial: (tutorial) => dispatch(setActiveTutorial(tutorial)),
    setTutorials: (tutorials) => dispatch(setTutorials(tutorials)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TutorialList);
