import * as React from 'react';
import {View} from 'react-native';
import GoogleIconButton from './../atoms/GoogleIconButton';
import FacebookIconButton from './../atoms/FacebookIconButton';
import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  root: {
    paddingBottom: '1rem',
  },
  iconButtonWrapper: {
    flexDirection: 'row',
    width: '100rem',
    alignSelf: 'center',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: '1rem',
  },
});

const GoogleAndFacebookSigninButton = ({onGoogleButtonClick}) => {
  return (
    <View style={styles.root}>
      <View style={styles.iconButtonWrapper}>
        <GoogleIconButton />
        <FacebookIconButton />
      </View>
    </View>
  );
};

export default GoogleAndFacebookSigninButton;
