import React from 'react';
import {View, Image, Text} from 'react-native';
import {globalStyles} from '../../assets/styles/globalStyles';
import {
  BOX_BACKGROUND_COLOR_2,
  BOX_BORDER_COLOR_2,
  BLUE,
} from '../../assets/colors';
import {
  DEALS_BOX_BORDER_RADIUS,
  MARGIN_DEFAULT,
  PADDING_DEFAULT,
  MARGIN_LARGE,
  PADDING_SMALL,
} from '../../assets/dimens';
import checkIcon from '../../assets/images/ic-check.png';
import EStyleSheet from 'react-native-extended-stylesheet';
import _ from 'lodash';
import {
  toStringMoneyValue,
  decimalValuetoPercentage,
} from '../../utils/OptionsCalculatorUtil';
import {ValueType} from '../../assets/Constants';

const styles = EStyleSheet.create({
  icon: {
    width: '15rem',
    height: '15rem',
    tintColor: BLUE,
    marginTop: '5rem',
  },
  optionWrapper: {
    paddingTop: MARGIN_LARGE,
  },
  optionHeaderWrapper: {
    alignItems: 'flex-start',
    flexDirection: 'row',
    width: '100%',
  },
  optionName: {
    marginBottom: '5rem',
    fontWeight: '600',
  },
  optionHeader: {
    fontWeight: '600',
  },
  optionHeaderTextWrapper: {
    flex: 1,
    paddingLeft: PADDING_SMALL,
    lineHeight: '22rem',
  },
  optionContent: {
    marginVertical: MARGIN_DEFAULT,
  },
});

const MyDealOptions = ({dealOptions}) => {
  return Object.values(dealOptions).map((dealOption) => {
    const formattedValue = _.isEqual(dealOption.type, ValueType.price)
      ? toStringMoneyValue(dealOption.value)
      : `${decimalValuetoPercentage(dealOption.value)}%`;
    return (
      <Text style={[globalStyles.p2, styles.optionContent]}>
        {dealOption.label} {formattedValue}
      </Text>
    );
  });
};

const MyDealOptionsItem = ({item, name}) => {
  const {title, values} = item;
  return (
    <View style={styles.optionWrapper}>
      <View style={styles.optionHeaderWrapper}>
        <Image
          style={styles.icon}
          source={checkIcon}
          resizeMethod="scale"
          resizeMode="contain"
        />
        <View style={styles.optionHeaderTextWrapper}>
          <Text style={[globalStyles.p1, styles.optionName]}>{name}</Text>
          <Text style={[globalStyles.p1, styles.optionHeader]}>{title}</Text>
          <MyDealOptions dealOptions={item.options} />
        </View>
      </View>
    </View>
  );
};

export default MyDealOptionsItem;
