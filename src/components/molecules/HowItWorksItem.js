import React from 'react';
import {View, Image, Text} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import {GREY, BOX_BORDER_COLOR, BLUE} from '../../assets/colors';
import {globalStyles} from '../../assets/styles/globalStyles';
import {MARGIN_DEFAULT} from '../../assets/dimens';
import IconDivider from './../atoms/IconDivider';

const styles = EStyleSheet.create({
  root: {
    width: '100%',
  },
  wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
  },
  icon: {
    width: '60rem',
    height: '60rem',
    tintColor: BLUE,
    marginHorizontal: MARGIN_DEFAULT,
  },
  label: {
    fontWeight: '700',
    margin: 0,
    padding: 0,
    paddingBottom: '5rem',
  },
  textWrapper: {
    paddingLeft: '10rem',
    justifyContent: 'center',
    alignItems: 'flex-start',
    flexShrink: 1,
  },
  subText: {
    lineHeight: 18,
  },
});

const HowItWorksItem = ({
  containerStyle,
  icon,
  title,
  content,
  showDivider,
}) => {
  return (
    <View style={[styles.root, containerStyle ? containerStyle : {}]}>
      <View style={styles.wrapper}>
        <Image source={icon} style={styles.icon} resizeMode="stretch" />
        <View style={styles.textWrapper}>
          <Text style={[globalStyles.p2, styles.label]}>{title}</Text>
          <Text style={[globalStyles.p2, styles.subText]}>{content}</Text>
        </View>
      </View>
      {showDivider ? <IconDivider /> : null}
    </View>
  );
};

export default HowItWorksItem;
