import React from 'react';
import {View, Text, Image} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import Avatar, {IconTypes, Sizes} from 'rn-avatar';
import {globalStyles} from '../../assets/styles/globalStyles';
import {GREY, BLUE} from '../../assets/colors';
import TextButton from '../atoms/TextButton';
import menuIcon from '../../assets/images/ic-menu.png';
import {
  PADDING_DEFAULT,
  PADDING_SMALL,
  MARGIN_SMALL,
  MARGIN_DEFAULT,
} from '../../assets/dimens';
import IconButton from '../atoms/IconButton';
import _ from 'lodash';

const styles = EStyleSheet.create({
  root: {
    width: '100%',
    height: '52rem',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: PADDING_DEFAULT,
    paddingVertical: PADDING_SMALL,
    marginVertical: MARGIN_SMALL,
  },
  avatarWrapper: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  name: {
    color: GREY[0],
    width: '100rem',
  },
  buttonWrapper: {
    width: '150rem',
  },
  menuIcon: {
    tintColor: BLUE,
    margin: 0,
    padding: 0,
    width: '15rem',
    height: '15rem',
  },
  menuIconContainer: {
    width: '15rem',
    height: '15rem',
    marginRight: MARGIN_SMALL,
  },
  avatarContainer: {marginRight: 10},
});

const HomePageToolbar = ({
  onCreateDealsClicked,
  onLeftMenuClick,
  user,
  onPressAvatar,
}) => {
  const fullName = _.isNull(user) ? '' : user.info.fullName;
  const photoURL = _.isNull(user)
    ? null
    : {
        uri: user.info.photoURL,
      };

  return (
    <View style={styles.root}>
      <View style={styles.avatarWrapper}>
        <IconButton
          iconStyle={styles.menuIcon}
          icon={menuIcon}
          bgColor="transparent"
          containerStyle={styles.menuIconContainer}
          onPress={onLeftMenuClick}
        />
        <Avatar
          rounded
          source={photoURL || null}
          size={Sizes.MEDIUM}
          title={fullName}
          containerStyle={styles.avatarContainer}
          onPress={onPressAvatar}
        />
        <Text
          style={[globalStyles.p1, styles.name]}
          numberOfLines={1}
          ellipsizeMode="tail">
          {fullName}
        </Text>
      </View>
      <View style={styles.buttonWrapper}>
        <TextButton
          width={'100%'}
          text="Create Deals"
          type="primary"
          onPress={onCreateDealsClicked}
        />
      </View>
    </View>
  );
};

export default HomePageToolbar;
