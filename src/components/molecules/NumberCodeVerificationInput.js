import React, {useRef, useState} from 'react';
import {View} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import SingleNumberInput from '../atoms/SingleNumberInput';
import _ from 'lodash';

const styles = EStyleSheet.create({
  wrapper: {
    width: '100%',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
});

export default NumberCodeVerificationInput = ({numDigit, onValueChange}) => {
  const [boxRef, setBoxRef] = useState({});
  const [digitCode, setDigitCode] = useState([]);

  const onValueChangeHandler = () => {
    if (onValueChange) {
      onValueChange(digitCode.join(''));
    }
  };

  const renderInputBox = (numOfBoxes) => {
    const jumpToAnotherBox = (boxIndex) =>
      boxRef[`box_ref_${boxIndex}`].focus();

    const boxes = [];

    for (let ctr = 0; ctr < numOfBoxes; ctr++) {
      boxRef[`box_ref_${ctr}`] = useRef();
      boxes.push(
        <SingleNumberInput
          ref={(ref) => (boxRef[`box_ref_${ctr}`] = ref)}
          textInputProps={{
            onChangeText: (value) => {
              const isLastTextBox = ctr === numOfBoxes - 1;
              const isFirstTextBox = ctr === 0;
              if (value === '') {
                delete digitCode[ctr];
              } else {
                digitCode[ctr] = value;
              }
              onValueChangeHandler();
              if (value === '' && isFirstTextBox) return;

              if (value === '' && !isFirstTextBox) {
                jumpToAnotherBox(ctr - 1);
                return;
              }

              if (ctr < numOfBoxes && !isLastTextBox) {
                jumpToAnotherBox(ctr + 1);
                return;
              }
            },
            onKeyPress: ({nativeEvent}) => {
              const isFirstTextBox = ctr === 0;
              if (nativeEvent.key === 'Backspace' && !isFirstTextBox) {
                jumpToAnotherBox(ctr - 1);
                return;
              }
            },
          }}
        />,
      );
    }
    return boxes;
  };

  return numDigit ? (
    <View style={styles.wrapper}>{renderInputBox(numDigit)}</View>
  ) : null;
};
