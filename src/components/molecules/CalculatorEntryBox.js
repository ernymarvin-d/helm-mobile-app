import React, {useState} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import {GREY} from '../../assets/colors';
import {PADDING_DEFAULT, MARGIN_SMALL} from '../../assets/dimens';
import {globalStyles} from '../../assets/styles/globalStyles';
import CalculatorGradientProgress from '../atoms/CalculatorGradientProgress';
import Dialog from 'react-native-dialog';
import {isValidNumber} from '../../utils/OptionsCalculatorUtil';

const styles = EStyleSheet.create({
  calculatorEntryBox: {
    borderRadius: '5rem',
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: GREY[4],
    padding: PADDING_DEFAULT,
    marginVertical: MARGIN_SMALL,
  },
  entryBoxHeaderWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  boxEntryTitle: {
    width: '65%',
  },
  entryTitleText: {
    fontWeight: '600',
    lineHeight: '15rem',
  },
  entryEditButton: {
    fontWeight: '600',
    fontFamily: 'Montserrat-Medium',
  },
  invalidValue: {borderColor: 'red', borderWidth: '1rem'},
  defaultDialogInputStyle: {
    borderColor: '#cccccc',
    borderWidth: '1rem',
    paddingLeft: '5rem',
    paddingRight: '5rem',
  },
});

const CalculatorEntryBox = ({value, title, isPercentage, onUpdate}) => {
  const [editedValue, setEditedValue] = useState(value);
  const [isInvalidEditedValue, setIsInvalidEditedValue] = useState(false);
  const [showInputDialogBox, setShowInputDialogBox] = useState(false);
  const renderInputDialog = () => (
    <View>
      <Dialog.Container visible={showInputDialogBox}>
        <Dialog.Title>Update Settings</Dialog.Title>
        <Dialog.Description>{title}</Dialog.Description>
        <Dialog.Input
          keyboardType="numeric"
          placeholder={value}
          onChangeText={(newValue) => {
            setIsInvalidEditedValue(false);
            setEditedValue(newValue);
          }}
          wrapperStyle={
            isInvalidEditedValue
              ? styles.invalidValue
              : styles.defaultDialogInputStyle
          }
        />
        <Dialog.Button
          label="Cancel"
          onPress={() => setShowInputDialogBox(false)}
        />
        <Dialog.Button
          label="Save"
          onPress={() => {
            if (isValidNumber(editedValue)) {
              onUpdate(editedValue);
              setEditedValue(value);
              setShowInputDialogBox(false);
              setIsInvalidEditedValue(false);
            } else {
              setIsInvalidEditedValue(true);
            }
          }}
        />
      </Dialog.Container>
    </View>
  );

  return (
    <View style={styles.calculatorEntryBox}>
      <View style={styles.entryBoxHeaderWrapper}>
        <View style={styles.boxEntryTitle}>
          <Text style={[globalStyles.p2, styles.entryTitleText]}>{title}</Text>
        </View>
        <View style={styles.boxEntryEditButton}>
          <TouchableOpacity onPress={() => setShowInputDialogBox(true)}>
            <Text style={styles.entryEditButton}>EDIT</Text>
          </TouchableOpacity>
        </View>
      </View>
      <CalculatorGradientProgress value={value} showProgress={isPercentage} />
      {renderInputDialog()}
    </View>
  );
};

export default CalculatorEntryBox;
