import React from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import rightArrowIcon from '../../assets/images/right-arrow-icon.png';
import EStyleSheet from 'react-native-extended-stylesheet';
import {PADDING_SMALL, PADDING_DEFAULT} from '../../assets/dimens';
import {globalStyles} from '../../assets/styles/globalStyles';
import {GREY} from '../../assets/colors';

const styles = EStyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: PADDING_DEFAULT,
    backgroundColor: 'white',
    marginTop: PADDING_SMALL,
  },
  rightArrowIcon: {
    width: '10rem',
    height: '10rem',
    tintColor: GREY[2],
  },
  title: {
    fontWeight: '600',
  },
});

const SettingItem = ({onPress, title}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.wrapper}>
        <Text style={[globalStyles.p1, styles.title]}>{title}</Text>
        <Image
          style={styles.rightArrowIcon}
          source={rightArrowIcon}
          resizeMethod="resize"
          resizeMode="center"
        />
      </View>
    </TouchableOpacity>
  );
};

export default SettingItem;
