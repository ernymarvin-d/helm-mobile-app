import React, {useState} from 'react';
import {View, Text, TextInput} from 'react-native';
import {globalStyles} from '../../assets/styles/globalStyles';
import EStyleSheet from 'react-native-extended-stylesheet';
import {GREY, BLUE} from '../../assets/colors';
import TextButton from '../atoms/TextButton';
import {
  MARGIN_DEFAULT,
  PADDING_DEFAULT,
  PADDING_SMALL,
} from '../../assets/dimens';
import _ from 'lodash';

const styles = EStyleSheet.create({
  root: {
    flexDirection: 'column',
    width: '100%',
    height: '100%',
    padding: PADDING_DEFAULT,
  },
  label: {
    textTransform: 'uppercase',
    fontWeight: '500',
    fontSize: '10rem',
  },
  inputText: {
    width: '100%',
    borderBottomWidth: '1rem',
    borderBottomColor: GREY[4],
    paddingVertical: PADDING_SMALL,
    fontSize: '14rem',
    fontFamily: 'Montserrat-Regular',
  },
  buttonWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '40%',
    alignSelf: 'flex-end',
  },
});

const BasicInfoInputSheet = ({
  label,
  value,
  onCancellPressed,
  onSavePressed,
  keyboardType,
}) => {
  const handleOnSavedPress = () => {
    onSavePressed(inputValue);
  };
  const [inputValue, setInputValue] = useState(value);
  return (
    <View style={styles.root}>
      <Text style={[globalStyles.p2, styles.label]}>{`Enter ${label}`}</Text>
      <TextInput
        style={[styles.inputText]}
        value={inputValue}
        keyboardType={_.isBoolean(keyboardType) ? '' : keyboardType}
        onChangeText={(val) => setInputValue(val)}
      />
      <View style={styles.buttonWrapper}>
        <TextButton text="Cancel" type="link" onPress={onCancellPressed} />
        <TextButton text="Save" type="link" onPress={handleOnSavedPress} />
      </View>
    </View>
  );
};

export default BasicInfoInputSheet;
