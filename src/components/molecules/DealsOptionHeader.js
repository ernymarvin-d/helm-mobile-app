import React, {useState} from 'react';
import {View, Text, Alert} from 'react-native';
import GrayRoundedBox from '../atoms/GrayRoundedBox';
import EStyleSheet from 'react-native-extended-stylesheet';
import {globalStyles} from '../../assets/styles/globalStyles';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {
  BLUE,
  BLUE_SECONDARY,
  BOX_BORDER_COLOR_2,
  TEXT_SECONDARY,
  TEXT_PRIMARY,
  GREY,
} from '../../assets/colors';
import CheckBox from 'react-native-check-box';
import {PADDING_SMALL} from '../../assets/dimens';
import {
  updateIndividualDealOptions,
  toggleSelectedDealOptions,
} from './../../state-management/actions/DealsAction';
import {connect} from 'react-redux';

const styles = EStyleSheet.create({
  headerWrapper: {
    borderBottomWidth: '2rem',
    borderBottomColor: BOX_BORDER_COLOR_2,
    paddingBottom: PADDING_SMALL,
  },
  generalSettingsLink: {
    fontSize: '12rem',
    alignSelf: 'flex-end',
    fontFamily: 'Montserrat-Medium',
    fontWeight: '800',
    color: BLUE,
  },
  checkboxWrapper: {
    alignItems: 'center',
  },
  checkboxText: {
    paddingLeft: '5rem',
    fontSize: '12rem',
  },
  title: {
    fontFamily: 'Montserrat-Bold',
    paddingVertical: PADDING_SMALL,
    color: GREY[0],
  },
});

const DealsOptionHeader = ({
  name,
  title,
  onGeneralSettingsButtonClick,
  toggleSelectedDealOptions,
  dealOptionValue,
  settings,
  selectedDealOptions,
  loiTitle,
}) => {
  const [isChecked, setIsChecked] = useState();
  const onCheckboxClicked = (checked) => {
    if (selectedDealOptions.length === 3 && !isChecked) {
      console.log('Selected deal is full, Maximum of 3 only');
      Alert.alert('Deal Options', 'A maximum of 3 deals option can be saved.');
      return;
    }
    setIsChecked(!isChecked);
    const dealOption = {
      title: loiTitle || title,
      title2: title,
      values: dealOptionValue,
      settings: settings,
    };
    toggleSelectedDealOptions({dealOption});
  };

  return (
    <View style={styles.headerWrapper}>
      <TouchableOpacity onPress={onGeneralSettingsButtonClick}>
        <Text style={[styles.generalSettingsLink]}>General Settings</Text>
      </TouchableOpacity>
      <View style={globalStyles.checkboxWrapper}>
        <CheckBox
          onClick={onCheckboxClicked}
          checkBoxColor={TEXT_SECONDARY}
          isChecked={isChecked}
        />
        <Text style={[globalStyles.p2, styles.checkboxText]}>{name}</Text>
      </View>
      <Text style={[globalStyles.p1, styles.title]}>{title}</Text>
    </View>
  );
};

// export default DealsOptionHeader;
const mapStateToProps = (state) => {
  return {
    selectedDealOptions: state.dealsCalculator.selectedDealOptions,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateDealOptions: (key, value) =>
      dispatch(updateIndividualDealOptions(key, value)),
    toggleSelectedDealOptions: ({dealOption}) =>
      dispatch(toggleSelectedDealOptions({dealOption})),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DealsOptionHeader);
