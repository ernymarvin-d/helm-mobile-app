import React from 'react';
import {View, Image, Text} from 'react-native';
import emialIcon from './../../assets/images/mail.png';
import EStyleSheet from 'react-native-extended-stylesheet';
import {GREY, BOX_BORDER_COLOR, BLUE} from '../../assets/colors';
import {globalStyles} from '../../assets/styles/globalStyles';

const styles = EStyleSheet.create({
  root: {
    borderWidth: 1,
    borderRadius: 3,
    borderColor: BOX_BORDER_COLOR,
    backgroundColor: GREY[3],
    padding: '20rem',
    width: '100%',
    height: '123rem',
  },
  wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    flexGrow: 1,
  },
  mailIcon: {
    width: '60rem',
    height: '60rem',
    tintColor: BLUE,
  },
  label: {
    fontWeight: 'bold',
    margin: 0,
    padding: 0,
    lineHeight: 18,
  },
  textWrapper: {
    flexGrow: 1,
    paddingLeft: '10rem',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  subText: {lineHeight: 18},
  divider: {
    height: '100%',
    width: '2rem',
    backgroundColor: '#DDE2E6',
    marginHorizontal: '15rem',
  },
});

export default EmailContactDetails = () => {
  return (
    <View style={styles.root}>
      <View style={styles.wrapper}>
        <Image
          source={emialIcon}
          style={styles.mailIcon}
          resizeMode="stretch"
        />
        <View style={styles.divider} />
        <View style={styles.textWrapper}>
          <Text style={[globalStyles.h3, styles.label]}>Via Email</Text>
          <Text style={[globalStyles.p2, styles.subText]}>****@gmail.com</Text>
        </View>
      </View>
    </View>
  );
};
