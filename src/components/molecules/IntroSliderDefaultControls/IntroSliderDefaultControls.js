import * as React from 'react';
import {View, SafeAreaView, TouchableOpacity} from 'react-native';
import {styles} from './StylesIntroSliderDefaultControls';
import IntroSliderButton from '../../atoms/IntroSliderButton';
import {GREY} from '../../../assets/colors';
import rightIcon from './../../../assets/images/right-arrow-icon.png';
import leftIcon from './../../../assets/images/left-arrow-icon.png';

const IntroSliderDefaultControls = ({
  activeIndex,
  onPrevButtonClick,
  onNextButtonClick,
  onDotPress,
  data,
}) => {
  const getDotStyle = (i) =>
    i === activeIndex ? styles.activeDotStyle : styles.dot;

  return (
    <View style={styles.paginationContainer}>
      <SafeAreaView>
        <View style={styles.paginationWrapper}>
          <View style={styles.btnWrapper}>
            {activeIndex > 0 ? (
              <IntroSliderButton
                icon={leftIcon}
                tintColor={GREY[2]}
                backgroundColor="transparent"
                onPress={onPrevButtonClick}
              />
            ) : (
              <View />
            )}
          </View>
          <View style={styles.dotWrapper}>
            {data.length > 1 &&
              data.map((_, i) => (
                <TouchableOpacity
                  key={i}
                  style={getDotStyle(i)}
                  onPress={onDotPress}
                />
              ))}
          </View>
          <View style={styles.btnWrapper}>
            <IntroSliderButton icon={rightIcon} onPress={onNextButtonClick} />
          </View>
        </View>
      </SafeAreaView>
    </View>
  );
};

export default IntroSliderDefaultControls;
