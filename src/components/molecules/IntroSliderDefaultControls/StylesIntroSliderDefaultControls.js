import {ORANGE, GREY} from '../../../assets/colors';
import EStyleSheet from 'react-native-extended-stylesheet';
export const styles = EStyleSheet.create({
  paginationContainer: {
    position: 'absolute',
    bottom: 16,
    width: '100%',
  },
  paginationWrapper: {
    flex: 1,
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-evenly',
    alignContent: 'center',
  },
  btnWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  dotWrapper: {
    flex: 2,
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  activeDotStyle: {
    borderColor: ORANGE,
    borderWidth: 2,
    width: '11rem',
    height: '11rem',
    borderRadius: 5,
    marginHorizontal: 4,
  },
  dot: {
    width: '10rem',
    height: '10rem',
    borderRadius: 5,
    marginHorizontal: 4,
    backgroundColor: GREY[2],
  },
});
