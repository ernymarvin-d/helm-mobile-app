import React from 'react';
import {View, Text, Image} from 'react-native';
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from 'react-native-popup-menu';
import menuIcon from '../../assets/images/ic-vert-menu.png';
import {GREY, TEXT_SECONDARY} from '../../assets/colors';
import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  menuIcon: {
    width: '13rem',
    height: '13rem',
    tintColor: GREY[0],
  },
  menuIconWrapper: {
    padding: '10rem',
  },
  menuOption: {
    padding: '5rem',
    color: GREY[1],
  },
  bottomDivider: {
    borderBottomWidth: '1rem',
    borderBottomColor: GREY[3],
  },
  menuOptionsContainerStyle: {
    marginTop: '-85rem',
    marginLeft: '-18rem',
    width: '30%',
  },
});

const PopupMenuDealsItem = ({onSelectView, onSelectDelete}) => (
  <View>
    <Menu>
      <MenuTrigger>
        <View style={styles.menuIconWrapper}>
          <Image
            source={menuIcon}
            style={styles.menuIcon}
            resizeMethod="scale"
            resizeMode="contain"
          />
        </View>
      </MenuTrigger>
      <MenuOptions optionsContainerStyle={styles.menuOptionsContainerStyle}>
        <MenuOption style={styles.bottomDivider} onSelect={onSelectView}>
          <Text style={[styles.menuOption]}>View</Text>
        </MenuOption>
        <MenuOption onSelect={onSelectDelete}>
          <Text style={[styles.menuOption]}>Delete</Text>
        </MenuOption>
      </MenuOptions>
    </Menu>
  </View>
);

export default PopupMenuDealsItem;
