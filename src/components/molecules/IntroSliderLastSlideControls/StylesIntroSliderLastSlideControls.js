import {ORANGE, GREY} from '../../../assets/colors';
import {
  BUTTON_WIDTH_MEDIUM,
  BUTTON_HEIGHT_DEFAULT,
} from '../../../assets/dimens';
import EStyleSheet from 'react-native-extended-stylesheet';

export const styles = EStyleSheet.create({
  paginationContainer: {
    position: 'absolute',
    bottom: '16rem',
    width: '100%',
  },
  paginationWrapper: {
    flex: 1,
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-evenly',
    alignContent: 'center',
  },
  doneButton: {
    backgroundColor: ORANGE,
    borderRadius: 3,
    width: BUTTON_WIDTH_MEDIUM,
    height: BUTTON_HEIGHT_DEFAULT,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  doneButtonWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  btnWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  dotWrapper: {
    flex: 2,
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
