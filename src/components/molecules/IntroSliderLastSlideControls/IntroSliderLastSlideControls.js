import * as React from 'react';
import {View, SafeAreaView, TouchableOpacity, Text} from 'react-native';
import {styles} from './StylesIntroSliderLastSlideControls';
import IntroSliderButton from '../../atoms/IntroSliderButton';
import {GREY} from '../../../assets/colors';
import rightIcon from './../../../assets/images/right-arrow-icon.png';
import leftIcon from './../../../assets/images/left-arrow-icon.png';
import {globalStyles} from '../../../assets/styles/globalStyles';

const IntroSliderLastSlideControls = ({
  onPrevButtonClick,
  onNextButtonClick,
  onDoneButtonClick,
}) => (
  <View style={styles.paginationContainer}>
    <SafeAreaView>
      <View style={styles.paginationWrapper}>
        <View style={styles.btnWrapper}>
          <IntroSliderButton
            icon={leftIcon}
            tintColor={GREY[2]}
            backgroundColor="transparent"
            onPress={onPrevButtonClick}
          />
        </View>
        <View style={styles.dotWrapper}>
          <TouchableOpacity
            style={styles.doneButton}
            onPress={onDoneButtonClick}>
            <Text style={globalStyles.btnText}>GET STARTED</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.btnWrapper}>
          <IntroSliderButton
            icon={rightIcon}
            tintColor={GREY[2]}
            backgroundColor="transparent"
            onPress={onNextButtonClick}
          />
        </View>
      </View>
    </SafeAreaView>
  </View>
);

export default IntroSliderLastSlideControls;
