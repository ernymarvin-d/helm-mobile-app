import React, {useState} from 'react';
import {View, Text, Image} from 'react-native';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
import {GREY, BLUE, ORANGE} from '../../assets/colors';
import {set} from 'react-native-reanimated';
import {globalStyles} from '../../assets/styles/globalStyles';
import {MARGIN_SMALL, MARGIN_LARGE, MARGIN_DEFAULT} from '../../assets/dimens';
import paypalIcon from '../../assets/images/paypal.png';
import GrayRoundedBox from '../atoms/GrayRoundedBox';
import EStyleSheet from 'react-native-extended-stylesheet';
import {TouchableOpacity} from 'react-native-gesture-handler';

const styles = EStyleSheet.create({
  radioWrapper: {
    marginVertical: MARGIN_SMALL,
  },
  selectionWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  radioIcon: {
    width: '100rem',
    height: '50rem',
  },
  description: {
    marginVertical: MARGIN_DEFAULT,
  },
  grayImage: {
    tintColor: 'gray',
  },
});

const RbWrapper = ({
  paymentOptionImage,
  description,
  radioButton,
  isSelected,
  onPress,
}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <GrayRoundedBox
        primary
        style={[styles.radioWrapper, isSelected ? {borderColor: ORANGE} : {}]}>
        <View style={styles.selectionWrapper}>
          {radioButton}
          <Image
            source={paymentOptionImage}
            resizeMode="cover"
            resizeMethod="resize"
            style={[styles.radioIcon, !isSelected ? styles.grayImage : {}]}
          />
        </View>
        <Text style={[globalStyles.p2, styles.description]}>{description}</Text>
      </GrayRoundedBox>
    </TouchableOpacity>
  );
};

const RbPaymentOptions = ({options}) => {
  const [selected, setSelected] = useState(0);
  return (
    <RadioForm formHorizontal={false} animation={true}>
      {options.map((obj, i) => (
        <RbWrapper
          isSelected={selected === i}
          onPress={() => setSelected(i)}
          paymentOptionImage={paypalIcon}
          description="Protected Online Secure Payment Method"
          radioButton={
            <RadioButton labelHorizontal={true} key={i}>
              <RadioButtonInput
                obj={obj}
                index={i}
                isSelected={selected === i}
                onPress={() => setSelected(i)}
                borderWidth={1}
                buttonInnerColor={selected === i ? BLUE : GREY[3]}
                buttonOuterColor={GREY[5]}
                buttonSize={15}
                buttonOuterSize={20}
                buttonStyle={{borderWidth: 2, backgroundColor: GREY[3]}}
                buttonWrapStyle={{marginLeft: 10}}
              />
              <RadioButtonLabel
                obj={obj}
                index={i}
                labelHorizontal={true}
                onPress={() => {}}
                labelStyle={[globalStyles.p1]}
                labelWrapStyle={{}}
              />
            </RadioButton>
          }
        />
      ))}
    </RadioForm>
  );
};

export default RbPaymentOptions;
