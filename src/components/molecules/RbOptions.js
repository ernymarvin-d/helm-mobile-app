import React, {useState, useEffect} from 'react';
import {View} from 'react-native';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
import {GREY, BLUE} from '../../assets/colors';
import {globalStyles} from '../../assets/styles/globalStyles';
import EStyleSheet from 'react-native-extended-stylesheet';
import {MARGIN_DEFAULT, MARGIN_LARGE, MARGIN_SMALL} from '../../assets/dimens';

const RbOptions = ({options, reset, onSelect}) => {
  useEffect(() => {
    setSelected(0);
  }, [reset]);
  const [selected, setSelected] = useState(0);
  return (
    <RadioForm formHorizontal={false} animation={true}>
      {options.map((obj, i) => (
        <View style={{marginVertical: 16}}>
          <RadioButton labelHorizontal={true} key={i}>
            <RadioButtonInput
              obj={obj}
              index={i}
              isSelected={selected === i}
              onPress={() => {
                onSelect(options[i]);
                setSelected(i);
              }}
              borderWidth={1}
              buttonInnerColor={selected === i ? BLUE : GREY[3]}
              buttonOuterColor={GREY[5]}
              buttonSize={15}
              buttonOuterSize={20}
              buttonStyle={{borderWidth: 2, backgroundColor: GREY[3]}}
              buttonWrapStyle={{marginLeft: 10}}
            />
            <RadioButtonLabel
              obj={obj}
              index={i}
              labelHorizontal={true}
              onPress={() => {}}
              labelStyle={[globalStyles.p1]}
              labelWrapStyle={{}}
            />
          </RadioButton>
        </View>
      ))}
    </RadioForm>
  );
};

export default RbOptions;
