import React, {useState} from 'react';
import {View, Text, Image} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import addressIcon from '../../assets/images/ic-address.png';
import menuIcon from '../../assets/images/ic-vert-menu.png';
import MyDealOptionsItem from './MyDealOptionsItem';
import {
  BOX_BORDER_COLOR_2,
  BOX_BACKGROUND_COLOR_2,
  BLUE,
  BLUE_SECONDARY,
  GREY,
} from '../../assets/colors';
import {
  PADDING_SMALL,
  PADDING_DEFAULT,
  DEALS_BOX_BORDER_RADIUS,
  MARGIN_LARGE,
  MARGIN_DEFAULT,
} from '../../assets/dimens';
import {globalStyles} from '../../assets/styles/globalStyles';
import IconButton from '../atoms/IconButton';
import PopupMenuDealsItem from './PopupMenuDealsItem';
import moment from 'moment';
import {sortDealsOption} from '../../utils/DealsUtil';
import {PLAN_TYPE} from '../../assets/Constants';
import {connect} from 'react-redux';
import {TouchableOpacity} from 'react-native-gesture-handler';

const styles = EStyleSheet.create({
  container: {
    backgroundColor: BOX_BACKGROUND_COLOR_2,
    borderRadius: DEALS_BOX_BORDER_RADIUS,
    borderWidth: '1rem',
    borderColor: BOX_BORDER_COLOR_2,
    margin: MARGIN_DEFAULT,
    padding: PADDING_DEFAULT,
  },
  headerWrapper: {
    borderBottomWidth: '2rem',
    borderBottomColor: BOX_BORDER_COLOR_2,
  },
  date: {
    fontSize: '10rem',
    fontFamily: 'Montserrat-Medium',
    fontWeight: '800',
    color: '#A8A9AD',
    padding: 0,
    margin: 0,
    justifyContent: 'center',
    alignItems: 'center',
    paddingRight: PADDING_SMALL,
  },
  name: {
    fontWeight: 'bold',
    color: BLUE_SECONDARY,
  },
  addressWrapper: {
    alignItems: 'flex-start',
    flexDirection: 'row',
    width: '100%',
    marginVertical: MARGIN_LARGE,
  },
  icon: {
    width: '15rem',
    height: '15rem',
    tintColor: BLUE,
    marginTop: '5rem',
  },
  menuIcon: {
    width: '13rem',
    height: '13rem',
  },
  address: {
    flex: 1,
    paddingLeft: PADDING_SMALL,
    lineHeight: '22rem',
    fontWeight: '600',
  },
  optionWrapper: {
    paddingTop: MARGIN_LARGE,
  },
  menuIconContainer: {
    width: '10rem',
    height: '20rem',
  },
  menuButtonWrapper: {
    padding: 0,
    margin: 0,
  },
  dateWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  toggleLink: {
    textAlign: 'right',
    textDecorationLine: 'underline',
    paddingTop: '10rem',
    color: 'blue',
  },
});

const DealItemHeader = ({
  name,
  date,
  location,
  showMenu,
  onMenuSelectedDelete,
  onMenuSelectedView,
}) => {
  const renderMenuIcon = () => {
    return showMenu ? (
      <View style={styles.menuButtonWrapper}>
        <PopupMenuDealsItem
          onSelectDelete={onMenuSelectedDelete}
          onSelectView={onMenuSelectedView}
        />
      </View>
    ) : (
      <View />
    );
  };

  return (
    <View style={styles.headerWrapper}>
      <View style={styles.dateWrapper}>
        <Text style={[styles.date]}>{moment(date).format('MM/DD/YYYY')}</Text>
        {renderMenuIcon()}
      </View>
      <Text style={[globalStyles.p2, styles.name]}>{name}</Text>
      <View style={styles.addressWrapper}>
        <Image
          style={styles.icon}
          source={addressIcon}
          resizeMethod="scale"
          resizeMode="contain"
        />
        <Text style={[globalStyles.p1, styles.address]}>{location}</Text>
      </View>
    </View>
  );
};

const MyDeals = ({
  item,
  showMenu,
  name,
  onMenuSelectedView,
  onMenuSelectedDelete,
  planType,
}) => {
  const [toggleShowDeals, setToggleShowDeals] = useState(false);
  const {
    date,
    propertyInformation,
    dealOptions,
    sellerInformation,
    userId,
  } = item;
  const renderOptions = () => {
    const sortedDealsOption = sortDealsOption(dealOptions);
    const showLimit = planType === PLAN_TYPE.free ? 3 : 8;
    return Object.values(sortedDealsOption).map((value, index) => {
      if (index < showLimit) {
        return (
          <MyDealOptionsItem item={value} name={`Option no. ${index + 1}`} />
        );
      }
    });
  };
  return (
    <View style={styles.container}>
      <DealItemHeader
        date={date.toDate()}
        location={sellerInformation.propertyAddress}
        showMenu={showMenu}
        onMenuSelectedView={onMenuSelectedView}
        onMenuSelectedDelete={onMenuSelectedDelete}
        name={name}
      />
      {toggleShowDeals && (
        <View style={styles.optionsWrapper}>{renderOptions()}</View>
      )}
      <Text
        style={[globalStyles.p2, styles.toggleLink]}
        onPress={() => setToggleShowDeals(!toggleShowDeals)}>
        {toggleShowDeals ? 'Hide Details' : 'View Details'}
      </Text>
    </View>
  );
};

const mapStateToProps = (state) => {
  return {
    planType: state.planType,
  };
};

export default connect(mapStateToProps)(MyDeals);
