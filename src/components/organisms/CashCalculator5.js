import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {
  globalStyles,
  calculatorLayoutStyles,
  modalStyles,
} from '../../assets/styles/globalStyles';
import CalculatorEntryBox from '../molecules/CalculatorEntryBox';
import {
  decimalValuetoPercentage,
  toStringMoneyValue,
  percentageToDecimalValue,
} from '../../utils/OptionsCalculatorUtil';
import {connect} from 'react-redux';
import {
  updateCalculatorSettingsOption,
  updateCalupdateDealsSettingsOption,
} from '../../state-management/actions/AuthActions';
import _ from 'lodash';
import {updateUserDefaultCalculatorSetting} from '../../utils/CalculatorSettingsUtil';

// downPayment: 0.1
// numerOfYearsTillBalloon: 5
// paymentTermLength: 30
// priceDiscount: 1
// wholeSaleFee: 5000
const CashCalculator5 = ({
  style,
  route,
  navigation,
  settings,
  updateCalculatorSettingsOption,
  asModal,
  onDoneButtonPressed,
  updateCalupdateDealsSettingsOption,
  dealKey,
  user,
}) => {
  const onUpdateOption = (key, value) => {
    const payload = {
      dealKey,
      settingsKey: key,
      value,
      settingsOptionKey: 'option5',
      settings,
      stateAction: {
        updateCalupdateDealsSettingsOption,
        updateCalculatorSettingsOption,
      },
      userId: user.userId,
    };
    updateUserDefaultCalculatorSetting(payload);
  };
  const downPayment = settings.downPayment.value;
  const paymentTermLength = settings.paymentTermLength.value;
  const priceDiscount = settings.priceDiscount.value;
  const numerOfYearsTillBalloon = settings.numerOfYearsTillBalloon.value;
  const wholeSaleFee = settings.wholeSaleFee.value;
  const formatCapRateForEndBuyer = settings.formatCapRateForEndBuyer.value;
  const formatCashOnCashRateForEndBuyer =
    settings.formatCashOnCashRateForEndBuyer.value;

  const renderDoneButton = () => {
    if (asModal) {
      return (
        <TouchableOpacity onPress={onDoneButtonPressed}>
          <Text style={[globalStyles.p2, {alignSelf: 'flex-end'}]}>Done</Text>
        </TouchableOpacity>
      );
    } else {
      return <View />;
    }
  };
  return (
    <View
      style={[
        calculatorLayoutStyles.calculatorBox,
        style,
        asModal ? modalStyles.asModal : {},
      ]}>
      {renderDoneButton()}
      <View style={calculatorLayoutStyles.calculatorHeaderWrapper}>
        <Text style={[globalStyles.p2, calculatorLayoutStyles.calculatorName]}>
          Calculator 5
        </Text>
        <Text style={[globalStyles.p1, calculatorLayoutStyles.calculatorTitle]}>
          Land Contract Principle Only
        </Text>
      </View>
      <CalculatorEntryBox
        value={`${decimalValuetoPercentage(priceDiscount)}%`}
        isPercentage
        onUpdate={(value) =>
          onUpdateOption('priceDiscount', percentageToDecimalValue(value))
        }
        title="Preferred discount rate off of market value (standard)"
      />
      <CalculatorEntryBox
        value={toStringMoneyValue(wholeSaleFee)}
        onUpdate={(value) => onUpdateOption('wholeSaleFee', value)}
        title="Preferred wholesale fee"
      />
      <CalculatorEntryBox
        value={`${paymentTermLength} Years`}
        onUpdate={(value) => onUpdateOption('paymentTermLength', value)}
        title="Preferred payment term legnth"
      />
      <CalculatorEntryBox
        value={`${numerOfYearsTillBalloon} Years`}
        onUpdate={(value) => onUpdateOption('numerOfYearsTillBalloon', value)}
        title="Number of years until balloon"
      />
      <CalculatorEntryBox
        value={`${decimalValuetoPercentage(downPayment)}%`}
        isPercentage
        onUpdate={(value) =>
          onUpdateOption('downPayment', percentageToDecimalValue(value))
        }
        title="Preferred down payment"
      />
      <CalculatorEntryBox
        value={`${decimalValuetoPercentage(formatCapRateForEndBuyer)}%`}
        isPercentage
        onUpdate={(value) =>
          onUpdateOption(
            'formatCapRateForEndBuyer',
            percentageToDecimalValue(value),
          )
        }
        title="Cap Rate For End Buyer (Conditional Formating)"
      />
      <CalculatorEntryBox
        value={`${decimalValuetoPercentage(formatCashOnCashRateForEndBuyer)}%`}
        isPercentage
        onUpdate={(value) =>
          onUpdateOption(
            'formatCashOnCashRateForEndBuyer',
            percentageToDecimalValue(value),
          )
        }
        title="Cash on Cash Rate For End Buyer (Conditional Formating)"
      />
    </View>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateCalculatorSettingsOption: (settings) =>
      dispatch(updateCalculatorSettingsOption(settings)),
    updateCalupdateDealsSettingsOption: (settings) =>
      dispatch(updateCalupdateDealsSettingsOption(settings)),
  };
};

const mapStateToProps = (state) => {
  return {
    settings: state.auth.user.calculatorGeneralSettings.option5,
    user: state.auth.user,
  };
};

export default connect(
  () => mapStateToProps,
  mapDispatchToProps,
)(CashCalculator5);
