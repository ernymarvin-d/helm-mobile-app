import React, {useState} from 'react';
import {View, Text} from 'react-native';
import GrayRoundedBox from '../atoms/GrayRoundedBox';
import LabeledInputText from '../atoms/LabeledInputText';
import EStyleSheet from 'react-native-extended-stylesheet';
import DealsOptionHeader from '../molecules/DealsOptionHeader';
import {GREY, TEXT_SECONDARY} from '../../assets/colors';
import {globalStyles} from '../../assets/styles/globalStyles';
import {connect} from 'react-redux';
import {
  toStringMoneyValue,
  decimalValuetoPercentage,
  calculateOption8,
} from '../../utils/OptionsCalculatorUtil';
import CashCalculator8 from './CashCalculator8';
import CalculatorSettingsModalWrapper from '../atoms/CalculatorSettingsModalWrapper';
import {updateIndividualDealOptions} from './../../state-management/actions/DealsAction';

const styles = EStyleSheet.create({
  formWrapper: {marginTop: '20rem'},
});

const DealsOption8 = ({
  route,
  navigation,
  dealOption8,
  settings,
  propertyInformation,
  updateDealOptions,
  dealKey,
}) => {
  console.log({dealOption8, settings});
  const [showOptionSettings, setShowOptionSettings] = useState(false);
  const {
    capRate,
    cashOnCashReturnForEndBuyer,
    equityPosition,
    pitiPaymentPerMonth,
    // balanceOnMortgage,
  } = dealOption8;

  // const balanceOnMortgage = settings.balanceOnMortgage.value;
  const balanceOnMortgage = propertyInformation.currentMortgateBalance;
  const cashForKeysPurchaseAmountAboveBalance =
    settings.cashForKeysPurchaseAmountAboveBalance.value;

  const onCalculatorSettingsDoneButtonClick = () => {
    const dealOptionValue = calculateOption8({
      settings,
      propertyInformation,
    });
    const dealsOptionKey = 'dealOption8';
    updateDealOptions({
      dealsOptionKey,
      dealOptionValue,
    });
    setShowOptionSettings(false);
  };
  const renderSettingsModal = () => (
    <CalculatorSettingsModalWrapper
      visible={showOptionSettings}
      onBackdropPress={() => {}}>
      <CashCalculator8
        asModal
        dealKey={dealKey}
        onDoneButtonPressed={onCalculatorSettingsDoneButtonClick}
      />
    </CalculatorSettingsModalWrapper>
  );
  return (
    <GrayRoundedBox>
      <DealsOptionHeader
        title="Subject To Wholesale"
        loiTitle="Subject To Mortgage"
        name="Option 8"
        settings={settings}
        dealOptionValue={dealOption8}
        onGeneralSettingsButtonClick={() => setShowOptionSettings(true)}
      />
      <View style={styles.formWrapper}>
        <LabeledInputText
          label="Current Mortgage Balance"
          value={toStringMoneyValue(balanceOnMortgage)}
          editable={false}
          textInputStyle={globalStyles.textInputGray}
        />
        <LabeledInputText
          label="Equity Position"
          value={`${decimalValuetoPercentage(equityPosition.value)}%`}
          editable={false}
          textInputStyle={globalStyles.textInputGray}
        />
        <LabeledInputText
          label="PITI"
          value={toStringMoneyValue(pitiPaymentPerMonth.value)}
          editable={false}
          textInputStyle={globalStyles.textInputGray}
        />
        <LabeledInputText
          label="Cash for Keys / Purchase amount above Bal."
          value={toStringMoneyValue(cashForKeysPurchaseAmountAboveBalance)}
          editable={false}
          textInputStyle={globalStyles.textInputOrange}
        />
        <LabeledInputText
          label="Cap Rate"
          value={`${decimalValuetoPercentage(capRate.value)}%`}
          editable={false}
          textInputStyle={globalStyles.textInputGray}
        />

        <LabeledInputText
          label="Cash on Cash Return "
          value={`${decimalValuetoPercentage(
            cashOnCashReturnForEndBuyer.value,
          )}%`}
          editable={false}
          textInputStyle={
            cashOnCashReturnForEndBuyer.value >=
            settings.formatCashOnCashReturn.value
              ? globalStyles.textInputGreen
              : globalStyles.textInputRed
          }
        />
      </View>
      {renderSettingsModal()}
    </GrayRoundedBox>
  );
};

const mapStateToProps = (state) => {
  return {
    dealOption8: state.dealsCalculator.dealOptions.dealOption8.options,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateDealOptions: (key, value) =>
      dispatch(updateIndividualDealOptions(key, value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DealsOption8);
