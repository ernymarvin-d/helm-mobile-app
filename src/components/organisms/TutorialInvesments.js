import React, {useState} from 'react';
import {View, Text, Dimensions, RefreshControl} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import {globalStyles} from '../../assets/styles/globalStyles';
import SearchBar from '../atoms/SearchBar';
import TutorialList from '../molecules/TutorialList';
import {MARGIN_DEFAULT, MARGIN_LARGE} from '../../assets/dimens';
import {ScrollView} from 'react-native-gesture-handler';
import {connect} from 'react-redux';
import _ from 'lodash';
import {useEffect} from 'react';

const styles = EStyleSheet.create({
  searchBarWrapper: {
    margin: MARGIN_DEFAULT,
  },
  headerText: {
    marginBottom: MARGIN_DEFAULT,
    marginTop: MARGIN_LARGE,
    fontWeight: 'bold',
  },
  tutorialListWrapper: {height: '80%', paddingBottom: '20rem'},
});

const TutorialInvesments = ({jumpTo, tutorials, onRefresh}) => {
  useEffect(() => {
    setShowCourse(tutorials);
  }, [tutorials]);

  const [refreshingList, setRefreshingList] = useState(false);
  const [showCourse, setShowCourse] = useState(tutorials);
  const handleSearch = (words) => {
    if (_.isEmpty(words)) setShowCourse(tutorials);
    const searchResult = tutorials.filter((tutorial) =>
      _.toLower(tutorial.courseTitle).includes(_.toLower(words)),
    );
    setShowCourse(searchResult);
  };

  return (
    <View style={[globalStyles.rootNoPadding]}>
      <View style={styles.contentWrapper}>
        <View style={styles.searchBarWrapper}>
          <SearchBar placeholder="Search Courses" onChangeText={handleSearch} />
          <View>
            <Text style={[globalStyles.h3, styles.headerText]}>
              Explore the Course Catalog
            </Text>
          </View>
        </View>
        <View style={styles.tutorialListWrapper}>
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={refreshingList}
                onRefresh={onRefresh}
              />
            }>
            <TutorialList jumpTo={jumpTo} tutorials={showCourse} />
          </ScrollView>
        </View>
      </View>
    </View>
  );
};

const mapStateToProps = (state) => {
  return {
    tutorials: state.tutorials.tutorials,
  };
};

export default connect(mapStateToProps)(TutorialInvesments);
