import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {
  globalStyles,
  calculatorLayoutStyles,
  modalStyles,
} from '../../assets/styles/globalStyles';
import CalculatorEntryBox from '../molecules/CalculatorEntryBox';
import {
  decimalValuetoPercentage,
  toStringMoneyValue,
  percentageToDecimalValue,
} from '../../utils/OptionsCalculatorUtil';
import {
  updateCalculatorSettingsOption,
  updateCalupdateDealsSettingsOption,
} from '../../state-management/actions/AuthActions';
import _ from 'lodash';
import {connect} from 'react-redux';
import {updateUserDefaultCalculatorSetting} from '../../utils/CalculatorSettingsUtil';
// downPayment: 0.1
// interestRateOnLandContract: 0.05
// priceDiscount: 0.85
// wholeSaleFee: 5000
// yearsTillBalloonPayment: 5
const CashCalculator4 = ({
  style,
  route,
  navigation,
  settings,
  updateCalculatorSettingsOption,
  asModal,
  onDoneButtonPressed,
  updateCalupdateDealsSettingsOption,
  dealKey,
  user,
}) => {
  const downPayment = settings.downPayment.value;
  const interestRateOnLandContract = settings.interestRateOnLandContract.value;
  const priceDiscount = settings.priceDiscount.value;
  const wholeSaleFee = settings.wholeSaleFee.value;
  const yearsTillBalloonPayment = settings.yearsTillBalloonPayment.value;
  const formatCapRateForEndBuyer = settings.formatCapRateForEndBuyer.value;
  const formatCashOnCashRateForEndBuyer =
    settings.formatCashOnCashRateForEndBuyer.value;

  const onUpdateOption = (key, value) => {
    const payload = {
      dealKey,
      settingsKey: key,
      value,
      settingsOptionKey: 'option4',
      settings,
      stateAction: {
        updateCalupdateDealsSettingsOption,
        updateCalculatorSettingsOption,
      },
      userId: user.userId,
    };
    updateUserDefaultCalculatorSetting(payload);
  };

  const renderDoneButton = () => {
    if (asModal) {
      return (
        <TouchableOpacity onPress={onDoneButtonPressed}>
          <Text style={[globalStyles.p2, {alignSelf: 'flex-end'}]}>Done</Text>
        </TouchableOpacity>
      );
    } else {
      return <View />;
    }
  };
  return (
    <View
      style={[
        calculatorLayoutStyles.calculatorBox,
        style,
        asModal ? modalStyles.asModal : {},
      ]}>
      {renderDoneButton()}
      <View style={calculatorLayoutStyles.calculatorHeaderWrapper}>
        <Text style={[globalStyles.p2, calculatorLayoutStyles.calculatorName]}>
          Calculator 4
        </Text>
        <Text style={[globalStyles.p1, calculatorLayoutStyles.calculatorTitle]}>
          Land Contract Interest Only
        </Text>
      </View>
      <CalculatorEntryBox
        value={`${decimalValuetoPercentage(priceDiscount)}%`}
        isPercentage
        onUpdate={(value) =>
          onUpdateOption('priceDiscount', percentageToDecimalValue(value))
        }
        title="Preferred discount rate off of market value (standard)"
      />
      <CalculatorEntryBox
        value={toStringMoneyValue(wholeSaleFee)}
        onUpdate={(value) => onUpdateOption('wholeSaleFee', value)}
        title="Preferred wholesale fee"
      />
      <CalculatorEntryBox
        value={`${decimalValuetoPercentage(interestRateOnLandContract)}%`}
        isPercentage
        onUpdate={(value) =>
          onUpdateOption(
            'interestRateOnLandContract',
            percentageToDecimalValue(value),
          )
        }
        title="Interest Rate on Land Contract"
      />
      <CalculatorEntryBox
        value={`${decimalValuetoPercentage(downPayment)}%`}
        isPercentage
        onUpdate={(value) =>
          onUpdateOption('downPayment', percentageToDecimalValue(value))
        }
        title="Preferred down payment"
      />
      <CalculatorEntryBox
        value={`${yearsTillBalloonPayment} Years`}
        onUpdate={(value) => onUpdateOption('yearsTillBalloonPayment', value)}
        title="# of Years till Balloon"
      />
      <CalculatorEntryBox
        value={`${decimalValuetoPercentage(formatCapRateForEndBuyer)}%`}
        isPercentage
        onUpdate={(value) =>
          onUpdateOption(
            'formatCapRateForEndBuyer',
            percentageToDecimalValue(value),
          )
        }
        title="Cap Rate For End Buyer (Conditional Formating)"
      />
      <CalculatorEntryBox
        value={`${decimalValuetoPercentage(formatCashOnCashRateForEndBuyer)}%`}
        isPercentage
        onUpdate={(value) =>
          onUpdateOption(
            'formatCashOnCashRateForEndBuyer',
            percentageToDecimalValue(value),
          )
        }
        title="Cash on Cash Rate For End Buyer (Conditional Formating)"
      />
    </View>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateCalculatorSettingsOption: (settings) =>
      dispatch(updateCalculatorSettingsOption(settings)),
    updateCalupdateDealsSettingsOption: (settings) =>
      dispatch(updateCalupdateDealsSettingsOption(settings)),
  };
};

const mapStateToProps = (state) => {
  return {
    settings: state.auth.user.calculatorGeneralSettings.option4,
    user: state.auth.user,
  };
};

export default connect(
  () => mapStateToProps,
  mapDispatchToProps,
)(CashCalculator4);
