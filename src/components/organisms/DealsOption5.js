import React, {useState} from 'react';
import {View, Text} from 'react-native';
import GrayRoundedBox from '../atoms/GrayRoundedBox';
import LabeledInputText from '../atoms/LabeledInputText';
import EStyleSheet from 'react-native-extended-stylesheet';
import DealsOptionHeader from '../molecules/DealsOptionHeader';
import {GREY, TEXT_SECONDARY} from '../../assets/colors';
import {globalStyles} from '../../assets/styles/globalStyles';
import {connect} from 'react-redux';
import {
  toStringMoneyValue,
  decimalValuetoPercentage,
  calculateOption5,
} from '../../utils/OptionsCalculatorUtil';
import CashCalculator5 from './CashCalculator5';
import CalculatorSettingsModalWrapper from '../atoms/CalculatorSettingsModalWrapper';
import {updateIndividualDealOptions} from './../../state-management/actions/DealsAction';

const styles = EStyleSheet.create({
  formWrapper: {marginTop: '20rem'},
});

const DealsOption5 = ({
  route,
  navigation,
  dealOption5,
  settings,
  updateDealOptions,
  propertyInformation,
  dealKey,
}) => {
  console.log({dealOption5, settings});
  const [showOptionSettings, setShowOptionSettings] = useState(false);
  const {
    balloonPayment,
    capRateForEndBuyer,
    cashOnCashReturnForEndBuyer,
    monthlyPayment,
    priceToBuyer,
    priceToSeller,
    valueDownPaymentBuyer,
    valueDownPaymentSeller,
  } = dealOption5;
  const downPayment = settings.downPayment.value;
  const paymentTermLength = settings.paymentTermLength.value;
  const priceDiscount = settings.priceDiscount.value;
  const numerOfYearsTillBalloon = settings.numerOfYearsTillBalloon.value;
  const wholeSaleFee = settings.wholeSaleFee.value;

  const onCalculatorSettingsDoneButtonClick = () => {
    const dealOptionValue = calculateOption5({
      settings,
      propertyInformation,
    });
    const dealsOptionKey = 'dealOption5';
    updateDealOptions({
      dealsOptionKey,
      dealOptionValue,
    });
    setShowOptionSettings(false);
  };
  const renderSettingsModal = () => (
    <CalculatorSettingsModalWrapper
      visible={showOptionSettings}
      onBackdropPress={() => {}}>
      <CashCalculator5
        asModal
        dealKey={dealKey}
        onDoneButtonPressed={onCalculatorSettingsDoneButtonClick}
      />
    </CalculatorSettingsModalWrapper>
  );
  return (
    <GrayRoundedBox>
      <DealsOptionHeader
        title=" Land Contract Principle Only"
        name="Option 5"
        settings={settings}
        dealOptionValue={dealOption5}
        onGeneralSettingsButtonClick={() => setShowOptionSettings(true)}
      />
      <View style={styles.formWrapper}>
        <LabeledInputText
          label="Price Discount"
          value={`${decimalValuetoPercentage(priceDiscount)}%`}
          editable={false}
          textInputStyle={globalStyles.textInputOrange}
        />
        <LabeledInputText
          label="Wholesale Fee"
          value={toStringMoneyValue(wholeSaleFee)}
          editable={false}
          textInputStyle={globalStyles.textInputOrange}
        />
        <LabeledInputText
          label="Payment Term Length (years)"
          value={`${paymentTermLength}`}
          editable={false}
          textInputStyle={globalStyles.textInputOrange}
        />
        <LabeledInputText
          label="# of Years Till Balloon"
          value={`${numerOfYearsTillBalloon}`}
          editable={false}
          textInputStyle={globalStyles.textInputOrange}
        />
        <LabeledInputText
          label="Down Payment"
          value={`${decimalValuetoPercentage(downPayment)}%`}
          editable={false}
          textInputStyle={globalStyles.textInputOrange}
        />
        <LabeledInputText
          label="Price to Seller "
          value={toStringMoneyValue(priceToSeller.value)}
          editable={false}
          textInputStyle={globalStyles.textInputGray}
        />
        <LabeledInputText
          label="Price to Buyer "
          value={toStringMoneyValue(priceToBuyer.value)}
          editable={false}
          textInputStyle={globalStyles.textInputGray}
        />
        <LabeledInputText
          label="Monthly Payments"
          value={toStringMoneyValue(monthlyPayment.value)}
          editable={false}
          textInputStyle={globalStyles.textInputGray}
        />
        <LabeledInputText
          label="Balloon Payments"
          value={toStringMoneyValue(balloonPayment.value)}
          editable={false}
          textInputStyle={globalStyles.textInputGray}
        />
        <LabeledInputText
          label="Down Payment (seller)"
          value={toStringMoneyValue(valueDownPaymentSeller.value)}
          editable={false}
          textInputStyle={globalStyles.textInputGray}
        />
        <LabeledInputText
          label="Down Payment (buyer)"
          value={toStringMoneyValue(valueDownPaymentBuyer.value)}
          editable={false}
          textInputStyle={globalStyles.textInputGray}
        />
        <LabeledInputText
          label="Cap Rate for End Buyer"
          value={`${decimalValuetoPercentage(capRateForEndBuyer.value)}%`}
          editable={false}
          textInputStyle={
            capRateForEndBuyer.value >= settings.formatCapRateForEndBuyer.value
              ? globalStyles.textInputGreen
              : globalStyles.textInputRed
          }
        />
        <LabeledInputText
          label="Cash on Cash Return for End Buyer"
          value={`${decimalValuetoPercentage(
            cashOnCashReturnForEndBuyer.value,
          )}%`}
          editable={false}
          textInputStyle={
            cashOnCashReturnForEndBuyer.value >=
            settings.formatCashOnCashRateForEndBuyer.value
              ? globalStyles.textInputGreen
              : globalStyles.textInputRed
          }
        />
      </View>
      {renderSettingsModal()}
    </GrayRoundedBox>
  );
};

const mapStateToProps = (state) => {
  return {
    dealOption5: state.dealsCalculator.dealOptions.dealOption5.options,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateDealOptions: (key, value) =>
      dispatch(updateIndividualDealOptions(key, value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DealsOption5);
