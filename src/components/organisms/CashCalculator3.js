import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {
  globalStyles,
  calculatorLayoutStyles,
  modalStyles,
} from '../../assets/styles/globalStyles';
import CalculatorEntryBox from '../molecules/CalculatorEntryBox';
import {
  decimalValuetoPercentage,
  toStringMoneyValue,
  percentageToDecimalValue,
} from '../../utils/OptionsCalculatorUtil';
import {
  updateCalculatorSettingsOption,
  updateCalupdateDealsSettingsOption,
} from '../../state-management/actions/AuthActions';
import _ from 'lodash';
import {connect} from 'react-redux';
import {updateUserDefaultCalculatorSetting} from '../../utils/CalculatorSettingsUtil';

// downPayment: 0.2
// interestRateOnMortgate: 0.06
// numYearsOnMortgage: 30
// priceDiscount: 0.8
// wholeSaleFee: 10000
const CashCalculator3 = ({
  style,
  route,
  navigation,
  settings,
  updateCalculatorSettingsOption,
  asModal,
  onDoneButtonPressed,
  dealKey,
  updateCalupdateDealsSettingsOption,
  user,
}) => {
  const onUpdateOption = (key, value) => {
    const payload = {
      dealKey,
      settingsKey: key,
      value,
      settingsOptionKey: 'option3',
      settings,
      stateAction: {
        updateCalupdateDealsSettingsOption,
        updateCalculatorSettingsOption,
      },
      userId: user.userId,
    };
    updateUserDefaultCalculatorSetting(payload);
  };

  const downPayment = settings.downPayment.value;
  const interestRateOnMortgate = settings.interestRateOnMortgate.value;
  const numYearsOnMortgage = settings.numYearsOnMortgage.value;
  const priceDiscount = settings.priceDiscount.value;
  const wholeSaleFee = settings.wholeSaleFee.value;
  const formatCapRateForEndBuyer = settings.formatCapRateForEndBuyer.value;
  const formatCashOnCashRateForEndBuyer =
    settings.formatCashOnCashRateForEndBuyer.value;

  const renderDoneButton = () => {
    if (asModal) {
      return (
        <TouchableOpacity onPress={onDoneButtonPressed}>
          <Text style={[globalStyles.p2, {alignSelf: 'flex-end'}]}>Done</Text>
        </TouchableOpacity>
      );
    } else {
      return <View />;
    }
  };
  return (
    <View
      style={[
        calculatorLayoutStyles.calculatorBox,
        style,
        asModal ? modalStyles.asModal : {},
      ]}>
      {renderDoneButton()}
      <View style={calculatorLayoutStyles.calculatorHeaderWrapper}>
        <Text style={[globalStyles.p2, calculatorLayoutStyles.calculatorName]}>
          Calculator 3
        </Text>
        <Text style={[globalStyles.p1, calculatorLayoutStyles.calculatorTitle]}>
          Cash Offer for Wholesale To Buy/Hold
        </Text>
      </View>
      <CalculatorEntryBox
        value={`${decimalValuetoPercentage(priceDiscount)}%`}
        isPercentage
        onUpdate={(value) =>
          onUpdateOption('priceDiscount', percentageToDecimalValue(value))
        }
        title="Preferred discount rate off of market value (standard)"
      />
      <CalculatorEntryBox
        value={`${toStringMoneyValue(wholeSaleFee)}`}
        title="Preferred wholesale fee"
        onUpdate={(value) => onUpdateOption('wholeSaleFee', value)}
      />
      <CalculatorEntryBox
        value={`${decimalValuetoPercentage(interestRateOnMortgate)}%`}
        isPercentage
        onUpdate={(value) =>
          onUpdateOption(
            'interestRateOnMortgate',
            percentageToDecimalValue(value),
          )
        }
        title="Preferred interest Rate on Mortgage"
      />
      <CalculatorEntryBox
        value={`${numYearsOnMortgage} Years`}
        onUpdate={(value) => onUpdateOption('numYearsOnMortgage', value)}
        title="Preferred # of Years on Mortgage"
      />
      <CalculatorEntryBox
        value={`${decimalValuetoPercentage(downPayment)}%`}
        isPercentage
        onUpdate={(value) =>
          onUpdateOption('downPayment', percentageToDecimalValue(value))
        }
        title="Preferred down payment"
      />
      <CalculatorEntryBox
        value={`${decimalValuetoPercentage(formatCapRateForEndBuyer)}%`}
        isPercentage
        onUpdate={(value) =>
          onUpdateOption(
            'formatCapRateForEndBuyer',
            percentageToDecimalValue(value),
          )
        }
        title="Cap Rate For End Buyer (Conditional Formating)"
      />
      <CalculatorEntryBox
        value={`${decimalValuetoPercentage(formatCashOnCashRateForEndBuyer)}%`}
        isPercentage
        onUpdate={(value) =>
          onUpdateOption(
            'formatCashOnCashRateForEndBuyer',
            percentageToDecimalValue(value),
          )
        }
        title="Cash on Cash Rate For End Buyer (Conditional Formating)"
      />
    </View>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateCalculatorSettingsOption: (settings) =>
      dispatch(updateCalculatorSettingsOption(settings)),
    updateCalupdateDealsSettingsOption: (settings) =>
      dispatch(updateCalupdateDealsSettingsOption(settings)),
  };
};

const mapStateToProps = (state) => {
  return {
    settings: state.auth.user.calculatorGeneralSettings.option3,
    user: state.auth.user,
  };
};

export default connect(
  () => mapStateToProps,
  mapDispatchToProps,
)(CashCalculator3);
