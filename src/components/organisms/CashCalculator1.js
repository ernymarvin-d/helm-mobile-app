import React, {useReducer} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {
  globalStyles,
  calculatorLayoutStyles,
  modalStyles,
} from '../../assets/styles/globalStyles';
import CalculatorEntryBox from '../molecules/CalculatorEntryBox';
import {
  decimalValuetoPercentage,
  toStringMoneyValue,
  percentageToDecimalValue,
} from '../../utils/OptionsCalculatorUtil';
import {connect} from 'react-redux';
import {
  updateCalculatorSettingsOption,
  updateCalupdateDealsSettingsOption,
} from '../../state-management/actions/AuthActions';
import _ from 'lodash';
import {updateUserDefaultCalculatorSetting} from '../../utils/CalculatorSettingsUtil';
// option1: {wholeSaleFee: 10000, priceDiscount: 0.7}

const CashCalculator1 = ({
  route,
  navigation,
  settings,
  updateCalculatorSettingsOption,
  updateCalupdateDealsSettingsOption,
  asModal,
  onDoneButtonPressed,
  dealKey,
  user,
}) => {
  const onUpdateOption = (key, value) => {
    const payload = {
      dealKey,
      settingsKey: key,
      value,
      settingsOptionKey: 'option1',
      settings,
      stateAction: {
        updateCalupdateDealsSettingsOption,
        updateCalculatorSettingsOption,
      },
      userId: user.userId,
    };
    updateUserDefaultCalculatorSetting(payload);
  };

  console.log({settings});
  const priceDiscount = settings.priceDiscount.value;
  const wholeSaleFee = settings.wholeSaleFee.value;
  const renderDoneButton = () => {
    if (asModal) {
      return (
        <TouchableOpacity onPress={onDoneButtonPressed}>
          <Text style={[globalStyles.p2, {alignSelf: 'flex-end'}]}>Done</Text>
        </TouchableOpacity>
      );
    } else {
      return <View />;
    }
  };
  return (
    <View
      style={[
        calculatorLayoutStyles.calculatorBox,
        asModal ? modalStyles.asModal : {},
      ]}>
      {renderDoneButton()}
      <View style={calculatorLayoutStyles.calculatorHeaderWrapper}>
        <Text style={[globalStyles.p2, calculatorLayoutStyles.calculatorName]}>
          Calculator 1
        </Text>
        <Text style={[globalStyles.p1, calculatorLayoutStyles.calculatorTitle]}>
          Cash Offer to Whole Sale Fix/Flip1
        </Text>
      </View>
      <CalculatorEntryBox
        value={`${decimalValuetoPercentage(priceDiscount)}%`}
        isPercentage={true}
        onUpdate={(value) => {
          onUpdateOption('priceDiscount', percentageToDecimalValue(value));
        }}
        title="Preferred discount rate off of market value (standard)"
      />
      <CalculatorEntryBox
        value={`${toStringMoneyValue(wholeSaleFee)}`}
        title="Preferred assignment fee"
        onUpdate={(value) => onUpdateOption('wholeSaleFee', value)}
      />
    </View>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateCalculatorSettingsOption: (settings) =>
      dispatch(updateCalculatorSettingsOption(settings)),
    updateCalupdateDealsSettingsOption: (settings) =>
      dispatch(updateCalupdateDealsSettingsOption(settings)),
  };
};

const mapStateToProps = (state) => {
  console.log('hohoho', {state});
  return {
    settings: state.auth.user.calculatorGeneralSettings.option1,
    user: state.auth.user,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CashCalculator1);
