import React, {useState} from 'react';
import {View, Text} from 'react-native';
import GrayRoundedBox from '../atoms/GrayRoundedBox';
import LabeledInputText from '../atoms/LabeledInputText';
import EStyleSheet from 'react-native-extended-stylesheet';
import DealsOptionHeader from '../molecules/DealsOptionHeader';
import {GREY, TEXT_SECONDARY} from '../../assets/colors';
import {globalStyles} from '../../assets/styles/globalStyles';
import {connect} from 'react-redux';
import {
  toStringMoneyValue,
  decimalValuetoPercentage,
  calculateOption3,
} from '../../utils/OptionsCalculatorUtil';
import CalculatorSettingsModalWrapper from '../atoms/CalculatorSettingsModalWrapper';
import CashCalculator3 from './CashCalculator3';
import {updateIndividualDealOptions} from './../../state-management/actions/DealsAction';

const styles = EStyleSheet.create({
  formWrapper: {marginTop: '20rem'},
});

const DealsOption3 = ({
  route,
  navigation,
  dealOption3,
  settings,
  propertyInformation,
  updateDealOptions,
  dealKey,
}) => {
  console.log({dealOption3, settings});
  const [showOptionSettings, setShowOptionSettings] = useState(false);
  const {
    capRateForEndBuyer,
    cashOnCashReturnForEndBuyer,
    priceToBuyer,
    priceToSeller,
  } = dealOption3;

  const downPayment = settings.downPayment.value;
  const interestRateOnMortgate = settings.interestRateOnMortgate.value;
  const numYearsOnMortgage = settings.numYearsOnMortgage.value;
  const priceDiscount = settings.priceDiscount.value;
  const wholeSaleFee = settings.wholeSaleFee.value;

  const onCalculatorSettingsDoneButtonClick = () => {
    const dealOptionValue = calculateOption3({
      settings,
      propertyInformation,
    });
    const dealsOptionKey = 'dealOption3';
    updateDealOptions({
      dealsOptionKey,
      dealOptionValue,
    });
    setShowOptionSettings(false);
  };

  const renderSettingsModal = () => (
    <CalculatorSettingsModalWrapper
      visible={showOptionSettings}
      onBackdropPress={() => {}}>
      <CashCalculator3
        asModal
        dealKey={dealKey}
        onDoneButtonPressed={onCalculatorSettingsDoneButtonClick}
      />
    </CalculatorSettingsModalWrapper>
  );

  return (
    <GrayRoundedBox>
      <DealsOptionHeader
        loiTitle="Cash Offer"
        title="Cash Offer for Wholesale To Buy/Hold"
        name={'Option 3'}
        settings={settings}
        dealOptionValue={dealOption3}
        onGeneralSettingsButtonClick={() => setShowOptionSettings(true)}
      />
      <View style={styles.formWrapper}>
        <LabeledInputText
          label="Price Discount"
          value={`${decimalValuetoPercentage(priceDiscount)}%`}
          editable={false}
          textInputStyle={globalStyles.textInputOrange}
        />
        <LabeledInputText
          label="Wholesale Fee"
          value={toStringMoneyValue(wholeSaleFee)}
          editable={false}
          textInputStyle={globalStyles.textInputOrange}
        />
        <LabeledInputText
          label="Interest Rate on Mortgage"
          value={`${decimalValuetoPercentage(interestRateOnMortgate)}%`}
          editable={false}
          textInputStyle={globalStyles.textInputOrange}
        />
        <LabeledInputText
          label="# of Years on Mortgage"
          value={`${numYearsOnMortgage}`}
          editable={false}
          textInputStyle={globalStyles.textInputOrange}
        />
        <LabeledInputText
          label="Down Payment"
          value={`${decimalValuetoPercentage(downPayment)}%`}
          editable={false}
          textInputStyle={globalStyles.textInputOrange}
        />
        <LabeledInputText
          label="Price to Seller"
          value={toStringMoneyValue(priceToSeller.value)}
          editable={false}
          textInputStyle={globalStyles.textInputGray}
        />
        <LabeledInputText
          label="Price to Buyer"
          value={toStringMoneyValue(priceToBuyer.value)}
          editable={false}
          textInputStyle={globalStyles.textInputGray}
        />
        <LabeledInputText
          label="Cap Rate for End Buyer"
          value={`${decimalValuetoPercentage(capRateForEndBuyer.value)}%`}
          editable={false}
          textInputStyle={
            capRateForEndBuyer.value >= settings.formatCapRateForEndBuyer.value
              ? globalStyles.textInputGreen
              : globalStyles.textInputRed
          }
        />
        <LabeledInputText
          label="Cash on Cash Return for End Buyer"
          value={`${decimalValuetoPercentage(
            cashOnCashReturnForEndBuyer.value,
          )}%`}
          editable={false}
          textInputStyle={
            cashOnCashReturnForEndBuyer.value >=
            settings.formatCashOnCashRateForEndBuyer.value
              ? globalStyles.textInputGreen
              : globalStyles.textInputRed
          }
        />
      </View>
      {renderSettingsModal()}
    </GrayRoundedBox>
  );
};

const mapStateToProps = (state) => {
  return {
    dealOption3: state.dealsCalculator.dealOptions.dealOption3.options,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateDealOptions: (key, value) =>
      dispatch(updateIndividualDealOptions(key, value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DealsOption3);
