import React from 'react';
import {View, Text, Image, Linking} from 'react-native';
import helmBlueIcon from '../../assets/images/helm_icon_blue.png';
import TextButton from '../atoms/TextButton';
import EStyleSheet from 'react-native-extended-stylesheet';
import {globalStyles} from '../../assets/styles/globalStyles';
import {BLUE, GREY} from '../../assets/colors';
import {PADDING_SMALL, PADDING_DEFAULT} from '../../assets/dimens';
import {connect} from 'react-redux';
import moment from 'moment';

const styles = EStyleSheet.create({
  root: {flex: 1, justifyContent: 'space-evenly', alignItems: 'center'},
  headerIcon: {
    width: '200rem',
    height: '200rem',
  },
  authorWrapper: {
    width: '100%',
    alignItems: 'center',
  },
  authorName: {
    fontWeight: 'bold',
    color: BLUE,
  },
  contentWrapper: {
    width: '90%',
  },
  contentHeadline: {
    textAlign: 'center',
    lineHeight: '28rem',
    color: GREY[1],
  },
  date: {
    color: BLUE,
    fontWeight: '600',
    textAlign: 'center',
    marginVertical: PADDING_DEFAULT,
  },
  buttonWrapper: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const TutorialWebinar = ({selectedTutorial}) => {
  if (!selectedTutorial) {
    return <View />;
  }
  const {date, signUpLink, proctor, title} = selectedTutorial.webinar;
  return (
    <View style={styles.root}>
      <Image
        source={helmBlueIcon}
        resizeMethod="resize"
        resizeMode="contain"
        style={styles.headerIcon}
      />
      <View style={styles.authorWrapper}>
        <Text style={[globalStyles.h3, styles.authorName]}>{proctor.name}</Text>
        <Text style={globalStyles.p1}>{proctor.title}</Text>
      </View>
      <View style={styles.contentWrapper}>
        <Text style={[globalStyles.h2, styles.contentHeadline]}>{title}</Text>
        <Text style={[globalStyles.p1, styles.date]}>
          {/* {moment(date.toDate()).format('MMM. D, YYYY h:m:ss A')} */}
          {moment(date.toDate()).format('MMM. D, YYYY')}
        </Text>
      </View>
      <View style={styles.buttonWrapper}>
        <TextButton
          width="70%"
          text="SIGNUP FOR FREE NOW"
          type="primary"
          onPress={() => Linking.openURL(signUpLink)}
        />
      </View>
    </View>
  );
};

const mapStateToProps = (state) => {
  return {
    selectedTutorial: state.tutorials.activeTutorial,
  };
};

export default connect(mapStateToProps)(TutorialWebinar);
