import React from 'react';
import {View} from 'react-native';
import Modal from 'react-native-modal';
import EStyleSheet from 'react-native-extended-stylesheet';
import TextButton from '../atoms/TextButton';
import SettingsGeneral from '../../screens/SettingsGeneral';

const styles = EStyleSheet.create({
  root: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    alignSelf: 'center',
    backgroundColor: 'white',
    marginTop: '-10rem',
  },
  modalView: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: '90%',
    backgroundColor: 'white',
    borderRadius: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    height: '70%',
    borderWidth: 1,
    borderColor: 'white',
  },
  header: {
    width: '100%',
    textAlign: 'left',
    fontWeight: 'bold',
  },
  subHeader: {width: '100%', marginVertical: '10rem'},
  contentWrapper: {
    width: '100%',
    height: '400rem',
    justifyContent: 'space-between',
  },
  buttonWrapper: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    height: '15%',
  },
  listWrapper: {height: '85%', width: '100%'},
});

const GeneralSettingsModal = ({show, dismissModal, onSaved}) => {
  const _handleVerifyButtonOnPress = () => {
    dismissModal();
    onSaved();
  };
  return (
    <Modal
      style={[styles.root]}
      visible={show}
      animationType={'slide'}
      onRequestClose={() => console.log('request close')}
      transparent={true}
      onBackdropPress={dismissModal}>
      <View style={styles.modalView}>
        <View style={styles.listWrapper}>
          <SettingsGeneral asModal />
        </View>
        <View style={styles.buttonWrapper}>
          <TextButton
            width="90%"
            type="primary"
            text="CONTINUE"
            onPress={_handleVerifyButtonOnPress}
          />
        </View>
      </View>
    </Modal>
  );
};

export default GeneralSettingsModal;
