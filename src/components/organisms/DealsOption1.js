import React, {useState} from 'react';
import {View, Text} from 'react-native';
import GrayRoundedBox from '../atoms/GrayRoundedBox';
import LabeledInputText from '../atoms/LabeledInputText';
import EStyleSheet from 'react-native-extended-stylesheet';
import DealsOptionHeader from '../molecules/DealsOptionHeader';
import {GREY, TEXT_SECONDARY} from '../../assets/colors';
import {globalStyles, modalStyles} from '../../assets/styles/globalStyles';
import {connect} from 'react-redux';
import {
  decimalValuetoPercentage,
  toStringMoneyValue,
  calculateOption1,
} from '../../utils/OptionsCalculatorUtil';
import {CURRENCY_SYMBOL} from '../../assets/Constants';
import Modal from 'react-native-modal';
import CashCalculator1 from './CashCalculator1';
import CalculatorSettingsModalWrapper from '../atoms/CalculatorSettingsModalWrapper';
import {updateIndividualDealOptions} from './../../state-management/actions/DealsAction';

const styles = EStyleSheet.create({
  formWrapper: {marginTop: '20rem'},
  root: {
    backgroundColor: '#DCDCDC',
  },
});

const DealsOption1 = ({
  route,
  navigation,
  dealOption1,
  propertyInformation,
  settings,
  updateDealOptions,
  toggleSelectedDealOptions,
  dealKey,
  state,
}) => {
  console.log('calc1', {dealOption1, settings, state});
  const [showOptionSettings, setShowOptionSettings] = useState(false);
  const {initialPriceOffered} = dealOption1;
  console.log({propertyInformation, dealOption1});

  const priceDiscount = settings.priceDiscount.value;
  const wholeSaleFee = settings.wholeSaleFee.value;

  const onCalculatorSettingsDoneButtonClick = () => {
    console.log('hohohoho new settings', {settings});
    const dealOptionValue = calculateOption1({
      settings,
      propertyInformation,
    });
    console.log('new deal option value', {dealOptionValue});
    const dealsOptionKey = 'dealOption1';
    updateDealOptions({
      dealsOptionKey,
      dealOptionValue,
    });
    setShowOptionSettings(false);
  };

  const renderSettingsModal = () => (
    <CalculatorSettingsModalWrapper
      visible={showOptionSettings}
      onBackdropPress={() => {}}>
      <CashCalculator1
        dealKey={dealKey}
        asModal
        onDoneButtonPressed={onCalculatorSettingsDoneButtonClick}
      />
    </CalculatorSettingsModalWrapper>
  );
  return (
    <GrayRoundedBox>
      <DealsOptionHeader
        title="Cash Offer to Whole Sale Fix/Flip1"
        loiTitle="Cash Offer"
        name={'Option 1'}
        settings={settings}
        dealOptionValue={dealOption1}
        onGeneralSettingsButtonClick={() => setShowOptionSettings(true)}
      />
      <View style={styles.formWrapper}>
        <LabeledInputText
          label="Price Discount"
          value={decimalValuetoPercentage(priceDiscount) + '%'}
          editable={false}
          textInputStyle={globalStyles.textInputOrange}
        />
        <LabeledInputText
          label="Wholesale Fee"
          value={toStringMoneyValue(wholeSaleFee)}
          editable={false}
          textInputStyle={globalStyles.textInputOrange}
        />
        <LabeledInputText
          label="Initial price offered"
          value={toStringMoneyValue(initialPriceOffered.value)}
          editable={false}
          textInputStyle={globalStyles.textInputGray}
        />
      </View>
      {renderSettingsModal()}
    </GrayRoundedBox>
  );
};

const mapStateToProps = (state) => {
  return {
    dealOption1: state.dealsCalculator.dealOptions.dealOption1.options,
    state: state,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateDealOptions: (key, value) =>
      dispatch(updateIndividualDealOptions(key, value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DealsOption1);
