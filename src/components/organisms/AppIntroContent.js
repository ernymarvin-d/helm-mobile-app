import * as React from 'react';
import {ImageBackground, View, Text} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {globalStyles} from '../../assets/styles/globalStyles';
import {SPLASH_BG_GRADIENT_COLORS} from '../../assets/colors';
import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  appIntroContent: {
    flexGrow: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'black',
  },
  imageBackground: {flex: 1},
  title: {
    color: 'white',
    textAlign: 'center',
    marginBottom: '30rem',
    lineHeight: '40rem',
    fontWeight: 'bold',
  },
  description: {
    color: 'white',
    textAlign: 'center',
    lineHeight: 30,
  },
  wrapper: {
    flex: 1,
    width: '80%',
    justifyContent: 'flex-end',
    paddingBottom: '35%',
    alignItems: 'center',
  },
});

const AppIntroContent = (props) => {
  const {content} = props;
  return (
    <ImageBackground
      style={styles.imageBackground}
      source={content.backgroundImage}>
      <LinearGradient
        colors={SPLASH_BG_GRADIENT_COLORS}
        style={styles.appIntroContent}>
        <View style={styles.wrapper}>
          <Text style={[globalStyles.h1, styles.title]}>{content.title}</Text>
          <Text style={[globalStyles.h3, styles.description]}>
            {content.text}
          </Text>
        </View>
      </LinearGradient>
    </ImageBackground>
  );
};

export default AppIntroContent;
