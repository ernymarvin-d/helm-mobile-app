import React, {useState} from 'react';
import {View} from 'react-native';
import GrayRoundedBox from '../atoms/GrayRoundedBox';
import LabeledInputText from '../atoms/LabeledInputText';
import EStyleSheet from 'react-native-extended-stylesheet';
import DealsOptionHeader from '../molecules/DealsOptionHeader';
import {globalStyles} from '../../assets/styles/globalStyles';
import {connect} from 'react-redux';
import {
  toStringMoneyValue,
  decimalValuetoPercentage,
  calculateOption4,
} from '../../utils/OptionsCalculatorUtil';
import CashCalculator4 from './CashCalculator4';
import CalculatorSettingsModalWrapper from '../atoms/CalculatorSettingsModalWrapper';
import {updateIndividualDealOptions} from './../../state-management/actions/DealsAction';

const styles = EStyleSheet.create({
  formWrapper: {marginTop: '20rem'},
});

const DealsOption4 = ({
  route,
  navigation,
  dealOption4,
  settings,
  propertyInformation,
  updateDealOptions,
  dealKey,
}) => {
  console.log({dealOption4, settings});
  const [showOptionSettings, setShowOptionSettings] = useState(false);
  const {
    balloonPayment,
    capRateForEndBuyer,
    cashOnCashReturnForEndBuyer,
    landContractPriceToBuyer,
    landContractPriceToSeller,
    monthlyPayment,
    valueDownPaymentBuyer,
    valueDownPaymentSeller,
  } = dealOption4;

  const downPayment = settings.downPayment.value;
  const interestRateOnLandContract = settings.interestRateOnLandContract.value;
  const priceDiscount = settings.priceDiscount.value;
  const wholeSaleFee = settings.wholeSaleFee.value;
  const yearsTillBalloonPayment = settings.yearsTillBalloonPayment.value;

  const onCalculatorSettingsDoneButtonClick = () => {
    const dealOptionValue = calculateOption4({
      settings,
      propertyInformation,
    });
    const dealsOptionKey = 'dealOption4';
    updateDealOptions({
      dealsOptionKey,
      dealOptionValue,
    });
    setShowOptionSettings(false);
  };
  const renderSettingsModal = () => (
    <CalculatorSettingsModalWrapper
      visible={showOptionSettings}
      onBackdropPress={() => {}}>
      <CashCalculator4
        asModal
        dealKey={dealKey}
        onDoneButtonPressed={onCalculatorSettingsDoneButtonClick}
      />
    </CalculatorSettingsModalWrapper>
  );
  return (
    <GrayRoundedBox>
      <DealsOptionHeader
        title="Land Contract Interest Only"
        name="Option 4"
        settings={settings}
        dealOptionValue={dealOption4}
        onGeneralSettingsButtonClick={() => setShowOptionSettings(true)}
      />
      <View style={styles.formWrapper}>
        <LabeledInputText
          label="Price Discount"
          value={`${decimalValuetoPercentage(priceDiscount)}%`}
          editable={false}
          textInputStyle={globalStyles.textInputOrange}
        />
        <LabeledInputText
          label="Wholesale Fee"
          value={toStringMoneyValue(wholeSaleFee)}
          editable={false}
          textInputStyle={globalStyles.textInputOrange}
        />
        <LabeledInputText
          label="Interest Rate on Land Contract"
          value={`${decimalValuetoPercentage(interestRateOnLandContract)}%`}
          editable={false}
          textInputStyle={globalStyles.textInputOrange}
        />
        <LabeledInputText
          label="# of Years Till Balloon"
          value={`${yearsTillBalloonPayment}`}
          editable={false}
          textInputStyle={globalStyles.textInputOrange}
        />
        <LabeledInputText
          label="Down Payment"
          value={`${decimalValuetoPercentage(downPayment)}%`}
          editable={false}
          textInputStyle={globalStyles.textInputOrange}
        />
        <LabeledInputText
          label="Land Contract Price to Seller "
          value={toStringMoneyValue(landContractPriceToSeller.value)}
          editable={false}
          textInputStyle={globalStyles.textInputGray}
        />
        <LabeledInputText
          label="Land Contract Price to Buyer "
          value={toStringMoneyValue(landContractPriceToBuyer.value)}
          editable={false}
          textInputStyle={globalStyles.textInputGray}
        />
        <LabeledInputText
          label="Monthly Payments"
          value={toStringMoneyValue(monthlyPayment.value)}
          editable={false}
          textInputStyle={globalStyles.textInputGray}
        />
        <LabeledInputText
          label="Balloon Payments"
          value={toStringMoneyValue(balloonPayment.value)}
          editable={false}
          textInputStyle={globalStyles.textInputGray}
        />
        <LabeledInputText
          label="Down Payment (seller)"
          value={toStringMoneyValue(valueDownPaymentSeller.value)}
          editable={false}
          textInputStyle={globalStyles.textInputGray}
        />
        <LabeledInputText
          label="Down Payment (buyer)"
          value={toStringMoneyValue(valueDownPaymentBuyer.value)}
          editable={false}
          textInputStyle={globalStyles.textInputGray}
        />
        <LabeledInputText
          label="Cap Rate for End Buyer"
          value={`${decimalValuetoPercentage(capRateForEndBuyer.value)}%`}
          editable={false}
          textInputStyle={
            capRateForEndBuyer.value >= settings.formatCapRateForEndBuyer.value
              ? globalStyles.textInputGreen
              : globalStyles.textInputRed
          }
        />
        <LabeledInputText
          label="Cash on Cash Return for End Buyer"
          value={`${decimalValuetoPercentage(
            cashOnCashReturnForEndBuyer.value,
          )}%`}
          editable={false}
          textInputStyle={
            cashOnCashReturnForEndBuyer.value >=
            settings.formatCashOnCashRateForEndBuyer.value
              ? globalStyles.textInputGreen
              : globalStyles.textInputRed
          }
        />
        {renderSettingsModal()}
      </View>
    </GrayRoundedBox>
  );
};

const mapStateToProps = (state) => {
  return {
    dealOption4: state.dealsCalculator.dealOptions.dealOption4.options,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateDealOptions: (key, value) =>
      dispatch(updateIndividualDealOptions(key, value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DealsOption4);
