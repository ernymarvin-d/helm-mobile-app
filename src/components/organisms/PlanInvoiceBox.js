import React from 'react';
import {View, Text} from 'react-native';
import GrayRoundedBox from '../atoms/GrayRoundedBox';
import {globalStyles} from '../../assets/styles/globalStyles';
import EStyleSheet from 'react-native-extended-stylesheet';
import {
  BLUE,
  BLUE_SECONDARY,
  BLUE_FADED,
  DIVIDER_COLOR,
} from '../../assets/colors';
import {MARGIN_SMALL, MARGIN_LARGE, MARGIN_DEFAULT} from '../../assets/dimens';

const styles = EStyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: MARGIN_SMALL,
  },
  planLabel: {
    fontWeight: 'bold',
  },
  planValue: {
    fontWeight: 'bold',
    color: BLUE_FADED,
  },
  wrapperSubTotal: {
    marginBottom: MARGIN_DEFAULT,
  },
  divider: {
    height: '1rem',
    borderBottomWidth: '1rem',
    borderBottomColor: DIVIDER_COLOR,
  },
  totalLabel: {
    fontWeight: '600',
    color: BLUE_FADED,
  },
  totalValue: {
    fontWeight: '600',
    color: BLUE_FADED,
  },
});

const PlanInvoiceBox = ({navigation}) => {
  return (
    <GrayRoundedBox>
      <View style={[styles.wrapper, styles.wrapperPlanPrice]}>
        <Text style={[globalStyles.p1, styles.planLabel]}>Pay Per Month</Text>
        <Text style={[globalStyles.p1, styles.planValue]}>$29</Text>
      </View>
      <View style={[styles.wrapper, styles.wrapperSubTotal]}>
        <Text style={[globalStyles.p2, styles.subtotalLabel]}>Subtotal</Text>
        <Text style={[globalStyles.p2, styles.subtotalValue]}>$29.00</Text>
      </View>
      <View style={styles.divider} />
      <View style={[styles.wrapper, styles.wrapperTotal]}>
        <Text style={[globalStyles.p2, styles.totalLabel]}>Total</Text>
        <Text style={[globalStyles.p2, styles.totalValue]}>$29.00</Text>
      </View>
    </GrayRoundedBox>
  );
};

export default PlanInvoiceBox;
