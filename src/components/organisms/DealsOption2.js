import React, {useState} from 'react';
import {View, Text} from 'react-native';
import GrayRoundedBox from '../atoms/GrayRoundedBox';
import LabeledInputText from '../atoms/LabeledInputText';
import EStyleSheet from 'react-native-extended-stylesheet';
import DealsOptionHeader from '../molecules/DealsOptionHeader';
import {GREY, TEXT_SECONDARY} from '../../assets/colors';
import {globalStyles, modalStyles} from '../../assets/styles/globalStyles';
import {connect} from 'react-redux';
import {
  toStringMoneyValue,
  decimalValuetoPercentage,
  calculateOption2,
} from '../../utils/OptionsCalculatorUtil';
import CalculatorSettingsModalWrapper from '../atoms/CalculatorSettingsModalWrapper';
import CashCalculator2 from './CashCalculator2';
import {ScrollView} from 'react-native-gesture-handler';
import {updateIndividualDealOptions} from './../../state-management/actions/DealsAction';

const styles = EStyleSheet.create({
  formWrapper: {marginTop: '20rem'},
});

const DealsOption2 = ({
  route,
  navigation,
  dealOption2,
  settings,
  updateDealOptions,
  propertyInformation,
  toggleSelectedDealOptions,
  dealKey,
}) => {
  console.log({dealOption2, settings});
  const downPayment = settings.downPayment.value;
  const holdingPeriod = settings.holdingPeriod.value;
  const lendingApr = settings.lendingApr.value;
  const lendingPts = settings.lendingPts.value;
  const priceDiscount = settings.priceDiscount.value;
  const sellingCost = settings.sellingCost.value;

  const {
    initialPriceOffered,
    profit,
    profitMargin,
    returnOnCashInvestment,
  } = dealOption2;

  const onCalculatorSettingsDoneButtonClick = () => {
    const dealOptionValue = calculateOption2({
      settings,
      propertyInformation,
    });
    const dealsOptionKey = 'dealOption2';
    updateDealOptions({
      dealsOptionKey,
      dealOptionValue,
    });
    setShowOptionSettings(false);
  };

  const [showOptionSettings, setShowOptionSettings] = useState(false);
  const renderSettingsModal = () => (
    <CalculatorSettingsModalWrapper
      visible={showOptionSettings}
      onBackdropPress={() => {}}>
      <CashCalculator2
        dealKey={dealKey}
        asModal
        onDoneButtonPressed={onCalculatorSettingsDoneButtonClick}
      />
    </CalculatorSettingsModalWrapper>
  );

  return (
    <GrayRoundedBox>
      <DealsOptionHeader
        name="Option 2"
        title="Cash Offer for Your Fix/Flip"
        loiTitle="Cash Offer"
        settings={settings}
        dealOptionValue={dealOption2}
        onGeneralSettingsButtonClick={() => setShowOptionSettings(true)}
      />
      <View style={styles.formWrapper}>
        <LabeledInputText
          label="Price Discount"
          value={`${decimalValuetoPercentage(priceDiscount)}%`}
          editable={false}
          textInputStyle={globalStyles.textInputOrange}
        />
        <LabeledInputText
          label="Initial price offered"
          value={toStringMoneyValue(initialPriceOffered.value)}
          editable={false}
          textInputStyle={globalStyles.textInputDisabled}
        />
        <LabeledInputText
          label="Down Payment (if required)"
          value={toStringMoneyValue(downPayment)}
          editable={false}
          textInputStyle={globalStyles.textInputOrange}
        />
        <LabeledInputText
          label={`Lending APR${lendingApr}`}
          value={`${decimalValuetoPercentage(lendingApr)}%`}
          editable={false}
          textInputStyle={globalStyles.textInputOrange}
        />
        <LabeledInputText
          label="Lending Pts"
          value={`${decimalValuetoPercentage(lendingPts)}%`}
          editable={false}
          textInputStyle={globalStyles.textInputOrange}
        />
        <LabeledInputText
          label="Holding Period (mths)"
          value={`${holdingPeriod}`}
          editable={false}
          textInputStyle={globalStyles.textInputOrange}
        />
        <LabeledInputText
          label="Selling Cost"
          value={`${decimalValuetoPercentage(sellingCost)}%`}
          editable={false}
          textInputStyle={globalStyles.textInputOrange}
        />
        <LabeledInputText
          label="Profit Margin"
          value={`${decimalValuetoPercentage(profitMargin.value)}%`}
          editable={false}
          textInputStyle={
            profitMargin.value >= settings.formatProfitMargin.value
              ? globalStyles.textInputGreen
              : globalStyles.textInputRed
          }
        />
        <LabeledInputText
          label="Return on Cash Investment"
          value={`${decimalValuetoPercentage(returnOnCashInvestment.value)}%`}
          editable={false}
          textInputStyle={
            returnOnCashInvestment.value >=
            settings.formatReturnOnCachInvestment.value
              ? globalStyles.textInputGreen
              : globalStyles.textInputRed
          }
        />
        <LabeledInputText
          label="Profit"
          value={toStringMoneyValue(profit.value)}
          editable={false}
          textInputStyle={globalStyles.textInputDisabled}
        />
      </View>
      {renderSettingsModal()}
    </GrayRoundedBox>
  );
};

const mapStateToProps = (state) => {
  return {
    dealOption2: state.dealsCalculator.dealOptions.dealOption2.options,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateDealOptions: (key, value) =>
      dispatch(updateIndividualDealOptions(key, value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DealsOption2);
