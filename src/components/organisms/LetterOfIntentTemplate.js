import React from 'react';
import {View, Text} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import {PADDING_DEFAULT, MARGIN_DEFAULT} from '../../assets/dimens';
import {globalStyles} from '../../assets/styles/globalStyles';
import TextButton from '../atoms/TextButton';
import downloadIcon from '../../assets/images/ic-download.png';

const styles = EStyleSheet.create({
  contentWrapper: {
    height: '100%',
    padding: PADDING_DEFAULT,
  },
  contentText: {
    marginBottom: MARGIN_DEFAULT,
  },
  headerText: {
    marginBottom: MARGIN_DEFAULT,
    fontFamily: 'Montserrat-Bold',
  },
  bottomButtonWrapper: {
    marginTop: MARGIN_DEFAULT,
  },
});

const LetterOfIntentTemplate = ({sendToSellerBtnClick}) => {
  const handleOnPressGenerateProposalButtonClick = () => {
    sendToSellerBtnClick();
  };
  const handleOnPressDownloadPdf = () => {};
  return (
    <View style={styles.contentWrapper}>
      <Text style={[globalStyles.h3, styles.headerText]}>LOI Template</Text>
      <Text style={[globalStyles.p1, styles.contentText]}>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus illo
        quaerat provident soluta blanditiis perspiciatis, sequi nulla quos
        perferendis fugit nihil error facere reiciendis beatae laborum officia
        atque, quae ea.
      </Text>
      <Text style={[globalStyles.p1, styles.contentText]}>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus illo
        quaerat provident soluta blanditiis perspiciatis, sequi nulla quos
        perferendis fugit nihil error facere reiciendis beatae laborum officia
        atque, quae ea.
      </Text>
      <Text style={[globalStyles.p1, styles.contentText]}>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus illo
        quaerat provident soluta blanditiis perspiciatis, sequi nulla quos
        perferendis fugit nihil error facere reiciendis beatae laborum officia
        atque, quae ea.
      </Text>
      <Text style={[globalStyles.p1, styles.contentText]}>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus illo
        quaerat provident soluta blanditiis perspiciatis, sequi nulla quos
        perferendis fugit nihil error facere reiciendis beatae laborum officia
        atque, quae ea.
      </Text>
      <Text style={[globalStyles.p1, styles.contentText]}>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus illo
        quaerat provident soluta blanditiis perspiciatis, sequi nulla quos
        perferendis fugit nihil error facere reiciendis beatae laborum officia
        atque, quae ea.
      </Text>
      <Text style={[globalStyles.p1, styles.contentText]}>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus illo
        quaerat provident soluta blanditiis perspiciatis, sequi nulla quos
        perferendis fugit nihil error facere reiciendis beatae laborum officia
        atque, quae ea.
      </Text>
      <Text style={[globalStyles.p1, styles.contentText]}>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus illo
        quaerat provident soluta blanditiis perspiciatis, sequi nulla quos
        perferendis fugit nihil error facere reiciendis beatae laborum officia
        atque, quae ea.
      </Text>
      <View style={styles.bottomButtonWrapper}>
        <TextButton
          text="SEND TO SELLER"
          type="primary"
          onPress={handleOnPressGenerateProposalButtonClick}
        />
        <TextButton
          text="Download PDF (287kb)"
          type="link"
          leftIcon={downloadIcon}
          onPress={handleOnPressDownloadPdf}
        />
      </View>
    </View>
  );
};

export default LetterOfIntentTemplate;
