import React, {useState} from 'react';
import {View} from 'react-native';
import LabeledInputText from './../atoms/LabeledInputText';
import TextButton from './../atoms/TextButton';
import EStyleSheet from 'react-native-extended-stylesheet';
import {globalStyles} from '../../assets/styles/globalStyles';
import {validateEmailAddress} from '../../utils/TextUtil';
import _ from 'lodash';

const styles = EStyleSheet.create({
  root: {
    flex: 1,
    flexGrow: 1,
    justifyContent: 'space-between',
    backgroundColor: 'white',
  },
  contentWrapper: {
    padding: '16rem',
    flexGrow: 1,
    backgroundColor: '#fff',
  },
  buttonWrapper: {
    marginVertical: '18rem',
    width: '100%',
    height: '100rem',
    justifyContent: 'space-between',
  },
  formWrapper: {
    marginTop: '20rem',
  },
});

const SellerInformationForm = ({
  propertyAddressDefaultValue,
  emailAddressDefaultValue,
  phoneNumberDefaultValue,
  sellerNameDefaultValue,
  submitButtonText,
  onPressSubmitButton,
}) => {
  const [sellerName, setSellerName] = useState(
    sellerNameDefaultValue ? sellerNameDefaultValue : '',
  );
  const [emailAddress, setEmailAddress] = useState(
    emailAddressDefaultValue ? emailAddressDefaultValue : '',
  );
  const [propertyAddress, setPropertyAddress] = useState(
    propertyAddressDefaultValue ? propertyAddressDefaultValue : '',
  );
  const [phoneNumber, setPhoneNumber] = useState(
    phoneNumberDefaultValue ? phoneNumberDefaultValue : '',
  );

  const [isValidSellerName, setIsValidSellerName] = useState(true);
  const [isValidEmailAddress, setIsValidEmailAddress] = useState(true);
  const [isValidPropertyAddress, setIsValidPropertyAddress] = useState(true);
  const [isValidPhoneNumber, setIsValidPhoneNumber] = useState(true);

  const handleOnPressSubmitButton = () => {
    if (!isFormValueValid()) return;

    const formData = {
      sellerName,
      emailAddress,
      propertyAddress,
      phoneNumber,
    };

    onPressSubmitButton(formData);
  };

  const isFormValueValid = () => {
    let isValid = true;
    if (_.isEmpty(sellerName)) {
      setIsValidSellerName(false);
      isValid = false;
    } else {
      setIsValidSellerName(true);
    }

    if (!validateEmailAddress(emailAddress)) {
      setIsValidEmailAddress(false);
      isValid = false;
    } else {
      setIsValidEmailAddress(true);
    }

    if (_.isEmpty(propertyAddress)) {
      setIsValidPropertyAddress(false);
      isValid = false;
    } else {
      setIsValidPropertyAddress(true);
    }

    return isValid;
  };

  return (
    <View style={[styles.contentWrapper]}>
      <View style={styles.formWrapper}>
        <LabeledInputText
          label="Seller Name"
          type="name"
          placeholder="John Doe"
          textInputProps={{
            onChangeText: (value) => setSellerName(value),
            onFocus: () => setIsValidSellerName(true),
          }}
          value={sellerName}
          isValid={isValidSellerName}
        />
        <LabeledInputText
          label="Email Address"
          type="emailAddress"
          placeholder="ex:inform@gmail.com"
          textInputProps={{
            onChangeText: (value) => setEmailAddress(value),
            keyboardType: 'email-address',
            onFocus: () => setIsValidEmailAddress(true),
          }}
          value={emailAddress}
          isValid={isValidEmailAddress}
        />
        <LabeledInputText
          label="Properties Address"
          textInputProps={{
            multiline: true,
            onChangeText: (value) => setPropertyAddress(value),
            onFocus: () => setIsValidPropertyAddress(true),
            textAlignVertical: 'top',
          }}
          value={propertyAddress}
          textInputHeight={100}
          textInputStyle={globalStyles.addressTextInput}
          isValid={isValidPropertyAddress}
        />
        <LabeledInputText
          label="Phone Number"
          optional
          textInputProps={{
            onChangeText: (value) => setPhoneNumber(value),
            keyboardType: 'numeric',
          }}
          value={phoneNumber}
          isValid={isValidPhoneNumber}
        />
      </View>
      <View style={styles.buttonWrapper}>
        <TextButton
          type="primary"
          text={submitButtonText ? submitButtonText : 'Next'}
          onPress={handleOnPressSubmitButton}
        />
      </View>
    </View>
  );
};

export default SellerInformationForm;
