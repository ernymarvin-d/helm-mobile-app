import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {
  globalStyles,
  calculatorLayoutStyles,
  modalStyles,
} from '../../assets/styles/globalStyles';
import CalculatorEntryBox from '../molecules/CalculatorEntryBox';
import {
  decimalValuetoPercentage,
  toStringMoneyValue,
  percentageToDecimalValue,
} from '../../utils/OptionsCalculatorUtil';
import {connect} from 'react-redux';
import {
  updateCalculatorSettingsOption,
  updateCalupdateDealsSettingsOption,
} from '../../state-management/actions/AuthActions';
import _ from 'lodash';
import {updateUserDefaultCalculatorSetting} from '../../utils/CalculatorSettingsUtil';

// balanceOnMortgage: 70000
// cashForKeysPurchaseAmountAboveBalance: 5000
const CashCalculator8 = ({
  style,
  route,
  navigation,
  settings,
  updateCalculatorSettingsOption,
  asModal,
  onDoneButtonPressed,
  updateCalupdateDealsSettingsOption,
  dealKey,
  user,
}) => {
  const onUpdateOption = (key, value) => {
    const payload = {
      dealKey,
      settingsKey: key,
      value,
      settingsOptionKey: 'option8',
      settings,
      stateAction: {
        updateCalupdateDealsSettingsOption,
        updateCalculatorSettingsOption,
      },
      userId: user.userId,
    };
    updateUserDefaultCalculatorSetting(payload);
  };

  // const balanceOnMortgage = settings.balanceOnMortgage.value;
  const cashForKeysPurchaseAmountAboveBalance =
    settings.cashForKeysPurchaseAmountAboveBalance.value;
  const formatCashOnCashReturn = settings.formatCashOnCashReturn.value;

  const renderDoneButton = () => {
    if (asModal) {
      return (
        <TouchableOpacity onPress={onDoneButtonPressed}>
          <Text style={[globalStyles.p2, {alignSelf: 'flex-end'}]}>Done</Text>
        </TouchableOpacity>
      );
    } else {
      return <View />;
    }
  };
  return (
    <View
      style={[
        calculatorLayoutStyles.calculatorBox,
        style,
        asModal ? modalStyles.asModal : {},
      ]}>
      {renderDoneButton()}
      <View style={calculatorLayoutStyles.calculatorHeaderWrapper}>
        <Text style={[globalStyles.p2, calculatorLayoutStyles.calculatorName]}>
          Calculator 8
        </Text>
        <Text style={[globalStyles.p1, calculatorLayoutStyles.calculatorTitle]}>
          Subject To Wholesale
        </Text>
      </View>
      {/* <CalculatorEntryBox
        value={toStringMoneyValue(balanceOnMortgage)}
        title="preferred balance on Mortgage"
        onUpdate={(value) => onUpdateOption('balanceOnMortgage', value)}
      /> */}
      <CalculatorEntryBox
        value={toStringMoneyValue(cashForKeysPurchaseAmountAboveBalance)}
        title="Preferred cash for keys purchase amount above balance"
        onUpdate={(value) =>
          onUpdateOption('cashForKeysPurchaseAmountAboveBalance', value)
        }
      />
      <CalculatorEntryBox
        value={`${decimalValuetoPercentage(formatCashOnCashReturn)}%`}
        isPercentage
        onUpdate={(value) =>
          onUpdateOption(
            'formatCashOnCashReturn',
            percentageToDecimalValue(value),
          )
        }
        title="Cash on Cash Return (Conditional Formating)"
      />
    </View>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateCalculatorSettingsOption: (settings) =>
      dispatch(updateCalculatorSettingsOption(settings)),
    updateCalupdateDealsSettingsOption: (settings) =>
      dispatch(updateCalupdateDealsSettingsOption(settings)),
  };
};

const mapStateToProps = (state) => {
  return {
    settings: state.auth.user.calculatorGeneralSettings.option8,
    user: state.auth.user,
  };
};

export default connect(
  () => mapStateToProps,
  mapDispatchToProps,
)(CashCalculator8);
