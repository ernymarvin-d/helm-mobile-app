import React, {useState} from 'react';
import {
  View,
  Text,
  KeyboardAvoidingView,
  Platform,
  Keyboard,
} from 'react-native';
import Modal from 'react-native-modal';
import EStyleSheet from 'react-native-extended-stylesheet';
import {GREY} from '../../assets/colors';
import {globalStyles} from './../../assets/styles/globalStyles';
import NumberCodeVerificationInput from './../molecules/NumberCodeVerificationInput';
import {TouchableOpacity} from 'react-native-gesture-handler';
import TextButton from '../atoms/TextButton';

const styles = EStyleSheet.create({
  root: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    alignSelf: 'center',
    backgroundColor: GREY[3],
  },
  modalView: {
    // paddingVertical: '50rem',
    justifyContent: 'center',
    alignItems: 'center',
    width: '90%',
    backgroundColor: 'white',
    borderRadius: 20,
    paddingHorizontal: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    height: '75%',
  },
  header: {
    width: '100%',
    textAlign: 'left',
    fontWeight: 'bold',
  },
  subHeader: {width: '100%', marginVertical: '10rem'},
  contentWrapper: {
    width: '100%',
    height: '400rem',
    justifyContent: 'space-between',
  },
});

export default VerifyPhoneNumberModal = ({
  show,
  dismissModal,
  onVerifyButtonPressed,
  onResendCodePressed,
}) => {
  const [verificationCode, setVerificationCode] = useState('');
  const _handleVerifyButtonOnPress = () => {
    dismissModal();
    onVerifyButtonPressed(verificationCode);
  };
  const _handleResendButtonOnPress = () => {
    onResendCodePressed();
  };
  return (
    <Modal
      style={[styles.root]}
      visible={show}
      animationType={'slide'}
      onRequestClose={() => console.log('request close')}
      transparent={true}
      onBackdropPress={dismissModal}>
      <View style={styles.modalView}>
        <View style={styles.contentWrapper}>
          <KeyboardAvoidingView
            style={{flex: 1}}
            behavior={Platform.OS === 'ios' ? 'padding' : null}>
            <View>
              <Text style={[globalStyles.h3, styles.header]}>
                Verify Your Phone Number
              </Text>
              <Text style={[globalStyles.p1, styles.subHeader]}>
                We keep your phone as communications purposes. It will be secure
                in our database.
              </Text>
            </View>
            <View>
              <Text style={[globalStyles.h3, styles.subHeader]}>
                Enter 6-digit code number
              </Text>
              <NumberCodeVerificationInput
                numDigit={6}
                onValueChange={(value) => {
                  if (value.length === 6) {
                    Keyboard.dismiss();
                  }
                  setVerificationCode(value);
                }}
              />
            </View>
            <View style={{marginTop: 32}}>
              <TextButton
                width="100%"
                type="primary"
                text="VERIFY"
                onPress={_handleVerifyButtonOnPress}
              />
              <TextButton
                width="100%"
                type="link"
                text="Resend Code"
                textStyle={{fontWeight: 'bold'}}
                onPress={_handleResendButtonOnPress}
              />
            </View>
          </KeyboardAvoidingView>
        </View>
      </View>
    </Modal>
  );
};
