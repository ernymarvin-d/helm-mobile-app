import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {
  globalStyles,
  calculatorLayoutStyles,
  modalStyles,
} from '../../assets/styles/globalStyles';
import CalculatorEntryBox from '../molecules/CalculatorEntryBox';
import {
  percentageToDecimalValue,
  decimalValuetoPercentage,
  toStringMoneyValue,
} from '../../utils/OptionsCalculatorUtil';
import {connect} from 'react-redux';
import {
  updateCalculatorSettingsOption,
  updateCalupdateDealsSettingsOption,
} from '../../state-management/actions/AuthActions';
import _ from 'lodash';
import {updateUserDefaultCalculatorSetting} from '../../utils/CalculatorSettingsUtil';

// minOptionDepositFromTenantBuyer: 5000
// numYears: 5
// optionDepositToSeller: 500
// priceDiscount: 0.85
const CashCalculator6 = ({
  style,
  route,
  navigation,
  settings,
  updateCalculatorSettingsOption,
  asModal,
  onDoneButtonPressed,
  updateCalupdateDealsSettingsOption,
  dealKey,
  user,
}) => {
  const onUpdateOption = (key, value) => {
    const payload = {
      dealKey,
      settingsKey: key,
      value,
      settingsOptionKey: 'option6',
      settings,
      stateAction: {
        updateCalupdateDealsSettingsOption,
        updateCalculatorSettingsOption,
      },
      userId: user.userId,
    };
    updateUserDefaultCalculatorSetting(payload);
  };

  const numYears = settings.numYears.value;
  const optionDepositToSeller = settings.optionDepositToSeller.value;
  const priceDiscount = settings.priceDiscount.value;
  const minOptionDepositFromTenantBuyer =
    settings.minOptionDepositFromTenantBuyer.value;
  const formatMinOptionDepositFromTenantBuyer =
    settings.formatMinOptionDepositFromTenantBuyer.value;
  const formatTotalProfit = settings.formatTotalProfit.value;

  const renderDoneButton = () => {
    if (asModal) {
      return (
        <TouchableOpacity onPress={onDoneButtonPressed}>
          <Text style={[globalStyles.p2, {alignSelf: 'flex-end'}]}>Done</Text>
        </TouchableOpacity>
      );
    } else {
      return <View />;
    }
  };
  return (
    <View
      style={[
        calculatorLayoutStyles.calculatorBox,
        style,
        asModal ? modalStyles.asModal : {},
      ]}>
      {renderDoneButton()}
      <View style={calculatorLayoutStyles.calculatorHeaderWrapper}>
        <Text style={[globalStyles.p2, calculatorLayoutStyles.calculatorName]}>
          Calculator 6
        </Text>
        <Text style={[globalStyles.p1, calculatorLayoutStyles.calculatorTitle]}>
          Sandwich Lease Option
        </Text>
      </View>
      <CalculatorEntryBox
        value={`${toStringMoneyValue(minOptionDepositFromTenantBuyer)}`}
        title="Preferred minimum option deposit from Tenant"
        onUpdate={(value) =>
          onUpdateOption('minOptionDepositFromTenantBuyer', value)
        }
      />
      <CalculatorEntryBox
        value={`${decimalValuetoPercentage(priceDiscount)}%`}
        isPercentage
        onUpdate={(value) =>
          onUpdateOption('priceDiscount', percentageToDecimalValue(value))
        }
        title="Preferred discount rate off of market value (standard)"
      />
      <CalculatorEntryBox
        value={toStringMoneyValue(optionDepositToSeller)}
        onUpdate={(value) => onUpdateOption('optionDepositToSeller', value)}
        title="Preferred option deposit to seller"
      />
      <CalculatorEntryBox
        value={`${numYears} Years`}
        onUpdate={(value) => onUpdateOption('numYears', value)}
        title="Preferred number of years"
      />
      <CalculatorEntryBox
        value={toStringMoneyValue(formatMinOptionDepositFromTenantBuyer)}
        onUpdate={(value) =>
          onUpdateOption('formatMinOptionDepositFromTenantBuyer', value)
        }
        title="Min Option Deposit From Tenant/Buyer (Conditional Formating)"
      />
      <CalculatorEntryBox
        value={toStringMoneyValue(formatTotalProfit)}
        onUpdate={(value) => onUpdateOption('formatTotalProfit', value)}
        title="Total Profit (Conditional Formating)"
      />
    </View>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateCalculatorSettingsOption: (settings) =>
      dispatch(updateCalculatorSettingsOption(settings)),
    updateCalupdateDealsSettingsOption: (settings) =>
      dispatch(updateCalupdateDealsSettingsOption(settings)),
  };
};

const mapStateToProps = (state) => {
  return {
    settings: state.auth.user.calculatorGeneralSettings.option6,
    user: state.auth.user,
  };
};

export default connect(
  () => mapStateToProps,
  mapDispatchToProps,
)(CashCalculator6);
