import React from 'react';
import {Text, View, Image} from 'react-native';
import GrayRoundedBox from '../atoms/GrayRoundedBox';
import TextButton from '../atoms/TextButton';
import EStyleSheet from 'react-native-extended-stylesheet';
import {
  MARGIN_DEFAULT,
  MARGIN_SMALL,
  PADDING_DEFAULT,
  PADDING_SMALL,
  MARGIN_LARGE,
} from '../../assets/dimens';
import {
  BLUE,
  TEXT_SECONDARY,
  TEXT_PRIMARY,
  GREY,
  ORANGE,
} from '../../assets/colors';
import checkIcon from '../../assets/images/ic-check.png';
import {globalStyles} from '../../assets/styles/globalStyles';

const styles = EStyleSheet.create({
  root: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '80%',
    alignSelf: 'center',
  },
  title: {
    fontFamily: 'Montserrat-Light',
    fontSize: '26rem',
    textTransform: 'uppercase',
    marginVertical: MARGIN_SMALL,
    color: TEXT_SECONDARY,
  },
  price: {
    fontFamily: 'Montserrat-Bold',
    fontSize: '30rem',
    color: BLUE,
  },
  paymentTerms: {
    fontFamily: 'Montserrat-Medium',
    fontSize: '14rem',
    color: GREY[1],
    marginVertical: MARGIN_DEFAULT,
  },
  checklistWrapper: {
    marginTop: MARGIN_LARGE,
  },
  checklistItem: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: PADDING_SMALL,
  },
  borderBottom: {
    borderBottomWidth: 1,
    borderBottomColor: '#E5E6E8',
  },
  iconCheck: {
    tintColor: BLUE,
  },
  primary: {borderColor: ORANGE},
  prevPrice: {
    fontSize: '16rem',
    fontFamily: 'Montserrat-Bold',
    color: TEXT_SECONDARY,
    textDecorationLine: 'line-through',
  },
  lastChecklistItem: {
    marginBottom: MARGIN_DEFAULT,
  },
});

const UpgradeBox = ({
  title,
  price,
  prevPrice,
  paymentTerms,
  checklist,
  primary,
  containerStyle,
  onPressUpgradeButton,
}) => {
  const renderPrevPrice = prevPrice && (
    <Text style={styles.prevPrice}>{prevPrice}</Text>
  );

  const renderCheckList = () => {
    return checklist.map((val, index) => {
      const isLastItem = index === checklist.length - 1;
      return <CheckList text={val} index={index} isLastItem={isLastItem} />;
    });
  };

  return (
    <GrayRoundedBox
      style={[styles.root, primary ? styles.primary : {}, containerStyle]}>
      <Text style={styles.title}>{title}</Text>
      {renderPrevPrice}
      <Text style={styles.price}>{price}</Text>
      <Text style={styles.paymentTerms}>{paymentTerms}</Text>
      {onPressUpgradeButton ? (
        <TextButton
          width={200}
          text={'UPGRAGE'}
          type="primary"
          onPress={onPressUpgradeButton}
        />
      ) : null}

      <View
        style={onPressUpgradeButton ? styles.checklistWrapper : {marginTop: 0}}>
        {renderCheckList()}
      </View>
    </GrayRoundedBox>
  );
};

const CheckList = ({text, isLastItem}) => {
  const checklistWrapperStyle = [styles.checklistItem];
  if (isLastItem) {
    checklistWrapperStyle.push(styles.lastChecklistItem);
  } else {
    checklistWrapperStyle.push(styles.borderBottom);
  }
  return (
    <View style={checklistWrapperStyle}>
      <Image
        style={styles.iconCheck}
        source={checkIcon}
        resizeMethod="resize"
        resizeMode="center"
      />
      <Text style={globalStyles.p2}>{text}</Text>
    </View>
  );
};

export default UpgradeBox;
