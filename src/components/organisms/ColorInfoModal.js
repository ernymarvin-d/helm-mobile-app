import React from 'react';
import {View, Text} from 'react-native';
import Modal from 'react-native-modal';
import EStyleSheet from 'react-native-extended-stylesheet';
import {globalStyles, modalStyles} from './../../assets/styles/globalStyles';
import IconButton from '../atoms/IconButton';
import {colorInformation} from '../../assets/Constants';
import closeIcon from '../../assets/images/ic-close.png';
import {
  BUTTON_WIDTH_SMALL,
  BUTTON_HEIGHT_SMALL,
  MARGIN_SMALL,
  MARGIN_DEFAULT,
  MARGIN_LARGE,
} from '../../assets/dimens';
import {DIVIDER_COLOR, GREY} from '../../assets/colors';

const styles = EStyleSheet.create({
  modal: {
    backgroundColor: '#DCDCDC',
  },
  header: {
    width: '100%',
    textAlign: 'left',
    fontWeight: 'bold',
    marginTop: '25rem',
    marginBottom: MARGIN_SMALL,
  },
  contentWrapper: {
    width: '90%',
    height: '400rem',
  },
  madalView: {
    backgroundColor: '#F5F6F8',
  },
  colorBox: {
    height: BUTTON_HEIGHT_SMALL,
    width: BUTTON_WIDTH_SMALL,
    borderRadius: 10,
  },
  colorInfoWrapper: {
    flexDirection: 'row',
    width: '100%',
    margin: 0,
    paddingTop: '34rem',
    paddingBottom: MARGIN_LARGE,
    borderBottomWidth: '1rem',
    borderBottomColor: DIVIDER_COLOR,
  },
  description: {},
  descriptionWrapper: {
    marginLeft: MARGIN_DEFAULT,
    flex: 1,
  },
  closeIcon: {
    width: '10rem',
    height: '10rem',
  },
  iconContainer: {
    alignSelf: 'flex-end',
    margin: 0,
    padding: 0,
    width: '20rem',
    height: '20rem',
    marginTop: MARGIN_SMALL,
  },
});

const ColorInfoModal = ({show, dismissModal, onVerified}) => {
  const renderColorInformation = () => {
    return colorInformation.map((colorInfo) => {
      const {color, description} = colorInfo;
      return (
        <View style={styles.colorInfoWrapper}>
          <View style={[styles.colorBox, {backgroundColor: color}]} />
          <View style={styles.descriptionWrapper}>
            <Text style={[globalStyles.p2, styles.description]}>
              {description}
            </Text>
          </View>
        </View>
      );
    });
  };
  return (
    <Modal
      style={[modalStyles.root, styles.modal]}
      visible={show}
      animationType={'slide'}
      onRequestClose={() => console.log('request close')}
      transparent={true}
      onBackdropPress={dismissModal}>
      <View style={[modalStyles.modalView, styles.madalView]}>
        <View style={styles.contentWrapper}>
          <IconButton
            icon={closeIcon}
            bgColor="transparent"
            tintColor={GREY[0]}
            iconStyle={styles.closeIcon}
            containerStyle={styles.iconContainer}
            onPress={dismissModal}
          />
          <Text style={[globalStyles.h3, styles.header]}>
            Some Color Informations
          </Text>
          {renderColorInformation()}
        </View>
      </View>
    </Modal>
  );
};

export default ColorInfoModal;
