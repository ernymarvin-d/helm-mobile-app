import React, {useState, useEffect} from 'react';
import {View, Text, Dimensions} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import {globalStyles} from '../../assets/styles/globalStyles';
import SearchBar from '../atoms/SearchBar';
import VideoBox from '../atoms/VideoBox';
import {
  MARGIN_DEFAULT,
  MARGIN_LARGE,
  PADDING_DEFAULT,
  MARGIN_SMALL,
} from '../../assets/dimens';
import {FlatList} from 'react-native-gesture-handler';
import {BOX_BORDER_COLOR} from '../../assets/colors';
import {faqVideoData} from '../../utils/Mocks';
import {getAllFaqs} from '../../utils/DbUtils';
import PleaseWaitDialog from './../atoms/PleaseWaitDialog';
import _ from 'lodash';

const styles = EStyleSheet.create({
  searchBarWrapper: {
    margin: MARGIN_DEFAULT,
  },
  headerText: {
    marginBottom: MARGIN_DEFAULT,
    marginTop: MARGIN_LARGE,
    fontWeight: 'bold',
  },
  faqListWrapper: {
    flex: 1,
    marginTop: MARGIN_DEFAULT,
  },
  divider: {
    backgroundColor: BOX_BORDER_COLOR,
    height: '1rem',
  },
  faqDataWrapper: {
    padding: PADDING_DEFAULT,
  },
  contentWrapper: {
    flex: 1,
  },
});

const FaqVideos = () => {
  const [faqs, setFaqs] = useState([]);
  const [showFaqs, setShowFaqs] = useState([]);
  const [showPleaseWaitDialog, setShowPleaseWaitDialog] = useState(true);
  useEffect(() => {
    getAllFaqs({type: 'video'}).then((result) => {
      setShowPleaseWaitDialog(false);
      setFaqs(result);
      setShowFaqs(result);
    });
  }, []);
  const [videoPlayer, setVideoPlayer] = useState(null);
  const renderVideos = ({item}) => {
    const {url, title} = item;

    console.log({url});
    return (
      <View style={styles.faqDataWrapper}>
        <VideoBox faq={item} />
      </View>
    );
  };
  const handleSearch = (words) => {
    if (_.isEmpty(words)) setShowFaqs(faqs);
    const searchResult = faqs.filter((faq) =>
      _.toLower(faq.title).includes(_.toLower(words)),
    );
    setShowFaqs(searchResult);
  };
  return (
    <View style={[globalStyles.rootNoPadding]}>
      <View style={styles.contentWrapper}>
        <View>
          <View style={styles.searchBarWrapper}>
            <SearchBar
              placeholder="Search Answers"
              onChangeText={(value) => handleSearch(value)}
            />
          </View>
          <View style={styles.divider} />
        </View>
        <View style={styles.faqListWrapper}>
          <FlatList
            data={showFaqs}
            renderItem={renderVideos}
            keyExtractor={(item, index) => `${item.title}-${index}`}
          />
          <PleaseWaitDialog showDialog={showPleaseWaitDialog} />
        </View>
      </View>
    </View>
  );
};

export default FaqVideos;
