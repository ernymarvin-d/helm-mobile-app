import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {
  globalStyles,
  calculatorLayoutStyles,
  modalStyles,
} from '../../assets/styles/globalStyles';
import CalculatorEntryBox from '../molecules/CalculatorEntryBox';
import {
  toStringMoneyValue,
  decimalValuetoPercentage,
  percentageToDecimalValue,
} from '../../utils/OptionsCalculatorUtil';
import {
  updateCalculatorSettingsOption,
  updateCalupdateDealsSettingsOption,
} from '../../state-management/actions/AuthActions';
import _ from 'lodash';
import {connect} from 'react-redux';
import {updateUserDefaultCalculatorSetting} from '../../utils/CalculatorSettingsUtil';

// downPayment: 20000
// holdingPeriod: 6
// lendingApr: 0.11
// lendingPts: 0.01
// priceDiscount: 0.7
// sellingCost: 0.08
const CashCalculator2 = ({
  style,
  route,
  navigation,
  settings,
  onDoneButtonPressed,
  updateCalculatorSettingsOption,
  updateCalupdateDealsSettingsOption,
  asModal,
  dealKey,
  user,
}) => {
  console.log('calc2', {settings, dealKey});
  const onUpdateOption = (key, value) => {
    const payload = {
      dealKey,
      settingsKey: key,
      value,
      settingsOptionKey: 'option2',
      settings,
      stateAction: {
        updateCalupdateDealsSettingsOption,
        updateCalculatorSettingsOption,
      },
      userId: user.userId,
    };
    updateUserDefaultCalculatorSetting(payload);
  };

  const downPayment = settings.downPayment.value;
  const holdingPeriod = settings.holdingPeriod.value;
  const lendingApr = settings.lendingApr.value;
  const lendingPts = settings.lendingPts.value;
  const priceDiscount = settings.priceDiscount.value;
  const sellingCost = settings.sellingCost.value;
  const formatProfitMargin = settings.formatProfitMargin.value;
  const formatReturnOnCachInvestment =
    settings.formatReturnOnCachInvestment.value;

  const renderDoneButton = () => {
    if (asModal) {
      return (
        <TouchableOpacity onPress={onDoneButtonPressed}>
          <Text style={[globalStyles.p2, {alignSelf: 'flex-end'}]}>Done</Text>
        </TouchableOpacity>
      );
    } else {
      return <View />;
    }
  };
  return (
    <View style={[calculatorLayoutStyles.calculatorBox, style]}>
      {renderDoneButton()}
      <View style={calculatorLayoutStyles.calculatorHeaderWrapper}>
        <Text style={[globalStyles.p2, calculatorLayoutStyles.calculatorName]}>
          Calculator 2
        </Text>
        <Text style={[globalStyles.p1, calculatorLayoutStyles.calculatorTitle]}>
          Cash Offer for Your Fix/Flip
        </Text>
      </View>
      <CalculatorEntryBox
        value={`${decimalValuetoPercentage(priceDiscount)}%`}
        isPercentage
        title="Preferred discount rate off of market value (standard)"
        onUpdate={(value) =>
          onUpdateOption('priceDiscount', percentageToDecimalValue(value))
        }
      />
      <CalculatorEntryBox
        value={toStringMoneyValue(downPayment)}
        title="Preferred down payment"
        onUpdate={(value) => {
          console.log('www', {value});
          onUpdateOption('downPayment', value);
        }}
      />
      <CalculatorEntryBox
        value={`${decimalValuetoPercentage(lendingApr)}%`}
        isPercentage
        onUpdate={(value) =>
          onUpdateOption('lendingApr', percentageToDecimalValue(value))
        }
        title="Preferred lending APR"
      />
      <CalculatorEntryBox
        value={`${decimalValuetoPercentage(lendingPts)}%`}
        isPercentage
        onUpdate={(value) =>
          onUpdateOption('lendingPts', percentageToDecimalValue(value))
        }
        title="Preferred lending Pts"
      />
      <CalculatorEntryBox
        value={`${holdingPeriod} Months`}
        title="Preferred holding period"
        onUpdate={(value) => onUpdateOption('holdingPeriod', value)}
      />
      <CalculatorEntryBox
        value={`${decimalValuetoPercentage(sellingCost)}%`}
        isPercentage
        onUpdate={(value) =>
          onUpdateOption('sellingCost', percentageToDecimalValue(value))
        }
        title="Preferred selling cost"
      />
      <CalculatorEntryBox
        value={`${decimalValuetoPercentage(formatProfitMargin)}%`}
        isPercentage
        onUpdate={(value) =>
          onUpdateOption('formatProfitMargin', percentageToDecimalValue(value))
        }
        title="Profit Margin (Conditional Formating)"
      />
      <CalculatorEntryBox
        value={`${decimalValuetoPercentage(formatReturnOnCachInvestment)}%`}
        isPercentage
        onUpdate={(value) =>
          onUpdateOption(
            'formatReturnOnCachInvestment',
            percentageToDecimalValue(value),
          )
        }
        title="Return on Cash Investment (%) (Conditional Formating)"
      />
    </View>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateCalculatorSettingsOption: (option2) =>
      dispatch(updateCalculatorSettingsOption(option2)),
    updateCalupdateDealsSettingsOption: (settings) =>
      dispatch(updateCalupdateDealsSettingsOption(settings)),
  };
};

const mapStateToProps = (state) => {
  return {
    settings: state.auth.user.calculatorGeneralSettings.option2,
    user: state.auth.user,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CashCalculator2);
