import React, {useState} from 'react';
import {View, Text} from 'react-native';
import GrayRoundedBox from '../atoms/GrayRoundedBox';
import LabeledInputText from '../atoms/LabeledInputText';
import EStyleSheet from 'react-native-extended-stylesheet';
import DealsOptionHeader from '../molecules/DealsOptionHeader';
import {GREY, TEXT_SECONDARY} from '../../assets/colors';
import {globalStyles} from '../../assets/styles/globalStyles';
import {connect} from 'react-redux';
import {
  decimalValuetoPercentage,
  toStringMoneyValue,
  calculateOption6,
} from '../../utils/OptionsCalculatorUtil';
import CashCalculator6 from './CashCalculator6';
import CalculatorSettingsModalWrapper from '../atoms/CalculatorSettingsModalWrapper';
import {updateIndividualDealOptions} from './../../state-management/actions/DealsAction';

const styles = EStyleSheet.create({
  formWrapper: {marginTop: '20rem'},
});

const DealsOption6 = ({
  route,
  navigation,
  dealOption6,
  settings,
  updateDealOptions,
  propertyInformation,
  dealKey,
}) => {
  console.log({dealOption6, settings});
  const {
    leaseOptionRentToSeller,
    monthlyCashFlow,
    priceToSeller,
    priceToTenant,
    totalProfit,
  } = dealOption6;

  const numYears = settings.numYears.value;
  const optionDepositToSeller = settings.optionDepositToSeller.value;
  const priceDiscount = settings.priceDiscount.value;
  const minOptionDepositFromTenantBuyer =
    settings.minOptionDepositFromTenantBuyer.value;

  const [showOptionSettings, setShowOptionSettings] = useState(false);
  const onCalculatorSettingsDoneButtonClick = () => {
    const dealOptionValue = calculateOption6({
      settings,
      propertyInformation,
    });
    const dealsOptionKey = 'dealOption6';
    updateDealOptions({
      dealsOptionKey,
      dealOptionValue,
    });
    setShowOptionSettings(false);
  };
  const renderSettingsModal = () => (
    <CalculatorSettingsModalWrapper
      visible={showOptionSettings}
      onBackdropPress={() => {}}>
      <CashCalculator6
        dealKey={dealKey}
        asModal
        onDoneButtonPressed={onCalculatorSettingsDoneButtonClick}
      />
    </CalculatorSettingsModalWrapper>
  );
  return (
    <GrayRoundedBox>
      <DealsOptionHeader
        title="Sandwich Lease Option"
        name="Option 6"
        settings={settings}
        dealOptionValue={dealOption6}
        onGeneralSettingsButtonClick={() => setShowOptionSettings(true)}
      />
      <View style={styles.formWrapper}>
        <LabeledInputText
          label="Price Discount"
          value={`${decimalValuetoPercentage(priceDiscount)}%`}
          editable={false}
          textInputStyle={globalStyles.textInputOrange}
        />
        <LabeledInputText
          label="Price to Seller"
          value={`${toStringMoneyValue(priceToSeller.value)}`}
          editable={false}
          textInputStyle={globalStyles.textInputGray}
        />
        <LabeledInputText
          label="Price to Tenant - Buyer "
          value={`${toStringMoneyValue(priceToTenant.value)}`}
          editable={false}
          textInputStyle={globalStyles.textInputGray}
        />
        <LabeledInputText
          label="Lease Option Rent To Seller"
          value={`${toStringMoneyValue(leaseOptionRentToSeller.value)}`}
          editable={false}
          textInputStyle={globalStyles.textInputGray}
        />
        <LabeledInputText
          label="Option Deposit to Seller"
          value={toStringMoneyValue(optionDepositToSeller)}
          editable={false}
          textInputStyle={globalStyles.textInputOrange}
        />
        <LabeledInputText
          label="Number of Years"
          value={`${numYears}`}
          editable={false}
          textInputStyle={globalStyles.textInputOrange}
        />
        <LabeledInputText
          label="Min Option Deposit from Tenant Buyer"
          value={toStringMoneyValue(minOptionDepositFromTenantBuyer)}
          editable={false}
          textInputStyle={
            minOptionDepositFromTenantBuyer >=
            settings.formatMinOptionDepositFromTenantBuyer.value
              ? globalStyles.textInputGreen
              : globalStyles.textInputRed
          }
        />
        <LabeledInputText
          label="Monthly Cash Flow "
          value={`${toStringMoneyValue(monthlyCashFlow.value)}`}
          editable={false}
          textInputStyle={globalStyles.textInputGray}
        />
        <LabeledInputText
          label={`Total Profit (${numYears} years)`}
          value={`${toStringMoneyValue(totalProfit.value)}`}
          editable={false}
          textInputStyle={
            totalProfit.value >= settings.formatTotalProfit.value
              ? globalStyles.textInputGreen
              : globalStyles.textInputRed
          }
        />
      </View>
      {renderSettingsModal()}
    </GrayRoundedBox>
  );
};

const mapStateToProps = (state) => {
  return {
    dealOption6: state.dealsCalculator.dealOptions.dealOption6.options,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateDealOptions: (key, value) =>
      dispatch(updateIndividualDealOptions(key, value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DealsOption6);
