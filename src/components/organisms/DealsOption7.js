import React, {useState} from 'react';
import {View, Text} from 'react-native';
import GrayRoundedBox from '../atoms/GrayRoundedBox';
import LabeledInputText from '../atoms/LabeledInputText';
import EStyleSheet from 'react-native-extended-stylesheet';
import DealsOptionHeader from '../molecules/DealsOptionHeader';
import {GREY, TEXT_SECONDARY} from '../../assets/colors';
import {globalStyles} from '../../assets/styles/globalStyles';
import {connect} from 'react-redux';
import {
  toStringMoneyValue,
  decimalValuetoPercentage,
  calculateOption7,
} from '../../utils/OptionsCalculatorUtil';
import CashCalculator7 from './CashCalculator7';
import CalculatorSettingsModalWrapper from '../atoms/CalculatorSettingsModalWrapper';
import {updateIndividualDealOptions} from './../../state-management/actions/DealsAction';

const styles = EStyleSheet.create({
  formWrapper: {marginTop: '20rem'},
});

const DealsOption7 = ({
  route,
  navigation,
  dealOption7,
  settings,
  updateDealOptions,
  propertyInformation,
  dealKey,
}) => {
  console.log({dealOption7, settings});
  const [showOptionSettings, setShowOptionSettings] = useState(false);
  const {
    leaseOptionRentToSeller,
    priceToSeller,
    priceToTenant,
    totalProfit,
  } = dealOption7;
  const minOptionDepositFromTenantBuyer =
    settings.minOptionDepositFromTenantBuyer.value;
  const numYears = settings.numYears.value;
  const optionDepositToSeller = settings.optionDepositToSeller.value;
  const priceDiscount = settings.priceDiscount.value;

  const onCalculatorSettingsDoneButtonClick = () => {
    const dealOptionValue = calculateOption7({
      settings,
      propertyInformation,
    });
    const dealsOptionKey = 'dealOption7';
    updateDealOptions({
      dealsOptionKey,
      dealOptionValue,
    });
    setShowOptionSettings(false);
  };

  const renderSettingsModal = () => (
    <CalculatorSettingsModalWrapper
      visible={showOptionSettings}
      onBackdropPress={() => {}}>
      <CashCalculator7
        asModal
        dealKey={dealKey}
        onDoneButtonPressed={onCalculatorSettingsDoneButtonClick}
      />
    </CalculatorSettingsModalWrapper>
  );
  return (
    <GrayRoundedBox>
      <DealsOptionHeader
        title="Wholesale Lease Option"
        name="Option 7"
        settings={settings}
        dealOptionValue={dealOption7}
        onGeneralSettingsButtonClick={() => setShowOptionSettings(true)}
      />
      <View style={styles.formWrapper}>
        <LabeledInputText
          label="Price Discount"
          value={`${decimalValuetoPercentage(priceDiscount)}%`}
          editable={false}
          textInputStyle={globalStyles.textInputOrange}
        />
        <LabeledInputText
          label="Price to Seller "
          value={toStringMoneyValue(priceToSeller.value)}
          editable={false}
          textInputStyle={globalStyles.textInputGray}
        />
        <LabeledInputText
          label="Price to Tenant - Buyer "
          value={toStringMoneyValue(priceToTenant.value)}
          editable={false}
          textInputStyle={globalStyles.textInputGray}
        />
        <LabeledInputText
          label="Lease Option Rent To Seller"
          value={toStringMoneyValue(leaseOptionRentToSeller.value)}
          editable={false}
          textInputStyle={globalStyles.textInputGray}
        />
        <LabeledInputText
          label="Option Deposit to Seller"
          value={toStringMoneyValue(optionDepositToSeller)}
          editable={false}
          textInputStyle={globalStyles.textInputOrange}
        />
        <LabeledInputText
          label="Number of Years"
          value={`${numYears}`}
          editable={false}
          textInputStyle={globalStyles.textInputOrange}
        />
        <LabeledInputText
          label="Min Option Deposit from Tenant Buyer"
          value={toStringMoneyValue(minOptionDepositFromTenantBuyer)}
          editable={false}
          textInputStyle={
            minOptionDepositFromTenantBuyer >=
            settings.formatMinOptionDepositFromTenantBuyer.value
              ? globalStyles.textInputGreen
              : globalStyles.textInputRed
          }
        />
        <LabeledInputText
          label="Total Profit "
          value={toStringMoneyValue(totalProfit.value)}
          editable={false}
          textInputStyle={globalStyles.textInputGray}
        />
      </View>
      {renderSettingsModal()}
    </GrayRoundedBox>
  );
};

const mapStateToProps = (state) => {
  return {
    dealOption7: state.dealsCalculator.dealOptions.dealOption7.options,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateDealOptions: (key, value) =>
      dispatch(updateIndividualDealOptions(key, value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DealsOption7);
