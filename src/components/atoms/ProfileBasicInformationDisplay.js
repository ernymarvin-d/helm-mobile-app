import React, {useState} from 'react';
import {View, Text, Image, TouchableOpacity, TextInput} from 'react-native';
import editIcon from '../../assets/images/ic-edit.png';
import EStyleSheet from 'react-native-extended-stylesheet';
import {globalStyles} from '../../assets/styles/globalStyles';
import {GREY, BLUE} from '../../assets/colors';
import {MARGIN_DEFAULT} from '../../assets/dimens';
import BasicInfoInputSheet from '../molecules/BasicInfoInputSheet';
import RBSheet from 'react-native-raw-bottom-sheet';

const styles = EStyleSheet.create({
  root: {flexDirection: 'column', marginTop: MARGIN_DEFAULT},
  label: {
    textTransform: 'uppercase',
    fontWeight: '500',
    fontSize: '10rem',
  },
  textIconWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: '4rem',
    borderBottomWidth: '1rem',
    borderBottomColor: GREY[4],
  },
  value: {
    flex: 1,
    fontWeight: '500',
    fontFamily: 'Montserrat-Regular',
    color: '#505152',
    minHeight: '28rem',
  },
  icon: {width: '20rem', height: '20rem', tintColor: BLUE},
  iconContainer: {
    width: '35rem',
    height: '35rem',
    backgroundColor: '#F2F4F7',
    padding: '5rem',
    borderRadius: '20rem',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const ProfileBasicInformationDisplay = ({
  label,
  value,
  keyboardType,
  onSave,
  readOnly,
  onPress,
}) => {
  const [rbSheetRef, setRbSheetRef] = useState(null);

  const handleOnCancelled = () => rbSheetRef.close();
  const handleOnSaved = (newValue) => {
    rbSheetRef.close();
    onSave(newValue);
  };

  const handleOnPressEditIcon = () => {
    showBottomInputDialog();
  };

  const showBottomInputDialog = () => {
    if (onPress) {
      onPress();
      return;
    }
    if (!readOnly) {
      rbSheetRef.open();
    }
  };

  return (
    <View style={styles.root}>
      <TouchableOpacity onPress={handleOnPressEditIcon}>
        <View style={styles.textIconWrapper}>
          <View>
            <Text style={[globalStyles.p2, styles.label]}>{label}</Text>
            <Text style={[globalStyles.p1, styles.value]}>{value}</Text>
          </View>
          {!readOnly ? (
            <View style={styles.iconContainer}>
              <Image
                source={editIcon}
                resizeMethod="resize"
                resizeMode="contain"
                style={styles.icon}
              />
            </View>
          ) : null}
        </View>
      </TouchableOpacity>
      <RBSheet
        ref={(ref) => setRbSheetRef(ref)}
        height={200}
        duration={250}
        keyboardAvoidingViewEnabled={false}
        customStyles={{
          container: {
            justifyContent: 'center',
            alignItems: 'flex-start',
          },
        }}>
        <BasicInfoInputSheet
          label={label}
          value={value}
          onCancellPressed={handleOnCancelled}
          onSavePressed={handleOnSaved}
          keyboardType={keyboardType}
        />
      </RBSheet>
    </View>
  );
};

export default ProfileBasicInformationDisplay;
