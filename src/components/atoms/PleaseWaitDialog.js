import React from 'react';
import Dialog from 'react-native-dialog';
import {View, Text, ActivityIndicator} from 'react-native';
import {globalStyles} from '../../assets/styles/globalStyles';
import EStyleSheet from 'react-native-extended-stylesheet';
import {ORANGE} from '../../assets/colors';

const styles = EStyleSheet.create({
  wrapper: {
    width: '100%',
    justifyContent: 'center',
    alignSelf: 'center',
    marginBottom: '16rem',
  },
});

const PleaseWaitDialog = ({showDialog}) => {
  return (
    <Dialog.Container visible={showDialog}>
      <Dialog.Title>Please wait...</Dialog.Title>
      <View style={styles.wrapper}>
        <ActivityIndicator size="large" color={ORANGE} />
      </View>
    </Dialog.Container>
  );
};

export default PleaseWaitDialog;
