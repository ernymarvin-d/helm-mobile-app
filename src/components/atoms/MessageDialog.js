import React from 'react';
import {View} from 'react-native';
import Dialog from 'react-native-dialog';

const MessageDialog = ({
  title,
  description,
  onPressPositiveButton,
  positiveButtonText,
  visible,
}) => {
  return (
    <View>
      <Dialog.Container visible={visible}>
        <Dialog.Title>{title}</Dialog.Title>
        <Dialog.Description>{description}</Dialog.Description>
        <Dialog.Button
          onPress={onPressPositiveButton}
          label={positiveButtonText}
        />
      </Dialog.Container>
    </View>
  );
};

export default MessageDialog;
