import React, {Children} from 'react';
import {View, Text, Image} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import {
  PADDING_DEFAULT,
  MARGIN_DEFAULT,
  DEALS_BOX_BORDER_RADIUS,
} from '../../assets/dimens';
import {BOX_BORDER_COLOR_2, BOX_BACKGROUND_COLOR_2} from '../../assets/colors';
import {globalStyles} from '../../assets/styles/globalStyles';
import playIcon from '../../assets/images/play-button.png';
import Video from 'react-native-video';
import {useState} from 'react';
import storage from '@react-native-firebase/storage';
import {useEffect} from 'react';

const styles = EStyleSheet.create({
  container: {
    backgroundColor: BOX_BACKGROUND_COLOR_2,
    borderRadius: DEALS_BOX_BORDER_RADIUS,
    borderWidth: '1rem',
    borderColor: BOX_BORDER_COLOR_2,
  },
  title: {
    fontWeight: 'bold',
    padding: PADDING_DEFAULT,
  },
  videoWrapper: {
    backgroundColor: '#cccccc',
    width: '100%',
    height: '150rem',
  },
  playButton: {
    width: '100%',
    height: '100%',
    tintColor: 'white',
    // backgroundColor: '#cc',
  },
  backgroundVideo: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    width: '100%',
    height: '100%',
    backgroundColor: 'black',
  },
});

const VideoBox = ({children, style, faq}) => {
  useEffect(() => {
    getDownloadUrl();
  }, []);
  const {title, url} = faq;
  const customStyle = style ? style : {};
  const [videoPlayer, setVideoPlayer] = useState(null);
  const [videoUrl, setVideoUrl] = useState('');
  const getDownloadUrl = async () => {
    const downloadUrl = await storage().ref(url).getDownloadURL();
    console.log('get dl url', {downloadUrl}, typeof downloadUrl);

    setVideoUrl(downloadUrl);
  };
  return (
    <View style={[styles.container, customStyle]}>
      <View style={styles.videoWrapper}>
        <Video
          source={{
            uri: videoUrl,
          }} // Can be a URL or a local file.
          ref={(ref) => setVideoPlayer(ref)} // Store reference
          //   onBuffer={this.onBuffer} // Callback when remote video is buffering
          //   onError={this.videoError} // Callback when video cannot be loade
          style={styles.backgroundVideo}
          controls={true}
          paused={true}
          onLoad={() => {
            videoPlayer.seek(0);
          }}
        />
      </View>
      <Text style={[globalStyles.p2, styles.title]}>{title}</Text>
    </View>
  );
};

export default VideoBox;
