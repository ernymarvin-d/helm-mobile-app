import * as React from 'react';
import {View, Text, Image} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {BUTTON_HEIGHT_DEFAULT} from '../../assets/dimens';
import {ORANGE} from '../../assets/colors';
import EStyleSheet from 'react-native-extended-stylesheet';

const IntroSliderButton = ({icon, backgroundColor, tintColor, onPress}) => {
  const buttonColor = backgroundColor ? backgroundColor : ORANGE;
  const iconColor = tintColor ? tintColor : 'white';

  const styles = EStyleSheet.create({
    btn: {
      borderRadius: 10,
      height: BUTTON_HEIGHT_DEFAULT,
      width: BUTTON_HEIGHT_DEFAULT,
      backgroundColor: buttonColor,
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
    icon: {width: '40%', height: '40%', tintColor: iconColor},
  });

  return (
    <TouchableOpacity style={styles.btn} onPress={onPress}>
      <Image source={icon} style={styles.icon} />
    </TouchableOpacity>
  );
};

export default IntroSliderButton;
