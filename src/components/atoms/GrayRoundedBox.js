import React, {Children} from 'react';
import {View} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import {
  PADDING_DEFAULT,
  MARGIN_DEFAULT,
  DEALS_BOX_BORDER_RADIUS,
} from '../../assets/dimens';
import {BOX_BORDER_COLOR_2, BOX_BACKGROUND_COLOR_2} from '../../assets/colors';

const styles = EStyleSheet.create({
  container: {
    backgroundColor: BOX_BACKGROUND_COLOR_2,
    borderRadius: DEALS_BOX_BORDER_RADIUS,
    borderWidth: '1rem',
    borderColor: BOX_BORDER_COLOR_2,
    padding: PADDING_DEFAULT,
  },
});

const GrayRoundedBox = ({children, style}) => {
  const customStyle = style ? style : {};
  return <View style={[styles.container, customStyle]}>{children}</View>;
};

export default GrayRoundedBox;
