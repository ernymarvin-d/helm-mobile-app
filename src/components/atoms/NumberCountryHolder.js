import * as React from 'react';
import {TextInput, View, Image, Text} from 'react-native';
import {INPUT_TEXT_HEIGHT_DEFAULT, PADDING_DEFAULT} from '../../assets/dimens';
import {GREY, BLUE} from '../../assets/colors';
import EStyleSheet from 'react-native-extended-stylesheet';
import downArrowIcon from './../../assets/images/down-arrow-icon.png';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {globalStyles} from '../../assets/styles/globalStyles';
import _ from 'lodash';

export default NumberCountryHolder = ({name, onPress, country, isValid}) => {
  let pickerValue = 'Select A Country';

  if (!_.isEmpty(country)) {
    pickerValue = `${country.cca2}`;
    pickerValue = !_.isEmpty(country.callingCode)
      ? pickerValue + ` (+${country.callingCode[0]})`
      : pickerValue;
  }

  return (
    <TouchableOpacity
      style={[styles.wrapper, !isValid ? styles.inValidSelectedCountry : {}]}
      onPress={onPress}>
      <Text style={[globalStyles.p1, styles.pickerIos]}>{pickerValue}</Text>
      <Image style={styles.downArrowIcon} source={downArrowIcon} />
    </TouchableOpacity>
  );
};

const styles = EStyleSheet.create({
  wrapper: {
    width: '100%',
    flexDirection: 'row',
    height: INPUT_TEXT_HEIGHT_DEFAULT,
    borderBottomWidth: 1,
    borderBottomColor: GREY[4],
    padding: 0,
    margin: 0,
    backgroundColor: 'transparent',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  inValidSelectedCountry: {
    borderBottomColor: 'red',
  },
  pickerIos: {
    flex: 1,
    paddingLeft: PADDING_DEFAULT,
    color: GREY[2],
  },
  downArrowIcon: {
    width: '12rem',
    height: '12rem',
    marginRight: '16rem',
    tintColor: GREY[2],
  },
});
