import React from 'react';
import {Text} from 'react-native';
import GrayRoundedBox from './GrayRoundedBox';
import {globalStyles} from '../../assets/styles/globalStyles';
import EStyleSheet from 'react-native-extended-stylesheet';
import {BLUE} from '../../assets/colors';

const styles = EStyleSheet.create({
  messageBox: {
    borderRadius: '4rem',
    borderLeftWidth: '4rem',
    borderLeftColor: BLUE,
  },
  message: {marginBottom: '22rem'},
  success: {backgroundColor: '#bdfcce', borderColor: 'green'},
});

const Note = ({message, success, footerNote = <></>}) => {
  return (
    <GrayRoundedBox style={[styles.messageBox, success && styles.success]}>
      <Text style={[globalStyles.p1, styles.message]}>{message}</Text>
      {footerNote}
    </GrayRoundedBox>
  );
};

export default Note;
