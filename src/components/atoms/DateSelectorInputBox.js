import React, {useState} from 'react';
import {TextInput, View, Image, TouchableOpacity, Text} from 'react-native';
import {GREY, BLUE} from '../../assets/colors';
import EStyleSheet from 'react-native-extended-stylesheet';
import calendarIcon from './../../assets/images/ic-calendar.png';
import {globalStyles} from '../../assets/styles/globalStyles';

const styles = EStyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  textBox: {
    flex: 1,
  },
  icon: {
    width: '18rem',
    height: '18rem',
    tintColor: BLUE,
  },
  root: {
    flexDirection: 'column',
    marginVertical: '10rem',
    padding: 0,
    width: '100%',
  },
});

const DateSelectorInputBox = ({name, placeholder, onPress, label}) => {
  const onPressDateIcon = () => console.log('show date dialog');
  const renderIcon = () => {
    return (
      <TouchableOpacity onPress={onPressDateIcon}>
        <Image style={styles.icon} source={calendarIcon} />
      </TouchableOpacity>
    );
  };
  return (
    <View style={styles.root}>
      <Text style={[globalStyles.inputTextLabel]}>{label}</Text>
      <View style={[globalStyles.inputText, styles.wrapper]} onPress={onPress}>
        <TextInput
          style={[styles.textBox]}
          textContentType="none"
          name={name}
          placeholder={placeholder}
        />
        {renderIcon()}
      </View>
    </View>
  );
};

export default DateSelectorInputBox;
