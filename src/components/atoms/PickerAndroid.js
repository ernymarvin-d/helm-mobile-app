import * as React from 'react';
import {Picker, View} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import {GREY} from '../../assets/colors';

const styles = EStyleSheet.create({
  picker: {
    height: 44,
    width: '100%',
  },
  pickerWrapper: {
    borderBottomWidth: 2,
    borderBottomColor: GREY[4],
  },
});

const PickerAndroid = () => {
  return (
    <View style={styles.pickerWrapper}>
      <Picker
        selectedValue={{}}
        style={styles.picker}
        onValueChange={(itemValue, itemIndex) => console.log('value selected')}>
        <Picker.Item label="USA" value="java" />
        <Picker.Item label="Canda" value="js" />
      </Picker>
    </View>
  );
};

export default PickerAndroid;
