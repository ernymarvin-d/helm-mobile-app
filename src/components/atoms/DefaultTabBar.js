import React from 'react';
import {TabBar} from 'react-native-tab-view';
import {ORANGE, BLUE} from '../../assets/colors';

const DefaultTabBar = (props) => {
  return (
    <TabBar
      {...props}
      indicatorStyle={{backgroundColor: ORANGE}}
      style={{backgroundColor: BLUE}}
      onTabPress={props.onTabPress}
    />
  );
};

export default DefaultTabBar;
