import React, {useState} from 'react';
import {View, Alert, Platform} from 'react-native';
import facebookIcon from '../../assets/images/facebook.png';
import IconButton from './IconButton';
import {FB_BUTTON_COLOR} from '../../assets/colors';
import auth from '@react-native-firebase/auth';
import {LoginManager, AccessToken} from 'react-native-fbsdk';
import PleaseWaitDialog from './PleaseWaitDialog';

const FacebookIconButton = ({onPress}) => {
  const [showPleaseWaitDialog, setShowPleaseWaitDialog] = useState(false);
  const onFacebookButtonPress = async () => {
    try {
      console.log('Attempt login with permissions');
      // Attempt login with permissions
      setShowPleaseWaitDialog(true);
      const result = await LoginManager.logInWithPermissions([
        'public_profile',
        'email',
      ]);

      console.log('Got Result', result);
      if (result.isCancelled) {
        setShowPleaseWaitDialog(false);
        // throw 'User cancelled the login process';
      }

      // Once signed in, get the users AccesToken
      const data = await AccessToken.getCurrentAccessToken();
      console.log('got data access token', data);
      if (!data) {
        throw 'Something went wrong obtaining access token';
      }

      console.log('Create a Firebase credential with the AccessToken');
      // Create a Firebase credential with the AccessToken
      const facebookCredential = auth.FacebookAuthProvider.credential(
        data.accessToken,
      );

      console.log('Sign-in the user with the credential');
      // Sign-in the user with the credential
      await auth().signInWithCredential(facebookCredential);
      setShowPleaseWaitDialog(false);
      console.log('FB logged');
    } catch (error) {
      console.log('Facebook login error', error);
    }
  };

  return (
    <View>
      <IconButton
        icon={facebookIcon}
        bgColor={FB_BUTTON_COLOR}
        tintColor={'white'}
        onPress={onFacebookButtonPress}
      />
      <PleaseWaitDialog showDialog={showPleaseWaitDialog} />
    </View>
  );
};

export default FacebookIconButton;
