import React from 'react';
import {View, Image} from 'react-native';
import searchIcon from '../../assets/images/ic-search.png';
import {TextInput} from 'react-native-gesture-handler';
import EStyleSheet from 'react-native-extended-stylesheet';
import {globalStyles} from '../../assets/styles/globalStyles';
import {GREY} from '../../assets/colors';
import {PADDING_DEFAULT} from '../../assets/dimens';

const styles = EStyleSheet.create({
  root: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  searchIcon: {
    width: '20rem',
    height: '20rem',
    tintColor: GREY[2],
  },
  textInput: {
    paddingHorizontal: PADDING_DEFAULT,
    flex: 1,
  },
});

const SearchBar = ({placeholder, onChangeText}) => {
  return (
    <View style={[styles.root, globalStyles.inputText]}>
      <Image
        style={[styles.searchIcon]}
        source={searchIcon}
        resizeMode="contain"
      />
      <TextInput
        placeholder={placeholder}
        style={[styles.textInput, globalStyles.p1]}
        onChangeText={onChangeText}
      />
    </View>
  );
};

export default SearchBar;
