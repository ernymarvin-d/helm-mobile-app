import * as React from 'react';
import {Text, View, TouchableOpacity, Image} from 'react-native';
import {globalStyles} from '../../assets/styles/globalStyles';
import {GREY, ORANGE, BLUE} from '../../assets/colors';
import EStyleSheet from 'react-native-extended-stylesheet';
import downloadIcon from '../../assets/images/ic-download.png';
import {MARGIN_SMALL} from '../../assets/dimens';

const TextButton = ({
  text,
  type,
  onPress,
  width,
  textStyle,
  leftIcon,
  leftIconColor,
  buttonCustomStyle,
  style,
}) => {
  const customTextStyle = textStyle ? textStyle : {};
  let textColor = BLUE;
  let buttonColor;
  switch (type) {
    case 'primary':
      buttonColor = ORANGE;
      textColor = 'white';
      break;
    case 'secondary':
      buttonColor = GREY[4];
      break;
    case 'link':
      buttonColor = 'transparent';
      break;
    default:
      buttonColor = ORANGE;
      textColor = 'white';
      break;
  }

  const styles = EStyleSheet.create({
    buttonStyle: {
      backgroundColor: buttonColor,
      textAlign: 'center',
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'row',
    },
    text: {
      color: textColor,
    },
    leftIcon: {
      width: '12rem',
      height: '12rem',
      marginRight: MARGIN_SMALL,
    },
  });

  const renderLeftIcon = () => {
    const tintColor = leftIconColor ? leftIconColor : ORANGE;
    return leftIcon ? (
      <Image
        style={[styles.leftIcon, {tintColor: tintColor}]}
        source={leftIcon}
        resizeMethod="resize"
        resizeMode="contain"
      />
    ) : (
      <View />
    );
  };

  return (
    <View style={[{width: width}, style ? style : {}]}>
      <TouchableOpacity
        style={[
          globalStyles.btnLarge,
          styles.buttonStyle,
          buttonCustomStyle ? buttonCustomStyle : {},
        ]}
        onPress={onPress}>
        {renderLeftIcon()}
        <Text style={[globalStyles.btnText, styles.text, customTextStyle]}>
          {text}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default TextButton;
