import React, {useState} from 'react';
import {View, Text, TouchableOpacity, ScrollView} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import {GREY} from '../../assets/colors';
import {PADDING_DEFAULT, MARGIN_SMALL} from '../../assets/dimens';
import {globalStyles, modalStyles} from '../../assets/styles/globalStyles';
import CalculatorGradientProgress from '../atoms/CalculatorGradientProgress';
import Dialog from 'react-native-dialog';
import {
  percentageToDecimalValue,
  isValidNumber,
} from '../../utils/OptionsCalculatorUtil';
import Modal from 'react-native-modal';

const styles = EStyleSheet.create({
  formWrapper: {marginTop: '20rem'},
  root: {
    backgroundColor: '#DCDCDC',
  },
  modalScrollContainer: {
    justifyContent: 'center',
    flexGrow: 1,
  },
  viewWrapper: {
    height: '80%',
  },
});

const CalculatorSettingsModalWrapper = ({
  visible,
  onBackdropPress,
  children,
}) => {
  return (
    <Modal
      style={[modalStyles.root, styles.root]}
      visible={visible}
      animationType={'slide'}
      onRequestClose={() => console.log('request close')}
      transparent={true}
      onBackdropPress={onBackdropPress}>
      <View style={[styles.viewWrapper]}>
        <ScrollView
          style={modalStyles.asModal}
          contentContainerStyle={[styles.modalScrollContainer]}>
          {children}
        </ScrollView>
      </View>
    </Modal>
  );
};

export default CalculatorSettingsModalWrapper;
