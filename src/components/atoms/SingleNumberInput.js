import React, {forwardRef} from 'react';
import {TextInput} from 'react-native-gesture-handler';
import EStyleSheet from 'react-native-extended-stylesheet';
import {globalStyles} from '../../assets/styles/globalStyles';

const styles = EStyleSheet.create({
  box: {
    // height: '53rem',
    // width: '60rem',
    height: '35rem',
    width: '40rem',
    padding: 0,
    margin: 0,
    fontFamily: 'Montserrat-Bold',
    fontSize: '26rem',
    paddingHorizontal: 0,
    paddingLeft: '12rem',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
});

const SingleNumberInput = forwardRef(({textInputProps}, ref) => {
  return (
    <TextInput
      style={[globalStyles.inputText, globalStyles.h2, styles.box]}
      placeholder="-"
      numberOfLines={1}
      maxLength={1}
      keyboardType="numeric"
      ref={ref}
      {...textInputProps}
      selectTextOnFocus
    />
  );
});

export default SingleNumberInput;
