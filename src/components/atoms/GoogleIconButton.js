import React, {useState} from 'react';
import {View} from 'react-native';
import googleIcon from '../../assets/images/google.png';
import IconButton from './IconButton';
import auth from '@react-native-firebase/auth';
import {GoogleSignin, statusCodes} from '@react-native-community/google-signin';
import {GOOGLE_BUTTON_COLOR} from '../../assets/colors';
import {Alert} from 'react-native';
import PleaseWaitDialog from './PleaseWaitDialog';

const LoginWithGoogleButton = ({onPress}) => {
  const [showPleaseWaitDialog, setShowPleaseWaitDialog] = useState(false);
  const onGoogleButtonPress = async () => {
    try {
      const hasPlayService = await GoogleSignin.hasPlayServices();
      const user = await GoogleSignin.signIn();
      setShowPleaseWaitDialog(true);
      const googleCredential = auth.GoogleAuthProvider.credential(user.idToken);
      console.log('Google auth credentials 111');
      await auth().signInWithCredential(googleCredential);
      setShowPleaseWaitDialog(false);
    } catch (error) {
      setShowPleaseWaitDialog(false);
      if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        Alert.alert('Sign In', 'Play service is not available.');
      }
      console.log('Erro Error', {error}, error.message);
    }
  };

  return (
    <View>
      <IconButton
        icon={googleIcon}
        bgColor={GOOGLE_BUTTON_COLOR}
        tintColor={'white'}
        onPress={onGoogleButtonPress}
      />
      <PleaseWaitDialog showDialog={showPleaseWaitDialog} />
    </View>
  );
};

export default LoginWithGoogleButton;
