import React, {useState, useEffect, useLayoutEffect} from 'react';
import {Platform} from 'react-native';
import RNIap, {
  purchaseErrorListener,
  purchaseUpdatedListener,
  finishTransaction,
  getAvailablePurchases,
  acknowledgePurchaseAndroid,
} from 'react-native-iap';
import {addTransacction} from '../../utils/DbUtils';
import {firebase} from '@react-native-firebase/auth';
import {setPlanType} from '../../state-management/actions/PlanTypeAction';
import {connect} from 'react-redux';
import _ from 'lodash';
import {PLAN_TYPE} from '../../assets/Constants';
import {useIsFocused} from '@react-navigation/native';

const ITUNES_CONNECT_SHARED_SECRET = '0136ab8e8129431faa998ed7620ee479';
// const ITUNES_CONNECT_SHARED_SECRET = 'a4efc3cb346b4482a378261b6faf0ce7';
const IS_TEST_ENVIRONMENT = true;

const SubscriptionManager = (props) => {
  const isFocused = useIsFocused();
  useEffect(() => {
    console.log('subscription manager - RNIap run');
    // alert('subscription manager - RNIap run');
    // checkSubscriptions();
    RNIap.initConnection()
      .then((result) => {
        console.log('RNIap Connected', result);
        // alert('RNIap Connected');
        checkSubscriptions();
        purchaseErrorListener((error) => {
          console.log('purchaseErrorListener', error);
          // alert(`purchaseErrorListener 123 ${error.message}`);
        });
      })
      .catch((error) => {
        // alert(`RNIAP ERROR ${error.message}`);
        console.log('RNIAP ERROR', error);
      });
  }, []);

  const [purchaseUpdateSubscription, setPurchaseUpdateSubscription] = useState(
    null,
  );

  const setUserPlan = (productId) => {
    // alert(`setUserPlan ${productId}`);
    props.setPlanType(PLAN_TYPE.free);
    if (productId === 'helm_pro') {
      props.setPlanType(PLAN_TYPE.pro);
    }

    if (productId === 'helm_pro_plus') {
      props.setPlanType(PLAN_TYPE.proPlus);
    }
  };
  //   Clear test purchas
  //   adb shell pm clear com.android.vending
  const checkSubscriptions = () => {
    console.log('Checking subscription');
    props.setPlanType(PLAN_TYPE.free);
    getAvailablePurchases()
      .then((purchases) => {
        console.log({purchases});
        isSubscriptionActive(purchases);
      })
      .catch((error) => {
        console.log('check sub error', error);
      });

    const purchaseUpdateSubscriptionListener = purchaseUpdatedListener(
      (purchase) => {
        console.log('purchaseUpdatedListener', purchase);
        const receipt = purchase.transactionReceipt;
        if (receipt) {
          console.log('receipt OK', receipt);
          finishTransaction(purchase, false)
            .then((ackResult) => {
              console.log({ackResult});
              setUserPlan(purchase.productId);

              addTransacction({
                transaction: purchase,
                uid: firebase.auth().currentUser.uid,
              });
            })
            .catch((ackErr) => {
              console.warn('ackErr', ackErr);
            });
        } else {
          console.log('Purchase no receipt');
        }
      },
    );
    setPurchaseUpdateSubscription(purchaseUpdateSubscriptionListener);
  };
  const isSubscriptionActive = (availablePurchases) => {
    if (Platform.OS === 'ios') {
      console.log({availablePurchases});
      const sortedAvailablePurchases = availablePurchases.sort(
        (a, b) => b.transactionDate - a.transactionDate,
      );
      const latestAvailableReceipt =
        sortedAvailablePurchases[0].transactionReceipt;

      const handleDecodedReceipt = (decodedReceipt) => {
        console.log({decodedReceipt});
        // alert(JSON.stringify(decodedReceipt));
        const {latest_receipt_info: latestReceiptInfo} = decodedReceipt;
        const isSubValid = !!latestReceiptInfo.find((receipt) => {
          const expirationInMilliseconds = Number(receipt.expires_date_ms);
          const nowInMilliseconds = Date.now();
          return expirationInMilliseconds > nowInMilliseconds;
        });
        console.log('has valid sub', isSubValid);
        // alert(
        //   `is valid sub ${isSubValid} ${sortedAvailablePurchases[0].productId}`,
        // );
        props.setPlanType(PLAN_TYPE.free);
        if (isSubValid) {
          setUserPlan(sortedAvailablePurchases[0].productId);
        }
        return isSubValid;
      };

      const receiptParams = {
        'receipt-data': latestAvailableReceipt,
        password: ITUNES_CONNECT_SHARED_SECRET,
      };

      return RNIap.validateReceiptIos(receiptParams, false)
        .then(handleDecodedReceipt)
        .catch((error) => {
          return RNIap.validateReceiptIos(receiptParams, true)
            .then(handleDecodedReceipt)
            .catch(() => false);
        });
    }
    if (Platform.OS === 'android') {
      const sortedAvailablePurchases = availablePurchases.sort(
        (a, b) => b.transactionDate - a.transactionDate,
      );
      setUserPlan(sortedAvailablePurchases[0].productId);
      return sortedAvailablePurchases.length > 0;
    }
  };
  return <></>;
};

const mapDispatchToProps = (dispatch) => {
  return {
    setPlanType: (planType) => dispatch(setPlanType(planType)),
  };
};

const mapStateToProps = (state) => {
  return {
    planType: state.planType,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SubscriptionManager);
