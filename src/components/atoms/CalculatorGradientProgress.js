import React from 'react';
import {View, Text} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {globalStyles} from '../../assets/styles/globalStyles';
import EStyleSheet from 'react-native-extended-stylesheet';
import {BLUE} from '../../assets/colors';
import {MARGIN_DEFAULT} from '../../assets/dimens';

const styles = EStyleSheet.create({
  progressBarContent: {
    marginVertical: MARGIN_DEFAULT,
    justifyContent: 'center',
    alignItems: 'center',
  },
  entryValue: {
    color: BLUE,
    fontWeight: 'bold',
  },
  progress: {
    height: '6rem',
    borderRadius: '6rem',
  },
  progressBarWrapper: {
    width: '100%',
    backgroundColor: '#cccccc',
    borderRadius: '6rem',
  },
});

const CalculatorGradientProgress = ({value, showProgress}) => {
  const renderProgressBar = () => {
    if (showProgress) {
      const progressBarWidth = {
        width: parseInt(value) > 100 ? '100%' : value,
      };
      return (
        <View style={styles.progressBarWrapper}>
          <LinearGradient
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}
            colors={['#d46733', '#3f91D5', '#487ccc']}
            style={[styles.progress, progressBarWidth]}
          />
        </View>
      );
    }

    return null;
  };
  return (
    <View style={styles.progressBarContent}>
      <Text style={[globalStyles.h4, styles.entryValue]}>{value}</Text>
      {renderProgressBar()}
    </View>
  );
};

export default CalculatorGradientProgress;
