import * as React from 'react';
import {Image} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {globalStyles} from '../../assets/styles/globalStyles';
import EStyleSheet from 'react-native-extended-stylesheet';
import {BUTTON_PRIMARY} from '../../assets/colors';

const IconButton = ({
  icon,
  bgColor,
  tintColor,
  onPress,
  iconStyle,
  containerStyle,
}) => {
  const buttonColor = bgColor ? bgColor : BUTTON_PRIMARY;
  const iconTint = tintColor ? tintColor : 'white';

  const styles = EStyleSheet.create({
    buttonStyle: {
      backgroundColor: buttonColor,
    },
    icon: {width: '40%', height: '40%', tintColor: iconTint},
  });

  return (
    <TouchableOpacity
      style={[
        globalStyles.btnSmall,
        styles.buttonStyle,
        containerStyle ? containerStyle : {},
      ]}
      onPress={onPress}>
      <Image source={icon} style={[styles.icon, iconStyle ? iconStyle : {}]} />
    </TouchableOpacity>
  );
};

export default IconButton;
