import React from 'react';
import {View, Image} from 'react-native';
import arrorDownIcon from './../../assets/images/down-arrow.png';
import EStyleSheet from 'react-native-extended-stylesheet';
import {GREY} from '../../assets/colors';

const styles = EStyleSheet.create({
  wrapper: {width: '100%', paddingLeft: '42rem', marginVertical: '20rem'},
  icon: {width: '10rem', height: '10rem'},
  iconTop: {tintColor: GREY[2]},
  iconBottom: {marginTop: '-4rem', tintColor: GREY[1]},
});

const IconDivider = () => {
  return (
    <View style={styles.wrapper}>
      <Image source={arrorDownIcon} style={[styles.icon, styles.iconTop]} />
      <Image source={arrorDownIcon} style={[styles.icon, styles.iconBottom]} />
    </View>
  );
};

export default IconDivider;
