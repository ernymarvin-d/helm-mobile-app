import * as React from 'react';
import {View} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  progressWrapper: {width: '100%', backgroundColor: '#ccc', height: '5rem'},
  progress: {
    backgroundColor: '#38C662',
    height: '5rem',
  },
});

const ProgressBar = ({progress}) => {
  return (
    <View style={styles.progressWrapper}>
      <View style={[styles.progress, {width: progress}]} />
    </View>
  );
};

export default ProgressBar;
