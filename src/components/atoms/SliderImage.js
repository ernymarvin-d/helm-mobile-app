import React from 'react';
import {View} from 'react-native';
import {SliderBox} from 'react-native-image-slider-box';
import EStyleSheet from 'react-native-extended-stylesheet';
import {ORANGE} from '../../assets/colors';

// const defaultImages = [
//   'https://source.unsplash.com/1024x768/?house',
//   'https://source.unsplash.com/1024x768/?town',
//   'https://source.unsplash.com/1024x768/?villa',
// ];
const defaultImages = [
  require('../../assets/images/hp1.jpeg'),
  require('../../assets/images/hp2.jpeg'),
  require('../../assets/images/hp3.jpeg'),
];

const styles = EStyleSheet.create({
  root: {width: '100%'},
});

export default SliderImage = ({images}) => {
  return (
    <View style={styles.root}>
      <SliderBox
        autoplay={true}
        images={images ? images : defaultImages}
        onCurrentImagePressed={(index) =>
          console.warn(`image ${index} pressed`)
        }
        currentImageEmitter={(index) =>
          console.warn(`current pos is: ${index}`)
        }
        dotColor={ORANGE}
        inactiveDotColor="#CCCCCC"
        circleLoop
      />
    </View>
  );
};
