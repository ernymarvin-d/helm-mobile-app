import * as React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {globalStyles} from '../../assets/styles/globalStyles';
import {TEXT_SECONDARY, GREY} from '../../assets/colors';
import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  continueTextWrapper: {
    flexDirection: 'column',
    marginTop: '30rem',
    marginBottom: '30rem',
  },
  continueText: {
    textAlign: 'center',
    color: TEXT_SECONDARY,
  },
  divider: {
    width: '140rem',
    height: 1,
    borderTopWidth: 1,
    borderTopColor: GREY[2],
  },
});

const ContinueWithText = () => (
  <View style={styles.continueTextWrapper}>
    <Text style={[globalStyles.h4, styles.continueText]}>Or continue with</Text>
    <View style={styles.divider} />
  </View>
);

export default ContinueWithText;
