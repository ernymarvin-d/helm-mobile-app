import React, {useState} from 'react';
import {TextInput, View, Image, TouchableOpacity, Text} from 'react-native';
import {
  INPUT_TEXT_HEIGHT_DEFAULT,
  PADDING_DEFAULT,
  MARGIN_SMALL,
} from '../../assets/dimens';
import {GREY, BLUE} from '../../assets/colors';
import EStyleSheet from 'react-native-extended-stylesheet';
import showPasswordIcon from './../../assets/images/ic-show-password.png';
import hidePasswordIcon from './../../assets/images/ic-hide-password.png';
import {globalStyles} from '../../assets/styles/globalStyles';
import _ from 'lodash';

const styles = EStyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  textBox: {
    flex: 1,
  },
  icon: {
    width: '18rem',
    height: '18rem',
    tintColor: GREY[2],
  },
  root: {
    flexDirection: 'column',
    marginVertical: '10rem',
    padding: 0,
    width: '100%',
  },
  errorText: {
    fontFamily: 'Montserrat-Regular',
    fontSize: '12rem',
    marginTop: MARGIN_SMALL,
    color: 'red',
  },
  errorInputStyle: {
    borderColor: 'red',
  },
});

const PasswordTextInput = ({
  name,
  placeholder,
  onPress,
  label,
  textInputProps,
  isValid,
  errorText,
}) => {
  const [showPassword, setShowPassword] = useState(false);
  const toggleShowHidePassword = () =>
    console.log(setShowPassword(!showPassword));
  const renderErrorText = !isValid && errorText && (
    <Text style={[globalStyles.p2, styles.errorText]}>{errorText}</Text>
  );
  const renderIcon = () => {
    const icon = !showPassword ? showPasswordIcon : hidePasswordIcon;
    return (
      <TouchableOpacity onPress={toggleShowHidePassword}>
        <Image style={styles.icon} source={icon} />
      </TouchableOpacity>
    );
  };
  return (
    <View style={styles.root}>
      <Text style={[globalStyles.inputTextLabel]}>{label}</Text>
      <View
        style={[
          globalStyles.inputText,
          styles.wrapper,
          !isValid && _.isBoolean(isValid) ? styles.errorInputStyle : {},
        ]}
        onPress={onPress}>
        <TextInput
          style={[styles.textBox]}
          textContentType="password"
          secureTextEntry={!showPassword}
          name={name}
          placeholder={placeholder}
          {...textInputProps}
        />
        {renderIcon()}
      </View>
      {renderErrorText}
    </View>
  );
};

export default PasswordTextInput;
