import * as React from 'react';
import {View, Text, TextInput} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import {globalStyles} from '../../assets/styles/globalStyles';
import {MARGIN_SMALL} from '../../assets/dimens';
import _ from 'lodash';

const styles = EStyleSheet.create({
  wrapper: {
    flexDirection: 'column',
    marginVertical: '10rem',
    padding: 0,
    width: '100%',
  },
  footerText: {
    fontFamily: 'Montserrat-Regular',
    fontSize: '12rem',
    marginTop: MARGIN_SMALL,
  },
  errorText: {
    fontFamily: 'Montserrat-Regular',
    fontSize: '12rem',
    marginTop: MARGIN_SMALL,
    color: 'red',
  },
  errorInputSTyle: {
    borderColor: 'red',
  },
});

const LabeledInputText = (props) => {
  const {
    label,
    type,
    name,
    placeholder,
    textInputProps,
    textInputStyle,
    value,
    editable,
    optional,
    inputLabelStyle,
    footerText,
    isValid,
    errorText,
    keyboardType,
    containerStyle,
  } = props;

  const renderFooterText = footerText && (
    <Text style={[globalStyles.p2, styles.footerText]}>{footerText}</Text>
  );

  const renderErrorText = !isValid && errorText && (
    <Text style={[globalStyles.p2, styles.errorText]}>{errorText}</Text>
  );

  return (
    <View style={[styles.wrapper, containerStyle || {}]}>
      <Text
        style={[
          globalStyles.inputTextLabel,
          inputLabelStyle ? inputLabelStyle : {},
        ]}>
        {label}
        {optional ? <Text style={globalStyles.p2}> (Optional)</Text> : null}
      </Text>
      <TextInput
        style={[
          globalStyles.inputText,
          textInputStyle ? textInputStyle : {},
          !isValid && _.isBoolean(isValid) ? styles.errorInputSTyle : {},
        ]}
        textContentType={type}
        name={name}
        secureTextEntry={type === 'password'}
        placeholder={placeholder}
        value={value}
        editable={editable !== undefined ? editable : true}
        {...textInputProps}
      />
      {renderFooterText}
      {renderErrorText}
    </View>
  );
};

export default LabeledInputText;
