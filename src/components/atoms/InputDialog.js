import React from 'react';
import {View} from 'react-native';
import Dialog from 'react-native-dialog';

const InputDialog = ({
  title,
  description,
  placeholder,
  onChangeText,
  onPressOk,
  onPressCancel,
  keyboardType,
  visible,
}) => {
  console.log({visible});
  return (
    <View>
      <Dialog.Container visible={visible}>
        <Dialog.Title>{title}</Dialog.Title>
        <Dialog.Description>{description}</Dialog.Description>
        <Dialog.Input
          keyboardType={keyboardType}
          placeholder={placeholder}
          onChangeText={onChangeText}
        />
        <Dialog.Button label="Cancel" onPress={onPressCancel} />
        <Dialog.Button label="Ok" onPress={onPressOk} />
      </Dialog.Container>
    </View>
  );
};

export default InputDialog;
